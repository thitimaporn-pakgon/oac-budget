<?php

/**
 * 
 * Thai number Utility make convert currency amount to Thai currency word
 * @author Sarawutt.b <sarawutt.b@pakgon.com>
 * @since 2016.08.30
 * @license Zicure
 */
class ThaiNumber {

    /**
     * 
     * Convert for thai currency to thai word
     * @author Sarawutt.b <sarawutt.b@pakgon.com>
     * @param mix $amount_number of currentcy amount ecample 100000,1,000,000.21,1,000,000.21 ฿
     * @return string of thai word
     */
    public static function currency2ThaiWord($amount_number = null) {
        if ((is_null($amount_number)) || empty($amount_number)) {
            return '';
        }
        $ret = "";
        $tmp = explode('.', trim($amount_number));
        $amount_number = ereg_replace('[^[:digit:]]', '', $tmp[0]);
        if (isset($tmp[1])) {
            $amount_number .= '.' . ereg_replace('[^[:digit:]]', '', $tmp['1']);
        }
        $amount_number = number_format($amount_number, 2, ".", "");
        $pt = strpos($amount_number, ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        //Decimal calculation for converting
        $baht = static::_readNumber($number);
        if ($baht != "") {
            $ret .= $baht . "บาท";
        }

        //Fraction calculation for converting
        $satang = static::_readNumber($fraction);
        if ($satang != "") {
            $ret .= $satang . "สตางค์";
        } else {
            $ret .= "ถ้วน";
        }
        return $ret;
    }

    /**
     * 
     * Convert for thai currency to thai word
     * @author Sarawutt.b <sarawutt.b@pakgon.com>
     * @param mix $amount_number of currentcy amount ecample 100000,1,000,000.21,1,000,000.21 ฿
     * @return string of thai word
     */
    private static function _readNumber($number = null) {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0)
            return $ret;
        if ($number > 1000000) {
            $ret .= static::_readNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while ($number > 0) {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                    ((($divider == 10) && ($d == 1)) ? "" :
                            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

}
