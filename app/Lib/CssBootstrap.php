<?php

/**
 * 
 * Class CSS Bootstrap make for generate the bootstrap
 * @author  sarawutt.b
 * @since   2016-10-03
 */
class CssBootstrap {

    /**
     * 
     * Function make generate for bootstrap color signature class
     * @author  sarawutt.b
     * @param   type $index as integer of color code
     * @return  string code
     */
    public static function bootstrapBoxClass($index = 0) {
        $bclass = array('warning', 'success', 'info', 'default', 'primary', 'danger');
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }

    /**
     * 
     * Function make for bootstrap label
     * @author  sarawutt.b
     * @param   type $message as string of the content where you want wrap to the label
     * @param   type $class as string bootstrap class
     * @return  string
     */
    public static function bootstrapLabel($message = '', $class = 'default') {
        return "<label class='label label-{$class}'>" . __($message) . '</label>';
    }
}
