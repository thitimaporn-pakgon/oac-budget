<?php

App::uses('Component', 'Controller');

class CommonComponent extends Component {

    public function calCatalogueInstituteCode(&$array) {
        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $i = 0;
        foreach ($array as $i1 => $infos) {

            $code = substr($infos['FiscalYear']['year'], -2) . ' ';
            $code .= $infos['Strategy']['order'];
            $code .= $infos['Program']['order'] . ' ';
            $code .= $infos['Result']['order'];
            $code .= $infos['Activity']['order'] . ' ';
            $code .= $infos['JobBudgetsGroup']['order'] . ' ';
            if (strlen($infos['JobBudget']['order']) === 1) {
                $code .= str_pad($infos['JobBudget']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['JobBudget']['order']) === 2) {
                $code .= $infos['JobBudget']['order'] . ' ';
            }
            if (strlen($infos['Work']['order']) === 1) {
                $code .= str_pad($infos['Work']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Work']['order']) === 2) {
                $code .= $infos['Work']['order'] . ' ';
            }

            if (strlen($infos['Catalogue']['order']) === 1) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '00', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 2) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 3) {
                $code .= $infos['Catalogue']['order'] . ' ';
            }
            // print_r($infos['Institute']);
            foreach ($infos['Institute'] as $i2 => $institute) {
                // print_r($institute);
                $array[$i1]['Institute'][$i2]['code'] = $code . $institute['code'];
            }
        }
        // print_r($array);
    }

    public function calCatalogueInstituteCodeByCatalogue(&$array) {
        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        foreach ($array as $i1 => $infos) {
            $code = substr($infos['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['Strategy']['FiscalYear']['year'], -2) . ' ';
            $code .= $infos['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['Strategy']['order'];
            $code .= $infos['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['order'] . ' ';
            $code .= $infos['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['order'];
            $code .= $infos['Work']['JobBudget']['JobBudgetsGroup']['Activity']['order'] . ' ';
            $code .= $infos['Work']['JobBudget']['JobBudgetsGroup']['order'] . ' ';
            if (strlen($infos['Work']['JobBudget']['order']) === 1) {
                $code .= str_pad($infos['Work']['JobBudget']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Work']['JobBudget']['order']) === 2) {
                $code .= $infos['Work']['JobBudget']['order'] . ' ';
            }
            if (strlen($infos['Work']['order']) === 1) {
                $code .= str_pad($infos['Work']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Work']['order']) === 2) {
                $code .= $infos['Work']['order'] . ' ';
            }
            if (strlen($infos['Catalogue']['order']) === 1) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '00', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 2) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 3) {
                $code .= $infos['Catalogue']['order'] . ' ';
            }

            foreach ($infos['Institute'] as $i2 => $institute) {
                $array[$i1]['Institute'][$i2]['code'] = $code . $institute['code'];
            }
        }
    }

    public function calCatalogueInstituteCode2(&$array) {
        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        foreach ($array as $i => $infos) {
            $code = substr($infos['FiscalYear']['year'], -2) . ' ';
            $code .= $infos['Strategy']['order'];
            $code .= $infos['Program']['order'] . ' ';
            $code .= $infos['Result']['order'];
            $code .= $infos['Activity']['order'] . ' ';
            $code .= $infos['JobBudgetsGroup']['order'] . ' ';
            if (strlen($infos['JobBudget']['order']) === 1) {
                $code .= str_pad($infos['JobBudget']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['JobBudget']['order']) === 2) {
                $code .= $infos['JobBudget']['order'] . ' ';
            }
            if (strlen($infos['Work']['order']) === 1) {
                $code .= str_pad($infos['Work']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Work']['order']) === 2) {
                $code .= $infos['Work']['order'] . ' ';
            }
            if (strlen($infos['Catalogue']['order']) === 1) {
                $array[$i]['Institute']['code'] = $code . str_pad($infos['Catalogue']['order'], 3, '00', STR_PAD_LEFT) . ' ' . $infos['Institute']['code'];
            } else if (strlen($infos['Catalogue']['order']) === 2) {
                $array[$i]['Institute']['code'] = $code . str_pad($infos['Catalogue']['order'], 3, '0', STR_PAD_LEFT) . ' ' . $infos['Institute']['code'];
            } else if (strlen($infos['Catalogue']['order']) === 3) {
                $array[$i]['Institute']['code'] = $code . $infos['Catalogue']['order'] . ' ' . $infos['Institute']['code'];
            }
        }
    }

    public function calCatalogueInstituteCode3(&$array) {
        // print_r($array);
        // exit;

        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $code = substr($array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['Strategy']['FiscalYear']['year'], -2) . ' ';
        $code .= $array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['Strategy']['order'];
        $code .= $array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['Program']['order'] . ' ';
        $code .= $array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['Activity']['Result']['order'];
        $code .= $array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['Activity']['order'] . ' ';
        $code .= $array['Catalogue']['Work']['JobBudget']['JobBudgetsGroup']['order'] . ' ';
        if (strlen($array['Catalogue']['Work']['JobBudget']['order']) === 1) {
            $code .= str_pad($array['Catalogue']['Work']['JobBudget']['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($array['Catalogue']['Work']['JobBudget']['order']) === 2) {
            $code .= $array['Catalogue']['Work']['JobBudget']['order'] . ' ';
        }
        if (strlen($array['Catalogue']['Work']['order']) === 1) {
            $code .= str_pad($array['Catalogue']['Work']['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($array['Catalogue']['Work']['order']) === 2) {
            $code .= $array['Catalogue']['Work']['order'] . ' ';
        }
        if (strlen($array['Catalogue']['order']) === 1) {
            $array['Institute']['code'] = $code . str_pad($array['Catalogue']['order'], 3, '00', STR_PAD_LEFT) . ' ' . $array['Institute']['code'];
        } else if (strlen($array['Catalogue']['order']) === 2) {
            $array['Institute']['code'] = $code . str_pad($array['Catalogue']['order'], 3, '0', STR_PAD_LEFT) . ' ' . $array['Institute']['code'];
        } else if (strlen($array['Catalogue']['order']) === 3) {
            $array['Institute']['code'] = $code . $array['Catalogue']['order'] . ' ' . $array['Institute']['code'];
        }
    }

    public function calCatalogueInstituteCode4(&$array) {

        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        foreach ($array as $i => $infos) {
            $code = substr($infos['FiscalYear']['year'], -2) . ' ';
            $code .= $infos['StrategiesAllocate']['order'];
            $code .= $infos['ProgramsAllocate']['order'] . ' ';
            $code .= $infos['ResultsAllocate']['order'];
            $code .= $infos['ActivitiesAllocate']['order'] . ' ';
            $code .= $infos['JobBudgetsGroupsAllocate']['order'] . ' ';

            if (strlen($infos['JobBudgetsAllocate']['order']) === 1) {
                $code .= str_pad($infos['JobBudgetsAllocate']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['JobBudgetsAllocate']['order']) === 2) {
                $code .= $infos['JobBudgetsAllocate']['order'] . ' ';
            }
            if (strlen($infos['WorksAllocate']['order']) === 1) {
                $code .= str_pad($infos['WorksAllocate']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['WorksAllocate']['order']) === 2) {
                $code .= $infos['WorksAllocate']['order'] . ' ';
            }
            if (strlen($infos['CataloguesAllocate']['order']) === 1) {
                $code .= str_pad($infos['CataloguesAllocate']['order'], 3, '00', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['CataloguesAllocate']['order']) === 2) {
                $code .= str_pad($infos['CataloguesAllocate']['order'], 3, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['CataloguesAllocate']['order']) === 3) {
                $code .= $infos['CataloguesAllocate']['order'] . ' ';
            }

            if (isset($array[$i]['CataloguesInstitutesAllocateInstitute']['code'])) {
                $array[$i]['CataloguesInstitutesAllocateInstitute']['code'] = $code . $infos['CataloguesInstitutesAllocateInstitute']['code'];
            } else {
                foreach ($array[$i]['CataloguesInstitutesAllocate'] as $i2 => $infos2) {
                    $array[$i]['CataloguesInstitutesAllocate'][$i2]['Institute']['code'] = $code . $infos2['Institute']['code'];
                }
            }
        }
    }

    public function calCatalogueInstituteCode5(&$array) {
        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $catalogue = $array['CataloguesAllocate'];
        $work = $catalogue['WorksAllocate'];
        $job_budget = $work['JobBudgetsAllocate'];
        $job_budget_group = $job_budget['JobBudgetsGroupsAllocate'];
        $activity = $job_budget_group['ActivitiesAllocate'];
        $result = $activity['ResultsAllocate'];
        $program = $result['ProgramsAllocate'];
        $strategy = $program['StrategiesAllocate'];
        $fiscal_year = $strategy['FiscalYear'];

        $code = substr($fiscal_year['year'], -2) . ' ';
        $code .= $strategy['order'];
        $code .= $program['order'] . ' ';
        $code .= $result['order'];
        $code .= $activity['order'] . ' ';
        $code .= $job_budget_group['order'] . ' ';

        if (strlen($job_budget['order']) === 1) {
            $code .= str_pad($job_budget['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($job_budget['order']) === 2) {
            $code .= $job_budget['order'] . ' ';
        }
        if (strlen($work['order']) === 1) {
            $code .= str_pad($work['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($work['order']) === 2) {
            $code .= $work['order'] . ' ';
        }
        if (strlen($catalogue['order']) === 1) {
            $code .= str_pad($catalogue['order'], 3, '00', STR_PAD_LEFT) . ' ';
        } else if (strlen($catalogue['order']) === 2) {
            $code .= str_pad($catalogue['order'], 3, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($catalogue['order']) === 3) {
            $code .= $catalogue['order'] . ' ';
        }

        $array['Institute']['code'] = $code . $array['Institute']['code'];
        // $array['Institute']['code'] = $code.str_pad($catalogue['order'], 2, '0', STR_PAD_LEFT).' '.$array['Institute']['code'];
    }

    public function calCatalogueInstituteCode6(&$array) {
        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        foreach ($array as $i => $infos) {
            $code = substr($infos['FiscalYear']['year'], -2) . ' ';
            $code .= $infos['Strategy']['order'];
            $code .= $infos['Program']['order'] . ' ';
            $code .= $infos['Result']['order'];
            $code .= $infos['Activity']['order'] . ' ';
            $code .= $infos['JobBudgetsGroup']['order'] . ' ';

            if (strlen($infos['JobBudget']['order']) === 1) {
                $code .= str_pad($infos['JobBudget']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['JobBudget']['order']) === 2) {
                $code .= $infos['JobBudget']['order'] . ' ';
            }
            if (strlen($infos['Work']['order']) === 1) {
                $code .= str_pad($infos['Work']['order'], 2, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Work']['order']) === 2) {
                $code .= $infos['Work']['order'] . ' ';
            }
            if (strlen($infos['Catalogue']['order']) === 1) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '00', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 2) {
                $code .= str_pad($infos['Catalogue']['order'], 3, '0', STR_PAD_LEFT) . ' ';
            } else if (strlen($infos['Catalogue']['order']) === 3) {
                $code .= $infos['Catalogue']['order'] . ' ';
            }

            foreach ($array[$i]['CataloguesInstitutesBudget'] as $i2 => $infos2) {
                $array[$i]['CataloguesInstitutesBudget'][$i2]['Institute']['code'] = $code . $infos2['Institute']['code'];
            }
        }
    }

    public function mapBudget($array) {

        $maplist = array();
        foreach ($array as $i => $infos) {
            $catalogueID = $infos['CataloguesAllocate']['id'];
            $maplist[$catalogueID]['FiscalYear']['id'] = $infos['FiscalYear']['id'];
            $maplist[$catalogueID]['FiscalYear']['year'] = $infos['FiscalYear']['year'];
            $maplist[$catalogueID]['StrategiesAllocate']['order'] = $infos['StrategiesAllocate']['order'];
            $maplist[$catalogueID]['ProgramsAllocate']['order'] = $infos['ProgramsAllocate']['order'];
            $maplist[$catalogueID]['ResultsAllocate']['order'] = $infos['ResultsAllocate']['order'];
            $maplist[$catalogueID]['ActivitiesAllocate']['order'] = $infos['ActivitiesAllocate']['order'];
            $maplist[$catalogueID]['JobBudgetsGroupsAllocate']['order'] = $infos['JobBudgetsGroupsAllocate']['order'];
            $maplist[$catalogueID]['JobBudgetsAllocate']['order'] = $infos['JobBudgetsAllocate']['order'];
            $maplist[$catalogueID]['WorksAllocate']['order'] = $infos['WorksAllocate']['order'];
            $maplist[$catalogueID]['CataloguesAllocate']['order'][] = $infos['CataloguesAllocate']['order'];
        }
        return $maplist;
    }

    public function totalBudgetType($budget, $budget_types) {
        // print_r($budget_types);
        // exit;
        $return = array();
        foreach ($budget_types as $budget_type) {
            $sumL1 = 0;
            if (!$budget_type['ChildBudgetTypeLv1']) {

                continue;
            }
            foreach ($budget_type['ChildBudgetTypeLv1'] as $ChildBudgetTypeLv1) {
                $sumL2 = 0;
                // print_r($budget[$ChildBudgetTypeLv1['id']]); 
                // echo ' | ';
                if (isset($budget[$ChildBudgetTypeLv1['id']])) {
                    $sumL1 += doubleval($budget[$ChildBudgetTypeLv1['id']]);
                    $sumL2 += doubleval($budget[$ChildBudgetTypeLv1['id']]);
                }
                if ($ChildBudgetTypeLv1['ChildBudgetTypeLv2']) {
                    foreach ($ChildBudgetTypeLv1['ChildBudgetTypeLv2'] as $ChildBudgetTypeLv2) {
                        if (!isset($budget[$ChildBudgetTypeLv2['id']])) {
                            $budget[$ChildBudgetTypeLv2['id']] = 0;
                        }
                        // print_r($budget[$ChildBudgetTypeLv2['id']]); 
                        // echo ' | ';
                        $sumL2 += doubleval($budget[$ChildBudgetTypeLv2['id']]);
                        $sumL1 += doubleval($budget[$ChildBudgetTypeLv2['id']]);
                        $return[$ChildBudgetTypeLv2['id']] = doubleval($budget[$ChildBudgetTypeLv2['id']]);
                    }
                }
                $return[$ChildBudgetTypeLv1['id']] = doubleval($sumL2);
            }
            $return[$budget_type['BudgetType']['id']] = doubleval($sumL1);
        }
        // print_r($return);
        // exit;

        return $return;
    }

    public function totalBudgetPast($CataloguesInstitutesBudget, $fiscal_year, $code) {
        $total = array();

        if (!empty($fiscal_year) && !empty($code) && !empty($CataloguesInstitutesBudget)) {

            $cover_code = substr($code, 3, 16);

            $total = $CataloguesInstitutesBudget->find('first', array(
                'conditions' => array(
                    'CataloguesInstitutesBudget.fiscal_year_id' => $fiscal_year,
                    'CataloguesInstitutesBudget.code LIKE ' => '%' . $code . '%',
                    'CataloguesInstitutesBudget.removed' => NULL,
                    'CataloguesInstitutesBudget.removed_by' => NULL,
                    'CataloguesInstitutesBudget.status_confirm_owner' => 'Y',
                    'CataloguesInstitutesBudget.status_confirm_worker' => 'Y',
                    'CataloguesInstitutesBudget.status_confirm_sub' => 'Y',
                    'CataloguesInstitutesBudget.status_confirm_main' => 'Y',
                    'CataloguesInstitutesBudget.status_confirm_army' => 'Y',
                ),
                    )
            );
        }

        return $total;
    }

    public function gencodeAllocate($code) {
        $cover_code = explode(' ', $code);

        if (isset($cover_code[1]) && isset($cover_code[2]) && isset($cover_code[3]) && isset($cover_code[4]) && isset($cover_code[5]) && isset($cover_code[6]) && isset($cover_code[7])) {
            return $cover_code[0] . $cover_code[1] . $cover_code[2] . $cover_code[3] . $cover_code[4] . $cover_code[5] . $cover_code[6] . $cover_code[7];
        } else {
            return array();
        }

        // return $gencode;
    }

    public function totalBudgetPastAllocate($CataloguesInstitutesAllocate, $fiscal_year, $code) {
        $total = array();

        if (!empty($fiscal_year) && !empty($code) && !empty($CataloguesInstitutesAllocate)) {
            $cover_code = substr($code, 2, 16);
            $year = substr($fiscal_year, 2);
            // print_r($year.$cover_code);
            // exit;

            $total = $CataloguesInstitutesAllocate->find('first', array(
                'conditions' => array(
                    'CataloguesInstitutesAllocate.code LIKE ' => '%' . $year . $cover_code . '%', //%581111102030028600%
                    'CataloguesInstitutesAllocate.removed' => NULL,
                    'CataloguesInstitutesAllocate.removed_by' => NULL,
                    'CataloguesInstitutesAllocate.status_confirm_owner LIKE' => '%Y%',
                    'CataloguesInstitutesAllocate.status_confirm_worker LIKE' => '%Y%',
                    'CataloguesInstitutesAllocate.status_confirm_sub LIKE' => '%Y%',
                    'CataloguesInstitutesAllocate.status_confirm_main LIKE' => '%Y%',
                    'CataloguesInstitutesAllocate.status_confirm_army LIKE' => '%Y%',
                ),
                    )
            );
        }

        return $total;
    }

    public function calCatalogueInstituteCodeDetail(&$array) {

        $map_order = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $code = substr($array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['ActivitiesAllocate']['ResultsAllocate']['ProgramsAllocate']['StrategiesAllocate']['FiscalYear']['year'], -2) . ' ';
        $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['ActivitiesAllocate']['ResultsAllocate']['ProgramsAllocate']['StrategiesAllocate']['order'];
        $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['ActivitiesAllocate']['ResultsAllocate']['ProgramsAllocate']['order'] . ' ';
        $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['ActivitiesAllocate']['ResultsAllocate']['order'];
        $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['ActivitiesAllocate']['order'] . ' ';
        $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['JobBudgetsGroupsAllocate']['order'] . ' ';
        if (strlen($array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['order']) === 1) {
            $code .= str_pad($array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['order']) === 2) {
            $code .= $array['CataloguesAllocate']['WorksAllocate']['JobBudgetsAllocate']['order'] . ' ';
        }
        if (strlen($array['CataloguesAllocate']['WorksAllocate']['order']) === 1) {
            $code .= str_pad($array['CataloguesAllocate']['WorksAllocate']['order'], 2, '0', STR_PAD_LEFT) . ' ';
        } else if (strlen($array['CataloguesAllocate']['WorksAllocate']['order']) === 2) {
            $code .= $array['CataloguesAllocate']['WorksAllocate']['order'] . ' ';
        }
        if (strlen($array['CataloguesAllocate']['order']) === 1) {
            $array['Institute']['code'] = $code . str_pad($array['CataloguesAllocate']['order'], 3, '00', STR_PAD_LEFT) . ' ' . $array['Institute']['code'];
        } else if (strlen($array['CataloguesAllocate']['order']) === 2) {
            $array['Institute']['code'] = $code . str_pad($array['CataloguesAllocate']['order'], 3, '0', STR_PAD_LEFT) . ' ' . $array['Institute']['code'];
        } else if (strlen($array['CataloguesAllocate']['order']) === 3) {
            $array['Institute']['code'] = $code . $array['CataloguesAllocate']['order'] . ' ' . $array['Institute']['code'];
        }
    }

    // public function gencodeAllocate($code){
    //     $cover_code = explode(' ', $code);
    //     $gencode =  $cover_code[0].$cover_code[1].$cover_code[2].$cover_code[3].$cover_code[4].$cover_code[5].$cover_code[6].$cover_code[7];
    //     if(empty($gencode)){
    //         return array();
    //     }
    //     return $gencode;
    // }
}

?>