<?php

/**
 * 
 * Crezt Secure make the application are in secure
 * @author  sarawutt.b
 * @since   20161203 22:14:00
 */
App::uses('Component', 'Controller');
class CrazySecureComponent extends Component {

    public $uses = array();
    public $components = array('Session');

    public function initialize(Controller $controller) {
        foreach ($this->uses as $model_name) {
            App::import('Model', $model_name);
            $model_class = "{$model_name}";
            $this->$model_name = new $model_class();
        }
    }

    /**
     * 
     * Function secure generate signature (make hash check GET/POST)
     * @author  sarawutt.b
     * @param   type $data as association array() may included nonce (generate form version or uuid)
     * @param   type $secretKey as string of the site secret key
     * @return  string in SSA format 
     */
    function generateSignature($data, $secretKey) {
        //sort data array alphabetically by key
        ksort($data);

        //combine keys and values into one long string
        $dataString = '';
        foreach ($data as $key => $value) {
            $dataString .= $key . $value;
        }

        //lowercase everything
        $dataString = strtolower($dataString);

        //generate signature using the SHA256 hashing algorithm
        return hash_hmac("sha256", $dataString, $secretKey);
    }

    /**
     * 
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     * 
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

}

?>
