<?php

/**
 *
 * Reporting Manager manage report integrated with PHP , JAVA , PHP JAVA BRIDGE
 * @author      Sarawutt.b
 * @access      public
 * @license     Zicure Corp
 * @since       2012/12/31
 * @modified    2016/08/17 14:21
 */
define("JAVA_HOSTS", "127.0.0.1:8080");
define("JAVA_SERVLET", "/JavaBridge/servlet.phpjavabridge");

App::uses('Component', 'Controller');
App::uses('ConnectionManager', 'Model');

class ReportManagerComponent extends Component {

    //public $components = array('CrazyLog'); 
    private $_java_host = '127.0.0.1'; //Java hosting LOCAL
    //private $_java_host = '180.180.244.228'; //Java hosting UAT
    private $_databaseDriver = null; //Java JDBC database driver
    private $_driverManager = null; //Java JDBC drivermanager
    private $_conn = null; //Database Connection

    public function initialize(Controller $controller) {
        CakeLog::info("Report Manager initialize loading for java extension");
        //$this->checkJavaExtension();
        CakeLog::info("Report Manager initialize loading for init connection");
        //$this->_initConnection();
    }

    /**
     * 
     * Load database connection fore initialize
     * @author  sarawutt.b
     * @param   type $source datasource on the cake configure
     * @return  boolean
     */
    private function _initConnection() {
        $ds = ConnectionManager::getDataSource('default')->config;
        $driver = strtolower(trim($ds['datasource']));
        switch ($driver) {
            case 'database/postgres':
                $this->pgsqlConnect($ds['database'], $ds['login'], $ds['password'], $ds['host'], $ds['port']);
                break;
            case 'database/mysql':
                $this->mysqlConnnect($ds['database'], $ds['login'], $ds['password'], $ds['host'], $ds['port']);
                break;
            default :return false;
        }
        return true;
    }

    /**
     * 
     * Function setting JAVA or Apache TOMCAT hosting
     * @author  sarawutt.b
     * @param type $host as string of apahe tomcat hosting may be IP Address or host name
     * @return string
     */
    public function setJavaTomcatHost($host) {
        return $this->_java_host = $host;
    }

    /**
     *
     * Check for PHP Java bridge extension to be is loaded
     * @author	sarawutt.b
     * @access	public
     * @license	Zicure
     * @since	2012/12/31
     * @return	boolean true when load success and false otherwise
     */
    public function checkJavaExtension() {
        if (!extension_loaded('java')) {
            $sapi_type = php_sapi_name();
            $port = (isset($_SERVER['SERVER_PORT']) && (($_SERVER['SERVER_PORT']) > 1024)) ? $_SERVER['SERVER_PORT'] : '8080';
            if ($sapi_type == "cgi" || $sapi_type == "cgi-fcgi" || $sapi_type == "cli") {
                //echo PHP_SHLIB_SUFFIX;
                if (!(PHP_SHLIB_SUFFIX == "so" && @dl('java.so')) && !(PHP_SHLIB_SUFFIX == "dll" && @dl('php_java.dll')) && !(@include_once("JavaBridge/java/Java.inc")) && !(@require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc"))) {
                    $msg = "java extension not installed.";
                    CakeLog::critical($msg);
                    return $msg;
                }
            } else {
                if (!(@include_once("/JavaBridge/java/Java.inc"))) {
                    //require_once("http://180.180.244.228:8080/JavaBridge/java/Java.inc");
                    //Require the java extension from the local
                    $path = APP . "Vendor/javaBridge/Java.inc";
                    require_once($path);
                    CakeLog::info("Require_once => " . $path);
                }
            }
        }
        if (!function_exists("java_get_server_name")) {
            $msg = "The loaded java extension is not the PHP/Java Bridge";
            CakeLog::critical($msg);
            return $msg;
        }
        return true;
    }

    /**
     * 
     * POSTGRESQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function pgsqlConnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '5432') {
        CakeLog::info('PostgreSQL connecting');
        $this->_databaseDriver = 'org.postgresql.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * MySQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function mysqlConnnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '3306') {
        CakeLog::info('MySQL connecting');
        $this->_databaseDriver = 'com.mysql.jdbc.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * Database connection for eache dinamicically connection
     * @author  sarawutt.b
     * @param type $jdbcDriver as string for java JDBC Driver
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @return object of connection
     */
    private function makeDBConnection($jdbcDriver = 'msql', $host = '127.0.0.1', $port = '3306', $dbname = 'tmp', $user = 'tmp', $pass = 'password') {
        $connectionString = "jdbc:{$jdbcDriver}://{$host}:{$port}/{$dbname}?user={$user}&password={$pass}";
        java("java.lang.Class")->forName($this->_databaseDriver);
        $this->_driverManager = new JavaClass("java.sql.DriverManager");
        $this->_conn = $this->_driverManager->getConnection($connectionString);
        CakeLog::info("Open Database connention for the report with connection string:");
        CakeLog::info($connectionString);
        return true;
    }

//    /**
//     *
//     * Generate the report to output file
//     * @author      sarawutt.b
//     * @access      public
//     * @param       string name of report possible value PURCHASE_ORDER_REPORT(purchase orders report)|RECEIVED_REPORT(received report)|SHIPING_INVOICE_TAX_REPORT(shiping invoice tax report)
//     * @param       integer primary key of master table for find the report
//     * @param       string which of report format
//     * @license     Zicure
//     * @since       2012.12.31
//     * @modified    2016.08.18
//     * @return      void
//     */
//    public function generateReport($reportName = null, $formParams = array(), $reportFormat = 'pdf') {
//        CakeLog::info("Call init generateReport() with params report name {$reportName}.{$reportFormat};");
//        if ($this->checkJavaExtension()) {
//            try {
//                try {
//                    $this->_initConnection();
//                    $compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
//                    $viewer = new JavaClass("net.sf.jasperreports.view.JasperViewer");
//                    $report = $compileManager->compileReport($reportName);
//                    $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
//                    $params = array();
//                    if (!empty($formParams) && (is_array($formParams))) {
//                        $params = new java('java.util.HashMap');
//                        foreach ($formParams as $k => $v) {
//                            $params->put($k, $v);
//                            CakeLog::info("Report name :: {$reportName} with param name => {$k} = {$v}");
//                        }
//                    }
//
//                    $jasperPrint = $fillManager->fillReport($report, $params, $this->_conn);
//                    $outputPath = WWW_ROOT . "report-gen/output." . $reportFormat;
//                    java_set_file_encoding("UTF-8");
//                    $em = java('net.sf.jasperreports.engine.JasperExportManager');
//                    $em->exportReportToPdfFile($jasperPrint, $outputPath);
//                    header("Content-type: application/pdf");
//                    readfile($outputPath);
//                    unlink($outputPath);
//                    CakeLog::info("Generate for the report rptFile : {$reportName}");
//                    CakeLog::info($outputPath);
//                } catch (JavaException $ex) {
//                    $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
//                    echo $msg;
//                    CakeLog::error($msg);
//                    return false;
//                }
//            } catch (JavaException $ex) {
//                $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
//                echo $msg;
//                CakeLog::error($msg);
//                return false;
//            }
//        }
//    }

    /**
     *
     * Generate the report to output file
     * @author      sarawutt.b
     * @param       $reportName as string name of report possible value PURCHASE_ORDER_REPORT(purchase orders report)|RECEIVED_REPORT(received report)|SHIPING_INVOICE_TAX_REPORT(shiping invoice tax report)
     * @param       $formParams as a array parameter in SQL option correcly key => value
     * @param       $exportFileName as a string which of reportexport file name
     * @param       $isForceExport as a boolean true | false force to export to file download if true then you force
     * @param       $options as a string of the report option
     * @since       2107-06-19
     * @return      void
     */
    public function generateReport($reportName = null, $formParams = array(), $exportFileName = null, $isForceExport = false, $options = array()) {
        if (empty($exportFileName)) {
            $reportFormat = 'xls';
            $reportOptions = pathinfo($reportName);
            $exportFileName = $reportOptions['filename'] . '.' . $reportFormat;
        } else {
            $reportFormat = strtolower(substr($exportFileName, strrpos($exportFileName, '.') + 1));
        }

        CakeLog::info("Call init generateReport() source Jrxml name {$reportName} take output file to {$exportFileName};");
        if ($this->checkJavaExtension()) {
            try {
                $this->_initConnection();
                $compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
                $viewer = new JavaClass("net.sf.jasperreports.view.JasperViewer");
                $report = $compileManager->compileReport($reportName);
                $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
                $params = array();
                if (!empty($formParams) && (is_array($formParams))) {
                    $params = new java('java.util.HashMap');
                    foreach ($formParams as $k => $v) {
                        $params->put($k, $v);
                        CakeLog::info("Report name :: {$reportName} with param name => {$k} = {$v}");
                    }
                }
                $jasperPrint = $fillManager->fillReport($report, $params, $this->_conn);
                $outputPath = WWW_ROOT . 'report-gen' . DS . $exportFileName;
                $exporter = new java("net.sf.jasperreports.engine.JRExporter");
                java_set_file_encoding("UTF-8");
                switch ($reportFormat) {
                    case 'xls':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRXlsExporter");
                            //$exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_ONE_PAGE_PER_SHEET, java("java.lang.Boolean")->TRUE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_WHITE_PAGE_BACKGROUND, java("java.lang.Boolean")->FALSE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, java("java.lang.Boolean")->TRUE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'xlsx':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRXlsxExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsxExporter")->IS_WHITE_PAGE_BACKGROUND, java("java.lang.Boolean")->FALSE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsxExporter")->IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, java("java.lang.Boolean")->TRUE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsxExporter")->IS_COLLAPSE_ROW_SPAN, java("java.lang.Boolean")->FALSE);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        header('Content-Length: ' . filesize($exportFileName));
                        header('Content-Transfer-Encoding: binary');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'csv':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRCsvExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->FIELD_DELIMITER, ",");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->RECORD_DELIMITER, "\n");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRCsvExporterParameter")->CHARACTER_ENCODING, "UTF-8");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/csv");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'doc':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.ooxml.JRDocxExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/vnd.ms-word");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'docx':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.ooxml.JRDocxExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'html':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRHtmlExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        break;
                    case 'ods':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.oasis.JROdsExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/vnd.oasis.opendocument.spreadsheet");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'odt':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.oasis.JROdtExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/vnd.oasis.opendocument.text");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'txt':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRTextExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRTextExporterParameter")->PAGE_WIDTH, 120);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRTextExporterParameter")->PAGE_HEIGHT, 60);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: text/plain");
                        break;
                    case 'rtf':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.JRRtfExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: application/rtf");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'ppt':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.ooxml.JRPptxExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("Content-type: aapplication/vnd.ms-powerpoint");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'pptx':
                        try {
                            $exporter = new java("net.sf.jasperreports.engine.export.ooxml.JRPptxExporter");
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                            $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
                        } catch (JavaException $ex) {
                            echo $ex;
                        }
                        header("application/vnd.openxmlformats-officedocument.presentationml.presentation");
                        header("Content-Disposition: attachment; filename={$exportFileName}");
                        break;
                    case 'pdf':
                    default :
                        $exporter = new java("net.sf.jasperreports.engine.export.JRPdfExporter");
                        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
                        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);

                        header("Content-type: application/pdf");
                        if ($isForceExport === true) {
                            header("Content-Disposition: attachment; filename={$exportFileName}");
                        }
                        break;
                }
                $exporter->exportReport();
                readfile($outputPath);
                unlink($outputPath);
                $this->_closeDbConnection();
                CakeLog::info("Generate for the report rptFile : {$reportName}");
                CakeLog::info($outputPath);
            } catch (JavaException $ex) {
                $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
                echo $msg;
                CakeLog::error($msg);
                return false;
            }
        }
    }

    /**
     * 
     * Function closing for a JDBC database connection
     * @author sarawutt.b
     * @return boolean true if closed success otherwise return false
     */
    private function _closeDbConnection() {
        try {
            if (!$this->_conn->isClosed()) {
                $this->_conn->close();
            }
            CakeLog::info("Closed for JDBC Database Connection");
            return true;
        } catch (JavaException $ex) {
            CakeLog::error("<b>ERROR Closing JDBC Database Connection:</b><br/> " . $ex->getCause());
            return false;
        }
    }

    public function exampleDownloadPdfFile() {
        try {
            $params = report_parse_post_parameters();

            // Load the PostgreSQL database driver.
            java('java.lang.Class')->forName('org.postgresql.Driver');

            // Attempt a database connection.
            $conn = java('java.sql.DriverManager')->getConnection("jdbc:postgresql://$dbhost/$dbname?user=$dbuser&password=$dbpass");

            // Use the fill manager to produce the report.
            $fm = java('net.sf.jasperreports.engine.JasperFillManager');
            $pm = $fm->fillReport($report, $params, $conn);

            header('Cache-Control: private');
            header('Content-Description: File Transfer');
            header("Content-Disposition: attachment, filename=$filename.pdf");
            header('Content-Type: application/pdf');
            header('Content-Transfer-Encoding: binary');

            java_set_file_encoding('ISO-8859-1');

            $em = java('net.sf.jasperreports.engine.JasperExportManager');
            $result = $em->exportReportToPdf($pm);

            $conn->close();

            header('Content-Length: ' . strlen($result));
            echo $result;
        } catch (JavaException $ex) {
            $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
            echo $msg;
            CakeLog::error($msg);
            return false;
        }
    }

    /**
     * Convert PHP value to JAVA value
     * @author  sarawutt.b
     * @param   string $value as mix
     * @param   string $className as string
     * @returns boolean success
     */
    function convertValue($value, $className) {
        try {
            $temp = null;
            switch ($className) {
                case 'java.lang.String':
                case 'java.lang.Boolean':
                case 'java.lang.Integer':
                case 'java.lang.Long':
                case 'java.lang.Short':
                case 'java.lang.Double':
                case 'java.math.BigDecimal':
                    $temp = new Java($className, $value);
                    break;
                case 'java.sql.Timestamp':
                case 'java.sql.Time':
                    $tmp = new Java($className);
                    $temp = $tmp->valueOf($value);
                    break;
                default : $temp = null;
                    break;
                    return $temp;
            }
        } catch (Exception $err) {
            $msg = "unable to convert value, {$value} could not be converted to {$className}";
            echo $msg;
            CakeLog::error($msg);
            return false;
        }
        $msg = "unable to convert value, class name {$className} not recognised";
        echo $msg;
        CakeLog::notice($msg);
        return false;
    }

    /**
     * Write log file
     * @author  sarawutt.b
     * @param   string as $msg_log is log line content
     * @return  true with write content in the log file
     * @since   2016.08.18 15:19
     */
    public function writeLog($msg_log = null) {
        $LOG_PATH = LOGPATH . DS . 'REPORTS' . DS;
        $logFileName = date('Ymd') . '.componebt.ireport.log';
        //return $this->CrazyLog->WRITE_NLOG($LOG_PATH, $msg_log, $logFileName);
    }

    /**
     * 
     * Write log separate each block process
     * @author  sarawutt.b
     * @return  void
     * @since   2016.08.18 15:19
     */
    public function writeLogSeparateLine($logMode = 'writeLog') {
        //return $this->$logMode("-----------------------------------------------------------------------------------------------------------------------");
    }

}

?>
