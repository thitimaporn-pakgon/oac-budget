<?php
/**
 *
 * Reporting Manager manage report integrated with PHP , JAVA , PHP JAVA BRIDGE
 * @author      Sarawutt.b
 * @access      public
 * @license     Zicure Corp
 * @since       2012/12/31
 * @modified    2016/08/17 14:21
 */
App::uses('Component', 'Controller');
class ReportManagerComponent extends Component {

    private $_java_host = '127.0.0.1';
    private $_databaseDriver = null; //Java JDBC database driver
    private $_driverManager = null;//Java JDBC drivermanager
    private $_conn = null;//Database Connection

    public function initialize(Controller $controller) {
        $this->checkJavaExtension();
        $this->log("Report Manager initialize");
    }

    /**
     *
     * Check for PHP Java bridge extension to be is loaded
     * @author	sarawutt.b
     * @access	public
     * @license	Zicure
     * @since	2012/12/31
     * @return	boolean true when load success and false otherwise
     */
    public static function checkJavaExtension() {
        if (!extension_loaded('java')) {
            $sapi_type = php_sapi_name();
            $port = (isset($_SERVER['SERVER_PORT']) && (($_SERVER['SERVER_PORT']) > 1024)) ? $_SERVER['SERVER_PORT'] : '8080';
            if ($sapi_type == "cgi" || $sapi_type == "cgi-fcgi" || $sapi_type == "cli") {
                //echo PHP_SHLIB_SUFFIX;
                if (!(PHP_SHLIB_SUFFIX == "so" && @dl('java.so')) && !(PHP_SHLIB_SUFFIX == "dll" && @dl('php_java.dll')) && !(@include_once("JavaBridge/java/Java.inc")) && !(@require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc"))) {
                    $msg = "java extension not installed.";
                    $this->log($msg);
                    return $msg;
                }
            } else {
                if (!(@include_once("JavaBridge/java/Java.inc"))) {
                    require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc");
                }
            }
        }

        if (!function_exists("java_get_server_name")) {
            $msg = "The loaded java extension is not the PHP/Java Bridge";
            $this->log($msg);
            return $msg;
        }
        return true;
    }

    /**
     * 
     * POSTGRESQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function pgsqlConnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '5432') {
        $this->_databaseDriver = 'org.postgresql.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * MySQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function mysqlConnnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '5432') {
        $this->_databaseDriver = 'com.mysql.jdbc.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * Database connection for eache dinamicically connection
     * @author  sarawutt.b
     * @param type $jdbcDriver as string for java JDBC Driver
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @return object of connection
     */
    private function makeDBConnection($jdbcDriver = 'msql', $host = '127.0.0.1', $port = '3306', $dbname = 'tmp', $user = 'tmp', $pass = 'password') {
        $connectionString = "jdbc:{$jdbcDriver}://{$host}:{$port}/{$dbname}?user={$user}&password={$pass}";
        java("java.lang.Class")->forName($this->_databaseDriver);
        $this->_driverManager = new JavaClass("java.sql.DriverManager");
        $this->_conn = $this->_driverManager->getConnection($connectionString);
        return true;
    }

    /**
     *
     * โหลดรายงานต่างๆ แล้วนำไปแสดงที่หน้าเว็บในรูปแบบต่างๆ
     * @author	sarawutt.b
     * @access	public
     * @param	string name of report possible value PURCHASE_ORDER_REPORT(purchase orders report)|RECEIVED_REPORT(received report)|SHIPING_INVOICE_TAX_REPORT(shiping invoice tax report)
     * @param	integer primary key of master table for find the report
     * @param	string which of report format
     * @license	Zicure
     * @since	2012/12/31
     * @return	void
     */
    public function generateReport($reportName = null, $formParams = array(), $reportFormat = 'pdf') {

        if ($this->checkJavaExtension()) {
            try {
                //-------------------------------------------------------------------------
                //MySQL Connection
                //-------------------------------------------------------------------------
                //java("java.lang.Class")->forName("com.mysql.jdbc.Driver");
                //$driverManager = new JavaClass("java.sql.DriverManager");
                //$dbConnection = $driverManager->getConnection($this->connectionHost, $this->connectionUser, $this->connectionPassword);
                //-------------------------------------------------------------------------
                //PostgreSQL Conection
                //-------------------------------------------------------------------------
//                $class = new JavaClass("java.lang.Class");
//                $class->forName("org.postgresql.Driver");
//                $driverManager = new JavaClass("java.sql.DriverManager");
//                //$dbConnection = $driverManager->getConnection("jdbc:postgresql://localhost:5432/3363?user=demo_ireport2&password=password");
//                $dbConnection = $driverManager->getConnection("jdbc:postgresql://localhost:5432/spc_project?user=spc_project&password=password");


                try {
                    // Open jasper report
                    $compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
                    $viewer = new JavaClass("net.sf.jasperreports.view.JasperViewer");
                    $report = $compileManager->compileReport($reportName);
                    $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");

                    //Setup the report parameter
                    $params = array();
                    if (!empty($formParams) && (is_array($formParams))) {
                        $params = new java('java.util.HashMap');
                        foreach ($formParams as $k => $v) {
                            $params->put($k, $v);
                        }
                    }

                    $jasperPrint = $fillManager->fillReport($report, $params, $this->_conn);
                    $outputPath = realpath(".") . "/" . "output." . $reportFormat;
                    java_set_file_encoding("UTF-8");
                    $em = java('net.sf.jasperreports.engine.JasperExportManager');
                    $em->exportReportToPdfFile($jasperPrint, $outputPath);
                    header("Content-type: application/pdf");
                    readfile($outputPath);
                    unlink($outputPath);
                } catch (JavaException $ex) {
                    echo "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
                    return false;
                }
            } catch (JavaException $ex) {
                echo "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
                return false;
            }
        }
    }

    /**
     * Convert PHP value to JAVA value
     * @author  sarawutt.b
     * @param   string $value
     * @param   string $className
     * @returns boolean success
     */
    function convertValue($value, $className) {
        try {
            $temp = null;
            switch ($className) {
                case 'java.lang.String':
                case 'java.lang.Boolean':
                case 'java.lang.Integer':
                case 'java.lang.Long':
                case 'java.lang.Short':
                case 'java.lang.Double':
                case 'java.math.BigDecimal':
                    $temp = new Java($className, $value);
                    break;
                case 'java.sql.Timestamp':
                case 'java.sql.Time':
                    $tmp = new Java($className);
                    $temp = $tmp->valueOf($value);
                    break;
                default : $temp = null;
                    break;
                    return $temp;
            }
        } catch (Exception $err) {
            echo "unable to convert value, {$value} could not be converted to {$className}";
            return false;
        }
        echo "unable to convert value, class name {$className} not recognised";
        return false;
    }

//    public function printReportExampleWhitParams($LIMIT = 100) {
//        if (!is_numeric($LIMIT)) {
//            echo 'Your parameter is not interger pls try again';
//            exit;
//        }
//        $params = array('LIMIT' => (int) $LIMIT);
//        $this->autoRender = FALSE;
//        $this->ReportManager->generateReport(
//                Configure::read('Zicure.Report.Retail.rptExample0001'), $params
//        );
//    }
}

?>
