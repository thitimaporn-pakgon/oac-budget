<?php

/**
 *
 * Reporting Manager manage report integrated with PHP , JAVA , PHP JAVA BRIDGE
 * @author      Sarawutt.b
 * @access      public
 * @license     Zicure Corp
 * @since       2012/12/31
 * @modified    2016/08/17 14:21
 */
App::uses('Component', 'Controller');
App::uses('ConnectionManager', 'Model');

class ReportManagerComponent extends Component {

    //public $components = array('CrazyLog');
    //private $_java_host = 'rta-uat.pakgon.com'; //Java hosting
    private $_java_host = '127.0.0.1'; //Java hosting
    private $_databaseDriver = null; //Java JDBC database driver
    private $_driverManager = null; //Java JDBC drivermanager
    private $_conn = null; //Database Connection

    //private $_tomcatServer = 'http://rta-uat.pakgon.com:8080/';//Apache Tomcat server

    public function initialize(Controller $controller) {
        CakeLog::info("Report Manager initialize loading for java extension");
        $this->checkJavaExtension();
        CakeLog::info("Report Manager initialize loading for init connection");
        $this->_initConnection();
    }

    /**
     * 
     * Load database connection fore initialize
     * @author  sarawutt.b
     * @param   type $source datasource on the cake configure
     * @return  boolean
     */
    private function _initConnection() {
        $ds = ConnectionManager::getDataSource('default')->config;
        $driver = strtolower(trim($ds['datasource']));
        switch ($driver) {
            case 'database/postgres':
                $this->pgsqlConnect($ds['database'], $ds['login'], $ds['password'], $ds['host'], $ds['port']);
                break;
            case 'database/mysql':
                $this->mysqlConnnect($ds['database'], $ds['login'], $ds['password'], $ds['host'], $ds['port']);
                break;
            default :return false;
        }
        return true;
    }

    /**
     * 
     * Function setting JAVA or Apache TOMCAT hosting
     * @author  sarawutt.b
     * @param type $host as string of apahe tomcat hosting may be IP Address or host name
     * @return string
     */
    public function setJavaTomcatHost($host) {
        return $this->_java_host = $host;
    }

    /**
     *
     * Check for PHP Java bridge extension to be is loaded
     * @author	sarawutt.b
     * @access	public
     * @license	Zicure
     * @since	2012/12/31
     * @return	boolean true when load success and false otherwise
     */
    public function checkJavaExtension() {
        if (!extension_loaded('java')) {
            $sapi_type = php_sapi_name();
            $port = (isset($_SERVER['SERVER_PORT']) && (($_SERVER['SERVER_PORT']) > 1024)) ? $_SERVER['SERVER_PORT'] : '8080';
            if ($sapi_type == "cgi" || $sapi_type == "cgi-fcgi" || $sapi_type == "cli") {
                //echo PHP_SHLIB_SUFFIX;
                if (!(PHP_SHLIB_SUFFIX == "so" && @dl('java.so')) && !(PHP_SHLIB_SUFFIX == "dll" && @dl('php_java.dll')) && !(@include_once("JavaBridge/java/Java.inc")) && !(@require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc"))) {
                    $msg = "java extension not installed.";
                    CakeLog::critical($msg);
                    return $msg;
                }
            } else {
                //$path = "D:\\xampp\\htdocs\\SP_PROJECTS\\www\\app\\webroot\\JavaBridge\\java\\Java.inc";
                $path = WWW_ROOT . "Java.inc";//JavaBridge\\java\\Java.inc"
                //debug(file_exists($path));exit;
                if(file_exists($path)){
                    require_once($path) or die("JavaBridge/java/Java.inc");
                }
                else{
                    require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc") or die("Can't load => http://{$this->_java_host}:$port/JavaBridge/java/Java.inc");
                }
                
                
//                if (!(@include_once("/JavaBridge/java/Java.inc"))) {
//                    require_once("http://{$this->_java_host}:$port/JavaBridge/java/Java.inc");
//                }
            }
        }

        if (!function_exists("java_get_server_name")) {
            $msg = "The loaded java extension is not the PHP/Java Bridge";
            CakeLog::critical($msg);
            return $msg;
        }
        return true;
    }

    /**
     * 
     * POSTGRESQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function pgsqlConnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '5432') {
        CakeLog::info('PostgreSQL connecting');
        $this->_databaseDriver = 'org.postgresql.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * MySQL Connection
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @return object of connection
     */
    public function mysqlConnnect($dbname = 'tmp', $user = 'user', $pass = 'password', $host = '127.0.0.1', $port = '3306') {
        CakeLog::info('MySQL connecting');
        $this->_databaseDriver = 'com.mysql.jdbc.Driver';
        return $this->makeDBConnection('postgresql', $host, $port, $dbname, $user, $pass);
    }

    /**
     * 
     * Database connection for eache dinamicically connection
     * @author  sarawutt.b
     * @param type $jdbcDriver as string for java JDBC Driver
     * @param type $host as string database host
     * @param type $port as integer of database port number
     * @param type $dbname as string of database name
     * @param type $user as string of database owner
     * @param type $pass as string of database password
     * @return object of connection
     */
    private function makeDBConnection($jdbcDriver = 'msql', $host = '127.0.0.1', $port = '3306', $dbname = 'tmp', $user = 'tmp', $pass = 'password') {
        $connectionString = "jdbc:{$jdbcDriver}://{$host}:{$port}/{$dbname}?user={$user}&password={$pass}";
        java("java.lang.Class")->forName($this->_databaseDriver);
        $this->_driverManager = new JavaClass("java.sql.DriverManager");
        $this->_conn = $this->_driverManager->getConnection($connectionString);
        return true;
    }

    /**
     *
     * Generate the report to output file
     * @author      sarawutt.b
     * @access      public
     * @param       string name of report possible value PURCHASE_ORDER_REPORT(purchase orders report)|RECEIVED_REPORT(received report)|SHIPING_INVOICE_TAX_REPORT(shiping invoice tax report)
     * @param       integer primary key of master table for find the report
     * @param       string which of report format
     * @license     Zicure
     * @since       2012.12.31
     * @modified    2016.08.18
     * @return      void
     */
    public function generateReport($reportName = null, $formParams = array(), $reportFormat = 'pdf') {
        CakeLog::info("Generate for the report rptFile : {$reportName}");
        if ($this->checkJavaExtension()) {
            try {
                try {
                    $compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
                    $viewer = new JavaClass("net.sf.jasperreports.view.JasperViewer");
                    $report = $compileManager->compileReport($reportName);
                    $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
                    $params = array();
                    if (!empty($formParams) && (is_array($formParams))) {
                        $params = new java('java.util.HashMap');
                        foreach ($formParams as $k => $v) {
                            $params->put($k, $v);
                        }
                    }

                    $jasperPrint = $fillManager->fillReport($report, $params, $this->_conn);
                    $outputPath = WWW_ROOT . "output." . $reportFormat;
                    java_set_file_encoding("UTF-8");
                    $em = java('net.sf.jasperreports.engine.JasperExportManager');
                    $em->exportReportToPdfFile($jasperPrint, $outputPath);
                    header("Content-type: application/pdf");
                    readfile($outputPath);
                    unlink($outputPath);

                } catch (JavaException $ex) {
                    $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
                    echo $msg;
                    CakeLog::error($msg);
                    return false;
                }
            } catch (JavaException $ex) {
                $msg = "<b>ERROR generating Report:</b><br/> " . $ex->getCause();
                echo $msg;
                CakeLog::error($msg);
                return false;
            }
        }
    }

    /**
     * Convert PHP value to JAVA value
     * @author  sarawutt.b
     * @param   string $value as mix
     * @param   string $className as string
     * @returns boolean success
     */
    function convertValue($value, $className) {
        try {
            $temp = null;
            switch ($className) {
                case 'java.lang.String':
                case 'java.lang.Boolean':
                case 'java.lang.Integer':
                case 'java.lang.Long':
                case 'java.lang.Short':
                case 'java.lang.Double':
                case 'java.math.BigDecimal':
                    $temp = new Java($className, $value);
                    break;
                case 'java.sql.Timestamp':
                case 'java.sql.Time':
                    $tmp = new Java($className);
                    $temp = $tmp->valueOf($value);
                    break;
                default : $temp = null;
                    break;
                    return $temp;
            }
        } catch (Exception $err) {
            $msg = "unable to convert value, {$value} could not be converted to {$className}";
            echo $msg;
            CakeLog::error($msg);
            return false;
        }
        $msg = "unable to convert value, class name {$className} not recognised";
        echo $msg;
        CakeLog::notice($msg);
        return false;
    }

    /**
     * Write log file
     * @author  sarawutt.b
     * @param   string as $msg_log is log line content
     * @return  true with write content in the log file
     * @since   2016.08.18 15:19
     */
    public function writeLog($msg_log = null) {
        $LOG_PATH = LOGPATH . DS . 'REPORTS' . DS;
        $logFileName = date('Ymd') . '.componebt.ireport.log';
        //return $this->CrazyLog->WRITE_NLOG($LOG_PATH, $msg_log, $logFileName);
    }

    /**
     * 
     * Write log separate each block process
     * @author  sarawutt.b
     * @return  void
     * @since   2016.08.18 15:19
     */
    public function writeLogSeparateLine($logMode = 'writeLog') {
        //return $this->$logMode("-----------------------------------------------------------------------------------------------------------------------");
    }

}

?>
