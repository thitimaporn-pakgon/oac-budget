<?php

/**
 * 
 * PHPMailer Component
 * @author  sarawutt.b
 * @since   2016/10/20
 * @license Zicure Corp.
 * @link    https://github.com/PHPMailer/PHPMailer
 */
App::uses('Component', 'Controller');
App::import('vendor', 'PHPMailer', array('file' => 'PHPMailer/PHPMailerAutoload.php'));

class PHPMailerComponent extends Component {

    public $uses = array();
    public $components = array('Session');
    private $_return_msg = array();
    //Send mail via SMTP
//    private $_smtp_host = 'smtp.gmail.com';
//    private $_smtp_port = '587';
  
    //Send mail via Open SSL
    private $_smtp_host = null;
    private $_smtp_port = null;
    private $_smtp_username = null;
    private $_smtp_password = null;
    private $_mail = null;
    private $_status = array('OK' => 'Successfully', 'WARN' => 'Warnning', 'ERR' => 'ERROR');

    public function initialize(Controller $controller) {
        foreach ($this->uses as $model_name) {
            App::import('Model', $model_name);
            $model_class = "{$model_name}";
            $this->$model_name = new $model_class();
        }
        $this->setSMTPHostAndPort();
    }

    /**
     * 
     * Mail initialize
     * @author  sarawutt.b
     * @license Zicure Corp.
     * @return  void
     */
    private function _mailerInit() {
        $this->_mail = new PHPMailer();
        /**
         * 
         * Enable SMTP debugging
         * 0 = off (for production use)
         * 1 = client messages
         * 2 = client and server messages
         */
        $this->_mail->SMTPDebug = Configure::read('debug');
        $this->_mail->Debugoutput = 'html';

        //Tell PHPMailer to use SMTP
        $this->_mail->isSMTP();

        //Ask for HTML-friendly debug output
        $this->_mail->CharSet = 'utf-8';

        //Set the hostname of the mail server
        $this->_mail->Host = $this->_smtp_host;

        //Whether to use SMTP authentication
        $this->_mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $this->_mail->Username = $this->_smtp_username;

        //Password to use for SMTP authentication
        $this->_mail->Password = $this->_smtp_password;

        //Set the encryption system to use - ssl (deprecated) or tls
        $this->_mail->SMTPSecure = 'tls';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->_mail->Port = $this->_smtp_port;
        $this->_mail->isHTML(true);
    }

    public function setSMTPHostAndPort($SMTPHost = 'ssl://smtp.gmail.com', $port = '465') {
        $this->_smtp_host = $SMTPHost;
        $this->_smtp_port = $port;
        return true;
    }

    public function setSMTPUsernameAndPassword($username = 'test@exampl.com', $password = 'password') {
        $this->_smtp_username = $username;
        $this->_smtp_password = $password;
        return true;
    }

    // public function send($to, $subject, $message) {
    public function send($from = array(), $to = null, $subject = null, $message = null, $replyTo = array(), $attachments = array(), $cc = array(), $bcc = array()) {
        if (empty($from)) {
            $this->_return_msg['status'] = __($this->_status['WARN']);
            $this->_return_msg['message'] = __('mail from must be define and array("email_address"=>"sender_name") !');
            return $this->_return_msg;
        }

        if (empty($to)) {
            $this->_return_msg['status'] = __($this->_status['WARN']);
            $this->_return_msg['message'] = __('mail to must be define and array("email_address"=>"to_name") !');
            return $this->_return_msg;
        }

        $this->_mailerInit();
        $this->_setSendFrom($from);
        $this->_setSendTo($to);
        $this->_setReply($replyTo);
        $this->_setcc($cc);
        $this->_setBcc($bcc);
        $this->_setAttachment($attachments);

        $this->_mail->Subject = $subject;
        $this->_mail->Body = nl2br($message);
        $this->_mail->AltBody = nl2br($message);

        if (!$this->_mail->send()) {
            $this->_return_msg['status'] = __($this->_status['ERR']);
            $this->_return_msg['message'] = __('Message could not be sent');
            $this->_return_msg['message_info'] = __('Mailer Error: %s', $this->_mail->ErrorInfo);
        } else {
            echo 'Message has been sent';
            $this->_return_msg['status'] = __($this->_status['OK']);
            $this->_return_msg['message'] = __('Message has been sent');
            $this->_return_msg['message_info'] = __('Message has been sent successfully');
        }

        $this->_mail->clearAddresses();
        $this->_mail->clearAttachments();
        return $this->_return_msg;
    }

    /**
     * 
     * set mail send from
     * @author  sarawutt.b
     * @param   type $from as array of send from list
     * @return  boolean
     */
    private function _setSendFrom($from = array()) {
        if (!is_array($from)) {
            $from = array($from => $from);
        }
        foreach ($from as $sender => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $this->_mail->setFrom($email, $sender);
        }
        return true;
    }

    /**
     * 
     * set mail send to list
     * @author  sarawutt.b
     * @param   type $to as array of send to list
     * @return  boolean
     */
    private function _setSendTo($to = array()) {
        if (!is_array($to)) {
            $to = array($to => $to);
        }
        foreach ($to as $sender => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $this->_mail->addAddress($email, $sender);
        }
        return true;
    }

    /**
     * 
     * set mail send Reply list
     * @author  sarawutt.b
     * @param   type $replyTo as array of reply list
     * @return  boolean
     */
    private function _setReply($replyTo = array()) {
        if (!is_array($replyTo)) {
            $replyTo = array($replyTo => $replyTo);
        }
        foreach ($replyTo as $sender => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $this->_mail->addReplyTo($email, $sender);
        }
        return true;
    }

    /**
     * 
     * set mail send CC list
     * @author  sarawutt.b
     * @param   type $cc as array of CC list
     * @return  boolean
     */
    private function _setcc($cc = array()) {
        if (!is_array($cc)) {
            $cc = array($cc => $cc);
        }
        foreach ($cc as $sender => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $this->_mail->addCC($email, $sender);
        }
        return true;
    }

    /**
     * 
     * set mail send BCC list
     * @author  sarawutt.b
     * @param   type $bcc as array of BCC list
     * @return  boolean
     */
    private function _setBcc($bcc = array()) {
        if (!is_array($bcc)) {
            $bcc = array($bcc => $bcc);
        }
        foreach ($bcc as $sender => $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $this->_mail->addBCC($email, $sender);
        }
        return true;
    }

    /**
     * 
     * set mail send Attachment file list
     * @author  sarawutt.b
     * @param   type $attachments as array of attachment file list
     * @return  boolean
     */
    private function _setAttachment($attachments = array()) {
        if (!is_array($attachments)) {
            $attachments = array($attachments => $attachments);
        }
        foreach ($attachments as $name => $file) {
            $this->_mail->addAttachment($file, $name);
        }
        return true;
    }

}

?>
