<?php

App::uses('AppController', 'Controller');

/**
 *
 * PlanManeuvers Controller
 * @author  sarawutt.b
 * @property PlanManeuver $PlanManeuver
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-04-18 17:41:24
 * @license Zicure Corp. 
 */
class PlanManeuversController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('StrategyProduct', 'StrategyPlan', 'StrategicManeuver', 'StrategyGov', 'PlanManeuver', 'Common');

    /**
     * 
     * index method view list for plan maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:41:23
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('StrategyPlan.id' => 'ASC', 'StrategyPlan.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trimAllData($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(StrategyPlan.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('StrategyPlan.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(StrategyPlan.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(StrategyPlan.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(StrategyPlan.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(StrategyPlan.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'StrategyPlan' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->StrategyPlan->recursive = 0;
        $planManeuvers = $this->Paginator->paginate('StrategyPlan');
        $this->set(array('planManeuvers' => $planManeuvers, '_serialize' => array('planManeuvers')));
        //$this->set('planManeuvers', $this->Paginator->paginate('PlanManeuver'));
        //$this->saveAccessLog('View list for %s', 'plan maneuver');
    }

    /**
     *
     * view method view for plan maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of plan maneuver id [PK] 
     * @since   2017-04-18 17:41:23
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        if (!$this->StrategyPlan->exists($id)) {
            //throw new NotFoundException(__('Invalid plan maneuver'));
            $this->Flash->error(__('Invalid not found plan maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('StrategyPlan.' . $this->StrategyPlan->primaryKey => $id));
        $planManeuver = $this->StrategyPlan->find('first', $options);
        $this->set(array('planManeuver' => $planManeuver, '_serialize' => array('StrategyPlan')));
        //$this->set('planManeuver', $this->PlanManeuver->find('first', $options));
        //$this->saveAccessLog('View for the plan maneuver with id = %s', $id);
    }

    /**
     *
     * add method add new for plan maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:41:23
     * @license Zicure Corp. 
     * @return void
     */
    public function add($stID = null) {
        $data = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $stID)));
        if ($this->request->is('post')) {
            $this->request->data['StrategyPlan']['strategy_gov_id'] = $stID;
            $this->request->data['StrategyPlan']['budget_year_id'] = $data['StrategyGov']['budget_year_id'];
            $this->request->data['StrategyPlan']['budget_year'] = $data['StrategyGov']['budget_year'];

            $checkCode = $this->Common->checkCode('budget.strategy_plans', $this->request->data['StrategyPlan']['code'], $data['StrategyGov']['budget_year_id'], '', $data['StrategyGov']['id']);
            if ($checkCode == false) {
                $this->Flash->error(__('The plan maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            $this->StrategyPlan->create();

            if ($this->StrategyPlan->save($this->request->data)) {
                $this->Flash->success(__('The plan maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $stID));
            } else {
                $this->Flash->error(__('The plan maneuver could not be saved. Please, try again.'));
            }
        }
        $strategicManeuvers = $this->StrategyPlan->find('list');
//        $budgetYears = $this->StrategyPlan->BudgetYear->find('list');
//        $systemHasProcesses = $this->StrategyPlan->SystemHasProcess->find('list');
        $year = $data['StrategyGov']['budget_year_id'];
        $this->set(compact('strategicManeuvers', 'budgetYears', 'systemHasProcesses'));
        $this->set(compact('year', 'data'));
    }

    /**
     *
     * edit method for plan maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of plan maneuver id [PK] 
     * @since   2017-04-18 17:41:23
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {
        if (!$this->StrategyPlan->exists($id)) {
            //throw new NotFoundException(__('Invalid plan maneuver'));
            $this->Flash->error(__('Invalid not found plan maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }

        $options = array('conditions' => array('StrategyPlan.' . $this->StrategyPlan->primaryKey => $id));
        $data = $this->StrategyPlan->find('first', $options);
        $dataall_StrategyGov = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $data['StrategyPlan']['strategy_gov_id'])));

        if ($this->request->is(array('post', 'put'))) {
            $stID = $this->request->data['StrategyPlan']['strategy_gov_id'];
            $checkCode = $this->Common->checkCode('budget.strategy_plans', $this->request->data['StrategyPlan']['code'], $data['StrategyPlan']['budget_year_id'], '', $data['StrategyPlan']['strategy_gov_id'], 'edit', $data['StrategyPlan']['code']);
            if ($checkCode == false) {
                $this->Flash->error(__('The plan maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            if ($this->StrategyPlan->save($this->request->data)) {
                $this->Flash->success(__('The plan maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $stID));
            } else {
                $this->Flash->error(__('The plan maneuver could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('StrategyPlan.' . $this->StrategyPlan->primaryKey => $id));
            $this->request->data = $data;
        }
        $year = $data['StrategyPlan']['budget_year_id'];
        $this->set(compact('year', 'strategicManeuvers', 'budgetYears', 'systemHasProcesses', 'data', 'dataall_StrategyGov'));
        
    }

    /**
     *
     * delete method delete for plan maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of plan maneuver id [PK] 
     * @since   2017-04-18 17:41:23
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->StrategyPlan->id = $id;
        if (!$this->StrategyPlan->exists()) {
            //throw new NotFoundException(__('Invalid plan maneuver'));
            $this->Flash->error(__('Invalid not found plan maneuver with id %s please try again !', $id));
            $this->redirect($this->referer());
        }
        $hasChild = $this->ck_tree_Bfdelete($id);
        if ($hasChild) {
            $this->StrategyPlan->delete($id);
            $this->Flash->success(__('The plan maneuver has been deleted.'));
        } else {
            $this->Flash->error(__('ไม่สามารถลบข้อมูลได้ เนื่องจากมีข้อมูลส่วนอื่นที่เกี่ยวข้องอยู่ กรุณาลบข้อมูลส่วนที่เกี่ยวข้องก่อน !!!'));
        }
        return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
    }

## Check Tree

    private function ck_tree_Bfdelete($id = NULL) {
        $data = $this->StrategyProduct->find('first', array('conditions' => array('strategy_plan_id' => $id)));
        if (empty($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
