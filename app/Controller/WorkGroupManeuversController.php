<?php

App::uses('AppController', 'Controller');

/**
 *
 * WorkGroupManeuvers Controller
 * @author  sarawutt.b
 * @property WorkGroupManeuver $WorkGroupManeuver
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-04-18 17:42:41
 * @license Zicure Corp. 
 */
class WorkGroupManeuversController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('WorkGroupManeuver', 'EventManeuver', 'Inbox');

    /**
     * 
     * index method view list for work group maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:42:40
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('WorkGroupManeuver.id' => 'ASC', 'WorkGroupManeuver.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trimAllData($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(WorkGroupManeuver.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('WorkGroupManeuver.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(WorkGroupManeuver.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(WorkGroupManeuver.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(WorkGroupManeuver.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(WorkGroupManeuver.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'WorkGroupManeuver' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->WorkGroupManeuver->recursive = 0;
        $workGroupManeuvers = $this->Paginator->paginate('WorkGroupManeuver');
        $this->set(array('workGroupManeuvers' => $workGroupManeuvers, '_serialize' => array('workGroupManeuvers')));
        //$this->set('workGroupManeuvers', $this->Paginator->paginate('WorkGroupManeuver'));
        //$this->saveAccessLog('View list for %s', 'work group maneuver');
    }

    /**
     *
     * view method view for work group maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of work group maneuver id [PK] 
     * @since   2017-04-18 17:42:40
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        if (!$this->WorkGroupManeuver->exists($id)) {
            //throw new NotFoundException(__('Invalid work group maneuver'));
            $this->Flash->error(__('Invalid not found work group maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('WorkGroupManeuver.' . $this->WorkGroupManeuver->primaryKey => $id));
        $workGroupManeuver = $this->WorkGroupManeuver->find('first', $options);
        $this->set(array('workGroupManeuver' => $workGroupManeuver, '_serialize' => array('workGroupManeuver')));
        //$this->set('workGroupManeuver', $this->WorkGroupManeuver->find('first', $options));
        //$this->saveAccessLog('View for the work group maneuver with id = %s', $id);
    }

    /**
     *
     * add method add new for work group maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:42:40
     * @license Zicure Corp. 
     * @return void
     */
    public function add($eventID = null) {
        $data = $this->EventManeuver->find('first', array('conditions' => array('EventManeuver.id' => $eventID)));
        $tranOK = true;
        $inbox = array();
        $this->WorkGroupManeuver->begin();
        $this->Inbox->begin();
        if ($this->request->is('post')) {
            $this->request->data['WorkGroupManeuver']['create_uid'] = $this->getCurrenSessionUserId();
            $this->request->data['WorkGroupManeuver']['strategic_maneuver_id'] = $data['StrategicManeuver']['id'];
            $this->request->data['WorkGroupManeuver']['plan_maneuver_id'] = $data['PlanManeuver']['id'];
            $this->request->data['WorkGroupManeuver']['result_maneuver_id'] = $data['ResultManeuver']['id'];
            $this->request->data['WorkGroupManeuver']['event_maneuver_id'] = $data['EventManeuver']['id'];
            $this->request->data['WorkGroupManeuver']['budget_year_id'] = $data['StrategicManeuver']['budget_year_id'];
            $this->request->data['WorkGroupManeuver']['system_has_process_id'] = 5;
            $this->request->data['WorkGroupManeuver']['from_department_id'] = $this->getCurrenSessionDepartmentId();
            $checkCode = $this->Common->checkCode('maneuver.work_group_maneuvers', $this->request->data['WorkGroupManeuver']['code'], $data['EventManeuver']['budget_year_id'], 'EventManeuver', $data['EventManeuver']['id']);
            if ($checkCode == false) {
                $this->Flash->error(__('The work group maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            $this->WorkGroupManeuver->create();
            if ($this->WorkGroupManeuver->save($this->request->data) === false) {
                $tranOK = false;
            }
            $this->Inbox->create();
            $inbox['Inbox']['create_uid'] = $this->getCurrenSessionUserId();
            $inbox['Inbox']['subject'] = 'ส่งให้ดำเนินการ[จัดทำงบงาน]';
            $inbox['Inbox']['from_department_id'] = $this->getCurrenSessionDepartmentId();
            $inbox['Inbox']['to_department_id'] = $this->request->data['WorkGroupManeuver']['to_department_id'];
            $inbox['Inbox']['ref_id'] = $this->WorkGroupManeuver->id;
            $inbox['Inbox']['ref_model'] = 'WorkGroupManeuver';
            $inbox['Inbox']['system_has_process_id'] = 6;
            $inbox['Inbox']['task_flag'] = 'MEV';
            $inbox['Inbox']['status'] = 'N';
            if ($this->Inbox->save($inbox) === false) {
                $tranOK = false;
            }
            if ($tranOK == true) {
                $this->WorkGroupManeuver->commit();
                $this->Inbox->commit();
                $this->Flash->success(__('The work group maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $data['StrategicManeuver']['id']));
            } else {
                $this->WorkGroupManeuver->rollback();
                $this->Inbox->rollback();
                $this->Flash->error(__('The work group maneuver could not be saved. Please, try again.'));
            }
        }
        $eventManeuvers = $this->WorkGroupManeuver->EventManeuver->find('list');
        $year = $data['StrategicManeuver']['budget_year_id'];
        $systemHasProcesses = $this->WorkGroupManeuver->SystemHasProcess->find('list');
        $this->set(compact('eventManeuvers', 'budgetYears', 'systemHasProcesses'));
        $this->set(compact('year', 'data'));
        //$this->saveAccessLog('Add new the work group maneuver');
    }

    /**
     *
     * edit method for work group maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of work group maneuver id [PK] 
     * @since   2017-04-18 17:42:40
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {
        if (!$this->WorkGroupManeuver->exists($id)) {
            //throw new NotFoundException(__('Invalid work group maneuver'));
            $this->Flash->error(__('Invalid not found work group maneuver with id %s please try again !', $id));
            return $this->redirect($this->referer());
        }
        $tranOK = true;
        $inbox = array();
        $this->WorkGroupManeuver->begin();
        $this->Inbox->begin();
        $options = array('conditions' => array('WorkGroupManeuver.' . $this->WorkGroupManeuver->primaryKey => $id));
        $data = $this->WorkGroupManeuver->find('first', $options);
        $dataInbox = $this->Inbox->find('first', array('conditions' => array('ref_model' => 'WorkGroupManeuver', 'ref_id' => $id, 'system_has_process_id' => 6, 'status' => 'N')));
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['WorkGroupManeuver']['update_uid'] = $this->getCurrenSessionUserId();
            $checkCode = $this->Common->checkCode('maneuver.work_group_maneuvers', $this->request->data['WorkGroupManeuver']['code'], $data['EventManeuver']['budget_year_id'], 'EventManeuver', $data['EventManeuver']['id'], 'edit', $data['WorkGroupManeuver']['code']);
            if ($checkCode == false) {
                $this->Flash->error(__('The work group maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            if ($this->WorkGroupManeuver->save($this->request->data) === false) {
                $tranOK = false;
            }
            $inbox['Inbox']['id'] = $dataInbox['Inbox']['id'];
            $inbox['Inbox']['to_department_id'] = $this->request->data['WorkGroupManeuver']['to_department_id'];
            $inbox['Inbox']['update_uid'] = $this->getCurrenSessionUserId();
            if ($this->Inbox->save($inbox) === false) {
                $tranOK = false;
            }

            if ($tranOK == true) {
                $this->WorkGroupManeuver->commit();
                $this->Inbox->commit();
                $this->Flash->success(__('The work group maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $data['StrategicManeuver']['id']));
            } else {
                $this->WorkGroupManeuver->rollback();
                $this->Inbox->rollback();
                $this->Flash->error(__('The work group maneuver could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $data;
        }
        $eventManeuvers = $this->WorkGroupManeuver->EventManeuver->find('list');
        $budgetYears = $this->WorkGroupManeuver->BudgetYear->find('list');
        $systemHasProcesses = $this->WorkGroupManeuver->SystemHasProcess->find('list');
        $this->set(compact('eventManeuvers', 'budgetYears', 'systemHasProcesses', 'data'));
        //$this->saveAccessLog('Edit for the work group maneuver with id = %s', $id);
    }

    /**
     *
     * delete method delete for work group maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of work group maneuver id [PK] 
     * @since   2017-04-18 17:42:40
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->WorkGroupManeuver->id = $id;
        if (!$this->WorkGroupManeuver->exists()) {
            //throw new NotFoundException(__('Invalid work group maneuver'));
            $this->Flash->error(__('Invalid not found work group maneuver with id %s please try again !', $id));
            return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
        }
        $hasChild = $this->WorkGroupManeuver->deleteStrategic($id, $model, $sector);
        if ($hasChild) {
            $this->Flash->success(__('The work group maneuver has been deleted.'));
        } else {
            $this->Flash->error(__('ไม่สามารถลบข้อมูลได้ เนื่องจากมีข้อมูลส่วนอื่นที่เกี่ยวข้องอยู่ กรุณาลบข้อมูลส่วนที่เกี่ยวข้องก่อน !!!'));
        }
        return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
    }

    /**
     *
     * find work group maneuver ajax by budget_year_id
     * @author  Dosz
     * @param   integer $budget_year_id as integer of work group maneuver $budget_year_id
     */
    function findWorkGroup($budget_year_id = null, $empty = '---- please select ----') {
        $empty = __($empty);
        if ($empty == '1') {
            $empty = __('---- please select ----');
        }

        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->WorkGroupManeuver->find('list', array('order' => array('WorkGroupManeuver.name ASC'), 'conditions' => array('WorkGroupManeuver.budget_year_id' => $budget_year_id)));
        $selectamphur = "<option value='' selected='selected' class='chosen-select' style='width:200px'>{$empty}</option>";

        foreach ($data as $k => $v) {
            $selectamphur .= "<option value='$k'>$v</option>";
        }
        echo $selectamphur;
    }

}
