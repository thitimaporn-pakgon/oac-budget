<?php

App::uses('AppController', 'Controller');

/**
 * SysActions Controller
 *
 * @property SysAction $SysAction
 */
class SysActionsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SysAction->recursive = 0;
        $this->set('sysActions', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SysAction->exists($id)) {
            throw new NotFoundException(__('Invalid sys action'));
        }
        $options = array('conditions' => array('SysAction.' . $this->SysAction->primaryKey => $id));
        $this->set('sysAction', $this->SysAction->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SysAction->create();
            if ($this->SysAction->save($this->request->data)) {
                $this->Session->setFlash(__('The sys action has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sys action could not be saved. Please, try again.'));
            }
        }
        $sysControllers = $this->SysAction->SysController->find('list');
        $sysStatuses = $this->SysAction->SysStatus->find('list');
        $this->set(compact('sysControllers', 'sysStatuses'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SysAction->exists($id)) {
            throw new NotFoundException(__('Invalid sys action'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->SysAction->save($this->request->data)) {
                $this->Session->setFlash(__('The sys action has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sys action could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SysAction.' . $this->SysAction->primaryKey => $id));
            $this->request->data = $this->SysAction->find('first', $options);
        }
        $sysControllers = $this->SysAction->SysController->find('list');
        $sysStatuses = $this->SysAction->SysStatus->find('list');
        $this->set(compact('sysControllers', 'sysStatuses'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SysAction->id = $id;
        if (!$this->SysAction->exists()) {
            throw new NotFoundException(__('Invalid sys action'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SysAction->delete()) {
            $this->Session->setFlash(__('Sys action deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Sys action was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
