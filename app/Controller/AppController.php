<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('L10n', 'I18n');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $theme = "AdminLTE";
    public $helpers = array(
        'Form' => array('className' => 'Bs3Helpers.Bs3Form'),
        'Html' => array('className' => 'Bs3Helpers.Bs3Html')
    );
    public $components = array(
        'Session',
        'Flash',
        'Auth' => array(
            'loginRedirect' => '/',
            'logoutRedirect' => '/',
            'authenticate' => array(
                'Form' => array('passwordHasher' => 'Blowfish')
            ),
            'authorize' => array('Controller')
        ),
        'Utility',
        'DebugKit.Toolbar'
    );


	/* backup �ͧ����
	public $uses = array('Menu', 'SysAcl', 'AccessLog', 'Department', 'Common');
    public $_server_domain = null;
    public $_server_port = null;
    public $L10n = null;

    public $_server_protocal = NULL;
    public $_session_user_id = null;
    public $_session_role_id = null;
    public $select_empty_msg = '---- please select ----';
    protected $_badge_style = array('inbox' => 'blue', 'pending' => 'yellow', 'wait' => 'yello', 'outbox' => 'aqua', 'aqua' => 'aqua', 'blue' => 'blue', 'info' => 'info', 'red' => 'red');
	*/

    public function beforeFilter() {

		/* backup	�к�����
        if ($this->Session->check('SessionLanguage') == false) {
            $this->Session->write('SessionLanguage', 'tha');
        }
        $this->_setLanguage();
        $geturl = $this->params['controller'] . '/' . $this->params['action'];
        $checkpin = $this->Session->read('Pinpass');
        $this->set('select_empty_msg', __($this->select_empty_msg));
        $this->Auth->allow('login');

        if ($this->checkPinAuthUsers() && (strtolower($this->params['action']) != 'pin')) {
            $this->redirect(array('controller' => 'Users', 'action' => 'pin'));
        }

        if ($this->checkAuthUsers()) {
            $this->_server_domain = $_SERVER['SERVER_NAME'];
            $this->_server_port = $_SERVER['SERVER_PORT'];
            $this->_session_role_id = $this->getCurrenSessionRoleId();
            $this->_session_user_id = $this->getCurrenSessionUserId();
            $this->_server_protocal = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

            /**
             *
             * Select dynamic ACL and the dynamic menu
             * @author sarawutt.b

            $page = 1;
            switch ($this->layout) {
                case 'approve': $page = 2;
                    break;
                case 'approveup': $page = 3;
                    break;
                case 'appploveAllocatedDown': $page = 4;
                    break;
                case 'appploveAllocatedUp': $page = 5;
                    break;
                case 'appploveManageDown': $page = 6;
                    break;
                case 'appploveManageUp': $page = 7;
                    break;
            }
            $this->set('menuListHtml', $this->getDynamicMenu($page));
            $this->layout = 'default';
        }
        //Generate for access log
        //$this->saveAccessLog($geturl);
		*/
    }


    public function isAuthorized($user) {
        return true;
    }

    function budgetYearOption($param = null, $flag = 'BE') {
        return date('Y') + 543;
    }
	/* backup �ͧ����
    public function _setLanguage() {
        $this->L10n = new L10n();
        $language = $this->Session->read('SessionLanguage');
        Configure::write('Config.language', $language);
        $this->L10n->get($language);
    }

    public function getEmptySelect() {
        return array('' => __($this->select_empty_msg));
    }

    public function getCurrenSessionUserId() {
        return $this->Session->read('Auth.User.id');
    }

    public function getCurrenSessionRoleId() {
        return $this->Session->read('Auth.User.role_id');
    }

    public function getCurrenSessionDepartmentId() {
        return $this->Session->read('Auth.User.department_id');
    }

    public function checkAuthUsers() {
        return $this->Session->check('Auth.User');
    }

    public function checkPinAuthUsers() {
        return $this->Session->check('xPinAuth.User');
    }

    public function getCurrentLanguage() {
        return $this->Session->read('SessionLanguage');
    }

    function uploadFiles($folder, $file, $itemId = null) {
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;

        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }

        //Bould new path if $itemId to be not null
        if ($itemId) {
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            $rel_url = $folder . '/' . $itemId;
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }

        //define for file type where it allow to upload
        $map = array(
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/excel' => '.xls',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx'
        );

        //Bould file extension keep to the database
        $userfile_extn = substr($file['name'], strrpos($file['name'], '.') + 1);
        if (array_key_exists($file['type'], $map)) {
            $typeOK = true;
        }

        //Rename for the file if not change of the upload file makbe duplicate
        $filename = $this->VERSION() . $map[$file['type']];
        if ($typeOK) {
            switch ($file['error']) {
                case 0:
                    @unlink($folder_url . '/' . $filename); //Delete the file if it already existing
                    $full_url = $folder_url . '/' . $filename;
                    $url = $rel_url . '/' . $filename;
                    $success = move_uploaded_file($file['tmp_name'], $url);
                    if ($success) {
                        $result['urls'][] = '/' . $url;
                        $result['upfilename'][] = $filename;
                        $result['ext'][] = $userfile_extn;
                        $result['origin_name'][] = $file['name'];
                        $result['type'][] = $file['type'];
                    } else {
                        $result['errors'][] = __("Error uploaded {$filename}. Please try again.");
                    }
                    break;
                case 3:
                    $result['errors'][] = __("Error uploading {$filename}. Please try again.");
                    break;
                case 4:
                    $result['nofiles'][] = __("No file Selected");
                    break;
                default:
                    $result['errors'][] = __("System error uploading {$filename}. Contact webmaster.");
                    break;
            }
        } else {
            $permiss = '';
            foreach ($map as $k => $v) {
                $permiss .= "{$v}, ";
            }
            $result['errors'][] = __("{$filename} cannot be uploaded. Acceptable file types in : %s", trim($permiss, ','));
        }
        return $result;
    }


    public function saveAccessLog($actions = '', $params = null) {
        return $this->AccessLog->saveAccessLog($actions, $params);
    }

    public function getDynamicMenu($pageLevel = 1) {
        $_FULL_URL = $this->_server_protocal . $this->_server_domain . ":" . $this->_server_port;
        $menuList = $this->Menu->getDynamicAclMenu($this->_server_domain, $this->_server_port, $this->_session_role_id, $this->_session_user_id, $pageLevel);
        $name_field = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $menuListHtml = "";
        foreach ($menuList as $k => $v) {
            if (strpos($v[0]['url'], '#') !== false) {
                $menuListHtml .= "<li class='treeview'>
                                            <a href='#'>
                                                <i class='{$v[0]['glyphicon']}'></i>
                                                <span>{$v[0][$name_field]}</span>
                                                <i class='fa fa-angle-left pull-right'></i>
                                            </a><ul class='treeview-menu'>";
                $childMenuLists = $this->Menu->getDynamicChildMenu($this->_server_domain, $this->_server_port, $this->_session_role_id, $this->_session_user_id, $v[0]['id']);
                foreach ($childMenuLists as $kk => $vv) {
                    $badge = null;
                    if (!empty($vv[0]['badge'])) {
                        $badge_count = $this->Menu->badgeFinder($vv[0]['badge']);
                        $badgeAction = null;
                        if (strpos($vv[0]['url'], 'inbox') !== false) {
                            $badgeAction = 'inbox';
                        } elseif (strpos($vv[0]['url'], 'outbox') !== false) {
                            $badgeAction = 'outbox';
                        } elseif ((strpos($vv[0]['url'], 'pending') !== false) || (strpos($vv[0]['url'], 'wait') !== false)) {
                            $badgeAction = 'pending';
                        }
                        $badgeClass = array_key_exists($badgeAction, $this->_badge_style) ? $this->_badge_style[$badgeAction] : 'info';
                        $badge = "<span class='pull-right badge bg-{$badgeClass}'>{$badge_count}</span>";
                    }
                    $menuListHtml .= "<li><a href='{$vv[0]['url']}'><i class='{$vv[0]['glyphicon']}'></i> {$vv[0][$name_field]} {$badge}</a></li>";
                }
                $menuListHtml .= "</ul></li>";
            } elseif ((strpos($v[0]['url'], 'http://') !== false) || (strpos($v[0]['url'], 'https://') !== false)) {
                $menuListHtml .= "<li><a href='{$v[0]['url']}' target='_blank'><i class='{$v[0]['glyphicon']}'></i> <span>{$v[0][$name_field]}</span></a></li>";
            } else {
                $menuListHtml .= "<li><a href='{$_FULL_URL}{$v[0]['url']}'><i class='{$v[0]['glyphicon']}'></i> <span>{$v[0][$name_field]}</span></a></li>";
            }
        }
        return $menuListHtml;
    }


    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    function budgetYearOption($param = null, $flag = 'BE') {
        return date('Y') + 543;
    }

    public function secureDecodeParam($param = null) {
        return $this->Common->secureDecodeParam($param);
    }

    public function secureEncodeParam($param = null) {
        return $this->Common->secureEncodeParam($param);
    }

    public function generateBudgetCode($expenseHeaderId = null, $part = 'A') {
        $this->loadModel('ExpenseHeaderManeuver');
        return $this->ExpenseHeaderManeuver->generateBudgetCode($expenseHeaderId, $part);
    }

    public function insertExpenseHeader($expenseHeaderId = null, $part = 'A') {
        $this->loadModel('ExpenseHeaderManeuver');
        return $this->ExpenseHeaderManeuver->insertExpenseHeader($expenseHeaderId, $part);
    }
    */

    public function unformatCurrencyParams(&$params = array()) {
        return $this->Utility->unformatCurrencyParams($params);
    }

    public function unformatCurrency($currency = 0, $showPrecision = true) {
        return $this->Common->unformatCurrency($currency, $showPrecision);
    }

    public function clearEmptyRequestData(&$params = null) {
        return $this->Utility->clearEmptyRequestData($params);
    }
	
}
