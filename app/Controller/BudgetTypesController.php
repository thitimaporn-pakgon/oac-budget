<?php

App::uses('AppController', 'Controller');

/**
 *
 * BudgetTypes Controller
 * @author  sarawutt.b
 * @property BudgetType $BudgetType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-06-06 17:40:44
 * @license Zicure Corp. 
 */
class BudgetTypesController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');

    /**
     * 
     * index method view list for budget type
     * @author  sarawutt.b 
     * @since   2017-06-06 17:40:42
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('BudgetType.id' => 'ASC', 'BudgetType.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trimAllData($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(BudgetType.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('BudgetType.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(BudgetType.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(BudgetType.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(BudgetType.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(BudgetType.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'BudgetType' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->BudgetType->recursive = 0;
        $budgetTypes = $this->Paginator->paginate('BudgetType');
        $this->set(array('budgetTypes' => $budgetTypes, '_serialize' => array('budgetTypes')));
        //$this->set('budgetTypes', $this->Paginator->paginate('BudgetType'));
        //$this->saveAccessLog('View list for %s', 'budget type');
    }

    /**
     *
     * view method view for budget type
     * @author  sarawutt.b 
     * @param   string $id as integer of budget type id [PK] 
     * @since   2017-06-06 17:40:42
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        if (!$this->BudgetType->exists($id)) {
            $this->Flash->error(__('Invalid not found budget type with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('BudgetType.' . $this->BudgetType->primaryKey => $id));
        $budgetType = $this->BudgetType->find('first', $options);
        $this->set(array('budgetType' => $budgetType, '_serialize' => array('budgetType')));
        //$this->set('budgetType', $this->BudgetType->find('first', $options));
        //$this->saveAccessLog('View for the budget type with id = %s', $id);
    }

    /**
     *
     * add method add new for budget type
     * @author  sarawutt.b 
     * @since   2017-06-06 17:40:42
     * @license Zicure Corp. 
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->request->data['BudgetType']['create_uid'] = $this->getCurrenSessionUserId();
            $this->request->data['BudgetType']['status'] = 'A';
            $this->BudgetType->create();
            if ($this->BudgetType->save($this->request->data)) {
                $this->Flash->success(__('The budget type has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The budget type could not be saved. Please, try again.'));
            }
        }
        //$this->saveAccessLog('Add new the budget type');
    }

    /**
     *
     * edit method for budget type
     * @author  sarawutt.b 
     * @param   string $id as integer of budget type id [PK] 
     * @since   2017-06-06 17:40:42
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {
        if (!$this->BudgetType->exists($id)) {
            $this->Flash->error(__('Invalid not found budget type with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['BudgetType']['update_uid'] = $this->getCurrenSessionUserId();
            if ($this->BudgetType->save($this->request->data)) {
                $this->Flash->success(__('The budget type has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The budget type could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('BudgetType.' . $this->BudgetType->primaryKey => $id));
            $this->request->data = $this->BudgetType->find('first', $options);
        }
        //$this->saveAccessLog('Edit for the budget type with id = %s', $id);
    }

    /**
     *
     * delete method delete for budget type
     * @author  sarawutt.b 
     * @param   string $id as integer of budget type id [PK] 
     * @since   2017-06-06 17:40:42
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->BudgetType->id = $id;
        if (!$this->BudgetType->exists()) {
            $this->Flash->error(__('Invalid not found budget type with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->BudgetType->delete()) {
                $responds = array('message' => __('The budget type has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The budget type could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->BudgetType->delete()) {
                $this->Flash->success(__('The budget type has been deleted.'));
            } else {
                $this->Flash->error(__('The budget type could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the Budget Type with id = %s', $id);
    }

}
