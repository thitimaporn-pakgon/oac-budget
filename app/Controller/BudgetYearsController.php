<?php

App::uses('AppController', 'Controller');

/**
 * BudgetYears Controller
 *
 * @property BudgetYear $BudgetYear
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BudgetYearsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $conditions = array();
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Search']['name']) || strlen($this->request->data['Search']['name']) > 0) {
                $conditions['AND'][] = array('LOWER(BudgetYear.name) LIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }
            if (!empty($this->request->data['Search']['description']) || strlen($this->request->data['Search']['description']) > 0) {
                $conditions['AND'][] = array('LOWER(BudgetYear.description) LIKE' => '%' . strtolower($this->request->data['Search']['description']) . '%');
            }
            if (!empty($this->request->data['Search']['status'])) {
                $conditions['AND'][] = array('BudgetYear.status' => $this->request->data['Search']['status']);
            }
        } else {
            $conditions = array('BudgetYear.name' => $this->Utility->getCurrenBudgetYearTH());
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('BudgetYear.name' => 'ASC', 'BudgetYear.description' => 'ASC', 'BudgetYear.created' => 'DESC'),
            'limit' => Configure::read('Pagination.Limit')
        );
        $this->set('budgetYears', $this->Paginator->paginate('BudgetYear'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->BudgetYear->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของปีงบประมาณ');
            throw new NotFoundException(__('Invalid budget year'));
        }
        $options = array('conditions' => array('BudgetYear.' . $this->BudgetYear->primaryKey => $id));
        $this->set('budgetYear', $this->BudgetYear->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['BudgetYear']['id'] = $this->request->data['BudgetYear']['name'];
            $this->request->data['BudgetYear']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['BudgetYear']['status'] = 'A';
            $this->BudgetYear->create();
            if ($this->BudgetYear->save($this->request->data)) {
                $this->Session->setFlash(__('The budget year has been saved.'));
                $this->saveAccessLog('บันทึกข้อมูลของปีงบประมาณ');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The budget year could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->BudgetYear->exists($id)) {
            throw new NotFoundException(__('Invalid budget year'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['BudgetYear']['update_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['BudgetYear']['id'] = $this->request->data['BudgetYear']['name'];
            if ($this->BudgetYear->save($this->request->data)) {
                $this->Session->setFlash(__('The budget year has been saved.'));
                $this->saveAccessLog('แก้ไขข้อมูลของปีงบประมาณ');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The budget year could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('BudgetYear.' . $this->BudgetYear->primaryKey => $id));
            $this->request->data = $this->BudgetYear->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for budget year
     * @author  sarawutt.b 
     * @param   string $id as integer of budget year id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->BudgetYear->id = $id;
        if (!$this->BudgetYear->exists()) {
            $this->Flash->error(__('Invalid not found budget year with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->BudgetYear->delete()) {
                $responds = array('message' => __('The budget year has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The budget year could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->BudgetYear->delete()) {
                $this->Flash->success(__('The budget year has been deleted.'));
            } else {
                $this->Flash->error(__('The budget year could not be deleted. Please, try again.'));
            }
        }
        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the budget year with id = %s', $id);
    }

    public function check_already_budget($name, $id = null) {
        $this->autoRender = false;
        $condition = (is_null($id) || empty($id) || !is_numeric($id)) ? array() : array('id !=' => $id);
        echo ($this->BudgetYear->find('count', array('conditions' => array($condition, 'name' => $name))) > 0) ? 'Y' : 'N';
    }

}
