<?php

App::uses('AppController', 'Controller');

class SetLanguagesController extends AppController {

//    public function index($lang , $url_base64){
//        $currentLang = strtolower($lang);
//        $this->Session->write('SessionLanguage', $currentLang);
//        $url = base64_decode($url_base64);
//        $this->redirect($url);
//    }

    public function index($lang, $url_base64 = null) {
        $currentLang = strtolower($lang);
        $this->Session->write('SessionLanguage', $currentLang);
        //$url = base64_decode($url_base64);
        //$this->redirect($url);
        $this->redirect($this->referer());
    }

}

?>
