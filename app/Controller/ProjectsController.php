<?php
App::uses('AppController', 'Controller');
/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 */
class ProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Flash', 'Session', 'Utility', 'PHPMailer', 'RequestHandler');
	public $uses = array('StrPlan', 'Project', 'ProjectTask', 'BudgetYear');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$paginate = array();
		$paginate = array(
        'Project' => array(
            'limit' => Configure::read('Pagination.Limit'),
				)
		);
		$this->paginate = $paginate;
    $this->Project->recursive = -1;
		$this->set('projects', $this->Paginator->paginate('Project'));
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
		$this->set('project', $this->Project->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		if($this->request->is('post')) {
			$post = $this->request->data;
			$datasource = $this->Project->getDataSource();
			$datasource->begin();
			//$isError = false;
			$saveOK = true;

			// 1. INSERT projects
			$years = $this->BudgetYear->find('first', array(
				'conditions' => array(
					'BudgetYear.id' => $post['Project']['budget_year_id'],
					'BudgetYear.status' => 'Y'
				)
			));
			$post['Project']['budget_year'] = $years['BudgetYear']['budget_year'];
			$this->Project->create();
			if($this->Project->save($post['Project']) === false) {
				//$isError = true;
				$saveOK = false;
				break;
			}

			$project_id = $this->Project->id;

			// 2. INSERT project_tasks
			if($saveOK === true){
				foreach($post['ProjectTask'] as $key=>$val) {
					$this->clearEmptyRequestData($val);
	        $this->unformatCurrencyParams($val);
					if( empty($val['task_name']) || empty($val['budget_req1']) )
						continue;

					$this->ProjectTask->create();
					$data['ProjectTask']['project_id'] 	= $project_id;
					$data['ProjectTask']['budget_year'] = $post['Project']['budget_year'];
					$data['ProjectTask']['task_name'] 	= trim($val['task_name']);
					$data['ProjectTask']['task_code'] 	= trim($val['task_code']);
					$data['ProjectTask']['budget_req1'] = $val['budget_req1'];
					$data['ProjectTask']['budget_req2'] = $val['budget_req2'];

					if($this->ProjectTask->save($data['ProjectTask']) === false) {
						//$isError = true;
						$saveOK = false;
						break;
					}
				}
			}

			if($saveOK === false) {
				$datasource->rollback();
				$this->Flash->error(__('The project could not be saved. Please, try again.'));
				$this->redirect($this->referer());
			} else {
				$datasource->commit();
				$this->Flash->success(__('The project has been saved.'));
				$this->redirect('/Projects/index');
			}


			/**************** Tae Code *******************/
			/*$datasource = $this->Project->getDataSource();
			$datasource->begin();
			$isError = false;
			$saveOK = true;

			// 1. INSERT projects
			$years = $this->BudgetYear->find('first', array(
				'conditions' => array(
					'BudgetYear.id' => $post['Project']['budget_year_id'],
					'BudgetYear.status' => 'Y'
				)
			));
			$post['Project']['budget_year'] = $years['BudgetYear']['budget_year'];
			$this->Project->create();
			if($this->Project->save($post['Project']) === false) {
				$isError = true;
				$saveOK = false;
				break;
			}

			$project_id = $this->Project->id;

			// 2. INSERT project_tasks
			if($isError === false){
				foreach($post['ProjectTask'] as $key=>$val) {
					$this->clearEmptyRequestData($val);
	        $this->unformatCurrencyParams($val);
					if( empty($val['task_name']) || empty($val['budget_req1']) )
						continue;

					$this->ProjectTask->create();
					//$data['ProjectTask'] = $val;
					$data['ProjectTask']['project_id'] 	= $project_id;
					$data['ProjectTask']['budget_year'] = $post['Project']['budget_year'];
					$data['ProjectTask']['task_name'] 	= trim($val['task_name']);
					//$data['ProjectTask']['task_code'] 	= trim($val['task_code']);
					$data['ProjectTask']['budget_req1'] = $val['budget_req1'];
					$data['ProjectTask']['budget_req2'] = $val['budget_req2'];

					if($this->ProjectTask->save($data['ProjectTask']) === false) {
						$isError = true;
						$saveOK = false;
						break;
					}
				}
			}


			if($saveOK === false) {
				$datasource->rollback();
				$this->Flash->error(__('The project could not be saved. Please, try again.'));
			} else {
				$datasource->commit();
				$this->Flash->success(__('The project has been saved.'));
			}
			/**************** End Tae Code **********************/
		}

		$empty = __('---- please select ----');
		$strategyGovs = $this->Project->StrategyGov->find('list',array('fields'=>array('StrategyGov.id','StrategyGov.strategy_gov_name')));
		$strategyPlans = $this->Project->StrategyPlan->find('list',array('fields'=>array('StrategyPlan.id','StrategyPlan.strategy_plan_name')));
		$strategyProducts = $this->Project->StrategyProduct->find('list',array('fields'=>array('StrategyProduct.id','StrategyProduct.strategy_product_name')));
		$strategyActivities = $this->Project->StrategyActivity->find('list',array('fields'=>array('StrategyActivity.id','StrategyActivity.strategy_act_name')));
		$strPlans = $this->Project->StrPlan->find('list',array('fields'=>array('StrPlan.id','StrPlan.plan_name')));
		$groupPlans = $this->StrPlan->GroupPlan->find('list',array('fields'=>array('GroupPlan.id','GroupPlan.group_plan_name')));
		$budgetYears = $this->Project->BudgetYear->find('list',array('fields'=>array('BudgetYear.id','BudgetYear.budget_year')));
		$users = $this->Project->User->find('list',array('fields'=>array('User.id','User.username')));
		$this->set(compact('strategyGovs', 'strategyPlans', 'strategyProducts', 'strategyActivities','groupPlans', 'strPlans', 'budgetYears', 'docs', 'users', 'empty'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}

		if ($this->request->is(array('post', 'put'))) {
			/*8if ($this->Project->save($this->request->data)) {
				$this->Flash->success(__('The project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The project could not be saved. Please, try again.'));
			}*/
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
			$this->request->data['Project']['group_plan_id'] = $this->request->data['StrPlan']['group_plan_id'];
		}
		$empty = __('---- please select ----');
		$strategyGovs = $this->Project->StrategyGov->find('list',array('fields'=>array('StrategyGov.id','StrategyGov.strategy_gov_name')));
		$strategyPlans = $this->Project->StrategyPlan->find('list',array('fields'=>array('StrategyPlan.id','StrategyPlan.strategy_plan_name')));
		$strategyProducts = $this->Project->StrategyProduct->find('list',array('fields'=>array('StrategyProduct.id','StrategyProduct.strategy_product_name')));
		$strategyActivities = $this->Project->StrategyActivity->find('list',array('fields'=>array('StrategyActivity.id','StrategyActivity.strategy_act_name')));
		$strPlans = $this->Project->StrPlan->find('list',array('fields'=>array('StrPlan.id','StrPlan.plan_name')));
		$groupPlans = $this->StrPlan->GroupPlan->find('list',array('fields'=>array('GroupPlan.id','GroupPlan.group_plan_name')));
		$budgetYears = $this->Project->BudgetYear->find('list',array('fields'=>array('BudgetYear.id','BudgetYear.budget_year')));
		$users = $this->Project->User->find('list',array('fields'=>array('User.id','User.username')));
		$this->set(compact('strategyGovs', 'strategyPlans', 'strategyProducts', 'strategyActivities','groupPlans', 'strPlans', 'budgetYears', 'docs', 'users', 'empty'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Project->delete()) {
			$this->Flash->success(__('The project has been deleted.'));
		} else {
			$this->Flash->error(__('The project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


}
