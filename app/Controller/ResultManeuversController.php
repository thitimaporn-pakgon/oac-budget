<?php

App::uses('AppController', 'Controller');

/**
 *
 * ResultManeuvers Controller
 * @author  sarawutt.b
 * @property ResultManeuver $ResultManeuver
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-04-18 17:42:05
 * @license Zicure Corp. 
 */
class ResultManeuversController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('StrategyActivity', 'StrategyGov', 'StrategyPlan', 'StrategyProduct', 'ResultManeuver', 'PlanManeuver');

    /**
     * 
     * index method view list for result maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:42:04
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('ResultManeuver.id' => 'ASC', 'ResultManeuver.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trimAllData($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(ResultManeuver.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('ResultManeuver.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(ResultManeuver.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(ResultManeuver.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(ResultManeuver.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(ResultManeuver.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'ResultManeuver' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->ResultManeuver->recursive = 0;
        $resultManeuvers = $this->Paginator->paginate('ResultManeuver');
        $this->set(array('resultManeuvers' => $resultManeuvers, '_serialize' => array('resultManeuvers')));
        //$this->set('resultManeuvers', $this->Paginator->paginate('ResultManeuver'));
        //$this->saveAccessLog('View list for %s', 'result maneuver');
    }

    /**
     *
     * view method view for result maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of result maneuver id [PK] 
     * @since   2017-04-18 17:42:04
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        if (!$this->ResultManeuver->exists($id)) {
            //throw new NotFoundException(__('Invalid result maneuver'));
            $this->Flash->error(__('Invalid not found result maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('ResultManeuver.' . $this->ResultManeuver->primaryKey => $id));
        $resultManeuver = $this->ResultManeuver->find('first', $options);
        $this->set(array('resultManeuver' => $resultManeuver, '_serialize' => array('resultManeuver')));
        //$this->set('resultManeuver', $this->ResultManeuver->find('first', $options));
        //$this->saveAccessLog('View for the result maneuver with id = %s', $id);
    }

    public function add($planID = null) {
        $data = $this->StrategyPlan->find('first', array('conditions' => array('StrategyPlan.id' => $planID)));
        $data_StrategyGov = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $data['StrategyPlan']['strategy_gov_id'])));
        if ($this->request->is('post')) {
            $this->request->data['StrategyProduct']['strategy_gov_id'] = $data_StrategyGov['StrategyGov']['id'];
            $this->request->data['StrategyProduct']['strategy_plan_id'] = $planID;
            $this->request->data['StrategyProduct']['budget_year'] = $data['StrategyPlan']['budget_year_id'];
            $this->request->data['StrategyProduct']['budget_year_id'] = $data['StrategyPlan']['budget_year_id'];

            $checkCode = $this->Common->checkCode('budget.strategy_products', $this->request->data['StrategyProduct']['code'], $data['StrategyPlan']['budget_year_id'], 'StrategyPlan', $data['StrategyPlan']['id']);
            if ($checkCode == false) {
                $this->Flash->error(__('The result maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            $this->StrategyProduct->create();
            if ($this->StrategyProduct->save($this->request->data)) {
                $this->Flash->success(__('The result maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $data_StrategyGov['StrategyGov']['id']));
            } else {
                $this->Flash->error(__('The result maneuver could not be saved. Please, try again.'));
            }
        }

//        $planManeuvers = $this->StrategyPlan->find('list');
//        $systemHasProcesses = $this->StrategyProduct->SystemHasProcess->find('list');
        $year = $data['StrategyPlan']['budget_year'];
        $this->set('data_StrategyGov', $data_StrategyGov);
        $this->set(compact('planManeuvers', 'budgetYears', 'systemHasProcesses'));
        $this->set(compact('year', 'data'));
        //$this->saveAccessLog('Add new the result maneuver');
    }

    /**
     *
     * edit method for result maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of result maneuver id [PK] 
     * @since   2017-04-18 17:42:04
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {

        if (!$this->StrategyProduct->exists($id)) {
            //throw new NotFoundException(__('Invalid result maneuver'));
            $this->Flash->error(__('Invalid not found result maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('StrategyProduct.' . $this->StrategyProduct->primaryKey => $id));
        $data = $this->StrategyProduct->find('first', $options);

        $data_plan = $this->StrategyPlan->find('first', array('conditions' => array('id' => $data['StrategyProduct']['strategy_plan_id'])));

        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['StrategyProduct']['id'] = $id; //$this->getCurrenSessionUserId();
            $checkCode = $this->Common->checkCode('budget.strategy_products', $this->request->data['StrategyProduct']['code'], $data_plan['StrategyPlan']['budget_year_id'], 'StrategyPlan', $data_plan['StrategyPlan']['id']);

            if ($checkCode == false) {
                $this->Flash->error(__('The result maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }

            if ($this->StrategyProduct->save($this->request->data)) {
                $this->Flash->success(__('The result maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $data['StrategyProduct']['strategy_gov_id']));
            } else {
                $this->Flash->error(__('The result maneuver could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $data;
        }
        $data_StrategyGov = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $data_plan['StrategyPlan']['strategy_gov_id'])));

        $year = $data['StrategyProduct']['budget_year'];
        $this->set(compact('year', 'data', 'data_plan'));
        $this->set('data_StrategyGov', $data_StrategyGov);
        $this->set(compact('planManeuvers', 'budgetYears', 'systemHasProcesses', 'data'));
    }

    /**
     *
     * delete method delete for result maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of result maneuver id [PK] 
     * @since   2017-04-18 17:42:04
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->StrategyProduct->id = $id;

        if (!$this->StrategyProduct->exists()) {
            //throw new NotFoundException(__('Invalid result maneuver'));
            $this->Flash->error(__('Invalid not found result maneuver with id %s please try again !', $id));
            return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
        }
        if ($this->ck_tree_Bfdelete($id)) {
            $this->StrategyProduct->delete($id);
            $this->Flash->success(__('The result maneuver has been deleted.'));
        } else {
            $this->Flash->error(__('ไม่สามารถลบข้อมูลได้ เนื่องจากมีข้อมูลส่วนอื่นที่เกี่ยวข้องอยู่ กรุณาลบข้อมูลส่วนที่เกี่ยวข้องก่อน !!!'));
        }
        return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
    }

    ## Check Tree

    private function ck_tree_Bfdelete($id = NULL) {
        $data = $this->StrategyActivity->find('first', array('conditions' => array('strategy_product_id' => $id)));
        if (empty($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
