<?php

App::uses('AppController', 'Controller');

/**
 * Districts Controller
 *
 * @property District $District
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DistrictsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');
    public $uses = array('District', 'Province');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        if (empty($this->passedArgs['District'])) {
            $this->passedArgs['District'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['District'];
        }
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['District']['province_id'])) {
                $conditions['AND'][] = array('lower(District.id) ILIKE' => '%' . strtolower($this->request->data['District']['province_id']) . '%');
            }

            if (!empty($this->request->data['District']['district_name'])) {
                $conditions['AND'][] = array('lower(District.name) ILIKE' => '%' . strtolower($this->request->data['District']['district_name']) . '%');
            }

            if (!empty($this->request->data['District']['status'])) {
                $conditions['AND'][] = array('District.status' => $this->request->data['District']['status']);
            }
        } else {
            $conditions[] = array('District.status' => 'A');
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('District.id' => 'ASC', 'District.name' => 'ASC', 'District.name_eng' => 'ASC', 'District.modified' => 'DESC'),
            'limit' => Configure::read('Pagination.Limit')
        );

        $this->set('districts', $this->Paginator->paginate('District'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->District->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของอำเภอ');
            throw new NotFoundException(__('Invalid district'));
        }
        $options = array('conditions' => array('District.' . $this->District->primaryKey => $id));
        $this->set('district', $this->District->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['District']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['District']['status'] = 'A';
            $this->District->create();
            if ($this->District->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของอำเภอ');
                $this->Session->setFlash(__('The district has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The district could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {

        if (!$this->District->exists($id)) {
            throw new NotFoundException(__('Invalid district'));
        }
        $this->request->data['District']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->District->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของอำเภอ');
                $this->Session->setFlash(__('The district has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The district could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('District.' . $this->District->primaryKey => $id));
            $this->request->data = $this->District->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for district
     * @author  sarawutt.b 
     * @param   string $id as integer of district id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->District->id = $id;
        if (!$this->District->exists()) {
            $this->Flash->error(__('Invalid not found district with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->District->delete()) {
                $responds = array('message' => __('The district has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The district could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->District->delete()) {
                $this->Flash->success(__('The district has been deleted.'));
            } else {
                $this->Flash->error(__('The district could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the district with id = %s', $id);
    }

    public function load_Province($pvID = null) {

        $this->autoRender = false;
        $this->layout = 'ajax';
        if (is_null($pvID) || empty($pvID)) {
            echo '';
            exit;
        }

        $Sub_pvID = substr($pvID, -4, 1);
        $sql = "SELECT  MAX(CAST(SUBSTRING(districts.id FROM 2 FOR 4) AS INT))+1 AS next_id FROM master.districts WHERE districts.id LIKE '" . $pvID . "%'";
        $new_district_id = $this->District->query($sql);

        if ($new_district_id[0][0]['next_id'] == null) {

            $new_district_id[0][0]['next_id'] = $pvID . '01';
            echo $new_district_id[0][0]['next_id'];
        } else {
            echo $Sub_pvID . $new_district_id[0][0]['next_id'];
        }
    }

    public function check_already_district($name, $id = null) {
        $this->autoRender = false;
        $pvID = substr($id, 0, 3);

        $a = $this->District->find('count', array('conditions' => array('id' => $id, 'name' => $name)));
        $b = $this->District->find('count', array('conditions' => array('id like' => '%' . $pvID . '%', 'name' => $name)));

        if ($a == 1 || $b == 0) {
            echo 'N';
        } else {
            echo 'Y';
        }
    }

    public function return_array($id = null) {

        $this->autoRender = false;
        $district = $this->District->find('all', array(
            'fields' => array('id', 'name'),
            'conditions' => array('District.id ILIKE' => '%' . $id . '%'),
        ));

        $result['District'] = $district;
        $this->set('result', $result);
        $this->set('_serialize', array('result'));
    }

}
