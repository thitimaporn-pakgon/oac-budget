<?php

App::uses('AppController', 'Controller');

class StrategicManeuversController extends AppController {

    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('StrategyGov', 'StrategyPlan', 'StrategyProduct', 'StrategyActivity', 'PlanManeuver', 'EventManeuver', 'ResultManeuver', 'WorkGroupManeuver', 'ExpenseDetailManeuver', 'ExpenseHeaderManeuver', 'WorkManeuver', 'ProjectManeuver', 'ProjectListManeuver',
        'StrategicAllocated', 'PlanAllocated', 'ResultAllocated', 'EventAllocated', 'WorkGroupAllocated', 'WorkAllocated', 'ProjectAllocated', 'ProjectListAllocated', 'ExpenseHeaderAllocated', 'BudgetYear');

    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('StrategyGov.id' => 'ASC', 'StrategyGov.created' => 'ASC');

        if (!empty($this->request->data)) {
//            $this->request->data = $this->Utility->trimAllData($this->request->data);
            //Find by budget_year_id
            if (!empty($this->request->data['Search']['budget_year_id'])) {
                $conditions[] = array('StrategyGov.budget_year_id' => $this->request->data['Search']['budget_year_id']);
            }
        } else {
//            $conditions[] = array('StrategyGov.budget_year_id' => $this->Utility->getCurrenBudgetYearTH());
        }
      
        $paginate = array(
            'StrategyGov' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        $this->paginate = $paginate;
        $this->StrategyGov->recursive = 0;
        $strategicManeuvers = $this->Paginator->paginate('StrategyGov');
        $this->set('strategicManeuvers', $strategicManeuvers);
    }

    public function view($id = null) {
        if (!$this->StrategyGov->exists($id)) {
            $this->Flash->error(__('Invalid not found strategic maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('StrategyGov.' . $this->StrategyGov->primaryKey => $id));
        $strategicManeuver = $this->StrategyGov->find('first', $options);

        $data = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $id)));
        foreach ($data AS $k => $v) {
            $button = "<a class='separate-button pull-right project-treeview'  onclick = 'window.location.href = \"/PlanManeuvers/delete/{$v['id']}/Plan/A/{$data['StrategyGov']['id']}\"'><span class='glyphicon glyphicon-trash'>" . __('Delete') . "</span></a>";
            $button .= $this->getLink('/PlanManeuvers/edit', $v['id'], 'edit', 'Edit');
            $button .= $this->getLink('/ResultManeuvers/add', $v['id'], 'add', 'Add Work Product');
            $Child2 = array();
            foreach ($this->StrategyPlan->getDataTree($v['id']) AS $k1 => $v1) {
                $button2 = "<a class='separate-button pull-right project-treeview'  onclick = 'window.location.href = \"/PlanManeuvers/delete/{$v1['StrategyPlan']['id']}/Result/A/{$data['StrategyGov']['id']}\"'><span class='glyphicon glyphicon-trash'>" . __('Delete') . "</span></a>";
                $button2 .= $this->getLink('/PlanManeuvers/edit', $v1['StrategyPlan']['id'], 'edit', 'Edit');
                $button2 .= $this->getLink('/ResultManeuvers/add', $v1['StrategyPlan']['id'], 'add', 'Add Work Product');

                $Child3 = array();

                foreach ($this->StrategyProduct->getDataTree($v1['StrategyPlan']['id']) AS $k2 => $v2) {
                    $button3 = "<a class='separate-button pull-right project-treeview'  onclick = 'window.location.href = \"/ResultManeuvers/delete/{$v2['StrategyProduct']['id']}/Event/A/{$data['StrategyGov']['id']}\"'><span class='glyphicon glyphicon-trash'>" . __('Delete') . "</span></a>";
                    $button3 .= $this->getLink('/ResultManeuvers/edit', $v2['StrategyProduct']['id'], 'edit', 'Edit');
                    $button3 .= $this->getLink('/EventManeuvers/add', $v2['StrategyProduct']['id'], 'add', 'Add Event');
                    $Child4 = array();
                    foreach ($this->StrategyActivity->getDataTree($v2['StrategyProduct']['id']) AS $k3 => $v3) {
                        $button4 = "<a class='separate-button pull-right project-treeview'  onclick = 'window.location.href = \"/EventManeuvers/delete/{$v3['StrategyActivity']['id']}/EventManeuver/A/{$data['StrategyGov']['id']}\"'><span class='glyphicon glyphicon-trash'>" . __('Delete') . "</span></a>";
                        $button4 .= $this->getLink('/EventManeuvers/edit', $v3['StrategyActivity']['id'], 'edit', 'Edit');
                        ## กลุ่มงบประมาณ 
                        ## $button4 .= $this->getLink('/WorkGroupManeuvers/add', $v2['StrategyProduct']['id'], 'add', 'Add Event');

                        $Child4[] = array('text' => $v3['StrategyActivity']['strategy_act_name'] . $button4, 'href' => '#parent');
                    }
                    $Child3[] = array('text' => $v2['StrategyProduct']['strategy_product_name'] . $button3, 'href' => '#parent', 'nodes' => $Child4, 'state' => array('expanded' => true));
                }
                $Child2[] = array('text' => $v1['StrategyPlan']['strategy_plan_name'] . $button2, 'href' => '#parent', 'nodes' => $Child3, 'state' => array('expanded' => true));
            }
            $Child[] = array('text' => $data['StrategyGov']['strategy_gov_name'] . $button, 'href' => '#parent', 'nodes' => $Child2, 'state' => array('expanded' => true));
        }

        $button = "<a class='separate-button pull-right project-treeview'  onclick = 'window.location.href = \"/StrategicManeuvers/deleteGet/{$data['StrategyGov']['id']}/Strategic/A\"'><span class='glyphicon glyphicon-trash'>" . __('Delete') . "</span></a>";
        $button .= $this->getLink('/StrategicManeuvers/edit', $data['StrategyGov']['id'], 'edit');
        $button .= $this->getLink('/PlanManeuvers/add', $data['StrategyGov']['id'], 'add', 'Add Plan');
        $data = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $id)));

        $data2 = json_encode(array('text' => $data['StrategyGov']['strategy_gov_name'] . $button, 'href' => '#parent', 'nodes' => $Child2));

        $this->set(array('StrategyGov' => $strategicManeuver, '_serialize' => array('StrategyGov')));
        $this->set(compact('id', 'data', 'data2'));
        //$this->set('strategicManeuver', $this->StrategicManeuver->find('first', $options));
        //$this->saveAccessLog('View for the strategic maneuver with id = %s', $id);
    }

    public function add() {
        if ($this->request->is('post')) {
            $data['StrategyGov']['budget_year_id'] = $this->request->data['StrategyGov']['budget_year_id'];
            $data['StrategyGov']['budget_year'] = $this->request->data['StrategyGov']['budget_year_id'];
            $data['StrategyGov']['strategy_gov_name'] = $this->request->data['StrategyGov']['strategy_gov_name'];
            $data['StrategyGov']['code'] = $this->request->data['StrategyGov']['code'];
            $checkCode = $this->Common->checkCode('budget.strategy_govs', $this->request->data['StrategyGov']['code'], $this->request->data['StrategyGov']['budget_year_id'], '', null);
            if ($checkCode == false) {
                $this->Flash->error(__('The strategic code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            $this->StrategyGov->create();
            if ($this->StrategyGov->save($data['StrategyGov'])) {
                $this->Flash->success(__('The strategic maneuver has been saved.'));
                return $this->redirect(array('action' => 'view', $this->StrategyGov->id));
            } else {
                $this->Flash->error(__('The strategic maneuver could not be saved. Please, try again.'));
            }
        }
        $this->saveAccessLog('Add new the strategic maneuver');
    }

    public function edit($id = null) {
        if (!$this->StrategyGov->exists($id)) {
            $this->Flash->error(__('Invalid not found strategic maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('StrategyGov.' . $this->StrategyGov->primaryKey => $id));
        $data = $this->StrategyGov->find('first', $options);
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['StrategyGov']['id'] = $id;
            $checkCode = $this->Common->checkCode('budget.strategy_govs', $this->request->data['StrategyGov']['code'], $this->request->data['StrategyGov']['budget_year_id'], '', null);
            if ($checkCode == false) {
                $this->Flash->error(__('The strategic code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }

            if ($this->StrategyGov->save($this->request->data)) {
                $this->Flash->success(__('The strategic maneuver has been saved.'));
                return $this->redirect(array('action' => 'view', $id));
            } else {
                $this->Flash->error(__('The strategic maneuver could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $data;
        }
        $this->set(compact('budgetYears', 'systemHasProcesses'));
        //$this->saveAccessLog('Edit for the strategic maneuver with id = %s', $id);
    }

    public function delete($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->StrategicManeuver->id = $id;
        if (!$this->StrategicManeuver->exists()) {
            //throw new NotFoundException(__('Invalid strategic maneuver'));
            $this->Flash->error(__('Invalid not found strategic maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');
        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->StrategicManeuver->delete()) {
                $responds = array('message' => __('The strategic maneuver has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The strategic maneuver could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->StrategicManeuver->delete()) {
                $this->Flash->success(__('The strategic maneuver has been deleted.'));
            } else {
                $this->Flash->error(__('The strategic maneuver could not be deleted. Please, try again.'));
            }
        }
        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the Strategic Maneuver with id = %s', $id);
    }

    public function deleteGet($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->StrategyGov->id = $id;
        if (!$this->StrategyGov->exists()) {
            //throw new NotFoundException(__('Invalid strategic maneuver'));
            $this->Flash->error(__('Invalid not found strategic maneuver with id %s please try again !', $id));
            return $this->redirect("/StrategicManeuvers/view/{$id}");
        }
        if ($this->ck_tree_Bfdelete($id)) {
            $this->StrategyGov->delete($id);
            $this->Flash->success(__('The strategic maneuver has been deleted.'));
        } else {
            $this->Flash->error(__('ไม่สามารถลบข้อมูลได้ เนื่องจากมีข้อมูลส่วนอื่นที่เกี่ยวข้องอยู่ กรุณาลบข้อมูลส่วนที่เกี่ยวข้องก่อน !!!'));
        }
        return $this->redirect("/StrategicManeuvers/view/{$id}");
    }

    ## Check แผนงาน

    private function ck_tree_Bfdelete($id = NULL) {
        $data = $this->StrategyPlan->find('first', array('conditions' => array('strategy_gov_id' => $id)));
        if (empty($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function getLink($url = '', $id = null, $action = 'add', $title = 'Edit') {
        if ($action == 'add') {
            return "<a class='separate-button pull-right project-treeview' onclick = 'window.location.href = \"{$url}/{$id}\"'><span class='glyphicon glyphicon-plus'>" . __($title) . "</span></a>";
        } else {
            return "<a class='separate-button pull-right project-treeview' onclick = 'window.location.href = \"{$url}/{$id}\"'><span class='glyphicon glyphicon-pencil'>" . __($title) . "</span></a>";
        }
    }

//
//    private function postLink($url = '', $id = null, $action = 'add', $title = 'Edit') {
//        return "";
//    }
//
//    public function getPullDataManeuver() {
//        $this->autoRender = FALSE;
//        if ($this->request->is(array('post'))) {
//            $budgetYear = $this->request->data;
//            if ($budgetYear['Search']['budget_year_id'] <= $budgetYear['Search']['source_budget_year_id']) {
//                $this->Flash->warning(__('The could not be clone data because budget year less than target budget year. Please, try again.'));
//                return $this->redirect($this->referer());
//            }
//            $transOK = true;
//            $this->StrategicManeuver->begin();
//            $this->PlanManeuver->begin();
//            $this->ResultManeuver->begin();
//            $this->EventManeuver->begin();
//            $this->WorkGroupManeuver->begin();
//            $this->WorkManeuver->begin();
//            $this->ProjectManeuver->begin();
//            $this->ProjectListManeuver->begin();
//            $this->ExpenseHeaderManeuver->begin();
//            $this->ExpenseDetailManeuver->begin();
//            $strYear = substr($budgetYear['Search']['source_budget_year_id'], 2, 2);
//            $strategicDeletes = $this->StrategicManeuver->find('all', array('conditions' => array('StrategicManeuver.budget_year_id' => $budgetYear['Search']['budget_year_id'])));
//            if ($this->StrategicManeuver->getStrategicDelete($strategicDeletes, $budgetYear['Search']['budget_year_id']) === FALSE) {
//                $transOK = FALSE;
//            }
//            $expenses = $this->ExpenseHeaderManeuver->find('all', array('conditions' => array('ExpenseHeaderManeuver.digit_budget_year' => $strYear, 'ExpenseHeaderManeuver.status' => 'A')));
//            $dataPush = array();
//            $beforeStrategicId = 0;
//            $beforePlanId = 0;
//            $beforeResultId = 0;
//            $beforeEventId = 0;
//            $beforeWorkGroupId = 0;
//            $beforeWorkId = 0;
//            $beforeProjectId = 0;
//            $beforeProjectListId = 0;
//
//            $strategicId = $planId = $resultId = $eventId = $workGroupId = $workId = $projectId = $projectListId = 0;
//
//            foreach ($expenses as $k => $expense) {
//                if ($beforeStrategicId != $expense['StrategicManeuver']['id']) {
//                    $beforeStrategicId = $expense['StrategicManeuver']['id'];
//                    $dataPush['StrategicManeuver'] = $expense['StrategicManeuver'];
//                    $dataPush['StrategicManeuver']['id'] = null;
//                    $dataPush['StrategicManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['StrategicManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['StrategicManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['StrategicManeuver']['is_active'] = 'N';
//                    $this->StrategicManeuver->create();
//                    if ($this->StrategicManeuver->save($dataPush['StrategicManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $strategicId = $this->StrategicManeuver->id;
//                }
//                if ($beforePlanId != $expense['PlanManeuver']['id']) {
//                    $beforePlanId = $expense['PlanManeuver']['id'];
//                    $dataPush['PlanManeuver'] = $expense['PlanManeuver'];
//                    $dataPush['PlanManeuver']['id'] = null;
//                    $dataPush['PlanManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['PlanManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['PlanManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['PlanManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['PlanManeuver']['is_active'] = 'N';
//                    $this->PlanManeuver->create();
//                    if ($this->PlanManeuver->save($dataPush['PlanManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $planId = $this->PlanManeuver->id;
//                }
//                if ($beforeResultId != $expense['ResultManeuver']['id']) {
//                    $beforeResultId = $expense['ResultManeuver']['id'];
//                    $dataPush['ResultManeuver'] = $expense['ResultManeuver'];
//                    $dataPush['ResultManeuver']['id'] = null;
//                    $dataPush['ResultManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['ResultManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['ResultManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['ResultManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ResultManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ResultManeuver']['is_active'] = 'N';
//                    $this->ResultManeuver->create();
//                    if ($this->ResultManeuver->save($dataPush['ResultManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $resultId = $this->ResultManeuver->id;
//                }
//                if ($beforeEventId != $expense['EventManeuver']['id']) {
//                    $beforeEventId = $expense['EventManeuver']['id'];
//                    $dataPush['EventManeuver'] = $expense['EventManeuver'];
//                    $dataPush['EventManeuver']['id'] = null;
//                    $dataPush['EventManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['EventManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['EventManeuver']['result_maneuver_id'] = $resultId;
//                    $dataPush['EventManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['EventManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['EventManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['EventManeuver']['is_active'] = 'N';
//                    $this->EventManeuver->create();
//                    if ($this->EventManeuver->save($dataPush['EventManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $eventId = $this->EventManeuver->id;
//                }
//                if ($beforeWorkGroupId != $expense['WorkGroupManeuver']['id']) {
//                    $beforeWorkGroupId = $expense['WorkGroupManeuver']['id'];
//                    $dataPush['WorkGroupManeuver'] = $expense['WorkGroupManeuver'];
//                    $dataPush['WorkGroupManeuver']['id'] = null;
//                    $dataPush['WorkGroupManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['WorkGroupManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['WorkGroupManeuver']['result_maneuver_id'] = $resultId;
//                    $dataPush['WorkGroupManeuver']['event_maneuver_id'] = $eventId;
//                    $dataPush['WorkGroupManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['WorkGroupManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['WorkGroupManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['WorkGroupManeuver']['is_active'] = 'N';
//                    $this->WorkGroupManeuver->create();
//                    if ($this->WorkGroupManeuver->save($dataPush['WorkGroupManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $workGroupId = $this->WorkGroupManeuver->id;
//                }
//                if ($beforeWorkId != $expense['WorkManeuver']['id']) {
//                    $beforeWorkId = $expense['WorkManeuver']['id'];
//                    $dataPush['WorkManeuver'] = $expense['WorkManeuver'];
//                    $dataPush['WorkManeuver']['id'] = null;
//                    $dataPush['WorkManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['WorkManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['WorkManeuver']['result_maneuver_id'] = $resultId;
//                    $dataPush['WorkManeuver']['event_maneuver_id'] = $eventId;
//                    $dataPush['WorkManeuver']['work_group_maneuver_id'] = $workGroupId;
//                    $dataPush['WorkManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['WorkManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['WorkManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['WorkManeuver']['is_active'] = 'N';
//                    $this->WorkManeuver->create();
//                    if ($this->WorkManeuver->save($dataPush['WorkManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $workId = $this->WorkManeuver->id;
//                }
//                if ($beforeProjectId != $expense['ProjectManeuver']['id']) {
//                    $beforeProjectId = $expense['ProjectManeuver']['id'];
//                    $dataPush['ProjectManeuver'] = $expense['ProjectManeuver'];
//                    $dataPush['ProjectManeuver']['id'] = null;
//                    $dataPush['ProjectManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['ProjectManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['ProjectManeuver']['result_maneuver_id'] = $resultId;
//                    $dataPush['ProjectManeuver']['event_maneuver_id'] = $eventId;
//                    $dataPush['ProjectManeuver']['work_group_maneuver_id'] = $workGroupId;
//                    $dataPush['ProjectManeuver']['work_maneuver_id'] = $workId;
//                    $dataPush['ProjectManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['ProjectManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ProjectManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ProjectManeuver']['is_active'] = 'N';
//                    $this->ProjectManeuver->create();
//                    if ($this->ProjectManeuver->save($dataPush['ProjectManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $projectId = $this->ProjectManeuver->id;
//                }
//                if ($beforeProjectListId != $expense['ProjectListManeuver']['id']) {
//                    $beforeProjectListId = $expense['ProjectListManeuver']['id'];
//                    $dataPush['ProjectListManeuver'] = $expense['ProjectListManeuver'];
//                    $dataPush['ProjectListManeuver']['id'] = null;
//                    $dataPush['ProjectListManeuver']['strategic_maneuver_id'] = $strategicId;
//                    $dataPush['ProjectListManeuver']['plan_maneuver_id'] = $planId;
//                    $dataPush['ProjectListManeuver']['result_maneuver_id'] = $resultId;
//                    $dataPush['ProjectListManeuver']['event_maneuver_id'] = $eventId;
//                    $dataPush['ProjectListManeuver']['work_group_maneuver_id'] = $workGroupId;
//                    $dataPush['ProjectListManeuver']['work_maneuver_id'] = $workId;
//                    $dataPush['ProjectListManeuver']['project_maneuver_id'] = $projectId;
//                    $dataPush['ProjectListManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataPush['ProjectListManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ProjectListManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $dataPush['ProjectListManeuver']['is_active'] = 'N';
//                    $this->ProjectListManeuver->create();
//                    if ($this->ProjectListManeuver->save($dataPush['ProjectListManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    $projectListId = $this->ProjectListManeuver->id;
//                }
//                $dataPush['ExpenseHeaderManeuver'] = $expense['ExpenseHeaderManeuver'];
//                $dataPush['ExpenseHeaderManeuver']['id'] = null;
//                $dataPush['ExpenseHeaderManeuver']['strategic_maneuver_id'] = $strategicId;
//                $dataPush['ExpenseHeaderManeuver']['plan_maneuver_id'] = $planId;
//                $dataPush['ExpenseHeaderManeuver']['result_maneuver_id'] = $resultId;
//                $dataPush['ExpenseHeaderManeuver']['event_maneuver_id'] = $eventId;
//                $dataPush['ExpenseHeaderManeuver']['work_group_maneuver_id'] = $workGroupId;
//                $dataPush['ExpenseHeaderManeuver']['work_maneuver_id'] = $workId;
//                $dataPush['ExpenseHeaderManeuver']['project_maneuver_id'] = $projectId;
//                $dataPush['ExpenseHeaderManeuver']['project_list_maneuver_id'] = $projectListId;
//                $dataPush['ExpenseHeaderManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataPush['ExpenseHeaderManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataPush['ExpenseHeaderManeuver']['digit_budget_year'] = $this->BudgetYear->conversBudgetYear($budgetYear['Search']['budget_year_id']);
//                $dataPush['ExpenseHeaderManeuver']['ref_ducument_no'] = null;
//                $dataPush['ExpenseHeaderManeuver']['date_approve'] = null;
//                $dataPush['ExpenseHeaderManeuver']['is_active'] = 'N';
//                $this->ExpenseHeaderManeuver->create();
//                if ($this->ExpenseHeaderManeuver->save($dataPush['ExpenseHeaderManeuver']) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $expenseId = $this->ExpenseHeaderManeuver->id;
//                foreach ($expense['ExpenseDetailManeuver'] AS $kk => $vv) {
//                    $dataDetail = array();
//                    $dataDetail['ExpenseDetailManeuver'] = $vv;
//                    $dataDetail['ExpenseDetailManeuver']['id'] = null;
//                    $dataDetail['ExpenseDetailManeuver']['expense_header_maneuver_id'] = $expenseId;
//                    $dataDetail['ExpenseDetailManeuver']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                    $dataDetail['ExpenseDetailManeuver']['create_uid'] = $this->getCurrenSessionUserId();
//                    $dataDetail['ExpenseDetailManeuver']['update_uid'] = $this->getCurrenSessionUserId();
//                    $this->ExpenseDetailManeuver->create();
//                    if ($this->ExpenseDetailManeuver->save($dataDetail['ExpenseDetailManeuver']) === FALSE) {
//                        $transOK = FALSE;
//                    }
//                    unset($dataDetail);
//                }
//                unset($dataPush);
//            }
//            if ($transOK === true) {
//                $this->StrategicManeuver->commit();
//                $this->PlanManeuver->commit();
//                $this->ResultManeuver->commit();
//                $this->EventManeuver->commit();
//                $this->WorkGroupManeuver->commit();
//                $this->WorkManeuver->commit();
//                $this->ProjectManeuver->commit();
//                $this->ProjectListManeuver->commit();
//                $this->ExpenseHeaderManeuver->commit();
//                $this->ExpenseDetailManeuver->commit();
//                $this->Session->setFlash(__('The has been clone data maneuver.'));
//                return $this->redirect(array('action' => "index"));
//            } else {
//                $this->StrategicManeuver->rollback();
//                $this->PlanManeuver->rollback();
//                $this->ResultManeuver->rollback();
//                $this->EventManeuver->rollback();
//                $this->WorkGroupManeuver->rollback();
//                $this->WorkManeuver->rollback();
//                $this->ProjectManeuver->rollback();
//                $this->ProjectListManeuver->rollback();
//                $this->ExpenseHeaderManeuver->rollback();
//                $this->ExpenseDetailManeuver->rollback();
//                $this->Session->setFlash(__('The could not be clone data maneuver. Please, try again.'));
//                return $this->redirect($this->referer());
//            }
//        }
//    }
//
//    public function getPullDataAllocate() {
//        $this->autoRender = FALSE;
//        if ($this->request->is(array('post'))) {
//            $budgetYear = $this->request->data;
//            if ($budgetYear['Search']['budget_year_id'] < $budgetYear['Search']['source_budget_year_id']) {
//                $this->Flash->warning(__('The could not be clone data because budget year less than target budget year. Please, try again.'));
//                return $this->redirect($this->referer());
//            }
//            $transOK = true;
//            $this->StrategicAllocated->begin();
//            $this->PlanAllocated->begin();
//            $this->ResultAllocated->begin();
//            $this->EventAllocated->begin();
//            $this->WorkGroupAllocated->begin();
//            $this->WorkAllocated->begin();
//            $this->ProjectAllocated->begin();
//            $this->ProjectListAllocated->begin();
//            $this->ExpenseHeaderAllocated->begin();
//            $strategicDeletes = $this->StrategicAllocated->find('all', array('conditions' => array('StrategicAllocated.budget_year_id' => $budgetYear['Search']['budget_year_id'])));
//            if ($this->StrategicAllocated->getStrategicDelete($strategicDeletes, $budgetYear['Search']['budget_year_id']) === FALSE) {
//                $transOK = FALSE;
//            }
//            $strategics = $this->StrategicAllocated->find('all', array('conditions' => array('StrategicAllocated.budget_year_id' => $budgetYear['Search']['source_budget_year_id'])));
//            foreach ($strategics as $k => $strategic) {
//                $dataStrategic = array();
//                $dataStrategic['StrategicAllocated']['code'] = $strategic['StrategicAllocated']['code'];
//                $dataStrategic['StrategicAllocated']['name'] = $strategic['StrategicAllocated']['name'];
//                $dataStrategic['StrategicAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataStrategic['StrategicAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataStrategic['StrategicAllocated']['to_department_id'] = $strategic['StrategicAllocated']['to_department_id'];
//                $dataStrategic['StrategicAllocated']['system_has_process_id'] = $strategic['StrategicAllocated']['system_has_process_id'];
//                $dataStrategic['StrategicAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataStrategic['StrategicAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataStrategic['StrategicAllocated']['is_active'] = 'N';
//                $this->StrategicAllocated->create();
//                if ($this->StrategicAllocated->save($dataStrategic) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $strategicId = $this->StrategicAllocated->id;
//                $dataPlan = array();
//                $dataPlan['PlanAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataPlan['PlanAllocated']['code'] = $strategic['PlanAllocated'][$k]['code'];
//                $dataPlan['PlanAllocated']['name'] = $strategic['PlanAllocated'][$k]['name'];
//                $dataPlan['PlanAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataPlan['PlanAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataPlan['PlanAllocated']['to_department_id'] = $strategic['PlanAllocated'][$k]['to_department_id'];
//                $dataPlan['PlanAllocated']['system_has_process_id'] = $strategic['PlanAllocated'][$k]['system_has_process_id'];
//                $dataPlan['PlanAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataPlan['PlanAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataPlan['PlanAllocated']['is_active'] = 'N';
//                $this->PlanAllocated->create();
//                if ($this->PlanAllocated->save($dataPlan) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $planId = $this->PlanAllocated->id;
//                $dataResult = array();
//                $dataResult['ResultAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataResult['ResultAllocated']['plan_allocated_id'] = $planId;
//                $dataResult['ResultAllocated']['code'] = $strategic['ResultAllocated'][$k]['code'];
//                $dataResult['ResultAllocated']['name'] = $strategic['ResultAllocated'][$k]['name'];
//                $dataResult['ResultAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataResult['ResultAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataResult['ResultAllocated']['to_department_id'] = $strategic['ResultAllocated'][$k]['to_department_id'];
//                $dataResult['ResultAllocated']['system_has_process_id'] = $strategic['ResultAllocated'][$k]['system_has_process_id'];
//                $dataResult['ResultAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataResult['ResultAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataResult['ResultAllocated']['is_active'] = 'N';
//                $this->ResultAllocated->create();
//                if ($this->ResultAllocated->save($dataResult) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $resultId = $this->ResultAllocated->id;
//                $dataEvent = array();
//                $dataEvent['EventAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataEvent['EventAllocated']['plan_allocated_id'] = $planId;
//                $dataEvent['EventAllocated']['result_allocated_id'] = $resultId;
//                $dataEvent['EventAllocated']['code'] = $strategic['EventAllocated'][$k]['code'];
//                $dataEvent['EventAllocated']['name'] = $strategic['EventAllocated'][$k]['name'];
//                $dataEvent['EventAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataEvent['EventAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataEvent['EventAllocated']['to_department_id'] = $strategic['EventAllocated'][$k]['to_department_id'];
//                $dataEvent['EventAllocated']['system_has_process_id'] = $strategic['EventAllocated'][$k]['system_has_process_id'];
//                $dataEvent['EventAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataEvent['EventAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataEvent['EventAllocated']['is_active'] = 'N';
//                $this->EventAllocated->create();
//                if ($this->EventAllocated->save($dataEvent) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $eventId = $this->EventAllocated->id;
//                $dataWorkGroup = array();
//                $dataWorkGroup['WorkGroupAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataWorkGroup['WorkGroupAllocated']['plan_allocated_id'] = $planId;
//                $dataWorkGroup['WorkGroupAllocated']['result_allocated_id'] = $resultId;
//                $dataWorkGroup['WorkGroupAllocated']['event_allocated_id'] = $eventId;
//                $dataWorkGroup['WorkGroupAllocated']['code'] = $strategic['WorkGroupAllocated'][$k]['code'];
//                $dataWorkGroup['WorkGroupAllocated']['name'] = $strategic['WorkGroupAllocated'][$k]['name'];
//                $dataWorkGroup['WorkGroupAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataWorkGroup['WorkGroupAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataWorkGroup['WorkGroupAllocated']['to_department_id'] = $strategic['WorkGroupAllocated'][$k]['to_department_id'];
//                $dataWorkGroup['WorkGroupAllocated']['system_has_process_id'] = $strategic['WorkGroupAllocated'][$k]['system_has_process_id'];
//                $dataWorkGroup['WorkGroupAllocated']['budget_scope'] = $strategic['WorkGroupAllocated'][$k]['budget_scope'];
//                $dataWorkGroup['WorkGroupAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataWorkGroup['WorkGroupAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataWorkGroup['WorkGroupAllocated']['is_active'] = 'N';
//                $this->WorkGroupAllocated->create();
//                if ($this->WorkGroupAllocated->save($dataWorkGroup) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $workGroupId = $this->WorkGroupAllocated->id;
//                $dataWork = array();
//                $dataWork['WorkAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataWork['WorkAllocated']['plan_allocated_id'] = $planId;
//                $dataWork['WorkAllocated']['result_allocated_id'] = $resultId;
//                $dataWork['WorkAllocated']['event_allocated_id'] = $eventId;
//                $dataWork['WorkAllocated']['work_group_allocated_id'] = $workGroupId;
//                $dataWork['WorkAllocated']['code'] = $strategic['WorkAllocated'][$k]['code'];
//                $dataWork['WorkAllocated']['name'] = $strategic['WorkAllocated'][$k]['name'];
//                $dataWork['WorkAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataWork['WorkAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataWork['WorkAllocated']['to_department_id'] = $strategic['WorkAllocated'][$k]['to_department_id'];
//                $dataWork['WorkAllocated']['system_has_process_id'] = $strategic['WorkAllocated'][$k]['system_has_process_id'];
//                $dataWork['WorkAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataWork['WorkAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataWork['WorkAllocated']['is_active'] = 'N';
//                $this->WorkAllocated->create();
//                if ($this->WorkAllocated->save($dataWork) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $workId = $this->WorkAllocated->id;
//                $dataProject = array();
//                $dataProject['ProjectAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataProject['ProjectAllocated']['plan_allocated_id'] = $planId;
//                $dataProject['ProjectAllocated']['result_allocated_id'] = $resultId;
//                $dataProject['ProjectAllocated']['event_allocated_id'] = $eventId;
//                $dataProject['ProjectAllocated']['work_group_allocated_id'] = $workGroupId;
//                $dataProject['ProjectAllocated']['work_allocated_id'] = $workId;
//                $dataProject['ProjectAllocated']['code'] = $strategic['ProjectAllocated'][$k]['code'];
//                $dataProject['ProjectAllocated']['name'] = $strategic['ProjectAllocated'][$k]['name'];
//                $dataProject['ProjectAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataProject['ProjectAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataProject['ProjectAllocated']['to_department_id'] = $strategic['ProjectAllocated'][$k]['to_department_id'];
//                $dataProject['ProjectAllocated']['system_has_process_id'] = $strategic['ProjectAllocated'][$k]['system_has_process_id'];
//                $dataProject['ProjectAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataProject['ProjectAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataProject['ProjectAllocated']['is_active'] = 'N';
//                $this->ProjectAllocated->create();
//                if ($this->ProjectAllocated->save($dataProject) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $projectId = $this->ProjectAllocated->id;
//                $dataProjectList = array();
//                $dataProjectList['ProjectListAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataProjectList['ProjectListAllocated']['plan_allocated_id'] = $planId;
//                $dataProjectList['ProjectListAllocated']['result_allocated_id'] = $resultId;
//                $dataProjectList['ProjectListAllocated']['event_allocated_id'] = $eventId;
//                $dataProjectList['ProjectListAllocated']['work_group_allocated_id'] = $workGroupId;
//                $dataProjectList['ProjectListAllocated']['work_allocated_id'] = $workId;
//                $dataProjectList['ProjectListAllocated']['project_allocated_id'] = $projectId;
//                $dataProjectList['ProjectListAllocated']['code'] = $strategic['ProjectListAllocated'][$k]['code'];
//                $dataProjectList['ProjectListAllocated']['name'] = $strategic['ProjectListAllocated'][$k]['name'];
//                $dataProjectList['ProjectListAllocated']['budget_year_id'] = $budgetYear['Search']['budget_year_id'];
//                $dataProjectList['ProjectListAllocated']['from_department_id'] = $this->getCurrenSessionDepartmentId();
//                $dataProjectList['ProjectListAllocated']['system_has_process_id'] = $strategic['ProjectListAllocated'][$k]['system_has_process_id'];
//                $dataProjectList['ProjectListAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataProjectList['ProjectListAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataProjectList['ProjectListAllocated']['is_active'] = 'N';
//                $this->ProjectListAllocated->create();
//                if ($this->ProjectListAllocated->save($dataProjectList) === FALSE) {
//                    $transOK = FALSE;
//                }
//                $projectListId = $this->ProjectListAllocated->id;
//                $dataExpenseHeader = array();
//                $dataExpenseHeader['ExpenseHeaderAllocated']['strategic_allocated_id'] = $strategicId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['plan_allocated_id'] = $planId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['result_allocated_id'] = $resultId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['event_allocated_id'] = $eventId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['work_group_allocated_id'] = $workGroupId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['work_allocated_id'] = $workId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['project_allocated_id'] = $projectId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['project_list_allocated_id'] = $projectListId;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['to_department_id'] = $strategic['ExpenseHeaderAllocated'][$k]['to_department_id'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['system_has_process_id'] = $strategic['ExpenseHeaderAllocated'][$k]['system_has_process_id'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['budget_scope'] = $strategic['ExpenseHeaderAllocated'][$k]['budget_scope'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['objective'] = $strategic['ExpenseHeaderAllocated'][$k]['objective'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi1'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi1'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi1_result'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi1_result'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi2'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi2'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi2_result'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi2_result'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi3'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi3'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi3_result'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi3_result'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi4'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi4'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['kpi4_result'] = $strategic['ExpenseHeaderAllocated'][$k]['kpi4_result'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['create_uid'] = $this->getCurrenSessionUserId();
//                $dataExpenseHeader['ExpenseHeaderAllocated']['update_uid'] = $this->getCurrenSessionUserId();
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_budget_year'] = $this->BudgetYear->conversBudgetYear($budgetYear['Search']['budget_year_id']);
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_strategic'] = $strategic['StrategicAllocated']['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_plan'] = $strategic['PlanAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_result'] = $strategic['ResultAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_event'] = $strategic['EventAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_work_group'] = $strategic['WorkGroupAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_work'] = $strategic['WorkAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_project'] = $strategic['ProjectAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_project_list'] = $strategic['ProjectListAllocated'][$k]['code'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['digit_owner_dept'] = $strategic['ExpenseHeaderAllocated'][$k]['digit_owner_dept'];
//                $dataExpenseHeader['ExpenseHeaderAllocated']['ref_ducument_no'] = null;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['date_approve'] = null;
//                $dataExpenseHeader['ExpenseHeaderAllocated']['is_active'] = 'N';
//                $this->ExpenseHeaderAllocated->create();
//                if ($this->ExpenseHeaderAllocated->save($dataExpenseHeader) === FALSE) {
//                    $transOK = FALSE;
//                }
//            }
//            if ($transOK === true) {
//                $this->StrategicAllocated->commit();
//                $this->PlanAllocated->commit();
//                $this->ResultAllocated->commit();
//                $this->EventAllocated->commit();
//                $this->WorkGroupAllocated->commit();
//                $this->WorkAllocated->commit();
//                $this->ProjectAllocated->commit();
//                $this->ProjectListAllocated->commit();
//                $this->ExpenseHeaderAllocated->commit();
//                $this->Session->setFlash(__('The has been saved.'));
//                return $this->redirect(array('action' => "index"));
//            } else {
//                $this->StrategicAllocated->rollback();
//                $this->PlanAllocated->rollback();
//                $this->ResultAllocated->rollback();
//                $this->EventAllocated->rollback();
//                $this->WorkGroupAllocated->rollback();
//                $this->WorkAllocated->rollback();
//                $this->ProjectAllocated->rollback();
//                $this->ProjectListAllocated->rollback();
//                $this->ExpenseHeaderAllocated->rollback();
//                $this->Session->setFlash(__('The could not be saved. Please, try again.'));
//                return $this->redirect($this->referer());
//            }
//        }
//    }
}
