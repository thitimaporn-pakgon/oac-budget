<?php

App::uses('AppController', 'Controller');

/**
 * Departments Controller
 *
 * @property Department $Department
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DepartmentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');
    public $uses = array('User', 'Department', 'DepartmentLevel');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $conditions = array();
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['Search']['dept_no'])) {
                $conditions['AND'][] = array('LOWER(Department.dept_no) ILIKE' => '%' . strtolower($this->request->data['Search']['dept_no']) . '%');
            }
            if (!empty($this->request->data['Search']['name'])) {
                $conditions['OR'][] = array('LOWER(Department.short_name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
                $conditions['OR'][] = array('LOWER(Department.full_name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }
            if (!empty($this->request->data['Search']['department_level_id'])) {
                $conditions['AND'][] = array('LOWER(CAST(Department.department_level_id AS VARCHAR)) ILIKE' => '%' . strtolower($this->request->data['Search']['department_level_id']) . '%');
            }
            if (!empty($this->request->data['Search']['status'])) {
                $conditions['AND'][] = array('Department.status' => $this->request->data['Search']['status']);
            }
        } else {
            $conditions[] = array('Department.status' => 'A');
        }
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('Department.dept_no' => 'ASC'),
            'limit' => Configure::read('Pagination.Limit')
        );

        $this->set('departments', $this->Paginator->paginate('Department'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Department->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของหน่วยงาน');
            throw new NotFoundException(__('Invalid department'));
        }
        $options = array('conditions' => array('Department.' . $this->Department->primaryKey => $id));
        $this->set('department', $this->Department->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->set('departmentList', $this->Department->findListDepartment());
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Department']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['Department']['status'] = 'A';
            $this->Department->create();
            if ($this->Department->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของหน่วยงาน');
                $this->Session->setFlash(__('The department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The department could not be saved. Please, try again.'));
            }
        }
        $departmentLevels = $this->Department->DepartmentLevel->find('list');
//		$departmentCovers = $this->Department->DepartmentCover->find('list');
        $this->set(compact('departmentLevels', 'departmentCovers'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Department->exists($id)) {
            throw new NotFoundException(__('Invalid department'));
        }
        $this->request->data['Department']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Department->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของหน่วยงาน');
                $this->Session->setFlash(__('The department has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The department could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Department.' . $this->Department->primaryKey => $id));
            $this->request->data = $this->Department->find('first', $options);
        }
        $this->set(compact('departmentLevels'));
    }

    /**
     *
     * delete method delete for department
     * @author  sarawutt.b 
     * @param   string $id as integer of department id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->Department->id = $id;
        if (!$this->Department->exists()) {
            $this->Flash->error(__('Invalid not found department with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->Department->delete()) {
                $responds = array('message' => __('The department has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The department could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->Department->delete()) {
                $this->Flash->success(__('The department has been deleted.'));
            } else {
                $this->Flash->error(__('The department could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the department with id = %s', $id);
    }

    /**
     * 
     * Function check for unique input with same name and same department must to differance the department level
     * @author  Wongsakorn.a
     * @param   $dept_no as string of department no
     * @param   $short_name as string of the department short name
     * @param   $full_name as string of the department full name
     * @param   $departmentLevel
     * @param   $id as integer of department id [PK]
     * @return  character Y if not unique N permit can be save
     */
    public function check_already_department($dept_no, $short_name, $full_name, $departmentLevel, $id = null) {
        $this->autoRender = false;
        echo ($this->Department->find('count', array('conditions' => array('AND' => array('Department.id !=' => $id, array('OR' => array('dept_no' => $dept_no, 'short_name' => $short_name, 'full_name' => $full_name)), 'department_level_id' => $departmentLevel)))) > 0) ? 'Y' : 'N';
    }

}
