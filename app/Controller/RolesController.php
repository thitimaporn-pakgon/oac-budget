<?php

App::uses('AppController', 'Controller');

/**
 * Roles Controller
 *
 * @property Role $Role
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RolesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Roles';
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('Role');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->data;
        }
        if (empty($this->data)) {
            $this->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('Role.name' => 'ASC', 'Role.name_eng' => 'ASC', 'Role.priority' => 'ASC');
        if (!empty($this->data)) {
            $this->data = $this->Utility->trim_all_data($this->data);
            //Find by name
            if (!empty($this->data['Search']['name'])) {
                $conditions['OR'][] = array('LOWER(Role.name) LIKE' => '%' . strtolower($this->data['Search']['name']) . '%');
                $conditions['OR'][] = array('LOWER(Role.name_eng) LIKE' => '%' . strtolower($this->data['Search']['name']) . '%');
            }
            //Find by detail
            if (!empty($this->data['Search']['description'])) {
                $conditions[] = array('LOWER(Role.description) ILIKE ' => '%' . strtolower($this->data['Search']['description']) . '%');
            }
            //Find by status
            if (!empty($this->data['Search']['status'])) {
                $conditions[] = array('Role.status' => $this->data['Search']['status']);
            }
        }
        $paginate = array(
            'Role' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        $this->paginate = $paginate;
        $this->Role->recursive = 0;
        $this->set('roles', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__('Invalid role'));
        }
        $options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
        $this->set('role', $this->Role->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->request->data['Role']['status'] = 'A';
            $this->request->data['Role']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->Role->create();
            if ($this->Role->save($this->request->data)) {
                $this->Session->setFlash(__('The role has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The role could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__('Invalid role'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Role']['update_uid'] = $this->Session->read('Auth.User.id');
            if ($this->Role->save($this->request->data)) {
                $this->Session->setFlash(__('The role has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The role could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
            $this->request->data = $this->Role->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for role
     * @author  sarawutt.b 
     * @param   string $id as integer of role id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->Role->id = $id;
        if (!$this->Role->exists()) {
            $this->Flash->error(__('Invalid not found role with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->Role->delete()) {
                $responds = array('message' => __('The role has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The role could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->Role->delete()) {
                $this->Flash->success(__('The role has been deleted.'));
            } else {
                $this->Flash->error(__('The role could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the role with id = %s', $id);
    }

    /**
     * 
     * Check Role may be already existing on the table when add or edit
     * @author sarawutt.b
     * @param type $nameTh as string of name in thai
     * @param type $nameEng as string of name in english
     * @param type $id as integer role id in case update
     * @return character of Y if already exist otherwise N
     */
    public function check_already_roles($nameTh = null, $nameEng = null, $id = null) {
        $this->autoRender = false;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id <> ' => $id);
        echo ($this->Role->find('count', array('conditions' => array($conditions, 'OR' => array('name' => $nameTh, 'name_eng' => $nameEng)))) > 0) ? 'Y' : 'N';
    }

}
