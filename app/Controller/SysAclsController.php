<?php

App::uses('AppController', 'Controller');

class SysAclsController extends AppController {

    var $name = 'SysAcls';
    var $uses = array('SysAcl', 'SysController', 'SysAction', 'User', 'Menu', 'Role');
    var $helpers = array('Session', 'Paginator');
    var $components = array('Utility', 'Session');

    //OK Checked
    //Add Controller with actions together
    //Not to OK
    //Since 20131031
    public function index() {
        $paginate = array();
        $order = array('SysAcl.created' => 'ASC');
        $conditions = array();

        if ($this->request->is(array('put', 'post'))) {
            $this->data = $this->Utility->trim_all_data($this->data);
            if (!empty($this->data['Search']['sys_controller_id'])) {
                $conditions[] = array('SysAcl.sys_controller_id' => $this->data['Search']['sys_controller_id']);
                $sysActions = $this->SysAction->find('list', array('order' => array("SysAction.name ASC"), 'conditions' => array('SysAction.sys_controller_id' => $this->data['Search']['sys_controller_id'])));
                $this->set('sysActions', $sysActions);
            }
            if (!empty($this->data['Search']['sys_action_id'])) {
                $conditions[] = array('SysAcl.sys_action_id' => $this->data['Search']['sys_action_id']);
            }
            if (!empty($this->data['Search']['user_id'])) {
                $conditions[] = array('SysAcl.user_id' => $this->data['Search']['user_id']);
            }
            if (!empty($this->data['Search']['stakeholder_id'])) {
                $conditions[] = array('SysAcl.stakeholder_id' => $this->data['Search']['stakeholder_id']);
                $users = $this->User->find('list', array('order' => array("User.display ASC"), 'conditions' => array('User.stakeholder_id' => $this->data['Search']['stakeholder_id'])));
                $this->set("users", $users);
            }
            if (!empty($this->data['Search']['status'])) {
                $conditions[] = array('SysAcl.status' => $this->data['Search']['status']);
            }
        }


        $paginate = array(
            'SysAcl' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));


        $this->paginate = $paginate;
        //$this->SysAcl->recursive = -1;
        //pr($this->paginate('SysAcl'));exit;
        $this->set('sysAcls', $this->paginate('SysAcl'));

        $roles = $this->getEmptySelect() + $this->Role->find('list');
        $sysControllers = $this->getEmptySelect() + $this->SysController->find('list');
        //$sysActions = $this->getEmptySelect() + $this->SysAction->find('list');
        $sysActions = $this->getEmptySelect();
        $users = $this->User->findListUser();
        $statuses = $this->getEmptySelect() + $this->SysAcl->getSysStatus();

        $this->set(compact('sysControllers', 'roles', 'statuses', 'users', 'menus', 'sysActions'));

        $this->set('title_for_layout', __('sysACLControl'));
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function view($id = null) {
        if (!$this->SysAcl->exists($id)) {
            throw new NotFoundException(__('Invalid sys acl'));
        }
        $options = array('conditions' => array('SysAcl.' . $this->SysAcl->primaryKey => $id));
        $this->set('sysAcl', $this->SysAcl->find('first', $options));
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function add() {
        $this->set('roleInfos', $this->Role->find('all', array("order" => "name")));
        $this->set('title_for_layout', 'sysACLControl');

        $this->set('users', $this->User->find('all', array("order" => "username")));
        $this->set('title_for_layout', 'sysACLControl');
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function add_permission($role_id = null) {
        if (!empty($this->data)) {
            $data = $this->data;
//            pr($data);exit;
            $affectedRows = 0;
            foreach ($data['SysAcl']['caction'] as $k => $v) {
                if (empty($v)) {
                    continue;
                }
                $control_action = explode('|', $v);

                //If exiting acl mapped don't save dupplicated
                $sql = "SELECT COUNT(*) FROM system.sys_acls WHERE sys_controller_id={$control_action[0]} AND sys_action_id={$control_action[1]} AND role_id={$data['SysAcl']['role_id']};";
                $count_existing = $this->SysAcl->query($sql);
                if ($count_existing[0][0]['count'] > 0) {
                    continue;
                }

                $data['SysAcl'] = array(
                    'sys_controller_id' => $control_action[0],
                    'sys_action_id' => $control_action[1],
                    'role_id' => $data['SysAcl']['role_id'],
                    'status' => 'A',
                    'create_uid' => $this->getCurrenSessionUserId()
                );
                $this->SysAcl->create();
                if ($this->SysAcl->save($data)) {
                    $affectedRows++;
                }
            }

            if ($affectedRows > 0) {
                $this->Session->setFlash(__('The Role\'s ACL has been Added.'));
                $this->redirect("/SysAcls/add_permission/{$role_id}");
            } else {
                $this->Session->setFlash(__('The SysAcl could not be save. Please, try again.'));
            }
        }//end of submit data for save
        //Complete Package render for package choossing
        $tmp_complete = $this->SysAcl->find('all', array('conditions' => array('SysAcl.role_id' => $role_id, 'SysAcl.status' => 'A')));

        //pr($tmp_complete);exit;
        $completed_controlpackages = array('' => __('All'));
        $complete_map_permission = array();
        foreach ($tmp_complete as $kcom => $vcom) {
            $com_controll_val = $vcom['SysController']['id'];
            $com_controll_name = $vcom['SysController']['name'];

            if (is_null($vcom['SysAction']['id']) || empty($vcom['SysAction']['id'])) {
                continue;
            }
            $com_action_val = $vcom['SysAction']['id'];
            $com_action_name = $vcom['SysAction']['name'];

            $_com_val = $com_controll_val . '/' . $com_action_val;
            $_com_name = $com_controll_name . '/' . $com_action_name;

            $completed_controlpackages[$_com_val] = __($_com_name);
            $complete_map_permission[] = $com_action_val;
        }

        //All package fill all to mullti chossing on LEFT side page
        $controllers = $this->SysAction->find('all', array(
            'conditions' => array('SysAction.id !=' => $complete_map_permission, 'SysController.status' => 'A', 'SysAction.status' => 'A'),
            'fields' => array('SysAction.id', 'SysAction.name', 'SysController.id', 'SysController.name'),
        ));


        $available_menus = $this->SysController->find('list', array('fields' => array('id', 'name')));
        $controllers_packages = array('' => __('All'));
        $control = "";

        foreach ($controllers as $k => $v) {
            $control_val = $v['SysController']['id'];
            $control_name = $v['SysController']['name'];
            $action_value = $v['SysAction']['id'];
            $action_name = $v['SysAction']['name'];

            $_val = $control_val . '|' . $action_value;
            $_name = $control_name . '/' . $action_name;
            $controllers_packages[$_val] = __($_name);
        }

        //if not has new permission mus to map remove choossing all together
        if (count($controllers_packages) <= 1) {
            $controllers_packages = array();
        }

        $this->set('controllers_packages', $controllers_packages);
        $this->set('completed_controlpackages', $completed_controlpackages);
        $this->set('available_menus', $available_menus);
        //$this->set('user_id', $user_id);
        $this->set('role_id', $role_id);
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function edit($id = null) {
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->data)) {
                $data = $this->data;
                $data['SysAcl']['update_uid'] = $this->getCurrenSessionUserId();
                if ($this->SysAcl->save($data)) {
                    $this->Session->setFlash(__('The SysAcl has been save.'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash("The SysAcl could not be save. Please, try again.");
                    $this->redirect(array('action' => 'index'));
                }
            }
        } else {
            $options = array('conditions' => array('SysAcl.' . $this->SysAcl->primaryKey => $id));
            $this->request->data = $this->SysAcl->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SysAcl->id = $id;
        if (!$this->SysAcl->exists()) {
            throw new NotFoundException(__('Invalid SysAcl.'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->SysAcl->delete()) {
            $this->flash(__('The SysAcl has been deleted.'));
        } else {
            $this->flash(__('The SysAcl could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    //Delete from permission page fill in list
    public function detete_bath($role_id, $controller_id = null, $action_id = null) {
        if ($this->request->is(array('POST', 'PUT'))) {
//            pr($this->data); die;
            $data = $this->data;
            foreach ($data['SysAcl']['completedaction'] as $k => $v) {
                if (empty($v)) {
                    continue;
                }
                $control_action = explode('/', $v);

                if ($this->SysAcl->detete_bath($data['SysAcl']['role_id'], $control_action[0], $control_action[1])) {
                    $this->Session->setFlash(__('The SysAcl has been deleted.'));
                } else {
                    $this->flash(__('The SysAcl could not be deleted. Please, try again.'));
                }
            }
        }
        $this->redirect("/SysAcls/add_permission/{$role_id}");
    }

    public function add_permissionByuser($role_id = null, $user_id = null) {
        if (!empty($this->data)) {
            $data = $this->data;
            $affectedRows = 0;
            foreach ($data['SysAcl']['caction'] as $k => $v) {
                if (empty($v)) {
                    continue;
                }
                $control_action = explode('|', $v);

                //If exiting acl mapped don't save dupplicated
                $sql = "SELECT COUNT(*) FROM system.sys_acls WHERE sys_controller_id={$control_action[0]} AND sys_action_id={$control_action[1]} AND  user_id={$data['SysAcl']['user_id']};";

                $count_existing = $this->SysAcl->query($sql);

                if ($count_existing[0][0]['count'] > 0) {
                    continue;
                }

                $data['SysAcl'] = array(
                    'sys_controller_id' => $control_action[0],
                    'sys_action_id' => $control_action[1],
                    'user_id' => $data['SysAcl']['user_id'],
                    'status' => 'A',
                    'create_uid' => $this->getCurrenSessionUserId()
                );

                $this->SysAcl->create();
                if ($this->SysAcl->save($data)) {
                    $affectedRows++;
                }
            }

            if ($affectedRows > 0) {
                $this->Session->setFlash(__('The User\'s ACL has been Added.'));
                $this->redirect("/SysAcls/add_permissionByuser/{$role_id}/{$user_id}");
            } else {
                $this->Session->setFlash(__('The SysAcl could not be save. Please, try again.'));
            }
        }//end of submit data for save
        //Complete Package render for package choossing
        $tmp_complete = $this->SysAcl->find('all', array('conditions' => array('SysAcl.status' => 'A', 'OR' => array('SysAcl.user_id' => $user_id, 'SysAcl.role_id' => $role_id))));

        //pr($tmp_complete);exit;
        $completed_controlpackages = array('' => __('All'));
        $complete_map_permission = array();
        foreach ($tmp_complete as $kcom => $vcom) {
            $com_controll_val = $vcom['SysController']['id'];
            $com_controll_name = $vcom['SysController']['name'];

            if (is_null($vcom['SysAction']['id']) || empty($vcom['SysAction']['id'])) {
                continue;
            }
            $com_action_val = $vcom['SysAction']['id'];
            $com_action_name = $vcom['SysAction']['name'];

            $_com_val = $com_controll_val . '/' . $com_action_val;
            $_com_name = $com_controll_name . '/' . $com_action_name;

            $completed_controlpackages[$_com_val] = __($_com_name);
            $complete_map_permission[] = $com_action_val;
        }
        if (count($completed_controlpackages) <= 1) {
            $completed_controlpackages = array();
        }

//        debug($_com_name); exit;
        //All package fill all to mullti chossing on LEFT side page
        $controllers = $this->SysAction->find('all', array(
            'conditions' => array('SysAction.id !=' => $complete_map_permission, 'SysController.status' => 'A', 'SysAction.status' => 'A'),
            'fields' => array('SysAction.id', 'SysAction.name', 'SysController.id', 'SysController.name'),
        ));


        $available_menus = $this->SysController->find('list', array('fields' => array('id', 'name')));
        $controllers_packages = array('' => __('All'));
        $control = "";

        foreach ($controllers as $k => $v) {
            $control_val = $v['SysController']['id'];
            $control_name = $v['SysController']['name'];
            $action_value = $v['SysAction']['id'];
            $action_name = $v['SysAction']['name'];

            $_val = $control_val . '|' . $action_value;
            $_name = $control_name . '/' . $action_name;
            $controllers_packages[$_val] = __($_name);
        }

//       debug($_val); exit;
        //if not has new permission mus to map remove choossing all together
        if (count($controllers_packages) <= 1) {
            $controllers_packages = array();
        }

//        pr($available_menus);
        $this->set('controllers_packages', $controllers_packages);
        $this->set('completed_controlpackages', $completed_controlpackages);
        $this->set('available_menus', $available_menus);
        $this->set('user_id', $user_id);
        $this->set('role_id', $role_id);
    }

    public function detete_bathbyuser($role_id, $user_id, $controller_id = null, $action_id = null) {
        if ($this->request->is(array('POST', 'PUT'))) {
            $data = $this->data;
            foreach ($data['SysAcl']['completedaction'] as $k => $v) {
                if (empty($v)) {
                    continue;
                }
                $control_action = explode('/', $v);

                $res = $this->SysAcl->detete_bathbyuser($data['SysAcl']['user_id'], $control_action[0], $control_action[1]);
                if ($res === true) {
                    $this->Session->setFlash(__('The SysAcl has been deleted.'));
                } elseif ($res === false) {
                    $this->flash(__('The SysAcl could not be deleted. Please, try again.'));
                } else {
                    $this->Session->setFlash(__('User Can\'t  Delete Role\'s ACL.'));
                }
            }
        }
        $this->redirect("/SysAcls/add_permissionByuser/{$role_id}/{$user_id}");
    }

}
