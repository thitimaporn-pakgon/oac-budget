<?php

App::uses('AppController', 'Controller');

class SysControllersController extends AppController {

    public $name = 'SysControllers';
    public $uses = array('SysAcl', 'SysAction', 'SysController');
    public $helpers = array('Html', 'Form', 'Session', 'Paginator', 'DisplayFormat');
    public $components = array('Utility');

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function index() {
        $paginate = array();
        $conditions = array();
        $order = array();

        $conditions[] = array('SysController.status' => 'A');

        if (!empty($this->data)) {
            $this->data = $this->Utility->trim_all_data($this->data);
            //Find by Controller
            if (!empty($this->data['Search']['controller_id'])) {
                $conditions[] = array('SysController.id' => $this->data['Search']['controller_id']);
                //$sysActions = $this->SysAction->find('list', array('order' => array("SysAction.name ASC"), 'conditions' => array('SysAction.sys_controller_id' => $this->data['Search']['controller_id'])));
                //$this->set('sysActions', $sysActions);
            }

//            //Find by Action
//            if (!empty($this->data['Search']['sys_action_id'])) {
//                $conditions[] = array('SysAction.id' => $this->data['Search']['sys_action_id']);
//            }
            //Find by Action status
            if (!empty($this->data['Search']['status'])) {
                $conditions[] = array('SysController.status' => $this->data['Search']['status']);
            }
        }

        $order = array('SysController.modified' => 'DESC');
        $paginate = array(
            'SysController' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->SysController->recursive = 0;
        $this->paginate = $paginate;

        $controllers = $this->SysController->findListSysController();
        $statuses = $this->SysAcl->getSysStatus();
        $sysActions = $this->getEmptySelect();
        $this->set(compact('controllers', 'statuses', 'sysActions'));
        $this->set('sysControllers', $this->paginate('SysController'));
    }

    //OK Checked
    //View Controller with actions together
    //Since 20131031
    public function view($id = null) {
        if (!$this->SysController->exists($id)) {
            throw new NotFoundException(__('Invalid sys controller'));
        }
        $options = array('conditions' => array('SysController.' . $this->SysController->primaryKey => $id), 'recursive' => -1);
        $this->set('sysControllers', $this->SysController->find('first', $options));
        $this->SysAction->recursive = -1;
        $this->set('sysActions', $this->SysAction->findAllBySysControllerId($id));
    }

    //OK Checked
    //Add Controller with actions together
    //Since 20131031
    public function add() {
        if (!empty($this->request->data)) {
            $SysController = $this->request->data;
            $createUID = $this->Session->read('Auth.User.id');
            $SysController['SysController']['status'] = 'A';
            $SysController['SysController']['create_uid'] = $createUID;
            $crudeGenerate = isset($SysController['SysController']['crude']) ? true : false;
            $sysControllerId = null;
            $this->SysController->create();
            if ($this->SysController->save($SysController)) {
                $sysControllerId = $this->SysController->getLastInsertId();
                if ($crudeGenerate === true) {
                    $crudeOpt = array('index' => 'หน้าหลัก', 'view' => 'รายละเอียด', 'add' => 'เพิ่มข้อมูล', 'edit' => 'แก้ไขข้อมูล', 'delete' => 'ลบข้อมูล');
                    foreach ($crudeOpt as $k => $v) {
                        $SysAction = array();
                        $SysAction['SysAction']['name'] = $k;
                        $SysAction['SysAction']['sys_controller_id'] = $sysControllerId;
                        $SysAction['SysAction']['description'] = $v;
                        $SysAction['SysAction']['status'] = 'A';
                        $SysAction['SysAction']['create_uid'] = $createUID;
                        $this->SysAction->create();
                        $this->SysAction->save($SysAction);
                    }
                }
                $this->Session->setFlash(__("The sys has been saved."));
                $this->redirect(array('action' => 'edit', $sysControllerId));
            } else {
                $this->Session->setFlash(__('The sys could not be saved. Please, try again.'));
                $this->redirect(array('action' => 'add'));
            }
        }

        $statuses = $this->SysAcl->getSysStatus();
        $this->set(compact('statuses'));
    }

    //OK Checked
    //Edit Controller with actions together
    //Since 20131031
    public function edit($id = null) {
        if (!$this->SysController->exists($id)) {
            throw new NotFoundException(__('Invalid sys controller'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->data)) {
                $SysController = $this->data;
                $SysController['SysController']['status'] = 'A';
                $SysController['SysController']['create_uid'] = $this->Session->read('Auth.User.id');

                if ($this->SysController->save($SysController)) {
                    $this->Session->setFlash(__("The sys has been saved."));
                } else {
                    $this->Session->setFlash(__('The sys could not be saved. Please, try again.'));
                }
                $this->redirect(array('action' => 'edit', $id));
            }
        } else {
            $options = array('conditions' => array('SysController.' . $this->SysController->primaryKey => $id));
            $this->data = $this->SysController->find('first', $options);
            //$sysActionInfos = $this->SysAction->findActionByControllerId($id);
            $this->set('sysActionInfos', $this->SysAction->findActionByControllerId($id));
        }

        $statuses = $this->SysAcl->getSysStatus();
        $this->set(compact('statuses', 'id'));
    }

    public function addActions($id = null) {
        if (!$this->SysController->exists($id)) {
            throw new NotFoundException(__('Invalid sys controller'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->data)) {
                $data = $this->data;

                $actionName = $this->request->data['SysAction']['name'];
                $condition = array('SysAction.name' => $actionName, 'SysAction.sys_controller_id' => $id);

                if ($this->SysAction->hasAny($condition)) {
                    $this->Session->setFlash(__('Function %s is already exists', $actionName));
                    $this->redirect($this->referer());
                }

                $data['SysAction']['sys_controller_id'] = $id;
                $data['SysAction']['status'] = 'A';
                $data['SysAction']['create_uid'] = $this->Session->read('Auth.User.id');
                if ($this->SysAction->save($data)) {
                    $this->Session->setFlash(__("The sys has been saved."));
                } else {
                    $this->Session->setFlash(__('The sys could not be saved. Please, try again.'));
                }
                $this->redirect(array('action' => 'edit', $id));
            }
        } else {
            $this->Session->setFlash(__('Invalid calling method please try again !'));
            $this->redirect(array('action' => 'edit', $id));
        }
    }

    //OK Checked
    //Edit Controller with actions together
    //Since 20131031
    public function deleteAction($contollerId, $actionId) {
        if (!$this->SysController->exists($contollerId)) {
            throw new NotFoundException(__('Invalid system action'));
        }
        if ($this->SysAction->delete($actionId)) {
            //$this->redirect(array('action' => 'index'));
        }
        $this->redirect(array('action' => 'edit', $contollerId));
    }

    //OK Checked
    //Edit Controller with actions together
    //Since 20131031
    public function delete($controller_id, $action_id) {
        if (!$this->SysAction->exists($action_id)) {
            throw new NotFoundException(__('Invalid system action'));
        }
        if (!$this->SysController->exists($controller_id)) {
            throw new NotFoundException(__('Invalid system Controller'));
            if ($this->SysController->deleteControllers($controller_id, $action_id)) {
                $this->redirect(array('action' => 'index'));
            }
            $this->redirect(array('action' => 'index'));
        }
    }

}
