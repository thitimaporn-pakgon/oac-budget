<?php

App::uses('AppController', 'Controller');

/**
 * Positions Controller
 *
 * @property Position $Position
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PositionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Positions';
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('Position');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('Position.name' => 'ASC', 'Position.name_eng' => 'ASC');
        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trim_all_data($this->request->data);
            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions['OR'][] = array('LOWER(Position.name) LIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
                $conditions['OR'][] = array('LOWER(Position.name_eng) LIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }
            //Find by detail
            if (!empty($this->request->data['Search']['description'])) {
                $conditions[] = array('LOWER(Position.description) ILIKE ' => '%' . strlolower($this->request->data['Search']['description']) . '%');
            }
            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('Position.status' => $this->request->data['Search']['status']);
            }
        }else{
            $conditions[] = array('Position.status'=>'A');
        }
        $paginate = array(
            'Position' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        $this->paginate = $paginate;
        $this->Position->recursive = 0;
        $this->set('positions', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Position->exists($id)) {
            throw new NotFoundException(__('Invalid position'));
        }
        $options = array('conditions' => array('Position.' . $this->Position->primaryKey => $id));
        $this->set('position', $this->Position->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {

            $checkName = $this->Position->find('list', array('conditions' => array('Position.name' => $this->request->data['Position']['name'])));
            $this->request->data['Position']['status'] = 'A';
            $this->request->data['Position']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->Position->create();
            if (empty($checkName)) {
                if ($this->Position->save($this->request->data)) {
                    $this->Session->setFlash(__('The position has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The position could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The position could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Position->exists($id)) {
            throw new NotFoundException(__('Invalid position'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Position']['update_uid'] = $this->Session->read('Auth.User.id');
            if ($this->Position->save($this->request->data)) {
                $this->Session->setFlash(__('The position has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The position could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Position.' . $this->Position->primaryKey => $id));
            $this->request->data = $this->Position->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for position
     * @author  sarawutt.b 
     * @param   string $id as integer of position id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->Position->id = $id;
        if (!$this->Position->exists()) {
            //throw new NotFoundException(__('Invalid position'));
            $this->Flash->error(__('Invalid not found position with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->Position->delete()) {
                $responds = array('message' => __('The position has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The position could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->Position->delete()) {
                $this->Flash->success(__('The position has been deleted.'));
            } else {
                $this->Flash->error(__('The position could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the posiontion with id = %s', $id);
    }

}
