<?php

App::uses('AppController', 'Controller');

/**
 * AccessLogs Controller
 *
 * @property AccessLog $AccessLog
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class AccessLogsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->data;
        }
        if (empty($this->data)) {
            $this->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('AccessLog.created' => 'ASC');
        //$this->saveAccessLog('View AccessLog');
        if (!empty($this->data)) {
            $this->data = $this->Utility->trim_all_data($this->data);
            //Find by username
            if (!empty($this->data['Search']['actions'])) {
                $conditions[] = array('AccessLog.actions' => $this->data['Search']['actions']);
            }
            $timeFrom = "'" . $this->data['Search']['createFrom'] . "'";
            $timeTo = "'" . $this->data['Search']['createTo'] . "'";
            if (!empty($this->data['Search']['createFrom']) && empty($this->data['Search']['createTo'])) {
                $conditions[] = array('DATE(AccessLog.created)' => $timeFrom);
            } else if (!empty($this->data['Search']['createTo']) && empty($this->data['Search']['createFrom'])) {
                $conditions[] = array('DATE(AccessLog.created)' => $timeTo);
            } else if (!empty($this->data['Search']['createFrom']) && !empty($this->data['Search']['createTo'])) {
                $conditions[] = array('DATE(AccessLog.created) >= ' => $timeFrom);
                $conditions[] = array('DATE(AccessLog.created) <= ' => $timeTo);
            }
        }
        $paginate = array(
            'AccessLog' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        // pr($paginate);die;
        $this->paginate = $paginate;
        $this->AccessLog->recursive = 0;
        $this->set('accessLogs', $this->Paginator->paginate('AccessLog'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->AccessLog->exists($id)) {
            throw new NotFoundException(__('Invalid access log'));
        }
        $options = array('conditions' => array('AccessLog.' . $this->AccessLog->primaryKey => $id));
        $this->set('accessLog', $this->AccessLog->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['AccessLog']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->AccessLog->create();
            if ($this->AccessLog->save($this->request->data)) {
                $this->Session->setFlash(__('The access log has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The access log could not be saved. Please, try again.'));
            }
        }
        $menus = $this->AccessLog->Menu->find('list');
        $users = $this->AccessLog->User->find('list');
        $this->set(compact('menus', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->AccessLog->exists($id)) {
            throw new NotFoundException(__('Invalid access log'));
        }
        $this->request->data['AccessLog']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->AccessLog->save($this->request->data)) {
                $this->Session->setFlash(__('The access log has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The access log could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('AccessLog.' . $this->AccessLog->primaryKey => $id));
            $this->request->data = $this->AccessLog->find('first', $options);
        }
        $menus = $this->AccessLog->Menu->find('list');
        $users = $this->AccessLog->User->find('list');
        $this->set(compact('menus', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->AccessLog->id = $id;
        if (!$this->AccessLog->exists()) {
            throw new NotFoundException(__('Invalid access log'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->AccessLog->delete()) {
            $this->flash(__('The access log has been deleted.'));
        } else {
            $this->flash(__('The access log could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
