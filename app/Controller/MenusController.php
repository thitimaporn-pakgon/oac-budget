<?php

App::uses('AppController', 'Controller');

/**
 * Menus Controller
 *
 * @property Menu $Menu
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class MenusController extends AppController {

    public $name = 'Menus';
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('MenuList', 'SysAcl', 'SysAction', 'SysController');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->data;
        }
        if (empty($this->data)) {
            $this->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        if (!empty($this->data)) {
            if (!empty($this->data['Search']['name'])) {
                $conditions['OR'][] = array('LOWER(MenuList.menu_name) LIKE' => '%' . strtolower($this->data['Search']['name']) . '%');
            }
            if (!empty($this->data['Search']['url'])) {
                $conditions[] = array('LOWER(MenuList.menu_path)' => strtolower($this->data['Search']['url']));
            }
        }

        $order = array('MenuList.id' => 'ASC');
        $paginate = array(
            'MenuList' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->Menu->recursive = 0;
        $this->paginate = $paginate;
        $this->set('menus', $this->Paginator->paginate('MenuList'));
    }

    public function view($id = null) {
        if (!$this->MenuList->exists($id)) {
            throw new NotFoundException(__('Invalid menu'));
        }
        $options = array('conditions' => array('MenuList.' . $this->Menu->primaryKey => $id));
        $this->set('menu', $this->MenuList->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
//            $this->request->data['Menu']['create_uid'] = $this->Session->read('Auth.User.id');
            if (empty($this->request->data['MenuList']['menu_name'])) {
                unset($this->request->data['MenuList']['glyphicon']);
                unset($this->request->data['MenuList']['url']);
                unset($this->request->data['MenuList']['system_name_id']);
                
            }

            $this->MenuList->create();
//            debug($this->request->data);
//            die;
            if ($this->MenuList->save($this->request->data['MenuList'])) {
                $this->Session->setFlash(__('The menu has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
            }
        }
        $this->set('sysActions', $this->getEmptySelect());
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->MenuList->exists($id)) {
            throw new NotFoundException(__('Invalid menu'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['MenuList']['id'] = $id;
            if (empty($this->request->data['MenuList']['menu_name'])) {
                unset($this->request->data['MenuList']['icon_m']);
                unset($this->request->data['MenuList']['menu_path']);
            }

            if ($this->MenuList->save($this->request->data)) {
                $this->Session->setFlash(__('The menu has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('MenuList.' . $this->MenuList->primaryKey => $id));
            $this->request->data = $this->MenuList->find('first', $options);
            $sysActions = $this->request->data;
            $this->set(compact('sysActions'));
        }
    }

    /**
     *
     * delete method delete for menu
     * @author  sarawutt.b 
     * @param   string $id as integer of menu id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->MenuList->id = $id;
        if (!$this->MenuList->exists()) {
            $this->Flash->error(__('Invalid not found menu with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->MenuList->delete()) {
                $responds = array('message' => __('The menu has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The menu could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->MenuList->delete()) {
                $this->Flash->success(__('The menu has been deleted.'));
            } else {
                $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the menu with id = %s', $id);
    }

}
