<?php

App::uses('AppController', 'Controller');

/**
 * DepartmentLevels Controller
 *
 * @property DepartmentLevel $DepartmentLevel
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DepartmentLevelsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');
    public $uses = array('User', 'DepartmentLevel');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['DepartmentLevel']['name'])) {
                $conditions['OR'][] = array('lower(DepartmentLevel.name) ILIKE' => '%' . strtolower($this->request->data['DepartmentLevel']['name']) . '%');
                $conditions['OR'][] = array('lower(DepartmentLevel.name_eng) ILIKE' => '%' . strtolower($this->request->data['DepartmentLevel']['name']) . '%');
            }
            if (!empty($this->request->data['DepartmentLevel']['status'])) {
                $conditions['AND'][] = array('DepartmentLevel.status' => $this->request->data['DepartmentLevel']['status']);
            }
        } else {
            $conditions[] = array('DepartmentLevel.status' => 'A');
        }
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('DepartmentLevel.name' => 'asc', 'DepartmentLevel.name_eng' => 'asc', 'DepartmentLevel.id' => 'asc'),
            'limit' => Configure::read('Pagination.Limit')
        );
        $this->set('departmentLevels', $this->Paginator->paginate('DepartmentLevel'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->DepartmentLevel->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของระดับหน่วยงาน');
            throw new NotFoundException(__('Invalid department level'));
        }
        $options = array('conditions' => array('DepartmentLevel.' . $this->DepartmentLevel->primaryKey => $id));
        $this->set('departmentLevel', $this->DepartmentLevel->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['DepartmentLevel']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['DepartmentLevel']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['DepartmentLevel']['status'] = 'A';
            $this->DepartmentLevel->create();
            if ($this->DepartmentLevel->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของระดับหน่วยงาน');
                $this->Session->setFlash(__('The department level has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The department level could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->DepartmentLevel->exists($id)) {
            throw new NotFoundException(__('Invalid department level'));
        }
        $this->request->data['DepartmentLevel']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->DepartmentLevel->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของระดับหน่วยงาน');
                $this->Session->setFlash(__('The department level has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The department level could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('DepartmentLevel.' . $this->DepartmentLevel->primaryKey => $id));
            $this->request->data = $this->DepartmentLevel->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for department level
     * @author  sarawutt.b 
     * @param   string $id as integer of department level id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->DepartmentLevel->id = $id;
        if (!$this->DepartmentLevel->exists()) {
            $this->Flash->error(__('Invalid not found department level with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->DepartmentLevel->delete()) {
                $responds = array('message' => __('The department level has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The department level could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->DepartmentLevel->delete()) {
                $this->Flash->success(__('The department level has been deleted.'));
            } else {
                $this->Flash->error(__('The department level could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the department level with id = %s', $id);
    }

    public function check_already_departmentLevel($name, $id = null) {
        $this->autoRender = false;
        $condition = (is_null($id) || empty($id) || !is_numeric($id)) ? array() : array('id !=' => $id);
        echo ($this->DepartmentLevel->find('count', array('conditions' => array($condition, 'name' => $name))) > 0) ? 'Y' : 'N';
    }

}
