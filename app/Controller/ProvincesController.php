<?php

App::uses('AppController', 'Controller');

/**
 * Provinces Controller
 *
 * @property Province $Province
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ProvincesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        if (empty($this->passedArgs['Province'])) {
            $this->passedArgs['Province'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Province'];
        }

        if (!empty($this->request->data)) {
            if (!empty($this->request->data['Province']['name'])) {
                $conditions['OR'][] = array('lower(Province.name) ILIKE' => '%' . strtolower($this->request->data['Province']['name']) . '%');
                $conditions['OR'][] = array('lower(Province.name_eng) ILIKE' => '%' . strtolower($this->request->data['Province']['name']) . '%');
            }
            if (!empty($this->request->data['Province']['status'])) {
                $conditions['AND'][] = array('lower(Province.status)' => $this->request->data['Province']['status']);
            }
        } else {
            $conditions[] = array('Province.status' => 'A');
        }
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('Province.name' => 'ASC', 'Province.name_eng' => 'ASC', 'Province.modified' => 'desc'),
            'limit' => Configure::read('Pagination.Limit')
        );
        $this->set('provinces', $this->Paginator->paginate('Province'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Province->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของจังหวัด');
            throw new NotFoundException(__('Invalid province'));
        }
        $options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
        $this->set('province', $this->Province->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Province']['create_uid'] = $this->getCurrenSessionUserId();
            $this->request->data['Province']['status'] = 'A';
            $this->Province->create();
            if ($this->Province->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของจังหวัด');
                $this->Session->setFlash(__('The province has been saved.'), 'default');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The province could not be saved. Please, try again.'), 'default');
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Province->exists($id)) {
            throw new NotFoundException(__('Invalid province'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Province']['update_uid'] = $this->getCurrenSessionUserId();
            if ($this->Province->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของจังหวัด');
                $this->Session->setFlash(__('The province has been saved.'), 'default');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The province could not be saved. Please, try again.'), 'default');
            }
        } else {
            $options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
            $this->request->data = $this->Province->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for province
     * @author  sarawutt.b 
     * @param   string $id as integer of province id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->Province->id = $id;
        if (!$this->Province->exists()) {
            $this->Flash->error(__('Invalid not found province with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->Province->delete()) {
                $responds = array('message' => __('The province has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The province could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->Province->delete()) {
                $this->Flash->success(__('The province has been deleted.'));
            } else {
                $this->Flash->error(__('The province could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the province with id = %s', $id);
    }

    public function check_already_province($id = null, $name = null) {
        $this->autoRender = false;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);
        echo ($this->Province->find('count', array('conditions' => array('OR' => array($conditions, 'name' => $name)))) > 0) ? 'Y' : 'N';
    }

}
