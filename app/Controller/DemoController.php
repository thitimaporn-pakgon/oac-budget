<?php

App::uses('AppController', 'Controller');
App::uses('ThaiNumber', 'Lib');

class DemoController extends AppController {

    public $name = 'Demo';
    public $uses = array('User');
    public $components = array('PHPMailer', 'RequestHandler', 'ReportManager');

    public function index() {
        
    }

    public function testRequestActions() {
        return $this->User->find('all');
    }

    public function testRequestJson() {
        
    }

    public function downloadPdf() {
        $this->layout = 'pdf';
    }

    public function pdfHelper() {
        $this->layout = 'pdf';
    }

    public function phpmailer() {
        
    }

    public function editor() {
        if ($this->request->is(array('POST', 'PUT'))) {
            
        }
    }

    public function developer() {
        if ($this->request->is(array('POST', 'PUT'))) {
            $resultOK = array(); //Keep file handler
            //Reader from file attachment file
            if (!empty($this->data['button']['file_extension']['name'])) {
                $file_path = 'uploadfile';
                $resultOK = $this->uploadFiles($file_path, $this->data['button']['file_extension']);
                if (isset($resultOK['errors'])) {
                    $this->Flash->error(__(' %s. Please, try again.', $resultOK['errors'][0]));
                    $this->redirect($this->referer());
                }
                $this->request->data['button']['file_extension'] = $resultOK['urls'][0];
            } else {
                unset($this->request->data['button']['file_extension']);
            }
        }
    }

    public function upload() {
        
    }

    public function html5geolocation() {
        
    }

    public function bootstraptreeview() {
        
    }

    /**
     * 
     * Function make demo ireport exportting to custome file
     * @author sarawutt.b
     */
    public function jasperExportExcel() {
        $params = array('LIMIT' => 100);
        $this->autoRender = FALSE;
        $this->ReportManager->generateReport(IREPORT . 'rptExample001_excel.jrxml', $params, 'yyyy.pdf');
    }

    /**
     * 
     * @author Numpol.J
     * @param type $id as a integer id of model were your sandin g together
     * @param type $model as a string of model class
     * @param type $sector as a character posible value A = Maneuver, B = Allocated, C = Manage
     * @return boolean
     */
    public function deleteChildrenCheck($id = null, $model = null, $sector = 'A') {
        $this->autoRender = false;
        $sectorList = array('A' => 'Maneuver', 'B' => 'Allocated', 'C' => 'Manage');
        $_modelList = array("Strategic{$sectorList[$sector]}" => "Strategic{$sectorList[$sector]}", "Plan{$sectorList[$sector]}" => "Plan{$sectorList[$sector]}", "Result{$sectorList[$sector]}" => "Result{$sectorList[$sector]}", "Event{$sectorList[$sector]}" => "Event{$sectorList[$sector]}", "WorkGroup{$sectorList[$sector]}" => "WorkGroup{$sectorList[$sector]}", "Work{$sectorList[$sector]}" => "Work{$sectorList[$sector]}", "Project{$sectorList[$sector]}" => "Project{$sectorList[$sector]}", "ProjectList{$sectorList[$sector]}" => "ProjectList{$sectorList[$sector]}");
        $model = Inflector::camelize("{$model}{$sectorList[$sector]}");

        foreach ($_modelList as $k => $v) {
            if ($k === $model) {
                continue;
            }
            $this->loadModel($v);
            $conditions = array(Inflector::underscore($_modelList[$model]) . '_id' => $id);
            debug($v . ' => ' . array_search($id, $conditions) . ' => ' . $id);
            if ($this->$v->hasAny($conditions)) {
                debug('yes has any');
                //return true;
            } else {
                debug('not has any');
                //return false;
            }
        }
        exit;
    }

}
