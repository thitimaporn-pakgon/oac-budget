<?php

App::uses('AppController', 'Controller');

/**
 *
 * Configs Controller
 * @author  sarawutt.b
 * @property Config $Config
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-03-07 14:47:34
 * @license Zicure Corp. 
 */
class ConfigsController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');

    /**
     * 
     * index method view list for config
     * @author  sarawutt.b 
     * @since   2017-03-07 14:47:33
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('Config.id' => 'ASC', 'Config.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trim_all_data($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(Config.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('Config.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(Config.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(Config.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(Config.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(Config.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'Config' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->Config->recursive = 0;
        $configs = $this->Paginator->paginate('Config');
        $this->set(array('configs' => $configs, '_serialize' => array('configs')));
        //$this->set('configs', $this->Paginator->paginate('Config'));
        //$this->saveAccessLog('View list for %s', 'config');
    }

    /**
     *
     * view method view for config
     * @author  sarawutt.b 
     * @param   string $id as integer of config id [PK] 
     * @since   2017-03-07 14:47:33
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        //$id = $this->secureDecodeParam($id);
        if (!$this->Config->exists($id)) {
            $this->Flash->error(__('Invalid not found config with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('Config.' . $this->Config->primaryKey => $id));
        $config = $this->Config->find('first', $options);
        $this->set(array('config' => $config, '_serialize' => array('config')));
        //$this->set('config', $this->Config->find('first', $options));
        //$this->saveAccessLog('View for the config with id = %s', $id);
    }

    /**
     *
     * add method add new for config
     * @author  sarawutt.b 
     * @since   2017-03-07 14:47:33
     * @license Zicure Corp. 
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->request->data['Config']['create_uid'] = $this->getCurrenSessionUserId();
            $this->request->data['Config']['status'] = 'A';
            $this->Config->create();
            if ($this->Config->save($this->request->data)) {
                $this->Flash->success(__('The config has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The config could not be saved. Please, try again.'));
            }
        }
        //$this->saveAccessLog('Add new the config');
    }

    /**
     *
     * edit method for config
     * @author  sarawutt.b 
     * @param   string $id as integer of config id [PK] 
     * @since   2017-03-07 14:47:33
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {
        //$id = $this->secureDecodeParam($id);
        if (!$this->Config->exists($id)) {
            $this->Flash->error(__('Invalid not found config with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Config']['update_uid'] = $this->getCurrenSessionUserId();
            if ($this->Config->save($this->request->data)) {
                $this->Flash->success(__('The config has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The config could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Config.' . $this->Config->primaryKey => $id));
            $this->request->data = $this->Config->find('first', $options);
        }
        //$this->saveAccessLog('Edit for the config with id = %s', $id);
    }

    /**
     *
     * delete method delete for config
     * @author  sarawutt.b 
     * @param   string $id as integer of config id [PK] 
     * @since   2017-03-07 14:47:33
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        //$id = $this->secureDecodeParam($id);
        $this->Config->id = $id;
        if (!$this->Config->exists()) {
            $this->Flash->error(__('Invalid not found config with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->Config->delete()) {
                $responds = array('message' => __('The config has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The config could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->Config->delete()) {
                $this->Flash->success(__('The config has been deleted.'));
            } else {
                $this->Flash->error(__('The config could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the Config with id = %s', $id);
    }

}
