<?php

App::uses('AppController', 'Controller');

/**
 * SubDistricts Controller
 *
 * @property SubDistrict $SubDistrict
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SubDistrictsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');
    public $uses = array('District', 'SubDistrict');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        if (empty($this->passedArgs['SubDistrict'])) {
            $this->passedArgs['SubDistrict'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['SubDistrict'];
        }
        if (!empty($this->request->data)) {
            if (!empty($this->request->data['SubDistrict']['province_id'])) {
                $conditions['AND'][] = array('LOWER(SubDistrict.id) LIKE' => '%' . strtolower($this->request->data['SubDistrict']['province_id']) . '%');
            }
            if (!empty($this->request->data['SubDistrict']['district_id'])) {
                $conditions['AND'][] = array('LOWER(SubDistrict.id) LIKE' => '%' . strtolower($this->request->data['SubDistrict']['district_id']) . '%');
            }
            if (!empty($this->request->data['SubDistrict']['sub_district_name'])) {
                $conditions['AND'][] = array('lower(SubDistrict.name) ILIKE' => '%' . strtolower($this->request->data['SubDistrict']['sub_district_name']) . '%');
            }
            if (!empty($this->request->data['SubDistrict']['status'])) {
                $conditions['AND'][] = array('SubDistrict.status' => $this->request->data['SubDistrict']['status']);
            }
        } else {
            $conditions[] = array('SubDistrict.status' => 'A');
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('SubDistrict.id' => 'ASC', 'SubDistrict.name' => 'ASC', 'SubDistrict.name_eng' => 'ASC', 'SubDistrict.modified' => 'DESC'),
            'limit' => Configure::read('Pagination.Limit')
        );
        $this->set('subDistricts', $this->Paginator->paginate('SubDistrict'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SubDistrict->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของตำบล');
            throw new NotFoundException(__('Invalid sub district'));
        }
        $options = array('conditions' => array('SubDistrict.' . $this->SubDistrict->primaryKey => $id));
        $this->set('subDistrict', $this->SubDistrict->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['SubDistrict']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->request->data['SubDistrict']['status'] = 'A';
            $this->SubDistrict->create();
            if ($this->SubDistrict->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของตำบล');
                $this->Session->setFlash(__('The sub district has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sub district could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SubDistrict->exists($id)) {
            throw new NotFoundException(__('Invalid sub district'));
        }
        $this->request->data['SubDistrict']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->SubDistrict->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของตำบล');
                $this->Session->setFlash(__('The sub district has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sub district could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SubDistrict.' . $this->SubDistrict->primaryKey => $id));
            $this->request->data = $this->SubDistrict->find('first', $options);
            $this->set('id', $id);
        }
    }

    /**
     *
     * delete method delete for sub district
     * @author  sarawutt.b 
     * @param   string $id as integer of sub district id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->SubDistrict->id = $id;
        if (!$this->SubDistrict->exists()) {
            $this->Flash->error(__('Invalid not found sub district with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->SubDistrict->delete()) {
                $responds = array('message' => __('The sub district has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The sub district could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->SubDistrict->delete()) {
                $this->Flash->success(__('The sub district has been deleted.'));
            } else {
                $this->Flash->error(__('The sub district could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the sub district with id = %s', $id);
    }

    public function load_District($dtID = null) {
        $this->autoRender = false;
        $this->layout = 'ajax';
        if (is_null($dtID) || empty($dtID)) {
            echo '';
            exit;
        }
        $Sub_tdID = substr($dtID, -5, 1);
        $sql = "SELECT  MAX(CAST(SUBSTRING(sub_districts.id FROM  2) AS INT))+1 AS next_id FROM master.sub_districts WHERE sub_districts.id LIKE '" . $dtID . "%'";
        $new_subDistrict_id = $this->SubDistrict->query($sql);
        if ($new_subDistrict_id[0][0]['next_id'] == null) {
            $new_subDistrict_id[0][0]['next_id'] = $dtID . '01';
            echo $new_subDistrict_id[0][0]['next_id'];
        } else {

            echo $Sub_tdID . $new_subDistrict_id[0][0]['next_id'];
        }
    }

    public function check_already_subDistrict($name, $id = null) {
        $this->autoRender = false;
        $dtID = substr($id, 0, 5);
        $a = $this->SubDistrict->find('count', array('conditions' => array('id' => $id, 'name' => $name)));
        $b = $this->SubDistrict->find('count', array('conditions' => array('id like' => '%' . $dtID . '%', 'name' => $name)));
        if ($a == 1 || $b == 0) {
            echo 'N';
        } else {
            echo 'Y';
        }
    }

}
