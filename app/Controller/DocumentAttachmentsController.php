<?php

App::uses('AppController', 'Controller');

/**
 * DocumentAttachments Controller
 *
 * @property DocumentAttachment $DocumentAttachment
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DocumentAttachmentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->DocumentAttachment->recursive = 0;
        $this->set('documentAttachments', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->DocumentAttachment->exists($id)) {
            throw new NotFoundException(__('Invalid document attachment'));
        }
        $options = array('conditions' => array('DocumentAttachment.' . $this->DocumentAttachment->primaryKey => $id));
        $this->set('documentAttachment', $this->DocumentAttachment->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['DocumentAttachment']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->DocumentAttachment->create();
            if ($this->DocumentAttachment->save($this->request->data)) {
                $this->Session->setFlash(__('The document attachment has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The document attachment could not be saved. Please, try again.'));
            }
        }
        $projects = $this->DocumentAttachment->Project->find('list');
        $projectPlans = $this->DocumentAttachment->ProjectPlan->find('list');
        $this->set(compact('projects', 'projectPlans'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->DocumentAttachment->exists($id)) {
            throw new NotFoundException(__('Invalid document attachment'));
        }
        $this->request->data['DocumentAttachment']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->DocumentAttachment->save($this->request->data)) {
                $this->Session->setFlash(__('The document attachment has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The document attachment could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('DocumentAttachment.' . $this->DocumentAttachment->primaryKey => $id));
            $this->request->data = $this->DocumentAttachment->find('first', $options);
        }
        $projects = $this->DocumentAttachment->Project->find('list');
        $projectPlans = $this->DocumentAttachment->ProjectPlan->find('list');
        $this->set(compact('projects', 'projectPlans'));
    }

    /**
     *
     * delete method delete for document attachment
     * @author  sarawutt.b 
     * @param   string $id as integer of document attachment id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $id = $this->secureDecodeParam($id);
        $documentAttachmentInfo = $this->DocumentAttachment->find('first', array('conditions' => array('id' => $id), 'recursive' => -1));
        $this->DocumentAttachment->id = $id;
        if (empty($documentAttachmentInfo)) {
            $this->Flash->error(__('Invalid not found document attachment with id %s please try again !', $id));
            return $this->redirect($this->referer());
        }
        //$this->request->allowMethod('post', 'delete');
        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->DocumentAttachment->delete()) {
                @unlink(rtrim(WWW_ROOT, DS) . $documentAttachmentInfo['DocumentAttachment']['attachment_path']);
                $responds = array('message' => __('The document attachment has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The document attachment could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->DocumentAttachment->delete()) {
                @unlink(rtrim(WWW_ROOT, DS) . $documentAttachmentInfo['DocumentAttachment']['attachment_path']);
                $this->Flash->success(__('The document attachment has been deleted.'));
            } else {
                $this->Flash->error(__('The document attachment could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect($this->referer());
        //$this->saveAccessLog('Delete for the Document Attachment with id = %s', $id);
    }

}
