<?php

App::uses('AppController', 'Controller');

/**
 *
 * EventManeuvers Controller
 * @author  sarawutt.b
 * @property EventManeuver $EventManeuver
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 * @since   2017-04-18 17:39:50
 * @license Zicure Corp. 
 */
class EventManeuversController extends AppController {

    /**
     *
     * Components
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('StrategyActivity', 'StrategyPlan', 'StrategyGov', 'StrategyProduct');

    /**
     * 
     * index method view list for event maneuver
     * @author  sarawutt.b 
     * @since   2017-04-18 17:39:44
     * @license Zicure Corp. 
     * @return  void
     */
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }

        $paginate = array();
        $conditions = array();
        $order = array('EventManeuver.id' => 'ASC', 'EventManeuver.created' => 'ASC');

        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trimAllData($this->request->data);

            //Find by name
            if (!empty($this->request->data['Search']['name'])) {
                $conditions[] = array('LOWER(EventManeuver.name) ILIKE' => '%' . strtolower($this->request->data['Search']['name']) . '%');
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('EventManeuver.status' => $this->request->data['Search']['status']);
            }

            //Find by start created between created
            if (!empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(EventManeuver.created) >= ' => $this->request->data['Search']['dateFrom']);
                $conditions[] = array('DATE(EventManeuver.created) <= ' => $this->request->data['Search']['dateTo']);
            }
            //Find by created with before form input dateFrom
            else if (!empty($this->request->data['Search']['dateFrom']) && empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(EventManeuver.created)' => $this->request->data['Search']['dateFrom']);
            }
            //Find by created with after form input dateTo
            else if (empty($this->request->data['Search']['dateFrom']) && !empty($this->request->data['Search']['dateTo'])) {
                $conditions[] = array('DATE(EventManeuver.created)' => $this->request->data['Search']['dateTo']);
            }
        }

        $paginate = array(
            'EventManeuver' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));

        $this->paginate = $paginate;
        $this->EventManeuver->recursive = 0;
        $eventManeuvers = $this->Paginator->paginate('EventManeuver');
        $this->set(array('eventManeuvers' => $eventManeuvers, '_serialize' => array('eventManeuvers')));
        //$this->set('eventManeuvers', $this->Paginator->paginate('EventManeuver'));
        //$this->saveAccessLog('View list for %s', 'event maneuver');
    }

    /**
     *
     * view method view for event maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of event maneuver id [PK] 
     * @since   2017-04-18 17:39:44
     * @license Zicure Corp. 
     * @return  void
     */
    public function view($id = null) {
        if (!$this->EventManeuver->exists($id)) {
            //throw new NotFoundException(__('Invalid event maneuver'));
            $this->Flash->error(__('Invalid not found event maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('EventManeuver.' . $this->EventManeuver->primaryKey => $id));
        $eventManeuver = $this->EventManeuver->find('first', $options);
        $this->set(array('eventManeuver' => $eventManeuver, '_serialize' => array('eventManeuver')));
        //$this->set('eventManeuver', $this->EventManeuver->find('first', $options));
        //$this->saveAccessLog('View for the event maneuver with id = %s', $id);
    }

    public function add($resultId = null) {
        $dataall_StrategyProduct = $this->StrategyProduct->find('first', array('conditions' => array('StrategyProduct.id' => $resultId)));
        $data_strategy_plan_name = $this->StrategyPlan->find('first', array('conditions' => array('StrategyPlan.id' => $dataall_StrategyProduct['StrategyProduct']['strategy_plan_id'])));
        $data_strategy_gov_name = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $dataall_StrategyProduct['StrategyProduct']['strategy_gov_id'])));

        $year = $dataall_StrategyProduct['StrategyProduct']['budget_year_id'];
        $data_strategy_gov_name = $data_strategy_gov_name['StrategyGov']['strategy_gov_name'];
        $data_strategy_plan_name = $data_strategy_plan_name['StrategyPlan']['strategy_plan_name'];
        $data_strategy_product_name = $dataall_StrategyProduct['StrategyProduct']['strategy_product_name'];
        if ($this->request->is('post')) {
            $this->request->data['StrategyActivity']['budget_year_id'] = $year;
            $this->request->data['StrategyActivity']['budget_year'] = $year;
            $this->request->data['StrategyActivity']['strategy_gov_id'] = $dataall_StrategyProduct['StrategyProduct']['strategy_gov_id'];
            $this->request->data['StrategyActivity']['strategy_plan_id'] = $dataall_StrategyProduct['StrategyProduct']['strategy_plan_id'];
            $this->request->data['StrategyActivity']['strategy_product_id'] = $dataall_StrategyProduct['StrategyProduct']['id'];

            $checkCode = $this->Common->checkCode('budget.strategy_activitys', $this->request->data['StrategyActivity']['code'], $dataall_StrategyProduct['StrategyProduct']['budget_year_id'], 'StrategyProduct', $dataall_StrategyProduct['StrategyProduct']['id']);

            if ($checkCode == false) {
                $this->Flash->error(__('The event maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }
            $this->StrategyActivity->create();
            if ($this->StrategyActivity->save($this->request->data)) {
                $this->Flash->success(__('The event maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $dataall_StrategyProduct['StrategyProduct']['strategy_gov_id']));
            } else {
                $this->Flash->error(__('The event maneuver could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('year', 'data_strategy_gov_name', 'data_strategy_plan_name', 'data_strategy_product_name'));
    }

    /**
     *
     * edit method for event maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of event maneuver id [PK] 
     * @since   2017-04-18 17:39:44
     * @license Zicure Corp. 
     * @return  void
     */
    public function edit($id = null) {

        if (!$this->StrategyActivity->exists($id)) {
            //throw new NotFoundException(__('Invalid event maneuver'));
            $this->Flash->error(__('Invalid not found event maneuver with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $options = array('conditions' => array('StrategyActivity.' . $this->StrategyActivity->primaryKey => $id));
        $data = $this->StrategyActivity->find('first', $options);

        $dataall_StrategyProduct = $this->StrategyProduct->find('first', array('conditions' => array('StrategyProduct.id' => $data['StrategyActivity']['strategy_product_id'])));
        $data_strategy_plan_name = $this->StrategyPlan->find('first', array('conditions' => array('StrategyPlan.id' => $dataall_StrategyProduct['StrategyProduct']['strategy_plan_id'])));
        $data_strategy_gov_name = $this->StrategyGov->find('first', array('conditions' => array('StrategyGov.id' => $dataall_StrategyProduct['StrategyProduct']['strategy_gov_id'])));


        if ($this->request->is(array('post', 'put'))) {
//            debug($this->request->data);
//            die;
//            $this->request->data['id'] = $id;
            $checkCode = $this->Common->checkCode('budget.strategy_activitys', $this->request->data['StrategyActivity']['code'], $dataall_StrategyProduct['StrategyProduct']['budget_year_id'], 'StrategyProduct', $dataall_StrategyProduct['StrategyProduct']['id']);
            if ($checkCode == false) {
                $this->Flash->error(__('The event maneuver code could not be saved. Please, try again.'));
                $this->redirect($this->referer());
            }

            if ($this->StrategyActivity->save($this->request->data)) {
                $this->Flash->success(__('The event maneuver has been saved.'));
                return $this->redirect(array('controller' => 'StrategicManeuvers', 'action' => 'view', $data['StrategyActivity']['strategy_gov_id']));
            } else {
                $this->Flash->error(__('The event maneuver could not be saved. Please, try again.'));
            }
        } else {

            $this->request->data = $data;
        }
        $this->set(compact('data_strategy_gov_name', 'data_strategy_plan_name', 'dataall_StrategyProduct', 'data'));
    }

    /**
     *
     * delete method delete for event maneuver
     * @author  sarawutt.b 
     * @param   string $id as integer of event maneuver id [PK] 
     * @since   2017-04-18 17:39:44
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null, $model = null, $sector = 'A', $strategicId = null) {
        $this->StrategyActivity->id = $id;
        if (!$this->StrategyActivity->exists()) {
            //throw new NotFoundException(__('Invalid event maneuver'));
            $this->Flash->error(__('Invalid not found event maneuver with id %s please try again !', $id));
            return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
        }

        $hasChild = $this->StrategyActivity->delete($id);
        if ($hasChild) {
            $this->Flash->success(__('The event maneuver has been deleted.'));
        } else {
            $this->Flash->error(__('ไม่สามารถลบข้อมูลได้ เนื่องจากมีข้อมูลส่วนอื่นที่เกี่ยวข้องอยู่ กรุณาลบข้อมูลส่วนที่เกี่ยวข้องก่อน !!!'));
        }
        return $this->redirect("/StrategicManeuvers/view/{$strategicId}");
    }

}
