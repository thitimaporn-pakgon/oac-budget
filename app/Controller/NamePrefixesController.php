<?php

App::uses('AppController', 'Controller');

/**
 * NamePrefixes Controller
 *
 * @property NamePrefix $NamePrefix
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class NamePrefixesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $conditions = array();
        if (empty($this->passedArgs['NamePrefixes'])) {
            $this->passedArgs['NamePrefixes'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['NamePrefixes'];
        }

        if (!empty($this->request->data)) {
            if (!empty($this->request->data['NamePrefixes']['name'])) {
                $conditions['OR'][] = array('LOWER(NamePrefix.name) LIKE ' => '%' . strtolower($this->request->data['NamePrefixes']['name']) . '%');
                $conditions['OR'][] = array('LOWER(NamePrefix.name_eng) LIKE ' => '%' . strtolower($this->request->data['NamePrefixes']['name']) . '%');
                $conditions['OR'][] = array('LOWER(NamePrefix.long_name) LIKE ' => '%' . strtolower($this->request->data['NamePrefixes']['name']) . '%');
            }
            if (!empty($this->request->data['NamePrefixes']['status'])) {
                $conditions['AND'][] = array('LOWER(NamePrefix.status)' => $this->request->data['NamePrefixes']['status']);
            }
        } else {
            $conditions[] = array('NamePrefix.status' => 'A');
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('NamePrefix.order_no' => 'ASC', 'NamePrefix.name' => 'ASC', 'NamePrefix.long_name' => 'ASC', 'NamePrefix.created' => 'DESC'),
            'limit' => Configure::read('Pagination.Limit')
        );
        $this->set('namePrefixes', $this->Paginator->paginate('NamePrefix'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->NamePrefix->exists($id)) {
            $this->saveAccessLog('เข้าดูข้อมูลของคำนำหน้าชื่อ');
            throw new NotFoundException(__('Invalid name prefix'));
        }
        $options = array('conditions' => array('NamePrefix.' . $this->NamePrefix->primaryKey => $id));
        $this->set('namePrefix', $this->NamePrefix->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['NamePrefix']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->NamePrefix->create();
            if ($this->NamePrefix->save($this->request->data)) {
                $this->saveAccessLog('เพิ่มข้อมูลของคำนำหน้าชื่อ');
                $this->Session->setFlash(__('The name prefix has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The name prefix could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->NamePrefix->exists($id)) {
            throw new NotFoundException(__('Invalid name prefix'));
        }
        $this->request->data['NamePrefix']['update_uid'] = $this->Session->read('Auth.User.id');
        if ($this->request->is(array('post', 'put'))) {
            if ($this->NamePrefix->save($this->request->data)) {
                $this->saveAccessLog('แก้ไขข้อมูลของคำนำหน้าชื่อ');
                $this->Session->setFlash(__('The name prefix has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The name prefix could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('NamePrefix.' . $this->NamePrefix->primaryKey => $id));
            $this->request->data = $this->NamePrefix->find('first', $options);
        }
    }

    /**
     *
     * delete method delete for name prefix
     * @author  sarawutt.b 
     * @param   string $id as integer of name prefix id [PK] 
     * @since   2017-04-18 18:00:37
     * @license Zicure Corp. 
     * @return  void
     */
    public function delete($id = null) {
        $this->NamePrefix->id = $id;
        if (!$this->NamePrefix->exists()) {
            $this->Flash->error(__('Invalid not found name prefix with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }

        $this->request->onlyAllow('post', 'delete');
        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->NamePrefix->delete()) {
                $responds = array('message' => __('The name prefix has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The name prefix could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->NamePrefix->delete()) {
                $this->Flash->success(__('The name prefix has been deleted.'));
            } else {
                $this->Flash->error(__('The name prefix could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the name prefix with id = %s', $id);
    }

    public function check_already_name($name = null, $id = null) {
        $conditions = (is_null($id) || empty($id) || !is_numeric($id)) ? array() : array('id !=' => $id);
        $this->autoRender = false;
        echo ($this->NamePrefix->find('count', array('conditions' => array($conditions, 'name' => $name))) > 0) ? 'Y' : 'N';
    }

}
