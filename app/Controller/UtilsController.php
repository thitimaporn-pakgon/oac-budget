<?php

class UtilsController extends AppController {

    var $name = 'Utils';
    //var $uses = array('Region', 'Province', 'District', 'SubDistrict', 'Zipcode', 'User', 'SysAction', 'Department', 'ExpenseHeaderManage', 'Inbox', 'Outbox');
    var $uses = array('StrategyGov', 'StrategyPlan', 'StrategyProduct', 'StrategyActivity', 'StrPlan', 'Project');
    var $helpers = array('Html', 'Form', 'Session', 'Paginator');

    /**
     * Finde system action thougth ajax with fillter
     * @params id of action controllers on UUID format
     * @author Sarawutt.b
     * @Since 2013.11.04
     */
    function findActionsByControllers($id = '') {
        $this->autoRender = false;
        $this->layout = 'ajax';
        if (trim($id) == '') {
            $data = $this->SysAction->find('list', array('order' => array('ascii(SysAction.name) ASC')));
        } else {
            $data = $this->SysAction->find('list', array('order' => array('ascii(SysAction.name) ASC'), 'conditions' => array('SysAction.sys_controller_id' => $id)));
        }

        $selectactions = "<select id='SearchSysActionId' name='data[Search][sys_action_id]' style='width:200px'><option value='' selected='selected'>" . __("select all") . "</option>";
        foreach ($data as $k => $v) {
            $selectactions .= "<option value='$k'>$v</option>";
        }
        echo $selectactions . "</select>";
    }

    /**
     * Finde User in the system thougth ajax with fillter
     * @params id of stakeholder on UUID format
     * @author Sarawutt.b
     * @Since 2013.11.04
     */
    function findUsersByStakeholders($id = '') {
        $this->autoRender = false;
        $this->layout = 'ajax';
        if (trim($id) == '') {
            $data = $this->User->find('list', array('order' => array('ascii(User.display) ASC')));
        } else {
            $data = $this->User->find('list', array('order' => array('ascii(User.display) ASC'), 'conditions' => array('User.stakeholder_id' => $id)));
        }

        $selectusers = "<select id='SearchUserId' name='data[Search][user_id]' style='width:200px'><option value='' selected='selected'>.__('Select All').</option>";
        foreach ($data as $k => $v) {
            $selectusers .= "<option value='$k'>$v</option>";
        }
        echo $selectusers . "</select>";
    }

    /**
     *
     * Find thai province call form ajax
     * @param string id of area
     * @param optional string content of empty selected
     */
    function findProvince($id = ' ') {

        $this->autoRender = false;
        $this->layout = 'ajax';
        if (trim($id) == '') {
            $data = $this->Province->find('list', array('order' => array('Province.name ASC')));
        } else {
            $data = $this->Province->find('list', array('order' => array('Province.name ASC'), 'conditions' => array('Province.id LIKE' => $id . "%")));
        }

        $selectprovince = "<option value='' selected='selected' style='width:200px'>.__('Select Provice').</option>";

        foreach ($data as $k => $v) {
            $selectprovince .= "<option value='$k'>$v</option>";
        }
        echo $selectprovince;
    }

    /**
     *
     * Find thai amphur call form ajax
     * @param string id of province
     * @param optional string content of empty selected
     */
    function findDistrict($id = ' ', $empty = '---- please select ----') {
        $empty = __($empty);
        if ($empty == '1') {
            $empty = __('---- please select ----');
        }

        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->District->find('list', array('order' => array('District.name ASC'), 'conditions' => array('District.id LIKE' => $id . "%")));
        $selectamphur = "<option value='' selected='selected' class='chosen-select' style='width:200px'>{$empty}</option>";

        foreach ($data as $k => $v) {
            $selectamphur .= "<option value='$k'>$v</option>";
        }
        echo $selectamphur;
    }

    /**
     *
     * Find thai thambon call form ajax
     * @param string id of amphur
     * @param optional string content of empty selected
     */
    function findSubDistrict($id = ' ', $empty = '---- please select ----') {
        $empty = __($empty);
        if ($empty == '1' || $id == '1') {
            $empty = __('---- please select ----');
        }

        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->SubDistrict->find('list', array('conditions' => array('SubDistrict.id LIKE' => $id . "%"), 'order' => array('SubDistrict.name ASC')));
        $selecttambon = "<option value='' selected='selected' class='chosen-select' style='width:200px'>{$empty}</option>";

        foreach ($data as $k => $v) {
            $selecttambon .= "<option value='$k'>$v</option>";
        }
        echo $selecttambon;
    }

    /**
     *
     * Find system zipcode
     * @param	string id of tambon
     */
    function findZipcode($tambon_id = "", $ajaxload = NULL) {
        $this->autoRender = false;
        $this->layout = 'ajax';
        if (empty($tambon_id)) {
            echo "";
        } else {
            echo $this->SubDistrict->getZipcode($tambon_id);
        }
    }

    public function findActionByControllerId($controllerId = null, $empty = '---- please select ----') {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->SysAction->findActionByControllerId($controllerId);
        $html = "<option value='' selected='selected' class='chosen-select' style='width:200px'>{$empty}</option>";

        foreach ($data as $k => $v) {
            $key = $v['SysAction']['id'];
            $val = $v['SysAction']['name'];
            $html .= "<option value='{$key}'>{$val}</option>";
        }
        echo $html;
    }

    public function getPositionByUserId($id = null) {
        $this->autoRender = false;
        $this->layout = 'ajax';
        echo $this->User->getPositionByUserId($id);
    }

    /**
     *
     * Function find for department code with department id
     * @author sarawutt.b
     * @param type $id as a integr of department id
     * @return string department code if exist otherwise retur 9999
     */
    public function getDepartmentCode($id = null) {
        $this->autoRender = false;
        $this->layout = 'ajax';
        echo $this->Department->getDepartmentCode(array('id' => $id));
    }

    /**
     *
     * Function Filter Drilldown
     * @author thawatchai.T
     */
     public function findfilter($year = null, $strGov = null, $strPlan = null, $strProduct = null){
       $this->autoRender = false;
       $this->layout = 'ajax';
       $empty = __('---- please select ----');
       $selectdata = "<option value='' selected='selected' class='chosen-select' style='width:200px'>{$empty}</option>";

       if ($this->request->is('post')) {
         $boxes =  $this->request->data['boxes'];


           if($boxes == 'StrategyGov'){
             $showlist = $this->Project->StrategyGov->find('all', array(
               'conditions' => array(
                  'StrategyGov.budget_year_id' => $this->request->data['budget_year']
               ),
               'fields' => array('StrategyGov.id','StrategyGov.strategy_gov_name')
             ));
               foreach($showlist as $key=>$list){
                 $selectdata .= "<option value='" . $list['StrategyGov']['id'] . "'>" . $list['StrategyGov']['strategy_gov_name'] . "</option>";
               }
           }

           if($boxes == 'StrategyPlan'){
              $showlist = $this->Project->StrategyPlan->find('all', array(
                'conditions' => array(
                'StrategyPlan.budget_year_id' => $this->request->data['budget_year'],
                'StrategyPlan.strategy_gov_id' => $this->request->data['strategy'],
              ),
                'fields' => array('StrategyPlan.id','StrategyPlan.strategy_plan_name')
              ));
              foreach($showlist as $key=>$list){
                $selectdata .= "<option value='" . $list['StrategyPlan']['id'] . "'>" . $list['StrategyPlan']['strategy_plan_name'] . "</option>";
              }
           }

           if($boxes == 'StrategyProduct'){
              $showlist = $this->Project->StrategyProduct->find('all', array(
                'conditions' => array(
                'StrategyProduct.budget_year_id' => $this->request->data['budget_year'],
                'StrategyProduct.strategy_gov_id' => $this->request->data['strategy'],
                'StrategyProduct.strategy_plan_id' => $this->request->data['plan'],
              ),
                'fields' => array('StrategyProduct.id','StrategyProduct.strategy_product_name')
              ));
              foreach($showlist as $key=>$list){
                $selectdata .= "<option value='" . $list['StrategyProduct']['id'] . "'>" . $list['StrategyProduct']['strategy_product_name'] . "</option>";
              }
           }

           if($boxes == 'StrategyActivity'){
              $showlist = $this->Project->StrategyActivity->find('all', array(
                'conditions' => array(
                'StrategyActivity.budget_year_id' => $this->request->data['budget_year'],
                'StrategyActivity.strategy_gov_id' => $this->request->data['strategy'],
                'StrategyActivity.strategy_plan_id' => $this->request->data['plan'],
                'StrategyActivity.strategy_product_id' => $this->request->data['product'],


              ),
                'fields' => array('StrategyActivity.id','StrategyActivity.strategy_act_name')
              ));
              foreach($showlist as $key=>$list){
                $selectdata .= "<option value='" . $list['StrategyActivity']['id'] . "'>" . $list['StrategyActivity']['strategy_act_name'] . "</option>";
              }
           }

          if($boxes == 'StrPlan'){
              $showlist = $this->Project->StrPlan->find('all', array(
                'conditions' => array(
                'StrPlan.group_plan_id' => $this->request->data['groupPlan'],

              ),
                'fields' => array('StrPlan.id','StrPlan.plan_name')
              ));
              foreach($showlist as $key=>$list){
                $selectdata .= "<option value='" . $list['StrPlan']['id'] . "'>" . $list['StrPlan']['plan_name'] . "</option>";
              }
          }

       }
       echo $selectdata;
     }


}
?>
