<?php

App::uses('AppController', 'Controller');

App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class UsersController extends AppController {

    public $name = 'Users';
    public $components = array('Paginator', 'Flash', 'Session', 'Utility', 'RequestHandler');
    public $uses = array('User', 'Department', 'Role');

    public function beforeFilter() {
        $this->Auth->allow('login', 'logout', 'pin');
        parent::beforeFilter();
    }


    public function login() {
        $this->layout = 'signin';

		if($this->request->is('post')) {

			if($this->Auth->login()) {

        $getDataUser = $this->User->find('first', array(
          'conditions' => array(
            'User.id' => $this->Session->read('Auth.User.id')
          )
        ));
        $this->Session->write('Auth', $getDataUser);
				$this->redirect($this->Auth->redirectUrl('/Home/index'));

			} else {

				$this->Session->setFlash(__('Username or password is incorrect. Please, try again.'));
                $this->redirect(array('action' => 'login'));
			}
		}


		/*
        if ($this->request->is('post')) {
            $authRecapcha = true;
            //Check for google recapcha authentication
            if (Configure::read('CORE.ENABLED.AUTH.RECAPCHA') === true) {
                $params = array('secret' => Configure::read('CORE.PROJECT.SECRETKEY'), 'response' => $this->request->data['g-recaptcha-response'], 'remoteip' => $_SERVER['REMOTE_ADDR']);
                $result = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?' . http_build_query($params)));

                if (!$result->success) {
                    $this->Session->setFlash(__('Please input Captcha code.'));
                    $this->redirect(array('action' => 'login'));
                }
            }

            if ($this->Auth->login() && ($authRecapcha === true)) {
                if ($this->Session->read('Auth.User.loginfail') < 4) {
                    if ($this->Session->check('Auth.User')) {
                        $data['User']['id'] = $this->getCurrenSessionUserId();
                        $data['User']['last_login'] = date('Y-m-d H:i:s');
                        $this->User->save($data);
                    }
                    $this->Session->write('xPinAuth.User', $this->Session->read('Auth.User'));
                    $this->Session->delete('Auth.User');
                    $this->redirect(array('controller' => 'users', 'action' => 'pin'));
                } else {

                    $this->Session->setFlash(__('User has been Locked'));
                    $this->redirect(array('controller' => 'Users', 'action' => 'login'));
                }
            } // user and check 1 or null
            else {
                $this->User->UpdateLogin($this->request->data['User']['username'], $this->Auth->login());
                $this->Session->setFlash(__('Username or password is incorrect. Please, try again.'));
                $this->redirect(array('action' => 'login'));
            }
        }
		*/
    }


	/* backup ของเดิม
    public function pin() {
        if (!$this->Session->check('xPinAuth.User.username')) {
            $this->Session->destroy();
            $this->redirect(array('action' => 'login'));
        }
        $this->layout = 'signin';
        if ($this->request->is('post')) {
            $BlowfishPasswordHasher = new BlowfishPasswordHasher();
            $pin = $BlowfishPasswordHasher->check($this->request->data['User']['password2'], $this->Session->read('xPinAuth.User.password2'));

            if ($pin == 'TRUE') {
                $this->saveAccessLog('เข้าสู่ระบบ');
                $this->Session->write('Auth.User', $this->Session->read('xPinAuth.User'));
                $this->Session->delete('xPinAuth.User');
                return $this->redirect($this->Auth->redirectUrl('/Home/index'));
            } else {
                $this->User->UpdateLogin($this->Session->read('xPinAuth.User.username'), '');
                $this->Session->destroy();
                $this->Session->setFlash(__('PIN is incorrect. Please, try again.'));
                $this->redirect(array('action' => 'login'));
            }
        }
    }
	*/

    public function logout() {
        //$this->saveAccessLog('ออกจากระบบ');
        $this->Session->destroy();
        return $this->redirect($this->Auth->logout());
    }

	/* backup ของเดิม
    public function index() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('User.username' => 'ASC', 'User.first_name' => 'ASC', 'User.last_name' => 'ASC', 'created' => 'ASC');
        $this->saveAccessLog('ดูข้อมูลผู้ใช้งาน');
        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trim_all_data($this->request->data);
            //Find by username
            if (!empty($this->request->data['Search']['username'])) {
                $conditions[] = array('LOWER(User.username) ILIKE' => '%' . strtolower($this->request->data['Search']['username']) . '%');
            }
            //Find by username
            if (!empty($this->request->data['Search']['name'])) {
                $conditions['OR'][] = array('LOWER(User.first_name) ILIKE' => '%' . $this->request->data['Search']['name'] . '%');
                $conditions['OR'][] = array('LOWER(User.last_name) ILIKE' => '%' . $this->request->data['Search']['name'] . '%');
                $conditions['OR'][] = array("CONCAT(User.first_name,' ',User.last_name) ILIKE '%{$this->request->data['Search']['name']}%'"); //array('User.last_name' => $this->request->data['Search']['name']);
            }

            //Find by personal_card_no
            if (!empty($this->request->data['Search']['personal_card_no'])) {
                $conditions[] = array('LOWER(User.personal_card_no) ILIKE' => '%' . $this->request->data['Search']['personal_card_no'] . '%');
            }

            //Find by military no
            if (!empty($this->request->data['Search']['military_no'])) {
                $conditions[] = array('LOWER(User.military_no) ILIKE' => '%' . $this->request->data['Search']['military_no'] . '%');
            }

            //Find by email
            if (!empty($this->request->data['Search']['emial'])) {
                $conditions[] = array('LOWER(User.email) ILIKE' => '%' . $this->request->data['Search']['emial'] . '%');
            }

            //Find by phone
            if (!empty($this->request->data['Search']['phone_no'])) {
                $conditions[] = array('LOWER(User.phone_no) ILIKE' => '%' . $this->request->data['Search']['phone_no'] . '%');
            }

            //Find by departmen_id
            if (!empty($this->request->data['Search']['department_id'])) {

                $conditions[] = array('User.department_id' => $this->request->data['Search']['department_id']);
            }

            //Find by role_id
            if (!empty($this->request->data['Search']['role_id'])) {
                $conditions[] = array('User.role_id' => $this->request->data['Search']['role_id']);
            }

            //Find by status
            if (!empty($this->request->data['Search']['status'])) {
                $conditions[] = array('User.status' => $this->request->data['Search']['status']);
            }

            //Find by loginfail
            if (!empty($this->request->data['Search']['loginfail'])) {
                $conditions[] = array('User.loginfail' . $this->request->data['Search']['loginfail']);
            }
        } else {
            $conditions[] = array('User.status' => 'A');
        }

        $paginate = array(
            'User' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        $this->paginate = $paginate;

        $departments = $this->getEmptySelect() + $this->Department->find('list');
        $roles = $this->getEmptySelect() + $this->Role->find('list');
        $users = $this->Paginator->paginate('User');
        $this->set(compact('departments', 'roles'));
        $this->set(array('users' => $users, '_serialize' => array('users')));
    }


    public function Org() {
        if (empty($this->passedArgs['Search'])) {
            $this->passedArgs['Search'] = $this->request->data;
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->passedArgs['Search'];
        }
        $paginate = array();
        $conditions = array();
        $order = array('Department.id' => 'ASC', 'Department.full_name' => 'ASC', 'Department.created' => 'ASC');
        $this->saveAccessLog('ดูข้อมูลผู้ใช้งานในหน่วยงาน');
        if (!empty($this->request->data)) {
            $this->request->data = $this->Utility->trim_all_data($this->request->data);
            //Find by username
            if (!empty($this->request->data['Search']['full_name'])) {
                $conditions[] = array('Department.full_name' => $this->request->data['Search']['full_name']);
            }
        }
        $paginate = array(
            'Department' => array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => Configure::read('Pagination.Limit')
        ));
        $this->paginate = $paginate;
        $this->Department->recursive = -1;
        $this->set('departments', $this->Paginator->paginate('Department'));
    }


    public function add() {
        if ($this->request->is('post')) {

            $checkUm = $this->User->find('list', array('conditions' => array('User.username' => $this->request->data['User']['username'])));

            //process file
            $hasFile = FALSE;
            $resultOK = array(); //Keep file handler
            $file_path = "";
            //Reader from file attachment file
            if (!empty($this->request->data['User']['picture_path']['name'])) {
                $file_path = 'uploadfile/img';
                $resultOK = $this->uploadFiles($file_path, $this->request->data['User']['picture_path']);
                if (isset($resultOK['errors'])) {
                    $this->Session->setFlash(__("{$resultOK['errors'][0]}. Please, try again."));
                    $this->redirect($this->referer());
                }
                $hasFile = TRUE;
            }
            $this->request->data['User']['picture_path'] = '/uploadfile/img/' . $resultOK['upfilename'][0];
            $this->request->data['User']['status'] = 'A';
            $this->request->data['User']['create_uid'] = $this->Session->read('Auth.User.id');
            $this->User->create();
            if (empty($checkUm)) {
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('The user has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $districts = $subDistricts = $this->getEmptySelect();
        $this->set(compact('districts', 'subDistricts'));
    }


    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //process file
            $hasFile = FALSE;
            $resultOK = array(); //Keep file handler
            $file_path = "";
            //Reader from file attachment file
            if (!empty($this->request->data['User']['picture_path']['name'])) {
                $file_path = 'uploadfile/img';
                $resultOK = $this->uploadFiles($file_path, $this->request->data['User']['picture_path']);
                if (isset($resultOK['errors'])) {
                    $this->Session->setFlash(__("{$resultOK['errors'][0]}. Please, try again."));
                    $this->redirect($this->referer());
                }
                $hasFile = TRUE;
                $this->request->data['User']['picture_path'] = '/uploadfile/img/' . $resultOK['upfilename'][0];
            } else {
                unset($this->request->data['User']['picture_path']);
            }
            $this->request->data['User']['update_uid'] = $this->getCurrenSessionUserId();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }


        $namePrefixes = $this->User->NamePrefix->find('list');
        $positions = $this->User->Position->find('list');
        $roles = $this->User->Role->find('list');
        $departments = $this->User->Department->find('list');
        $provinces = $this->User->Province->find('list');
        $districts = $this->User->District->find('list', array('conditions' => array('District.id' => $this->request->data['User']['district_id'])));
        $subDistricts = $this->User->SubDistrict->find('list', array('conditions' => array('SubDistrict.id' => $this->request->data['User']['sub_district_id'])));
        //$this->set(compact('namePrefixes', 'positions', 'roles', 'departments', 'provinces', 'districts', 'subDistricts'));
        $this->set(compact('namePrefixes', 'positions', 'roles', 'departments', 'provinces', 'districts', 'subDistricts', 'id'));
    }

    public function changprofile($id = null) {
        $this->request->data['User']['update_uid'] = $this->getCurrenSessionUserId();
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('The user has been saved.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
        }
    }

    public function changpassword($id = null) {
        $this->request->data['User']['update_uid'] = $this->getCurrenSessionUserId();
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('The user has been saved.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
        }
    }

    public function changpin($id = null) {
        $this->request->data['User']['update_uid'] = $this->getCurrenSessionUserId();
        $BlowfishPasswordHasher = new BlowfishPasswordHasher();
        $this->request->data['User']['password2'] = $BlowfishPasswordHasher->hash($this->request->data['User']['password2']);
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('The user has been saved.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
        }
    }

    public function changpicture($id = null) {
        if ($this->request->is(array('post', 'put'))) {
            //process file
            $hasFile = FALSE;
            $resultOK = array(); //Keep file handler
            $file_path = "";
            //Reader from file attachment file
            if (!empty($this->request->data['User']['picture_path']['name'])) {
                $file_path = 'uploadfile/img';
                $resultOK = $this->uploadFiles($file_path, $this->request->data['User']['picture_path']);
                if (isset($resultOK['errors'])) {
                    $this->Session->setFlash(__("{$resultOK['errors'][0]}. Please, try again."));
                    $this->redirect($this->referer());
                }
                $hasFile = TRUE;
                $this->request->data['User']['picture_path'] = '/uploadfile/img/' . $resultOK['upfilename'][0];
            } else {
                unset($this->request->data['User']['picture_path']);
            }
        }
        $this->request->data['User']['update_uid'] = $this->getCurrenSessionUserId();
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('The user has been saved.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
        }
    }


    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Flash->error(__('Invalid not found user with id %s please try again !', $id));
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->allowMethod('post', 'delete');

        //Make Ajax respond to delete
        if ($this->request->is('ajax')) {
            $this->autoRender = $this->layout = false;
            $responds = array();
            if ($this->User->delete()) {
                $responds = array('message' => __('The user has been deleted.'), 'class' => 'success');
            } else {
                $responds = array('message' => __('The user could not be deleted. Please, try again.'), 'class' => 'danger');
            }
            echo json_encode($responds);
            exit;
        } else {
            if ($this->User->delete()) {
                $this->Flash->success(__('The user has been deleted.'));
            } else {
                $this->Flash->error(__('The user could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(array('action' => 'index'));
        //$this->saveAccessLog('Delete for the Expense Detail Allocated with id = %s', $id);
    }


    public function unlock($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $data = array('id' => $id, 'loginfail' => '0', 'update_uid' => $this->getCurrenSessionUserId());
        if ($this->User->save($data)) {
            $this->Session->setFlash(__('The user has been unlocked.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The user could not be unlocked. Please, try again.'));
        }
    }
	*/
}
