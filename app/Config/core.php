<?php

/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
//setLocale(LC_ALL, 'deu');
//Configure::write('Config.language', 'deu');

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
Configure::write('debug', 2);

/**
 * Configure the Error handler used to handle errors for your application. By default
 * ErrorHandler::handleError() is used. It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - integer - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
Configure::write('Error', array(
    'handler' => 'ErrorHandler::handleError',
    'level' => E_ALL & ~E_DEPRECATED,
    'trace' => true
));

/**
 * Configure the Exception handler used for uncaught exceptions. By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed. When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions. If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 * - `skipLog` - array - list of exceptions to skip for logging. Exceptions that
 *   extend one of the listed exceptions will also be skipped for logging.
 *   Example: `'skipLog' => array('NotFoundException', 'UnauthorizedException')`
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
Configure::write('Exception', array(
    'handler' => 'ErrorHandler::handleException',
    'renderer' => 'ExceptionRenderer',
    'log' => true
));

/**
 * Application wide charset encoding
 */
Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below. But keep in mind
 * that plugin assets such as images, CSS and JavaScript files
 * will not work without URL rewriting!
 * To work around this issue you should either symlink or copy
 * the plugin assets into you app's webroot directory. This is
 * recommended even when you are using mod_rewrite. Handling static
 * assets through the Dispatcher is incredibly inefficient and
 * included primarily as a development convenience - and
 * thus not recommended for production applications.
 */
//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * To configure CakePHP to use a particular domain URL
 * for any URL generation inside the application, set the following
 * configuration variable to the http(s) address to your domain. This
 * will override the automatic detection of full base URL and can be
 * useful when generating links from the CLI (e.g. sending emails)
 */
//Configure::write('App.fullBaseUrl', 'http://example.com');

/**
 * Web path to the public images directory under webroot.
 * If not set defaults to 'img/'
 */
//Configure::write('App.imageBaseUrl', 'img/');

/**
 * Web path to the CSS files directory under webroot.
 * If not set defaults to 'css/'
 */
//Configure::write('App.cssBaseUrl', 'css/');

/**
 * Web path to the js files directory under webroot.
 * If not set defaults to 'js/'
 */
//Configure::write('App.jsBaseUrl', 'js/');

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 * 	`admin_index()` and `/admin/controller/index`
 * 	`manager_index()` and `/manager/controller/index`
 */
//Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 */
//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 */
//Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler. Expects an array of callables,
 *    that can be used with `session_save_handler`. Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 */
Configure::write('Session', array(
    'defaults' => 'php',
    'cookieTimeout' => 0,
    'checkAgent'  => false,
    'timeout' => 180
));
/**
 * A random string used in security hashing methods.
 */
Configure::write('Security.salt', 'DYhG93b0qyJfIxfs2guVoUubWwvniR2G0FPakgon');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
Configure::write('Security.cipherSeed', '76859309657453542496749682016');


Configure::write('Security.level', 'high');
/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a query string parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JsHelper::link().
 */
//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The class name and database used in CakePHP's
 * access control lists.
 */
Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
//date_default_timezone_set('UTC');

/**
 * `Config.timezone` is available in which you can set users' timezone string.
 * If a method of CakeTime class is called with $timezone parameter as null and `Config.timezone` is set,
 * then the value of `Config.timezone` will be used. This feature allows you to set users' timezone just
 * once instead of passing it each time in function calls.
 */
//Configure::write('Config.timezone', 'Europe/Paris');

/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'File', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, //[optional]
 * 		'mask' => 0664, //[optional]
 * 	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Apc', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Xcache', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 * 		'user' => 'user', //user from xcache.admin.user settings
 * 		'password' => 'password', //plaintext password (xcache.admin.pass)
 * 	));
 *
 * Memcached (http://www.danga.com/memcached/)
 *
 * Uses the memcached extension. See http://php.net/memcached
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Memcached', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'persistent' => 'my_connection', // [optional] The name of the persistent connection.
 * 		'compress' => false, // [optional] compress data in Memcached (slower, but uses less memory)
 * 	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Wincache', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 */
/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
$engine = 'File';

// In development mode, caches should expire quickly.
$duration = '+999 days';
if (Configure::read('debug') > 0) {
    $duration = '+10 seconds';
}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'myapp_';

/**
 * Configure the cache used for general framework caching. Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config('_cake_core_', array(
    'engine' => $engine,
    'prefix' => $prefix . 'cake_core_',
    'path' => CACHE . 'persistent' . DS,
    'serialize' => ($engine === 'File'),
    'duration' => $duration
));

/**
 * Configure the cache for model and datasource caches. This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
    'engine' => $engine,
    'prefix' => $prefix . 'cake_model_',
    'path' => CACHE . 'models' . DS,
    'serialize' => ($engine === 'File'),
    'duration' => $duration
));

/**
 * | ---------------------------------------------------------------------------------------------------------------------------------------------------
 * |  Packgon File Configuration of each the application module
 * | ---------------------------------------------------------------------------------------------------------------------------------------------------
 */
/**
 * @var Configuration for pagination on cake render in paginate of each page
 * @author Sarawutt.b
 * @Since 20131031
 */
Configure::write('Pagination.Prev.Text', __('Prev'));
Configure::write('Pagination.Next.Text', __('Next'));
Configure::write('Pagination.Next.Class', 'next disabled');
Configure::write('Pagination.Prev.Class', 'prev disabled');
Configure::write('Pagination.Modulus', 9);
Configure::write('Pagination.Separate', '');

/**
 * @var Limit of paginate in cake php when they find data to any page
 * @author Sarawutt.b
 * @Since 20131031
 */
Configure::write('Pagination.Limit', 20);

/**
 * @var Configuration for production deployment if is deploy time make this to TRUE
 * @author     Sarawutt.b
 * @Since      2014/05/23
 */
Configure::write('DEPLOYTOPRODUCTIONTIME', FALSE);

/**
 * @var Configuration for permission mode TRUE is permission set FALSE when don't want is permission normally on develop mode
 * @author Sarawutt.b
 * @Since 20131031
 */
Configure::write('Permission.ModePermission', FALSE);

/**
 * @var Configuration for multiple language
 * @author     Sarawutt.b
 * @Since      2015/05/21
 */
Configure::write('Config.language', 'tha');
/**
 * @var Configuration not found data language
 * @author     Sarawutt.b
 * @Since      2015/05/21
 */
Configure::write('dataNotFound', 'Data not found');


Configure::write('Zicure.Currency', ' ฿ ');
Configure::write('Zicure.TestMode', TRUE); //eRetail testing mode set to true when test mode and set to false when production
Configure::write('Zicure.Retail.CanRecievedMode', TRUE); //constain for make if it true retailer can recieved product from RD

Configure::write('Zicure.Retail.MaxRetailOrderTotalAmount', 1 * 1000000); //Constance for maximum retail can buy a product each of a list order command
Configure::write('Zicure.Retail.MaxPurchaseOrderTotalAmount', 10 * 1000000); //Constance for maximum can purchase each of a list of order command
Configure::write('Zicure.Report.Connection.Driver', 'jdbc:mysql://localhost/eretail'); // Driver for report database in php bridge
Configure::write('Zicure.Report.Connection.UserDatabase', 'eportal'); //Username access for database host
Configure::write('Zicure.Report.Connection.UserPassword', 'password'); //User password for root in MySQL host

Configure::write('Zicure.Report.Retail.PurchaseOrder', realpath('..') . DS . 'report' . DS . 'phurchase_order.jrxml'); //Knowlaged purchase order report
Configure::write('Zicure.Report.Retail.RetailOrder', realpath('..') . DS . 'report' . DS . 'retails_order.jrxml'); //Report for retail order print to paper
Configure::write('Zicure.Report.Retail.Receipt', realpath('..') . DS . 'report' . DS . 'receipt.jrxml'); //Report for the receipt
Configure::write('Zicure.Report.Retail.ShipingInvoiceTax', realpath('..') . DS . 'report' . DS . 'shiping_invoice_tax.jrxml'); //Shiping invoince and tax Report
Configure::write('Zicure.Report.Retail.rptExample1', realpath('..') . DS . 'report' . DS . 'rptExample1.jrxml');
Configure::write('Zicure.Report.Retail.rptExample2_params', realpath('..') . DS . 'report' . DS . 'rptExample2_params.jrxml');

Configure::write('Zicure.Report.Retail.rptExample0001', realpath('..') . DS . 'report' . DS . 'rptExample001.jrxml');
Configure::write('Zicure.Report.Oacrmd.troop_report', realpath('..') . DS . 'report' . DS . 'troop_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.reasearch_report', realpath('..') . DS . 'report' . DS . 'reasearch_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.maintenance_report', realpath('..') . DS . 'report' . DS . 'maintenance_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.management_budget_report', realpath('..') . DS . 'report' . DS . 'management_budget_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.medical_budget_report', realpath('..') . DS . 'report' . DS . 'medical_budget_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.construct_budget_report', realpath('..') . DS . 'report' . DS . 'construct_budget_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.other_budget_report', realpath('..') . DS . 'report' . DS . 'other_budget_report.jrxml');
Configure::write('Zicure.Report.Oacrmd.rpt_05', realpath('..') . DS . 'report' . DS . 'rpt_05.jrxml');
Configure::write('Zicure.Report.Oacrmd.rpt_308', realpath('..') . DS . 'report' . DS . 'rpt_308.jrxml');
Configure::write('Zicure.Report.Oacrmd.record_budget_owner', realpath('..') . DS . 'report' . DS . 'rpt_comment_owner.jrxml');
Configure::write('Zicure.Report.Oacrmd.record_main', realpath('..') . DS . 'report' . DS . 'rpt_comment_cover.jrxml');
Configure::write('Zicure.Report.Oacrmd.record_conclusion', realpath('..') . DS . 'report' . DS . 'rpt_comment_conclusion.jrxml');
Configure::write('Zicure.Report.Retail.rptExample0001', realpath('..') . DS . 'report' . DS . 'rptExample001.jrxml');



Configure::write('Zicure.Report.Oacrmd.xxx', realpath('..') . DS . 'report' . DS . 'xxx.jrxml');

/**
 * Configure for allow document attachment file can be upload
 */
Configure::write('CORE.DOCUMENT.ALL', 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,image/gif,image/jpeg,image/png');
Configure::write('CORE.DOCUMENT.IMG', 'image/gif,image/jpeg,image/png');
Configure::write('CORE.DOCUMENT.PDF', 'application/pdf');
Configure::write('CORE.DOCUMENT.OFFICE', 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel');
Configure::write('CORE.DOCUMENT.IMG_PDF', 'application/pdf,image/gif,image/jpeg,image/png');
Configure::write('CORE.DOCUMENT.OFFICE_PDF', 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel');

/**
 *
 * Core project is enabled secure mose
 * make for dynamic encodeing with secure parame
 */
Configure::write('CORE.ENABLED.SECUREMODE', false);


/**
 *
 * Core project is enabled authentication with google recaption
 * @author sarawutt.b
 * make for dynamic for authentication with google recapcha
 */
Configure::write('CORE.ENABLED.AUTH.RECAPCHA', false);


/**
 *
 * Core variable making for enable or disabled (hidden from each page) of debuging mode
 * @author sarawutt.b
 */
Configure::write('CORE.ENABLED.CORE.DEBUGING', false);

/**
 *
 * Project application Version
 */
Configure::write('OAC.PROJECT.VERSION', '0.5.6');
/**
 *
 * OAC-RMD Report Creator
 */
Configure::write('OAC.REPORT.CREATOR', 'oacrmd-system');


/**
 *
 * UAT Google recapcha Site Key / Secret Key
 * URL :: http://oacbudget-uat.pakgon.com
 */
//Configure::write('CORE.PROJECT.SITEKEY', '6LdKfhsUAAAAAOGMojeYZ3jBL3iYDNobZV8bVnMu');
//Configure::write('CORE.PROJECT.SECRETKEY', '6LdKfhsUAAAAANt30rYJQy4B9C2fDbxcHz1qwWOr');

/**
 *
 * LOCAL Google recapcha Site Key / Secret Key
 * URL :: http://oacbgt-uat.local
 */
Configure::write('CORE.PROJECT.SITEKEY', '6LdLEBsUAAAAACt4X6zEHX9UCd6W9XqRDojSs8f1');
Configure::write('CORE.PROJECT.SECRETKEY', '6LdLEBsUAAAAAPHoUrDDbUzGlt5sm1twB-PUDAAT');

/**
 * Configure for allow document attachment file can be upload massage
 */
Configure::write('OAC.DOCUMENT.OFFICE_PDF.MASSAGE', 'File Support : word,pdf,excel,powerpoint');
Configure::write('OAC.DOCUMENT.OFFICE.MASSAGE', 'File Support : word,excel,powerpoint');
Configure::write('OAC.DOCUMENT.OFFICE.PDF.MASSAGE', 'File Support : PDF');
Configure::write('OAC.DOCUMENT.OFFICE.IMG_PDF.MASSAGE', 'File Support : PDF,JPEG,PNG,GIF');
Configure::write('OAC.DOCUMENT.OFFICE.IMG.MASSAGE', 'File Support : JPEG,PNG,GIF');
