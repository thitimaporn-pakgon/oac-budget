<?php

/**
 * AppShell file
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Shell', 'Console');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell {

    /**
     * 
     * Function make generate for bootstrap color signature class
     * @author  sarawutt.b
     * @param   type $index as integer of color code
     * @return  string code
     */
    public static function bootstrapBoxClass($index = 0) {
        $bclass = array('warning', 'success', 'info', 'default', 'primary', 'danger');
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }

    /**
     * 
     * Function make for bootstrap label
     * @author  sarawutt.b
     * @param   type $message as string of the content where you want wrap to the label
     * @param   type $class as string bootstrap class
     * @return  string
     */
    public static function bootstrapLabel($message = '', $class = 'default') {
        return "<label class='label label-{$class}'>" . __($message) . '</label>';
    }

    /**
     * 
     * Function integer randomize
     * @author sarawutt.b
     * @param type $min as integer of minimun for radomize
     * @param type $max as integer of maximum for randomize
     * @param type $lenght as integer of lenght of wanted rondom
     * @return integer
     */
    public static function randommize($min = 1, $max = 1, $lenght = 6) {
        $intMin = ($min ** $length) / 10;
        $intMax = ($max ** $length) - 1;
        return mt_rand($intMin, $intMax);
    }

}
