<?php
App::uses('AppModel', 'Model');
class StrategyGov extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $useTable= 'strategy_govs';
    
}