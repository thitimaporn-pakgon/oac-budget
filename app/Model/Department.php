<?php

App::uses('AppModel', 'Model');

/**
 * Department Model
 *
 * @property DepartmentLevel $DepartmentLevel
 * @property DepartmentCover $DepartmentCover
 */
class Department extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';
    //LOCAL
    private $coverId = 2;
    private $spcId = 1;
    //UAT
    //private $coverId = 24;
    //private $spcId = 26;
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'short_name';

    /**
     * 
     * Function find for department code
     * @author sarawutt.b
     * @param type $conditions as a array like cake conditions
     * @return string department code if exist otherwise retur 9999
     */
    public function getDepartmentCode($conditions = array()) {
        $conditions = is_array($conditions) ? $conditions : array($conditions);
        $result = $this->find('first', array('conditions' => $conditions, 'fields' => 'dept_no','recursive'=>-1));
        return empty($result) ? '9999' : $result[$this->alias]['dept_no'];
    }

    /**
     * 
     * Function find department full name by department id
     * @author  Numpol.S
     * @param   type $id as a integer of department id
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmenttName($id = null) {
        $result = $this->getDepartmentInfoById($id);
        return empty($result) ? '-' : $result[$this->alias]['full_name'];
    }

    /**
     * 
     * Function find department each name with department level name
     * @author  sarawutt.b
     * @param   type $id as a integer of department id
     * @param   type $display_style part of name posible value a|b default a for the short name
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmentNameWithDepartment($id = null, $display_style = 'a') {
        if(empty($id)) {return '-';}
        $sql = "SELECT short_name, full_name, b.name AS level_name, short_name || ' ' || '(' || b.name || ')' AS display_a, short_name || ' ' || '(' || b.name || ')' AS display_b
                FROM master.departments AS a
                LEFT JOIN master.department_levels AS b ON b.id = a.department_level_id
                WHERE a.id = {$id} AND a.status = 'A'
                LIMIT 1;";
        $result = $this->query($sql);
        $key = 'display_' . $display_style;
        return empty($result[0][0][$key]) ? '-' : $result[0][0][$key];
    }

    /**
     * 
     * Function find department short name by department id
     * @author  Numpol.S
     * @param   type $id as a integer of department id
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmentShortName($id = null) {
        $result = $this->getDepartmentInfoById($id);
        return empty($result) ? '-' : $result[$this->alias]['short_name'];
    }

    /**
     * 
     * Find for Department Infomation
     * @author sarawutt.b
     * @param type $id as integer of department id
     * @return array() department infomation
     */
    public function getDepartmentInfoById($id = null) {
        $this->recursive = -1;
        return $this->findById($id);
    }

    public function findListDepartment($id = null, $convert = false) {
        $not = ($convert === true) ? ' NOT' : null;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' . $not => $id);
        $options = (empty($conditions) || ($convert === true)) ? $this->getEmptySelect() : array();
        return $options + $this->find('list', array('fields' => array('id', 'short_name'), 'conditions' => array_merge($conditions, array('status' => 'A')), 'order' => array('dept_no' => 'asc', 'short_name' => 'asc', 'full_name' => 'asc')));
    }

    public function getDepartmentById($id = null) {
        $return = ((is_null($id)) || (empty($id)) || (!is_numeric($id))) ? array() : $this->find('first', array('recursive' => -1, 'fields' => array('short_name'), 'conditions' => array('id' => $id)));
        return (!empty($return)) ? "{$return['Department']['short_name']} " : '';
    }

    /**
     * 
     * Function request for parame department is the cover department
     * @author  sarawutt.b
     * @param   type $departmentId as a integer of department id [PK]
     * @return  boolean
     */
    public function isCoverDepartment($departmentId = null) {
        //หน่วยเหนือ id = 5
        $sql = "SELECT COUNT(*) AS count_result 
                FROM master.departments AS d
                INNER JOIN master.department_levels AS dl ON dl.id = d.department_level_id
                where d.id = {$departmentId} AND dl.id = {$this->coverId};";
        $count = $this->query($sql);
        return ($count[0][0]['count_result'] > 0) ? true : false;
    }

    /**
     * 
     * Function request ask for is the SPC department
     * @param type $departmentId as a current department id
     * @return boolean true if is SPC otherwise return false
     */
    public function isSpcDepartment($departmentId = null) {
        //หน่วยเหนือ id = 6
        $sql = "SELECT COUNT(*) AS count_result 
                FROM master.departments AS d
                INNER JOIN master.department_levels AS dl ON dl.id = d.department_level_id
                where d.id = {$departmentId} AND dl.id = {$this->spcId};";
        $count = $this->query($sql);
        return ($count[0][0]['count_result'] > 0) ? true : false;
    }

    /**
     * 
     * รายการหน่วยรับผิดชอบโครงการหลัก
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $convert boolean of is convert
     * @return  type array()
     */
    public function findListDepartmentCover($id = null, $convert = false) {
        $not = ($convert === true) ? ' NOT' : null;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' . $not => $id);
        $options = (empty($conditions) || ($convert === true)) ? $this->getEmptySelect() : array();
        return $options + $this->find('list', array('fields' => array('id', 'short_name'), 'conditions' => array_merge($conditions, array('status' => 'A', 'department_level_id' => $this->coverId)), 'order' => array('dept_no' => 'asc', 'short_name' => 'asc', 'full_name' => 'asc')));
    }

    /**
     * 
     * รายการหน่วยงาน สปช.
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $convert boolean of is convert
     * @return  type array()
     */
    public function findListDepartmentSPC($id = null, $convert = false) {
        $not = ($convert === true) ? ' NOT' : null;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' . $not => $id);
        $options = (empty($conditions) || ($convert === true)) ? $this->getEmptySelect() : array();
        return $options + $this->find('list', array('fields' => array('id', 'short_name'), 'conditions' => array_merge($conditions, array('status' => 'A', 'department_level_id' => $this->spcId)), 'order' => array('dept_no' => 'asc', 'short_name' => 'asc', 'full_name' => 'asc')));
    }

    /**
     * 
     * รายการหน่วยงาน สามารถอนุมัติโครงการทั้งหมด
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $convert boolean of is convert
     * @return  type array()
     */
    public function findListControlDepartment($id = null, $convert = false) {
        $not = ($convert === true) ? ' NOT' : null;
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' . $not => $id);
        $options = (empty($conditions) || ($convert === true)) ? $this->getEmptySelect() : array();
        return $options + $this->find('list', array('fields' => array('id', 'short_name'), 'conditions' => array_merge($conditions, array('status' => 'A', 'department_level_id' => array($this->coverId, $this->spcId))), 'order' => array('dept_no' => 'asc', 'short_name' => 'asc', 'full_name' => 'asc')));
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'DepartmentLevel' => array(
            'className' => 'DepartmentLevel',
            'foreignKey' => 'department_level_id',
            'conditions' => '',
            'fields' => '',
            'order' => 'DepartmentLevel.order_display ASC, DepartmentLevel.id ASC'
        )
    );

}
