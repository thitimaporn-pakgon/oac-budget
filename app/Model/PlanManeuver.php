<?php

App::uses('AppModel', 'Model');

/**
 *
 * PlanManeuver Model
 * @author  sarawutt.b
 * @property StrategicManeuver $StrategicManeuver
 * @property BudgetYear $BudgetYear
 * @property SystemHasProcess $SystemHasProcess
 * @property ResultManeuver $ResultManeuver
 * @since   2017-04-18 15:00:35
 * @license Zicure Corp. 
 */
class PlanManeuver extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $useTable = 'strategy_plans';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListPlanManeuver($id = null) {
        return $this->getEmptySelect() + $this->find('list');
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'name', 'code', 'strategic_maneuver_id'), 'conditions' => array('strategic_maneuver_id' => $id)));
        return $data;
    }

    /**
     * belongsTo associations
     *
     * @var array
     */
//    public $belongsTo = array(
//        'StrategicManeuver' => array(
//            'className' => 'StrategicManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        ),
//        'BudgetYear' => array(
//            'className' => 'BudgetYear',
//            'foreignKey' => 'budget_year_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        ),
//        'SystemHasProcess' => array(
//            'className' => 'SystemHasProcess',
//            'foreignKey' => 'system_has_process_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        )
//    );

    /**
     * hasMany associations
     *
     * @var array
     */
//    public $hasMany = array(
//        'ResultManeuver' => array(
//            'className' => 'ResultManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'EventManeuver' => array(
//            'className' => 'EventManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'WorkGroupManeuver' => array(
//            'className' => 'WorkGroupManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'WorkManeuver' => array(
//            'className' => 'WorkManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ProjectManeuver' => array(
//            'className' => 'ProjectManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ProjectListManeuver' => array(
//            'className' => 'ProjectListManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ExpenseHeaderManeuver' => array(
//            'className' => 'ExpenseHeaderManeuver',
//            'foreignKey' => 'plan_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
//    );
}
