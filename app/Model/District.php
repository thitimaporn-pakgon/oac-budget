<?php

App::uses('AppModel', 'Model');

/**
 * District Model
 *
 */
class District extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * 
     * Function find list for district in option list
     * @author sarawutt.b
     * @param type $provinceId as a string of a province ID
     * @return array() option or string distring name if contain in params
     */
    public function findListDistrict($provinceId = null) {
        $condition = (empty($provinceId) || !is_numeric($provinceId) || is_null($provinceId)) ? array('District.id LIKE' => $provinceId . "%") : array('District.status' => 'A');
        return $this->getEmptySelect() + $this->find('list', array('conditions' => $condition, 'fields' => array('id', 'name'), 'order' => array('id' => 'asc', 'name' => 'asc')));
    }

    public function getDistrictById($id = null) {
        $a = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id)));
        return $a['District']['name'];
    }

}
