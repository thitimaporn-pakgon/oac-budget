<?php

App::uses('AppModel', 'Model');

/**
 * Position Model
 *
 * @property User $User
 */
class Position extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'system';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListPosition() {
        $fieldName = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', $fieldName), 'order' => array($fieldName => 'asc', 'id' => 'asc', 'modified' => 'asc'), 'conditions' => array('status' => 'A')));
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'position_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
