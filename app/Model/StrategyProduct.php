<?php

App::uses('AppModel', 'Model');

class StrategyProduct extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $useTable = 'strategy_products';

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'strategy_product_name', 'code', 'strategy_gov_id','strategy_plan_id'), 'conditions' => array('strategy_plan_id' => $id)));
//       debug($data);
//       die;
        return $data;
    }

}
