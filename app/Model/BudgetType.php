<?php

App::uses('AppModel', 'Model');

/**
 *
 * BudgetType Model
 * @author  sarawutt.b
 * @since   2017-06-06 17:23:18
 * @license Zicure Corp. 
 */
class BudgetType extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ExpenseHeaderManage' => array(
            'className' => 'ExpenseHeaderManage',
            'foreignKey' => 'budget_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
