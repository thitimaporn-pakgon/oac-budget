<?php

App::uses('AppModel', 'Model');

/**
 * SysController Model
 *
 * @property SysStatus $SysStatus
 * @property SysAction $SysAction
 */
class SysController extends AppModel {

    public $name = 'SysController';
    public $useDbConfig = 'system';
    public $displayField = 'name';
    public $useTable = 'sys_controllers';
    public $primaryKey = 'id';

    public function findListSysController($id = null) {
        $condition = (is_null($id) || !is_numeric($id)) ? null : array('id' => $id);
        //$fieldName = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $fieldName = 'name';
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', $fieldName), 'conditions' => array($condition, 'status' => 'A'), 'order' => array($fieldName => 'ASC'), 'recursive' => -1));
    }

    public function deleteControllers($controller_id, $action_id) {
        if (is_null($controller_id) OR is_null($action_id))
            return false;
        $update_uid = $_SESSION['User']['id'];
        $version = $this->VERSION();
        $sql = "UPDATE system.sys_actions SET is_deleted='Y', update_uid='{$update_uid}', modified=NOW(), _version_={$version}
 				WHERE id = '{$action_id}' AND sys_controller_id = '{$controller_id}';";
        $this->query($sql);
        return true;
    }

    public function getSysControlllerNameById($id = null) {
        $data = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id), 'recursive' => -1));
        return empty($data) ? '' : $data['SysController']['name'];
    }

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'SysAction' => array(
            'className' => 'SysAction',
            'foreignKey' => 'sys_controller_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
