<?php

App::uses('AppModel', 'Model');

/**
 *
 * ResultManeuver Model
 * @author  sarawutt.b
 * @property PlanManeuver $PlanManeuver
 * @property BudgetYear $BudgetYear
 * @property SystemHasProcess $SystemHasProcess
 * @property EventManeuver $EventManeuver
 * @since   2017-04-18 15:01:58
 * @license Zicure Corp. 
 */
class ResultManeuver extends AppModel {

    public $useDbConfig = 'budget';
    public $useTable = 'strategy_products';

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'strategy_product_name', 'code', 'strategy_gov_id'), 'conditions' => array('strategy_gov_id' => $id)));
        return $data;
    }

}
