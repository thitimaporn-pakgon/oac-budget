<?php

App::uses('AppModel', 'Model');

/**
 * SysAction Model
 *
 * @property SysController $SysController
 * @property SysStatus $SysStatus
 */
class SysAction extends AppModel {

    public $name = 'SysAction';
    public $useDbConfig = 'system';
    public $displayField = 'name';
    public $useTable = 'sys_actions';
    public $primaryKey = 'id';

    public function findActionByControllerId($sysControllerId = null) {
        $condition = (is_null($sysControllerId) || !is_numeric($sysControllerId)) ? null : array('sys_controller_id' => $sysControllerId);
        return $this->find('all', array('conditions' => array($condition, 'status' => 'A'), 'recursive' => -1, 'order' => array('sys_controller_id' => 'asc', 'id' => 'asc', 'name' => 'asc')));
    }

    public function findListActionByControllerId($sysControllerId = null) {
        $condition = (is_null($sysControllerId) || !is_numeric($sysControllerId)) ? null : array('sys_controller_id' => $sysControllerId);
        return $this->find('list', array('conditions' => array($condition, 'status' => 'A'), 'recursive' => -1, 'order' => array('sys_controller_id' => 'asc', 'id' => 'asc', 'name' => 'asc')));
    }

    public function findListSysAction($id = null) {
        $condition = (is_null($id) || !is_numeric($id)) ? null : array('id' => $id);
        //$fieldName = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $fieldName = 'name';
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', $fieldName), 'conditions' => array($condition, 'status' => 'A'), 'order' => array($fieldName => 'ASC'), 'recursive' => -1));
    }

    public function getSysActionNameById($id = null) {
        $data = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id), 'recursive' => -1));
        return empty($data) ? '' : $data['SysAction']['name'];
    }

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public function print_excel() {
        $data = $this->find('all');
        // do whatever you need to get the data you want printed in the Excel

        App::uses('SomeExcel', 'Lib/Print/Excel');
        $someExcel = new SomeExcel($data);
        return $someExcel->create();
    }

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'SysController' => array(
            'className' => 'SysController',
            'foreignKey' => 'sys_controller_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
