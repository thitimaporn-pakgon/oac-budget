<?php

App::uses('AppModel', 'Model');

/**
 * BudgetYear Model
 *
 */
class BudgetYear extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * 
     * List of the month or find with the full name of the month name in key params
     * @author Sarawutt.b <sarawutt.b@pakgon.com>
     * @param   integer $key of month index posible value 1 - 12
     * @return  string
     */
    public function listMonth($key = 'xxx') {
        $options = array();
        for ($m = 1; $m <= 12; $m++) {
            $options[$m] = __(date("F", mktime(0, 0, 0, $m, 1, date('Y'))));
        }
        return (array_key_exists($key, $options)) ? $options[$key] : $this->getEmptySelect() + $options;
    }

    /**
     * 
     * List of the quarter or find with the full name of the quather name in key params
     * @author Sarawutt.b
     * @param   integer $key of the quarther index posible value 1 - 4
     * @return  string
     */
    public function listQuarter($key = 'xxx') {
        $options = array('1' => __('Quarter %s', 1), '2' => __('Quarter %s', 2), '3' => __('Quarter %s', 3), '4' => __('Quarter %s', 4));
        return (array_key_exists($key, $options)) ? $options[$key] : $this->getEmptySelect() + $options;
    }

    /**
     * 
     * 
     * Function find list budget year id =>name
     * @author sarawutt.b
     * @param type $id as optional a ID of budget year wish matched
     * @param type $xconditions optional for custom condition
     * @return array() of budget year list
     */
    public function findListBudgetYear($id = null, $xconditions = array()) {
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);
        $conditions = !empty($xconditions) ? array_merge($conditions, $xconditions) : $conditions;
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', 'name'), 'conditions' => array_merge($conditions, array('status' => 'A')), 'order' => array('id' => 'ASC', 'name' => 'ASC')));
    }

    /**
     * 
     * 
     * Function find list budget year name => name
     * @author sarawutt.b
     * @param type $id as optional a ID of budget year wish matched
     * @param type $xconditions optional for custom condition
     * @return array() of budget year list
     */
    public function findListBudgetYearName($id = null, $xconditions = array()) {
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);
        $conditions = !empty($xconditions) ? array_merge($conditions, $xconditions) : $conditions;
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('name', 'name'), 'conditions' => array_merge($conditions, array('status' => 'A')), 'order' => array('name' => 'ASC', 'id' => 'ASC')));
    }

    public function findBudgetYearById($id = null) {
        return $this->find('first', array('conditions' => array('BudgetYear.id' => $id)));
    }

    public function getBudgetYearById($id = null) {
        $a = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id)));
        return empty($a['BudgetYear']['name']) ? '' : $a['BudgetYear']['name'];
    }

    public function findBudgetProcessTime() {
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('name', 'name'), 'conditions' => array('status' => 'A'), 'order' => array('name' => 'ASC', 'id' => 'ASC')));
    }

    public function getNameById($budget_id = null) {
        $result = $this->find('first', array('conditions' => array('id' => $budget_id), 'fields' => array('name'), 'recursive' => -1));
        return empty($result) ? '' : $result[$this->alias]['name'];
    }

    public function findBudgetYearsByName($year_name) {
        $findBudgetYearsByName = $this->find('first', array('conditions' => array('BudgetYear.name' => $year_name), 'fields' => array('BudgetYear.id')));
        return empty($findBudgetYearsByName) ? -1 : $findBudgetYearsByName['BudgetYear']['id'];
    }

    public function findBudgetYearIdByName($name = null) {
        $result = $this->find('first', array('conditions' => array('name' => $name), 'fields' => array('id'), 'recursive' => -1));
        return empty($result) ? -1 : $result[$this->alias]['id'];
    }
    
    public function conversBudgetYear($budgetYearName = null) {
        return substr($budgetYearName, 2, 2);
    }

}
