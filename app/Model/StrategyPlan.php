<?php

App::uses('AppModel', 'Model');

class StrategyPlan extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $useTable = 'strategy_plans';

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'strategy_plan_name', 'code', 'strategy_gov_id'), 'conditions' => array('strategy_gov_id' => $id)));
        return $data;
    }

}
