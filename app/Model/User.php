<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');


class User extends AppModel {

    public $useDbConfig = 'budget';

	public $belongsTo = array(
        'TitleName' => array(
            'className' => 'TitleName',
            'foreignKey' => 'title_name_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
	);

  public $hasMany = array(
      'UserInGroup' => array(
          'className' => 'UserInGroup',
          'foreignKey' => 'user_id',
          'dependent' => false,
          'conditions' => '',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
      ),
      'Dept' => array(
          'className' => 'Dept',
          'foreignKey' => 'user_id',
          'dependent' => false,
          'conditions' => '',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
      ),
      'UserInDept' => array(
          'className' => 'UserInDept',
          'foreignKey' => 'user_id',
          'dependent' => false,
          'conditions' => '',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
      ),
  );

	/* backup �ͧ����
	public $useDbConfig = 'system';

//    public $virtualFields = array(
//        'displayFullName' => "SELECT CONCAT(f.name,' ',u.first_name,' ',u.last_name)
//                                    FROM system.users AS u
//                                    LEFT JOIN master.name_prefixes AS f ON u.name_prefix_id = f.id
//                                    WHERE u.id = User.id"
//    );


    public function countAdmin($id = null) {
        $sql = "select count(u.username)as admin FROM master.departments AS d
        LEFT JOIN system.users AS u ON d.id = u.department_id
        LEFT JOIN system.roles AS r ON u.role_id = r.id  ";
        $sql.=" where d.id='$id' ";
        $result = $this->query($sql);
        return empty($result) ? '' : $result[0][0]['admin'];
    }

    public function UpdateLogin($user = null, $login = null) {

        //finduser
        $sql = " SELECT  loginfail   FROM system.users WHERE  username ='$user' ";
        $result = $this->query($sql);
        if (!empty($result)) {
            if ($login == "1") {
                $update = "UPDATE system.users   SET  loginfail='0'  WHERE  username='$user' ";
                $this->query($update);
            } else {
                $logifail = $result[0][0]['loginfail'];
                $logifail++;
                $update = "UPDATE system.users   SET  loginfail='$logifail'  WHERE  username='$user' ";
                $this->query($update);
            }
        }
    }

    public function countUser($id = null) {
        $sql = "select count(u.username)as user FROM master.departments AS d
                LEFT JOIN system.users AS u ON d.id = u.department_id
                LEFT JOIN system.roles AS r ON u.role_id = r.id  ";
        $sql.=" where d.id='$id' ";
        $result = $this->query($sql);
        return empty($result) ? '' : $result[0][0]['user'];
    }

    public function countAlluser($id = null) {
        $sql = "select count(u.username)as alluser FROM master.departments AS d
                LEFT JOIN system.users AS u ON d.id = u.department_id
                LEFT JOIN system.roles AS r ON u.role_id = r.id  ";
        $sql.=" where d.id='$id' ";
        $result = $this->query($sql);
        return empty($result) ? '' : $result[0][0]['alluser'];
    }

    public function findListUser($id = null, $convert = false) {
        $conditions = "1=1";
        if (is_null($id) || empty($id) | !is_numeric($id)) {
            if ($convert === true) {
                $conditions .= " AND u.id NOT IN({$id})";
            }
        }


        $sql = "SELECT u.id, CONCAT(f.name,' ',u.first_name,' ',u.last_name) AS name
                FROM system.users AS u
                LEFT JOIN master.name_prefixes AS f ON u.name_prefix_id = f.id
                WHERE {$conditions}
                ORDER BY u.id ASC;";
        $results = $this->query($sql);
        $return = array();
        foreach ($results as $k => $v) {
            $return[$v[0]['id']] = $v[0]['name'];
        }
        return $this->getEmptySelect() + $return;
    }

    public function findUserByUid($id = null) {
        if (is_null($id) || empty($id) || !is_numeric($id)) {
            return null;
        }
        $sql = "SELECT u.id, CONCAT(f.name,' ',u.first_name,' ',u.last_name) AS name
                FROM system.users AS u
                LEFT JOIN master.name_prefixes AS f ON u.name_prefix_id = f.id
                WHERE u.id={$id}
                ORDER BY u.id ASC
                LIMIT 1;";
        $result = $this->query($sql);

        return empty($result) ? '' : $result[0][0]['name'];
    }

    public function getCurrentSessionUserId() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.id') : null;
    }

    public function getUserById($id = null) {
        $return = ((is_null($id)) || (empty($id)) || (!is_numeric($id))) ? array() : $this->find('first', array('recursive' => -1, 'fields' => array('first_name', 'last_name'), 'conditions' => array('id' => $id)));
        return (!empty($return)) ? "{$return['User']['first_name']}  {$return['User']['last_name']}" : '';
    }

    public function getUserNameById($id = null) {
        $field = ($this->getCurrentLanguage() == 'tha') ? 'username' : 'name_eng';
        $tmp = $this->find('first', array('fields' => array($field), 'conditions' => array('id' => $id), 'recursive' => -1));
        return empty($tmp[$this->alias]) ? '' : $tmp[$this->alias][$field];
    }


    public function getPositionByUserId($id = null) {
        //if(!is_numeric($id)){ return __('Oop!');}
        if (!is_numeric($id)) {
            return null;
        }
        $name_field = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $sql = "SELECT p.{$name_field} AS name
                FROM system.users AS u
                INNER JOIN system.positions AS p ON p.id = u.position_id
                WHERE u.id = {$id};";
        $result = $this->query($sql);
        return empty($result[0][0]['name']) ? __('Unknow') : $result[0][0]['name'];
    }

    public function beforeSave($options = array()) {
        $data1 = $this->data[$this->alias]['password'];
        if (isset($data1)) {
            if (isset($this->data[$this->alias]['id'])) {
                $id = $this->data[$this->alias]['id'];
                $user = $this->findById($id);
            } else {
                $id = false;
            }
            if (!$id || $user['User']['password'] != $this->data[$this->alias]['password']) {
                $passwordHasher = new BlowfishPasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
            }

            if (isset($this->data[$this->alias]['password2'])){
                if (!$id || $user['User']['password2'] != $this->data[$this->alias]['password2']) {
                    $passwordHasher = new BlowfishPasswordHasher();
                    $this->data[$this->alias]['password2'] = $passwordHasher->hash($this->data[$this->alias]['password2']);
                }
            }
        }
        return true;
    }


    public function findUserInfoById($id = null){
        $this->recursive = -1;
        return $this->findById($id);
    }
    // The Associations below have been created with all possible keys, those that are not needed can be removed


    public $belongsTo = array(
        'NamePrefix' => array(
            'className' => 'NamePrefix',
            'foreignKey' => 'name_prefix_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Position' => array(
            'className' => 'Position',
            'foreignKey' => 'position_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Role' => array(
            'className' => 'Role',
            'foreignKey' => 'role_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Department' => array(
            'className' => 'Department',
            'foreignKey' => 'department_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Province' => array(
            'className' => 'Province',
            'foreignKey' => 'province_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'District' => array(
            'className' => 'District',
            'foreignKey' => 'district_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SubDistrict' => array(
            'className' => 'SubDistrict',
            'foreignKey' => 'sub_district_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


    public $hasMany = array(
        'SysAcl' => array(
            'className' => 'SysAcl',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
	*/
}
