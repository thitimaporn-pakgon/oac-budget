<?php

App::uses('AppModel', 'Model');

/**
 * AccessLog Model
 *
 * @property Menu $Menu
 * @property User $User
 */
class AccessLog extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'log';

    public function saveAccessLog($actions = '', $params = array()) {
        $options = array(
            'menu_id' => $this->getMenuIdByUrl($_SERVER['REQUEST_URI']),
            'user_id' => $this->getCurrenSessionUserId(),
            'access_url' => $_SERVER['REQUEST_URI'],
            'actions' => $actions
        );
        $data = (is_array($params) && !empty($params)) ? array_merge($options, $params) : $options;
        $this->create();
        return $this->save($data);
    }

    public function getMenuIdByUrl($url) {
        $sql = "SELECT id FROM system.menus WHERE url ILIKE '{$url}' LIMIT 1;";
        $result = $this->query($sql);
        return isset($result[0][0]['id']) ? $result[0][0]['id'] : -1;
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Menu' => array(
            'className' => 'Menu',
            'foreignKey' => 'menu_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
