<?php

App::uses('AppModel', 'Model');

class GroupPlan extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $useTable = 'group_plans';

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'group_plan_name', 'code'), 'conditions' => array('strategy_gov_id' => $id)));
        return $data;
    }

}
