<?php

App::uses('AppModel', 'Model');

/**
 * DepartmentLevel Model
 *
 * @property Department $Department
 */
class DepartmentLevel extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListDepartmentLevel($id = null) {
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', 'name'), 'conditions' => array_merge($conditions, array('status' => 'A')), 'order' => array('id' => 'asc', 'name' => 'asc', 'name_eng' => 'asc')));
    }

    public function getDepartmentLevelById($id = null) {
        $return = ((is_null($id)) || (empty($id)) || (!is_numeric($id))) ? array() : $this->find('first', array('recursive' => -1, 'fields' => array('name'), 'conditions' => array('id' => $id)));
        return (!empty($return)) ? "{$return['DepartmentLevel']['name']} " : '';
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Department' => array(
            'className' => 'Department',
            'foreignKey' => 'department_level_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
