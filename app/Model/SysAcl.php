<?php

App::uses('AppModel', 'Model');

/**
 * SysAcl Model
 *
 * @property SysController $SysController
 * @property SysAction $SysAction
 * @property User $User
 * @property Stakeholder $Stakeholder
 * @property SysStatus $SysStatus
 */
class SysAcl extends AppModel {

    public $name = 'SysAcl';
    public $useDbConfig = 'system';
    public $displayField = 'name';
    public $useTable = 'sys_acls';
    public $primaryKey = 'id';

    public function getSysStatus($module = null) {
        return $this->getEmptySelect() + array('A' => __('Active'), 'N' => __('Inactive'), 'D' => __('Develop'));
    }

    //Delete from permission page fill in list
    public function detete_bath($role_id, $controller_id, $action_id) {
        if (is_null($role_id) AND is_null($controller_id) AND is_null($action_id)) {
            return FALSE;
        }
        $update_uid = $this->getCurrenSessionUserId();
        //$sql = "UPDATE system.sys_acls SET status = 'D', modified=NOW(), update_uid={$update_uid} WHERE sys_controller_id={$controller_id} AND sys_action_id={$action_id} AND role_id={$role_id};";
        $sql = "DELETE FROM system.sys_acls WHERE sys_controller_id={$controller_id} AND sys_action_id={$action_id} AND role_id={$role_id};";

        $this->query($sql);
        return TRUE;
    }

    public function detete_bathbyuser($user_id, $controller_id, $action_id) {
        if (is_null($user_id) AND is_null($controller_id) AND is_null($action_id)) {
            return FALSE;
        }

        $sql = "SELECT COUNT(id) AS count_acl FROM system.sys_acls WHERE sys_controller_id={$controller_id} AND sys_action_id={$action_id} AND user_id={$user_id} AND role_id IS NULL;";
        $rest = $this->query($sql);
        if ($rest[0][0]['count_acl'] > 0) {
            $conditions = array(
                array(
                    'SysAcl.sys_controller_id' => $controller_id,
                    'SysAcl.sys_action_id' => $action_id,
                    'SysAcl.user_id' => $user_id,
                    'SysAcl.role_id IS NULL'
                )
            );

//            $sql = "DELETE FROM system.sys_acls WHERE sys_controller_id={$controller_id} AND sys_action_id={$action_id} AND user_id={$user_id} AND role_id IS NULL;";
//            debug($this->query($sql));exit;
            //return $this->query($sql);
            return $this->deleteAll($conditions, false);
        } else {
            return -1;
        }

        //$sql = "UPDATE system.sys_acls SET status = 'D', modified=NOW(), update_uid={$update_uid} WHERE sys_controller_id={$controller_id} AND sys_action_id={$action_id} AND role_id={$role_id};";
    }

    //Delete from index of SysAcls page
    public function detete_acl($id) {
        if (is_null($id)) {
            return FALSE;
        }
//        $update_uid = $_SESSION['User']['id'];
//        $version = $this->VERSION();
//        $sql = "UPDATE system.sys_acls SET is_deleted='Y', modified=NOW(), update_uid='{$update_uid}', _version_={$version}
// 				WHERE id='{$id}'";
//        $this->query($sql);
//        return TRUE;
        return $this->delete($id);
    }

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'SysController' => array(
            'className' => 'SysController',
            'foreignKey' => 'sys_controller_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SysAction' => array(
            'className' => 'SysAction',
            'foreignKey' => 'sys_action_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Role' => array(
            'className' => 'Role',
            'foreignKey' => 'role_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
