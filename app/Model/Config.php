<?php

App::uses('AppModel', 'Model');

/**
 *
 * Defining each ID of configure
 * @author sarawutt.b
 */
define('ANNOUNCE', 'ANNOUNCE');
define('ADMINROLEID', 'ADMINROLEID');
define('SPCDEPARTMENTROLEID', 'SPCDEPARTMENTROLEID');
define('MAINDEPARTMENTROLEID', 'MAINDEPARTMENTROLEID');
define('SUBDEPARTMENTROLEID', 'SUBDEPARTMENTROLEID');
define('PRACTICEDEPARTMENTROLEID', 'PRACTICEDEPARTMENTROLEID');
define('OWNERDEPARTMENTROLEID', 'OWNERDEPARTMENTROLEID');
define('APPVERSION', 'APPVERSION');

/**
 *
 * Config Model
 * @author  sarawutt.b
 * @since   2017-03-07 14:46:32
 * @license Zicure Corp.
 */
class Config extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'system';

    /**
     *
     * Geting Application Version Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getAppVersion() {
        return $this->getConfigureValue(APPVERSION);
    }

    /**
     *
     * Geting Admin Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getAdminRoleId() {
        return $this->getConfigureValue(ADMINROLEID);
    }

    /**
     *
     * Geting SPC Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getSPCDepartmentRoleId() {
        return $this->getConfigureValue(SPCDEPARTMENTROLEID);
    }

    /**
     *
     * Geting Main Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getMainDepartmentRoleId() {
        return $this->getConfigureValue(MAINDEPARTMENTROLEID);
    }

    /**
     *
     * Geting Sub Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getSubDepartmentRoleId() {
        return $this->getConfigureValue(SUBDEPARTMENTROLEID);
    }

    /**
     *
     * Geting Practice Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getPracticeDepartmentRoleId() {
        return $this->getConfigureValue(PRACTICEDEPARTMENTROLEID);
    }

    /**
     *
     * Geting Owner Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getOwnerDepartmentRoleId() {
        return $this->getConfigureValue(OWNERDEPARTMENTROLEID);
    }

    /**
     *
     * Geting anount message display in maquee style on main view
     * @author  sarawutt.b
     * @return  string
     */
    public function getAnnounceMsg() {
        $msg = $this->findById(ANNOUNCE);
        return $msg[$this->alias]['note'];
    }

    /**
     *
     * Function update for message announce message
     * @author  sarawutt.b
     * @param   type $msg as a string of the announce message
     * @return  array info of the reccord saved otherwise return false
     */
    public function updateAnnounceMsg($msg = null) {
        $data[$this->alias]['id'] = ANNOUNCE;
        $data[$this->alias]['note'] = $msg;
        $data[$this->alias]['update_uid'] = $this->getCurrenSessionUserId();
        return $this->save($data);
    }

    /**
     *
     * Function get configure value display on hen
     * @author  sarawutt.b
     * @param   type $id as a string configre ID
     * @return  string configure value of matched otherwise boolean false
     */
    public function getConfigureValue($id = 'EMPTY') {
        $value = $this->find('first', array('fields' => array('value'), 'conditions' => array('id' => $id), 'recursive' => -1));
        return empty($value) ? '' : $value['Config']['value'];
    }

}
