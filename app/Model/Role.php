<?php

App::uses('AppModel', 'Model');

/**
 * Role Model
 *
 * @property SysAcl $SysAcl
 * @property User $User
 */
class Role extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'system';
    //LOCAL
//    private $coverId = 5;
//    private $spcId = 6;
    //UAT
    private $coverId = 21;
    private $spcId = 22;

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListRole($id = null) {
        $condition = (is_null($id) || !is_numeric($id)) ? null : array('id' => $id);
        $fieldName = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', $fieldName), 'conditions' => array($condition, 'status' => 'A'), 'order' => array($fieldName => 'ASC'), 'recursive' => -1));
    }

    /**
     * 
     * Find Role infomation by role ID
     * @author  sarawutt.b
     * @param   type $id as integer of role id
     * @return  array()
     */
    public function getRoleNameById($id = null) {
        $field = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        $tmp = $this->find('first', array('fields' => array($field), 'conditions' => array('id' => $id), 'recursive' => -1));
        return empty($tmp[$this->alias]) ? '' : $tmp[$this->alias][$field];
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'SysAcl' => array(
            'className' => 'SysAcl',
            'foreignKey' => 'role_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'role_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
