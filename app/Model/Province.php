<?php

App::uses('AppModel', 'Model');

/**
 * Province Model
 *
 */
class Province extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListProvince() {
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', 'name'), 'order' => array('id' => 'asc', 'name' => 'asc')));
    }

    /**
     * 
     * Function fine province name by sub id of province id
     * @author  sarawutt.b
     * @param   type $id as a string of province id
     * @return  type string province name
     */
    public function getProvinceById($id = null) {
        $a = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id)));
        return (!empty($a)) ? $a['Province']['name'] : '';
    }
    
    public function searchProvince($pv_name = null) {
        $search1 = array('lower(Province.name) ILIKE' => '%' . strtolower($pv_name) . '%');
//        $search2 = array('lower(Province.name_eng) ILIKE' => '%' . strtolower($pv_name) . '%');
        return $search1;
    }

}
