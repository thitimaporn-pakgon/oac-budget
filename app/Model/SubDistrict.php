<?php

App::uses('AppModel', 'Model');

/**
 * SubDistrict Model
 *
 */
class SubDistrict extends AppModel {

    public $useDbConfig = 'master';
    public $displayField = 'name';

    /**
     * 
     * Function get the zipcode with tambon params
     * @author  sarawutt.b
     * @param   type $tambon_id
     * @return  string
     */
    public function getZipcode($tambon_id) {
        if (empty($tambon_id)) {
            return '';
        }
        $sql = "SELECT zipcode FROM master.sub_districts WHERE id='{$tambon_id}'";
        $sResult = $this->query($sql);
        return $sResult[0][0]['zipcode'];
    }

    /**
     * 
     * Function fine option list for the sub district
     * @author  sarawutt.b
     * @param   type $districtId
     * @return  array() sub district option list or string if matched of the params
     */
    public function findListSubDistrict($districtId = null) {
        $condition = (empty($districtId) || !is_numeric($districtId) || is_null($districtId)) ? array('SubDistrict.id LIKE' => $districtId . "%") : array('SubDistrict.status' => 'A');
        return $this->getEmptySelect() + $this->find('list', array('conditions' => $condition, 'fields' => array('id', 'name'), 'order' => array('id' => 'asc', 'name' => 'asc')));
    }

}
