<?php

App::uses('AppModel', 'Model');

class StrategyActivity extends AppModel {

    public $useDbConfig = 'budget';
    public $useTable = 'strategy_activitys';

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'strategy_act_name', 'code', 'strategy_gov_id'), 'conditions' => array('strategy_product_id' => $id)));
        return $data;
    }

   
}
