<?php

App::uses('AppModel', 'Model');

class Common extends AppModel {

    public $useDbConfig = 'default';
    public $useTable = false;

    /**
     * Function update all each project budget header
     * @author  sarawutt.b
     * @param   $tableName as string of table name referance where you want to update
     * @param   $projectId as integer of project id referance
     * @return  boolean true
     */
    public function updateAfterDeleteEachBudgetDetail($tableName, $projectId) {
        $tableDetailName = $tableName;
        if ($tableName == 'reasearch') {
            $tableName = 'reasearche';
            $tableDetailName = 'reasearch';
        }

        $sql = "SELECT SUM(budget) AS sum_budget FROM project.{$tableDetailName}_details WHERE {$tableDetailName}_id IN(SELECT id FROM project.{$tableName}s WHERE project_id = {$projectId});";
        $result = $this->query($sql);
        if (!empty($result)) {
            $result = $result[0][0];
            $updateUid = $this->getCurrenSessionUserId();
            $sql = "UPDATE project.{$tableName}s SET total_budget = {$result['sum_budget']}, update_uid = '{$updateUid}', modified= NOW() WHERE id IN(SELECT id FROM project.{$tableName}s WHERE project_id = {$projectId});";
            $this->query($sql);
        }
        return true;
    }

    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $SQL = '';
        $SQL = "SELECT * FROM (SELECT id,ref_ducument_no,project_no,project_name,budget_year_id,department_id,cover_department_id,current_department_id,status,'ProjectPlan' as project_type
                    FROM project.project_plans
                    UNION
                    SELECT id,ref_ducument_no,project_no,project_name,budget_year_id,department_id,cover_department_id,current_department_id,status,'Project' as project_type
                    FROM project.projects) as tmp";
        return $this->query($SQL);
    }

    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $SQL = "SELECT count(*) AS count FROM (SELECT id,ref_ducument_no,project_no,project_name,budget_year_id,department_id,cover_department_id,current_department_id,status,'ProjectPlan' as project_type
                    FROM project.project_plans
                    UNION
                    SELECT id,ref_ducument_no,project_no,project_name,budget_year_id,department_id,cover_department_id,current_department_id,status,'Project' as project_type
                    FROM project.projects) as tmp";
        $re = $this->query($SQL);
        return $re[0][0]['count'];
    }

    /**
     * 
     * Function dynamic display box class
     * @author  sarawutt.b
     * @param   type $index as integer of index box class
     * @return  string of box class name
     */
    public function bootstrapBoxClass($index = null) {
        $bclass = array('warning', 'success', 'info', 'default', 'primary', 'danger');
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }

    /**
     * 
     * Function secure make for decoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureDecodeParam($param = null) {
        return (Configure::read('CORE.ENABLED.SECUREMODE')) ? base64_decode($param) : $param;
    }

    /**
     * 
     * Function secure make for encoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureEncodeParam($param = null) {
        return (Configure::read('CORE.ENABLED.SECUREMODE')) ? base64_encode($param) : $param;
    }

    public function getBudgetCode($code = null) {
        $budgetCode = str_replace(' ', '', substr($code, 2));
        return $budgetCode;
    }

    /**
     * 
     * Function cleansing all number with currency formate can be saving to the database | The function pass by refereces
     * @author sarawutt.b
     * @param type $params as a array contain all currency 
     * @return array() same params input except clean of the currency
     */
    public function unformatCurrencyParams(&$params = array()) {
        if (!is_array($params)) {
            $params = array($params);
        }
        $patern = '/($฿ ?)?([\d]{1,3},)+(.[\d]{0,2})?( ?$฿)?/';
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    if (preg_match($patern, $vv) || (strpos($vv, '฿') !== false) || (strpos($vv, '$') !== false)) {
                        $params[$k][$kk] = $this->unformatCurrency($vv);
                    }
                }
            } else {
                if (preg_match($patern, $v) || (strpos($v, '฿') !== false) || (strpos($v, '$') !== false)) {
                    $params[$k] = $this->unformatCurrency($v);
                }
            }
        }
        return true;
    }

    /**
     * 
     * Unformate currentcy where receiving params formate befor matematic operate
     * @author  sarawutt.b
     * @param   type $currency as string in currentcy format
     * @return  double
     */
    public function unformatCurrency($currency = 0, $showPrecision = true) {
        $currency = (strpos($currency, '.') === false) ? $currency . '.00' : $currency;
        $tmp = preg_replace('/[^\d]/', '', $currency);
        return ($showPrecision === true) ? substr($tmp, 0, -2) . "." . substr($tmp, -2) : substr($tmp, 0, -2);
    }

    /**
     * 
     * @author Numpol.J
     * @param type $id as a integer id of model were your sandin g together
     * @param type $model as a string of model class
     * @param type $sector as a character posible value A = Maneuver, B = Allocated, C = Manage
     * @return boolean
     */
    public function deleteChildrenCheck($id = null, $model = null, $sector = 'A') {
        return $this->deleteChildrenCheck($id, $model, $sector);
    }

    /**
     * 
     * @author Numpol.J
     * @param type $id as a integer id of model were your sandin g together
     * @param type $model as a string of model class
     * @param type $sector as a character posible value A = Maneuver, B = Allocated, C = Manage
     * @return boolean
     */
    public function deleteStrategic($id = null, $model = null, $sector = 'A') {
        return $this->deleteStrategic($id, $model, $sector);
    }
    
     /**
     * 
     * Function find list of dynamic of work group by budget year or departmentId
     * @author  Numpol.s
     * @param   type $budgetYear as integer of budget year id [PK] default current budget year id
     * @param   type $departmentId as integer of department id default get current session department id
     * @return  array List Of workGroup
     */
//    public function getWorkGroupManeuver($budgetYear = null, $departmentId = null, $model = null, $fromTable = null, $joinTable = null) {
//        return $this->getWorkGroupManeuver($budgetYear = null, $departmentId = null, $model = null, $fromTable = null, $joinTable = null);
//    }

    /**
     * function check process after delete  
     * @author  suphakid.s
     * @param type $id to integer id process now
     * @param type $model to string model now
     * @param type $afterHasProcess to integer has_process_id after
     * @return type boolean
     */
    public function countCheckProcess($id = null, $model = '', $afterHasProcess = null) {
        $sql = "SELECT count(id) FROM master.inboxs WHERE ref_id = {$id} AND ref_model = '{$model}' AND system_has_process_id = {$afterHasProcess};";
        $result = $this->query($sql);
        return ($result[0][0]['count'] > 0) ? false : true;
    }

    public function checkCode($table = '', $code = '', $year = '', $modelParent = '', $parentId = null, $action = 'add', $beforeCode = '') {
        $sql = '';
        $parent = "";
        if (empty($modelParent)) {
            $parent = "";
        } else {
            $modelParent = Inflector::underscore($modelParent) . '_id';
            $parent = " AND a.{$modelParent} = {$parentId}";
        }
        if (strtolower($action) == 'edit') {
            if ($beforeCode == $code) {
                return true;
            } else {
                $sql = "SELECT count(a.code) AS count FROM {$table} AS a WHERE a.code = '{$code}' AND a.budget_year_id = {$year} {$parent};";
                $result = $this->query($sql);
                return (($result[0][0]['count'] + 0) > 0) ? false : true;
            }
        } else {
            $sql = "SELECT count(a.code) AS count FROM {$table} AS a WHERE a.code = '{$code}' AND a.budget_year_id = {$year} {$parent};";
            $result = $this->query($sql);
            return (($result[0][0]['count'] + 0) > 0) ? false : true;
        }
    }

}
