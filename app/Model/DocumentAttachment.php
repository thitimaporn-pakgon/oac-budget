<?php

App::uses('AppModel', 'Model');

/**
 * DocumentAttachment Model
 *
 * @property Project $Project
 * @property ProjectPlan $ProjectPlan
 */
class DocumentAttachment extends AppModel {

    public $useDbConfig = 'master';

    /**
     * 
     * Insert document attachment for each anothe budget project or project plan
     * @author  sarawutt.b
     * @param   type $fileResults as file upload return resource
     * @param   type $params
     * @return  boolean
     */
    public function saveDocumentAttachment($fileResults = array(), $params = array()) {
        if (empty($fileResults) || !is_array($fileResults) || empty($params) || !is_array($params)) {
            return false;
        }

        $data = array();
        $data[$this->alias]['document_type'] = $fileResults['type'][0];
        $data[$this->alias]['attachment_name_origin'] = $fileResults['origin_name'][0];
        $data[$this->alias]['attachment_name'] = $fileResults['upfilename'][0];
        $data[$this->alias]['display_name'] = $fileResults['origin_name'][0];
        $data[$this->alias]['attachment_extension'] = $fileResults['ext'][0];
        $data[$this->alias]['attachment_path'] = $fileResults['urls'][0];

        $data[$this->alias]['ref_model'] = isset($params['ref_model']) ? $params['ref_model'] : null;
        $data[$this->alias]['ref_id'] = isset($params['ref_id']) ? $params['ref_id'] : null;
        $data[$this->alias]['status'] = 'A';
        $data[$this->alias]['create_uid'] = $this->getCurrenSessionUserId();

        $this->create();
        return $this->save($data);
    }

    // perhaps after deleting a record from the database, you also want to delete
// an associated file
    public function afterDelete() {
//        $file = new File(rtrim(WWW_ROOT, DS) . $this->data[$this->alias]['attachment_path']);
//        $file = new File(rtrim(WWW_ROOT, DS) . $this->data[$this->alias]['attachment_path'], false, 0777);
//        $file->delete();
        return true;
    }

    /**
     * 
     * Function read all Document attachment with custom conditions
     * @author sarawutt.b
     * @param type $refId as a integer of reference id
     * @param type $refModel as a string of reference model
     * @return array()
     */
    public function readAllDocumentAttachment($refId = null, $refModel = null) {
        return $this->find('all', array('conditions' => array('ref_id' => $refId, 'ref_model' => $refModel, 'status' => 'A'), 'recursive' => -1, 'order' => 'id ASC'));
    }

}
