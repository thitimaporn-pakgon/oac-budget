<?php

App::uses('AppModel', 'Model');

/**
 * NamePrefix Model
 *
 */
class NamePrefix extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'master';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListNamePrefix() {
        $fieldName = ($this->getCurrentLanguage() == 'tha') ? 'name' : 'name_eng';
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', $fieldName), 'order' => array($fieldName => 'asc', 'id' => 'asc', 'modified' => 'asc'), 'conditions' => array('status' => 'A')));
    }
    
    public function getNamePrefixById($id = null) {
        $a = $this->find('first', array('fields' => array('name'), 'conditions' => array('id' => $id)));
//        return (!empty($a['NamePrefix']['name'])) ? $a: '';
        return $a['NamePrefix']['name'];
    }
    
    

}
