<?php

App::uses('AppModel', 'Model');

class SystemName extends AppModel {

    public $name = 'SystemName';
    public $useDbConfig = 'budget';
    public $displayField = 'name';
    public $useTable = 'system_names';
    public $primaryKey = 'id';

    public function findListActionByControllerId($sysControllerId = null) {
        $condition = (is_null($sysControllerId) || !is_numeric($sysControllerId)) ? null : array('sys_controller_id' => $sysControllerId);
        return $this->find('list', array('conditions' => array($condition, 'status' => 'A'), 'recursive' => -1, 'order' => array('sys_controller_id' => 'asc', 'id' => 'asc', 'name' => 'asc')));
    }

    public function findListSystemName($id = null) {
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);       
        return $this->find('list');
    }

}
