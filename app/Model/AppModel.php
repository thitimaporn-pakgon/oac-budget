<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public $select_empty_msg = '---- please select ----';
    protected $_dataSource = null;
    public $uses = array('ExpenseHeaderManeuver', 'ExpenseHeaderAllocated');
    protected $_maneuverModelList = array('StrategicManeuver' => 'digit_strategic', 'PlanManeuver' => 'digit_plan', 'ResultManeuver' => 'digit_result', 'EvenManeuver' => 'digit_event', 'WorkGroupManeuver' => 'digit_work_group', 'WorkManeuver' => 'digit_work', 'ProjectManeuver' => 'digit_project', 'ProjectListManeuver' => 'digit_project_list');
    protected $_allocatedModelList = array('StrategicAllocated' => 'digit_strategic', 'PlanAllocated' => 'digit_plan', 'ResultAllocated' => 'digit_result', 'EvenAllocated' => 'digit_event', 'WorkGroupAllocated' => 'digit_work_group', 'WorkAllocated' => 'digit_work', 'ProjectAllocated' => 'digit_project', 'ProjectListAllocated' => 'digit_project_list');

    /**
     *
     * Function construct make object bouilder
     * @author sarawutt.b
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->_loadInitialModel($this->uses);
    }

    /**
     *
     * Function load initial for needed model
     * @author  sarawutt.b
     * @param   type $uses as array of needed model list
     * @return  boolean true when finit process
     */
    protected function _loadInitialModel($uses = array()) {
        if (!is_array($uses)) {
            $uses = array($uses);
        }
        foreach ($uses as $model) {
            $this->$model = ClassRegistry::init($model);
        }
        return true;
    }

    /**
     * ------------------------------------------------------------------------------------------------------------------------------------
     * Custom funtion start with below on this section
     * ------------------------------------------------------------------------------------------------------------------------------------
     */

    /**
     * 
     * Unformate currentcy where receiving params formate befor matematic operate
     * @author  sarawutt.b
     * @param   type $currency as string in currentcy format
     * @return  double
     */
    public function unformatCurrency($currency = 0, $showPrecision = true) {
        $currency = (strpos($currency, '.') === false) ? $currency . '.00' : $currency;
        $tmp = preg_replace('/[^\d]/', '', $currency);
        return ($showPrecision === true) ? substr($tmp, 0, -2) . "." . substr($tmp, -2) : substr($tmp, 0, -2);
    }

    /**
     *
     * Function generate budget code use for generate budget code.
     * @author  thawatchai.t
     * @param $expenseHeaderId, $part
     * @return array code
     */
    public function generateBudgetCode($expenseHeaderId = null, $part = 'A') {
        return $this->ExpenseHeaderManeuver->generateBudgetCode($expenseHeaderId, $part);
    }

    /**
     *
     * insertExpenseHeader.
     * @author  thawatchai.t
     * @param $expenseHeaderId, $part
     * @return array code
     *
     */
    public function insertExpenseHeader($expenseHeaderId = null, $part = 'A') {
        return $this->ExpenseHeaderManeuver->insertExpenseHeader($expenseHeaderId, $part);
    }

    /**
     *
     * Before insert or update on each model then lookup for create_uid when insert for new reccord and looking fore update_uid whent update the record
     * @author sarawutt.b
     * @param type $options
     * @return boolean
     */
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['id'])) {
            $this->data[$this->alias]['update_uid'] = $this->getCurrenSessionUserId();

            /**
             * 
             * Update structure block expens header digit code
             * @author sarawutt.b
             */
            if (array_key_exists($this->alias, $this->_maneuverModelList)) {
                $conditions = array("ExpenseHeaderManeuver." . Inflector::underscore($this->alias) . '_id' => $this->data[$this->alias]['id']);
                if ($this->ExpenseHeaderManeuver->hasAny($conditions)) {
                    $data = array();
                    if (in_array($this->alias, array('StrategicManeuver')) and isset($this->data[$this->alias]['budget_year_id'])) {//Check for if Strategic then update digit budget year together 
                        $data[$this->alias]['digit_budget_year'] = substr($this->data[$this->alias]['budget_year_id'], 2, 2);
                    }
                    $data['ExpenseHeaderManeuver'][$this->_maneuverModelList[$this->alias]] = $this->data[$this->alias]['code'];
                    $this->ExpenseHeaderManeuver->updateAll($data['ExpenseHeaderManeuver'], $conditions);
                }
            } else if (array_key_exists($this->alias, $this->_allocatedModelList)) {
                $conditions = array("ExpenseHeaderAllocated." . Inflector::underscore($this->alias) . '_id' => $this->data[$this->alias]['id']);
                if ($this->ExpenseHeaderAllocated->hasAny($conditions)) {
                    $data = array();
                    if (in_array($this->alias, array('StrategicAllocated')) and isset($this->data[$this->alias]['budget_year_id'])) {//Check for if Strategic then update digit budget year together 
                        $data[$this->alias]['digit_budget_year'] = substr($this->data[$this->alias]['budget_year_id'], 2, 2);
                    }
                    $data['ExpenseHeaderAllocated'][$this->_allocatedModelList[$this->alias]] = $this->data[$this->alias]['code'];
                    $this->ExpenseHeaderAllocated->updateAll($data['ExpenseHeaderAllocated'], $conditions);
                }
            }
        } else {
            $this->data[$this->alias]['create_uid'] = $this->getCurrenSessionUserId();
        }
        return true;
    }

    public function getEmptySelect() {
        return array('' => __($this->select_empty_msg));
    }

    public function getCurrenSessionUserId() {
        return CakeSession::read('Auth.User.id');
    }

    public function getCurrenSessionRoleId() {
        return CakeSession::read('Auth.User.role_id');
    }

    public function checkAuthUsers() {
        return CakeSession::check('Auth.User');
    }

    public function getCurrentLanguage() {
        return CakeSession::read('SessionLanguage');
    }

    public function getCurrenSessionDepartmentId() {
        return CakeSession::read('Auth.User.department_id');
    }

    /**
     *
     * Start for make the transaction
     * @author  sarawutt.b
     * @return object of the transaction
     */
    public function begin() {
        $this->_dataSource = $this->getDataSource();
        return $this->_dataSource->begin();
    }

    /**
     *
     * Commit permanance has change on each table
     * @author  sarawutt.b
     * @return object of the transaction
     */
    public function commit() {
        return $this->_dataSource->commit();
    }

    /**
     *
     * Rollback to before has changed
     * @author  sarawutt.b
     * @return object of the transaction
     */
    public function rollback() {
        return $this->_dataSource->rollback();
    }

    /**
     * 
     * Function used fro generate _VERSION_
     * @author  sarawutt.b
     * @return  biginteger of the version number
     */
    public function VERSION() {
        $parts = explode(' ', microtime());
        $micro = $parts[0] * 1000000;
        return(substr(date('YmdHis'), 2) . sprintf("%06d", $micro));
    }

    /**
     * 
     * Function used for generate UUID key patern
     * @author  sarawutt.b
     * @return  string uuid in version
     */
    function UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    /**
     * 
     * @author Numpol.J
     * @param type $id as a integer id of model were your sandin g together
     * @param type $model as a string of model class
     * @param type $sector as a character posible value A = Maneuver, B = Allocated, C = Manage
     * @return boolean
     */
//    public function deleteChildrenCheck($id = null, $model = null, $sector = 'A') {
//        $this->autoRender = false;
//        $sectorList = array('A' => 'Maneuver', 'B' => 'Allocated', 'C' => 'Manage');
//        $_modelList = array("Strategic{$sectorList[$sector]}" => "Strategic{$sectorList[$sector]}", "Plan{$sectorList[$sector]}" => "Plan{$sectorList[$sector]}", "Result{$sectorList[$sector]}" => "Result{$sectorList[$sector]}", "Event{$sectorList[$sector]}" => "Event{$sectorList[$sector]}", "WorkGroup{$sectorList[$sector]}" => "WorkGroup{$sectorList[$sector]}", "Work{$sectorList[$sector]}" => "Work{$sectorList[$sector]}", "Project{$sectorList[$sector]}" => "Project{$sectorList[$sector]}", "ProjectList{$sectorList[$sector]}" => "ProjectList{$sectorList[$sector]}");
//        $model = Inflector::camelize("{$model}{$sectorList[$sector]}");
//
//        foreach ($_modelList as $k => $v) {
//            if ($k != $model) {
//                continue;
//            }
//            $this->_loadInitialModel($v);
//            $conditions = array(Inflector::underscore($_modelList[$model]) . '_id' => $id);
//            debug($v . ' => ' . array_search($id, $conditions) . ' => ' . $id);
//            if ($this->$v->hasAny($conditions)) {
//                debug('yes has any');
//                //return true;
//            } else {
//                debug('not has any');
//                //return false;
//            }
//            
//        }
//        exit;
//    }

    public function deleteChildrenCheck($id = null, $model = null, $sector = 'A') {
        $sectorList = array('A' => 'Maneuver', 'B' => 'Allocated', 'C' => 'Manage');
        $_modelList = array("Strategic{$sectorList[$sector]}", "Plan{$sectorList[$sector]}", "Result{$sectorList[$sector]}", "Event{$sectorList[$sector]}", "WorkGroup{$sectorList[$sector]}", "Work{$sectorList[$sector]}", "Project{$sectorList[$sector]}", "ProjectList{$sectorList[$sector]}");
        $model = Inflector::camelize("{$model}{$sectorList[$sector]}");
        $startIndex = array_search($model, $_modelList) + 1;
        for ($i = $startIndex; $i < count($_modelList); $i++) {
            $this->_loadInitialModel($_modelList[$i]);
            $conditions = array(Inflector::underscore($_modelList[$i - 1]) . '_id' => $id);
            if ($this->$_modelList[$i]->hasAny($conditions)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 
     * @author Numpol.J
     * @param type $id as a integer id of model were your sandin g together
     * @param type $model as a string of model class
     * @param type $sector as a character posible value A = Maneuver, B = Allocated, C = Manage
     * @return boolean
     */
    public function deleteStrategic($id = null, $model = null, $sector = 'A') {
        $transOK = TRUE;
        $sectorList = array('A' => 'Maneuver', 'B' => 'Allocated', 'C' => 'Manage');
        $modelClass = Inflector::camelize("{$model}{$sectorList[$sector]}");
        if (!$this->deleteChildrenCheck($id, $model, $sector)) {
            $this->_loadInitialModel(array($modelClass, 'Inbox'));

            $this->$modelClass->begin();
            $this->Inbox->begin();
            if ($this->Inbox->deleteAll(array('Inbox.ref_id' => $id, 'Inbox.ref_model' => $modelClass)) === false) {
                $transOK = false;
            }
            if ($this->$modelClass->delete($id) === false) {
                $transOK = false;
            }
            if ($transOK === true) {
                $this->$modelClass->commit();
                $this->Inbox->commit();
            } else {
                $this->$modelClass->rollback();
                $this->Inbox->rollback();
            }
            return $transOK;
        } else {
            return false;
        }
    }
    
    /**
     * 
     * Function find list of dynamic of work group by budget year or departmentId
     * @author  Numpol.s
     * @param   type $budgetYear as integer of budget year id [PK] default current budget year id
     * @param   type $departmentId as integer of department id default get current session department id
     * @return  array List Of workGroup
     */
//    public function getWorkGroupManeuver($budgetYear = null, $departmentId = null, $model = null, $fromTable = null, $joinTable = null) {
//        $fromTableList = array('A' => 'WorkGroupManeuver', 'B' => 'WorkManeuver', 'C' => 'ProjectManeuver');
//        $modelClass = Inflector::camelize("{$fromTableList[$fromTable]}");
//        debug($fromTableList); exit;
//        $this->_loadInitialModel('Utility', $model);
//        $budgetYear = (is_null($budgetYear)) ? $this->Utility->getCurrenBudgetYearTH() : $budgetYear;
//        $departmentId = (is_null($departmentId)) ? $this->getCurrenSessionDepartmentId() : $departmentId;
//        $sql = "SELECT workGroup.id,workGroup.name 
//                FROM maneuver.work_group_maneuvers  AS workGroup
//                INNER JOIN maneuver.work_maneuvers AS work ON work.work_group_maneuver_id = workGroup.id
//                INNER JOIN master.outboxs AS outbox ON outbox.ref_id = work.id AND outbox.ref_model = {$model}
//                WHERE outbox.to_department_id = {$departmentId} AND work.budget_year_id = {$budgetYear}";
//        $results = $this->query($sql);
//        $returnList = array();
//        foreach ($results as $k => $v) {
//            $returnList[$v[0]['id']] = $v[0]['name'];
//        }
//        return $this->getEmptySelect() + $returnList;
//    }

}
