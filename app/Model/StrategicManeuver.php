<?php

App::uses('AppModel', 'Model');

/**
 *
 * StrategicManeuver Model
 * @author  sarawutt.b
 * @property BudgetYear $BudgetYear
 * @property SystemHasProcess $SystemHasProcess
 * @property PlanManeuver $PlanManeuver
 * @since   2017-04-18 15:02:57
 * @license Zicure Corp. 
 */
class StrategicManeuver extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'budget';
    public $uses = array('ExpenseHeaderManeuver', 'ExpenseDetailManeuver', 'PlanManeuver', 'ResultManeuver', 'EventManeuver', 'WorkGroupManeuver', 'WorkManeuver', 'ProjectManeuver', 'ProjectListManeuver', 'StrategicManeuver', 'BudgetYear');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public function findListStrategicManeuver($id = null) {
        return $this->getEmptySelect() + $this->find('list');
    }

    public function getPullDataManeuver($budgetYear = null) {
        $data = $this->find('all', array('conditions' => array('budget_year_id' => $budgetYear)));
        foreach ($data AS $k => $v) {
            if (empty($v['ExpenseHeaderManeuver'])) {
                continue;
            }
            foreach ($v['ExpenseHeaderManeuver'] AS $kk => $vv) {
                $expenseHeadder = $this->ExpenseHeaderManeuver->find('first', array('conditions' => array('ExpenseHeaderManeuver.id' => $vv['id'])));
                $data[$k]['ExpenseDetailManeuver'] = $expenseHeadder['ExpenseDetailManeuver'];
            }
        }
        return $data;
    }

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->_loadInitialModel($this->uses);
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    public function getStructureLevel($id = null) {
//        $sql = "SELECT * FROM (
//                    SELECT 0 AS level, code,name,id FROM  maneuver.strategic_maneuvers  WHERE   id = {$id}
//                    UNION
//                    SELECT 1 AS level, code,name,id FROM  maneuver.plan_maneuvers WHERE strategic_maneuver_id = {$id}
//                    UNION
//                    SELECT 2 AS level, code,name,id FROM  maneuver.result_maneuvers WHERE strategic_maneuver_id = {$id}
//                    UNION
//                    SELECT 3 AS level, code,name,id FROM  maneuver.event_maneuvers WHERE strategic_maneuver_id = {$id}
//                    UNION
//                    SELECT 4 AS level, code,name,id FROM  maneuver.work_group_maneuvers WHERE strategic_maneuver_id = {$id}
//                ) AS t
//                ORDER BY level ASC";
//        $result = $this->query($sql);
//        return $result;
    }

    public function getStrategicDelete($strategicDeletes = null, $budgetYear = null) {
        $transOK = true;
        foreach ($strategicDeletes as $k => $strategicDelete) {
            if ($this->ExpenseDetailManeuver->deleteAll(array('ExpenseDetailManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->ExpenseHeaderManeuver->deleteAll(array('ExpenseHeaderManeuver.digit_budget_year' => $this->BudgetYear->conversBudgetYear($budgetYear))) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->ProjectListManeuver->deleteAll(array('ProjectListManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->ProjectManeuver->deleteAll(array('ProjectManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->WorkManeuver->deleteAll(array('WorkManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->WorkGroupManeuver->deleteAll(array('WorkGroupManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->EventManeuver->deleteAll(array('EventManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->ResultManeuver->deleteAll(array('ResultManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->PlanManeuver->deleteAll(array('PlanManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
            if ($this->StrategicManeuver->deleteAll(array('StrategicManeuver.budget_year_id' => $budgetYear)) === FALSE) {
                $transOK = FALSE;
            }
        }
        return $transOK;
    }

    /**
     * belongsTo associations
     *
     * @var array
     */
//    public $belongsTo = array(
//        'BudgetYear' => array(
//            'className' => 'BudgetYear',
//            'foreignKey' => 'budget_year_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        ),
//        'SystemHasProcess' => array(
//            'className' => 'SystemHasProcess',
//            'foreignKey' => 'system_has_process_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        )
//    );

    /**
     * hasMany associations
     *
     * @var array
     */
//    public $hasMany = array(
//        'PlanManeuver' => array(
//            'className' => 'PlanManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ResultManeuver' => array(
//            'className' => 'ResultManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'EventManeuver' => array(
//            'className' => 'EventManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'WorkGroupManeuver' => array(
//            'className' => 'WorkGroupManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'WorkManeuver' => array(
//            'className' => 'WorkManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ProjectManeuver' => array(
//            'className' => 'ProjectManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ProjectListManeuver' => array(
//            'className' => 'ProjectListManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'ExpenseHeaderManeuver' => array(
//            'className' => 'ExpenseHeaderManeuver',
//            'foreignKey' => 'strategic_maneuver_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
//    );
}
