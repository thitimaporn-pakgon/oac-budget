<?php

App::uses('AppModel', 'Model');

/**
 * Menu Model
 *
 * @property SysController $SysController
 * @property SysAction $SysAction
 * @property SysAcl $SysAcl
 */
class MenuList extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
     public $useDbConfig = 'budget';
      public $useTable = 'menu_lists';
    /**
     * Display field
     *
     * @var string
     */
//     public $uses = array('menu_lists');
    public $displayField = 'name';

//    /**
//     * 
//     * Function list manu page level
//     * @author sarawutt.b
//     * @param type $key as a integer of page level of menu
//     * @return mix 
//     */
//    public function listMenuPageLevel($key = 'xxx') {
//        $options = $this->getEmptySelect() + array('1' => __('หน้าหลัก'), '2' => __('อนุมัติขาลง (ภาคจัดทำ)'), '3' => __('อนุมัติขาขึ้น (ภาคจำทำ)'), '4' => __('อนุมัติขาลง (ภาคจัดสรร)'), '5' => __('อนุมัติขาลง (ภาคจัดสรร)'), '6' => __('อนุมัติขาลง (ภาคบริหาร)'), '7' => __('อนุมัติขาขึ้น (ภาคบริหาร)'));
//        return array_key_exists($key, $options) ? $options[$key] : $options;
//    }
//
//    public function findListMenus($id = null) {
//        $conditions = (is_null($id) || empty($id)) ? null : array('id' => $id);
//        return $this->getEmptySelect() + $this->find('list', array('conditions' => $conditions, 'order' => array('menu_parent_id' => 'ASC', 'child_no' => 'ASC', 'order_display' => 'ASC', 'name' => 'ASC')));
//    }
//
//    public function getMenuType($key = null) {
//        $opt = array('-1' => $this->select_empty_msg, '0' => __('ACL'), '1' => __('MENU'), '2' => __('ALLOW'));
//        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
//    }
//
//    public function countMenu() {
//        return $this->find('count');
//    }
//
//    /**
//     * 
//     * Get dinamemic parent menu or top of the menu
//     * @author sarawutt.b
//     * @param type $domain as string domain name or IP address
//     * @param type $port as integer of server port number
//     * @param type $role_id as integer of the role ID
//     * @param type $user_id as integer of the user ID
//     * @return array()
//     */
//    public function getDynamicAclMenu($domain = null, $port = '80', $role_id = 0, $user_id = 0, $pageLevel = 1) {
//        $port = (($port == '443') || ($port == '80')) ? "'443','80'" : "'{$port}'";
//        $sql = "SELECT * FROM(
//                    SELECT DISTINCT a.id, a.glyphicon, a.domain, a.port, a.url, a.name, a.name_eng, a.menu_parent_id, a.child_no, a.order_display
//                    FROM system.menus AS a
//                    INNER JOIN (SELECT m.menu_parent_id
//				FROM system.menus as m
//				INNER JOIN system.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
//				WHERE (acl.role_id = $role_id OR acl.user_id = $user_id) AND domain='{$domain}' AND port in({$port}) AND m.status = 'A') AS b 
//                            ON b.menu_parent_id = a.id
//                    WHERE a.status = 'A' AND (a.url = '#' OR a.url ILIKE 'http://%' OR a.url ILIKE 'https://%') AND a.page_level = {$pageLevel}
//                    UNION 
//                    SELECT a.id, a.glyphicon, a.domain, a.port, a.url, a.name, a.name_eng, a.menu_parent_id, a.child_no, a.order_display
//                    FROM system.menus as a
//                    INNER JOIN system.sys_acls AS acl ON acl.sys_controller_id = a.sys_controller_id AND acl.sys_action_id = a.sys_action_id
//                    INNER JOIN (SELECT m.menu_parent_id
//                                FROM system.menus as m
//                                INNER JOIN system.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
//                                WHERE (acl.role_id = $role_id OR acl.user_id = $user_id) AND domain='{$domain}' AND port in({$port}) AND m.status = 'A') AS b  
//                            ON b.menu_parent_id = a.id
//                    WHERE (acl.role_id = $role_id OR acl.user_id = $user_id) AND a.domain='{$domain}' AND a.port in({$port})
//                        AND acl.status = 'A' AND a.status = 'A' AND a.menu_parent_id = 0 AND a.page_level = {$pageLevel}
//                ) AS tmenu
//                ORDER BY menu_parent_id ASC, child_no ASC, order_display ASC;";
//        return $this->query($sql);
//    }
//
//    /**
//     * 
//     * Find for dinamic choldrent manu
//     * @author sarawutt.b
//     * @param type $domain as string domain name or IP address
//     * @param type $port as integer of server port number
//     * @param type $role_id as integer of the role ID
//     * @param type $user_id as integer of the user ID
//     * @param type $menu_parent_id as integer of menu parent ID
//     * @return array()
//     */
//    public function getDynamicChildMenu($domain = null, $port = '80', $role_id = 0, $user_id = 0, $menu_parent_id = -1) {
//        $port = (($port == '443') || ($port == '80')) ? "'443','80'" : "'{$port}'";
//        $sql = "SELECT m.id, glyphicon, domain, port, url, m.name, m.name_eng, menu_parent_id, child_no, order_display, badge
//                FROM system.menus as m
//                LEFT JOIN system.sys_acls AS acl ON acl.sys_controller_id = m.sys_controller_id AND acl.sys_action_id = m.sys_action_id
//                WHERE (((acl.role_id = {$role_id} OR acl.user_id = {$user_id}) AND acl.status = 'A') OR m.url = '/') AND domain='{$domain}' AND port IN({$port})
//                     AND m.status = 'A' AND menu_parent_id = {$menu_parent_id}
//                ORDER BY menu_parent_id ASC, child_no ASC, order_display ASC;";
//        return $this->query($sql);
//    }
//
//    /**
//     * Find list of menus where the menu is parent menu 
//     * @author  sarawutt.b
//     * @param   integer of menu id
//     * @since   2016/05/11
//     * @return  array()
//     */
    public function findListParentMenu($id = null) {
        $conditions = (is_null($id) || empty($id)) ? array() : array('id' => $id);
        return $this->getEmptySelect() + $this->find('list', array('conditions' => array_merge($conditions, array('url' => '#')), 'order' => array('menu_parent_id' => 'ASC', 'child_no' => 'ASC', 'order_display' => 'ASC', 'name' => 'ASC')));
    }
//
//    /**
//     * 
//     * Finf dinamic count for badge on the menu left side
//     * @author  sarawutt.b
//     * @param   string $sql of query excutetion
//     * @param   string $statusList of project and plan status list
//     * @return  integer of badge count
//     */
//    public function badgeFinder($sql = null, $statusList = null) {
//        $currentDepartmentId = $this->getCurrenSessionDepartmentId();
//        $condition = null;
//        if (strpos($sql, 'master.inboxs') !== false) {
//            $condition = " AND to_department_id = {$currentDepartmentId};";
//        } elseif (strpos($sql, 'master.outboxs') !== false) {
//            $condition = " AND from_department_id = {$currentDepartmentId};";
//        }
//
//        try {
//            $sql .= $condition;
//            $result = $this->query($sql);
//            $count = @$result[0][0][key($result[0][0])];
//            return is_numeric($count) ? $count : 0;
//        } catch (Exception $e) {
//            return 0;
//        }
//    }
//
//    /**
//     * BelongTo associations
//     *
//     * @var array
//     */
//    public $belongTo = array(
//        'SysController' => array(
//            'className' => 'SysController',
//            'foreignKey' => 'sys_controller_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'SysAction' => array(
//            'className' => 'SysAction',
//            'foreignKey' => 'sys_action_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
//    );

}
