<?php

App::uses('AppModel', 'Model');

/**
 *
 * WorkGroupManeuver Model
 * @author  sarawutt.b
 * @property EventManeuver $EventManeuver
 * @property BudgetYear $BudgetYear
 * @property SystemHasProcess $SystemHasProcess
 * @property WorkManeuver $WorkManeuver
 * @since   2017-04-18 15:03:59
 * @license Zicure Corp. 
 */
class WorkGroupManeuver extends AppModel {

    /**
     * Use database config
     *
     * @var string
     */
    public $useDbConfig = 'maneuver';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * 
     * Function find list of dynamic of work group by budget year or departmentId
     * @author  Numpol.s
     * @param   type $budgetYear as integer of budget year id [PK] default current budget year id
     * @param   type $departmentId as integer of department id default get current session department id
     * @return  array List Of workGroup
     */
    public function getWorkGroupManeuver($budgetYear = null, $departmentId = null) {
        $this->_loadInitialModel('Utility');
        $budgetYear = (is_null($budgetYear)) ? $this->Utility->getCurrenBudgetYearTH() : $budgetYear;
        $departmentId = (is_null($departmentId)) ? $this->getCurrenSessionDepartmentId() : $departmentId;
        $sql = "SELECT workGroup.id,workGroup.name 
                FROM maneuver.work_group_maneuvers  AS workGroup
                INNER JOIN maneuver.work_maneuvers AS work ON work.work_group_maneuver_id = workGroup.id
                INNER JOIN master.outboxs AS outbox ON outbox.ref_id = work.id AND outbox.ref_model = 'WorkManeuver'
                WHERE outbox.to_department_id = {$departmentId} AND work.budget_year_id = {$budgetYear}";
        $results = $this->query($sql);
        $returnList = array();
        foreach ($results as $k => $v) {
            $returnList[$v[0]['id']] = $v[0]['name'];
        }
        return $this->getEmptySelect() + $returnList;
    }

    public function findListWorkGroupManeuver($id = null) {
        $condition = (is_null($id) || !is_numeric($id)) ? null : array('id' => $id);
        return $this->getEmptySelect() + $this->find('list', array('fields' => array('id', 'name'), 'conditions' => array($condition), 'order' => array('name' => 'ASC'), 'recursive' => -1));
    }

    public function getDataTree($id = null) {
        $data = $this->find('all', array('fields' => array('id', 'name', 'code', 'strategic_maneuver_id'), 'conditions' => array('event_maneuver_id' => $id)));
        return $data;
    }

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'EventManeuver' => array(
            'className' => 'EventManeuver',
            'foreignKey' => 'event_maneuver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'BudgetYear' => array(
            'className' => 'BudgetYear',
            'foreignKey' => 'budget_year_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SystemHasProcess' => array(
            'className' => 'SystemHasProcess',
            'foreignKey' => 'system_has_process_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'StrategicManeuver' => array(
            'className' => 'StrategicManeuver',
            'foreignKey' => 'strategic_maneuver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PlanManeuver' => array(
            'className' => 'PlanManeuver',
            'foreignKey' => 'plan_maneuver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ResultManeuver' => array(
            'className' => 'ResultManeuver',
            'foreignKey' => 'result_maneuver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'WorkManeuver' => array(
            'className' => 'WorkManeuver',
            'foreignKey' => 'work_group_maneuver_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ProjectManeuver' => array(
            'className' => 'ProjectManeuver',
            'foreignKey' => 'work_group_maneuver_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ProjectListManeuver' => array(
            'className' => 'ProjectListManeuver',
            'foreignKey' => 'work_group_maneuver_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ExpenseHeaderManeuver' => array(
            'className' => 'ExpenseHeaderManeuver',
            'foreignKey' => 'work_group_maneuver_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Inbox' => array(
            'className' => 'Inbox',
            'foreignKey' => 'ref_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
