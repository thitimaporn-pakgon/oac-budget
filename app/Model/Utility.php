<?php

App::uses('AppModel', 'Model');

class Utility extends AppModel {

    public $useDbConfig = 'default';
    public $useTable = false;

    public function getMainStatus($key = 'xxx') {
        //$opt = array('' => __($this->select_empty_msg), 'N' => __('New'), 'A' => __('Active'), 'I' => __('Inactive'), 'D' => __('Delete'));
        $opt = array('' => __($this->select_empty_msg), 'A' => __('Active'), 'I' => __('Inactive'));
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    public function getSex($key = 'xxx') {
        $opt = array('' => __($this->select_empty_msg), 'M' => __('Male'), 'F' => __('Fe-male'));
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    public function getUnlocked($key = 'xxx') {
        $opt = array('' => __($this->select_empty_msg), '<4' => __('Use'), '>=4' => __('locked'));
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

//    public function getProjectPlanStatus($key = 'xxx') {
//        $opt = array('' => __($this->select_empty_msg), 'DPL' => __('Draft'), 'APL' => __('Approved'), 'RPL' => __('Reject'), 'CAN' => __('Cancel'),'DEL'=>__('Delete'));
//        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
//    }

    public function getPriority($key = 'xxx') {
        $opt = array('' => __('Please select one'), '1' => __('1'), '2' => __('2'));
        return array_key_exists($key, $opt) ? $opt[$key] : $opt;
    }

    /*
     * This function for check current user where logedin can use any cnotroller/action
     * @author Sarawutt.b
     * @since 20/10/2013
     * @params Permission Mode True in has permission FALSE in not has
     * @return void
     */

    public function isPermission($isPermission = true, $url = NULL) {
        $bresult = FALSE;
        if (($isPermission === TRUE) && ($this->checkAuthUsers())) {
            $current_cotroller = strtolower($this->params['controller']);
            $current_action = strtolower($this->params['action']);
            $current_role_id = $this->getCurrenSessionRoleId();
            $current_user_id = $this->getCurrenSessionUserId();

            if (!empty($url)) {
                $urls = explode('/', $url);
                if (isset($urls[1]) && !empty($urls[1])) {
                    $current_cotroller = strtolower($urls[1]);
                }
                if (isset($urls[2]) && !empty($urls[2])) {
                    $current_action = strtolower($urls[2]);
                }
            }

            $sql = "SELECT COUNT(acl.id) AS count_permission 
                    FROM system.sys_acls AS acl
                    LEFT JOIN system.sys_actions AS _action ON _action.id=acl.sys_action_id
                    LEFT JOIN system.sys_controllers AS  controll ON controll.id=acl.sys_controller_id
                    WHERE acl.status='A' 
                        AND LOWER(controll.name)='{$current_cotroller}'
                        AND LOWER(_action.name)='{$current_action}'
                        AND (acl.role_id={$current_role_id} OR acl.user_id={$current_user_id});";
            $result = $this->query($sql);
            $bresult = ($result[0][0]['count_permission'] > 0);
        } else {
            $bresult = TRUE;
        }
        return $bresult;
    }



    public function checkCode($table = '', $code = '', $year = '', $action = 'add', $beforeCode = '') {
        $sql = '';
        if (strtolower($action) == 'edit') {
            if ($beforeCode == $code) {
                return true;
            } else {
                $sql = "SELECT count(a.code) FROM {$table} AS a WHERE a.code = '{$code}' AND a.budget_year_id = {$year};";
                $result = $this->query($sql);
                return ($result[0][0]['count'] > 0) ? false : true;
            }
        } else {
            $sql = "SELECT count(a.code) FROM {$table} AS a WHERE a.code = '{$code}' AND a.budget_year_id = {$year};";
            $result = $this->query($sql);
            return ($result[0][0]['count'] > 0) ? false : true;
        }
    }

    /**
     * Function get Curren BudgetYear 
     * @author  suphakid.s
     * @return string
     * 
     */
    public function getCurrenBudgetYearTH() {
//        $yearCurren = date('Y') + 543;
//        if (date('m') >= 10) {
//            $yearCurren = date('Y') + 544;
//        }
        return (date('m') >= 10) ? date('Y') + 544 : date('Y') + 543;
    }

    /**
     * 
     * @author  suphakid.s
     * @param string $afterDepartment AS after department ex: first = หน่วยหลัก, secondary = หน่วยรอง ,work = หน่วยปฏิบัติ , spc = สปช.
     * @param type $id as id in table process after
     * @return type integer department id 
     */
    public function getToDepartment($afterDepartment = '', $id = null) {
        $table = '';
        if ($afterDepartment == 'first') {
            $table = 'maneuver.work_group_maneuvers';
        } elseif ($afterDepartment == 'secondary') {
            $table = 'maneuver.project_maneuvers';
        } elseif ($afterDepartment == 'work') {
            $table = 'maneuver.project_list_maneuvers';
        } elseif ($afterDepartment == 'spc') {
            $table = 'maneuver.work_maneuvers';
        }
        $sql = "SELECT t.from_department_id AS department FROM {$table} AS t WHERE t.id = {$id}";
        $result = $this->query($sql);
        return (empty($result[0][0]['department'])) ? false : $result[0][0]['department'];
    }

}
