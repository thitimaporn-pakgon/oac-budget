<?php
/**
 *
 * Add page for workGroupManeuversController it Add of WorkGroupManeuver.
 * @author sarawutt.b 
 * @since 2017-04-18 17:58:50
 * @license Zicure Corp. 
 */
?>
<div class="workGroupManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Work Group Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('WorkGroupManeuver', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'value' => $year, 'disabled' => true)); ?>
        <?php echo $this->Form->input('strategic', array('readonly' => 'readonly', 'label' => __('Strategic Name'), 'value' => $data['StrategicManeuver']['name'])); ?>
        <?php echo $this->Form->input('plan', array('readonly' => 'readonly', 'label' => __('Work Plan Name'), 'value' => $data['PlanManeuver']['name'])); ?>
        <?php echo $this->Form->input('result', array('readonly' => 'readonly', 'label' => __('Work Product Name'), 'value' => $data['ResultManeuver']['name'])); ?>
        <?php echo $this->Form->input('event', array('readonly' => 'readonly', 'label' => __('Event Name'), 'value' => $data['EventManeuver']['name'])); ?>
        <?php echo $this->Form->input('name', array('class' => 'required', 'label' => __('Work Group Name'))); ?>
        <?php echo $this->Form->input('code', array('class' => 'required')); ?>
        <?php echo $this->Form->input('to_department_id', array('class' => 'required', 'options' => $this->Zicure->listDepartment())); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/WorkGroupManeuvers/add'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
