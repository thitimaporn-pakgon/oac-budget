<?php
/**
*
* View page for workGroupManeuversController it show for WorkGroupManeuver infomation.
* @author sarawutt.b 
* @since 2017-04-18 17:58:50
* @license Zicure Corp. 
*/
?>
<div class="workGroupManeuvers view div-view-information box box-warning">
    	<?php echo $this->element('boxOptionHeader', array('btitle'=>'Work Group Maneuver Information', 'bcollap'=>true, 'bclose'=>true));?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                		<tr>
			<td class="table-view-label"><?php echo __('Work Group Maneuver Id'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Event Maneuver'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($workGroupManeuver['EventManeuver']['name'],'info'), "/event_maneuvers/view/{$workGroupManeuver['EventManeuver']['id']}" ,array('title'=>__('View') . ' ' . __('Event Maneuver'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Code'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['code']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Work Group Maneuver Name'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['name']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Budget Year'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($workGroupManeuver['BudgetYear']['name'],'info'), "/budget_years/view/{$workGroupManeuver['BudgetYear']['id']}" ,array('title'=>__('View') . ' ' . __('Budget Year'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('From Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['from_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('To Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['to_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('System Has Process'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($workGroupManeuver['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$workGroupManeuver['SystemHasProcess']['id']}" ,array('title'=>__('View') . ' ' . __('System Has Process'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Budget Scope'); ?></td>
			<td class="table-view-detail"><?php echo h($workGroupManeuver['WorkGroupManeuver']['budget_scope']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Create Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($workGroupManeuver['WorkGroupManeuver']['create_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Update Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($workGroupManeuver['WorkGroupManeuver']['update_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Created'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($workGroupManeuver['WorkGroupManeuver']['created']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Modified'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($workGroupManeuver['WorkGroupManeuver']['modified']); ?></td>
		</tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    	<?php echo $this->element('boxOptionFooter');?>
</div><!-- ./div.box box-warning -->


    <div class="box box-info box-related">
        	<?php echo $this->element('boxOptionHeader',array('btitle'=>'Work Maneuver Relate List', 'bcollap'=>true, 'bclose'=>true, 'btnnew'=>'WorkManeuvers'));?>
        <div class="box-body">
            <table class="table-view-related-information table table-bodered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#');?></th>
                        		<th><?php echo __('WorkManeuver Id'); ?></th>
		<th><?php echo __('Work Group Maneuver Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('WorkManeuver Name'); ?></th>
		<th><?php echo __('Budget Year Id'); ?></th>
		<th><?php echo __('From Department Id'); ?></th>
		<th><?php echo __('To Department Id'); ?></th>
		<th><?php echo __('System Has Process Id'); ?></th>
                        		<th class="actions">&nbsp;</th>
                    </tr>
                </thead>

                <?php if(!empty($workGroupManeuver['WorkManeuver'])):?>
	<?php foreach ($workGroupManeuver['WorkManeuver'] as $k => $workManeuver): ?>
		<tr>
			<td class="nindex-black"><?php echo ++$k;?></td>
			<td><?php echo $workManeuver['id']; ?></td>
			<td><?php echo $workManeuver['work_group_maneuver_id']; ?></td>
			<td><?php echo $workManeuver['code']; ?></td>
			<td><?php echo $workManeuver['name']; ?></td>
			<td><?php echo $workManeuver['budget_year_id']; ?></td>
			<td><?php echo $workManeuver['from_department_id']; ?></td>
			<td><?php echo $workManeuver['to_department_id']; ?></td>
			<td><?php echo $workManeuver['system_has_process_id']; ?></td>
			<td class="actions"><?php echo $this->Permission->getActions($workManeuver['id'], 'WorkManeuvers'); ?></td>
		</tr>
	<?php endforeach; ?>
<?php else:?>
		<tr class="row-notfound">
			<td colspan="10"><?php echo __('Information Not Found');?></td>
		</tr>
<?php endif;?>
            </table>
        </div><!-- ./div.box-body -->
        	<?php echo $this->element('boxOptionFooter');?>
    </div><!-- /div.box box-info -->
    