<div class="projects index">
	<h2><?php echo __('Projects'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_name'); ?></th>
			<th><?php echo $this->Paginator->sort('project_code'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i =1;
	?>
	<?php foreach ($projects as $project): ?>
	<tr>
		<td><?php echo $i;?>&nbsp;</td>
		<!--<td><?php echo h($project['Project']['id']); ?>&nbsp;</td>-->
		<td><?php echo h($project['Project']['project_name']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['project_code']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Permission->link(__('Edit'), "/Projects/edit/{$project['Project']['id']}", array('class' => 'btn btn-sm btn-primary')); ?>
			<?php //echo $this->Html->link(__('Edit'), array('class'=>'btn btn-primary', 'action' => 'edit', $project['Project']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?>
		</td>
	</tr>
	<?php $i++; ?>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
		<!-- Page Pagination Section -->
		<?php echo $this->element('paginate_pages', array('options'=> true));?>
</div>
