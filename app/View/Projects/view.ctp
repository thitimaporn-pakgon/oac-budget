<div class="projects view">
<h2><?php echo __('Project'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($project['Project']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Name'); ?></dt>
		<dd>
			<?php echo h($project['Project']['project_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Code'); ?></dt>
		<dd>
			<?php echo h($project['Project']['project_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Strategy Gov'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['StrategyGov']['id'], array('controller' => 'strategy_govs', 'action' => 'view', $project['StrategyGov']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Strategy Plan'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['StrategyPlan']['id'], array('controller' => 'strategy_plans', 'action' => 'view', $project['StrategyPlan']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Strategy Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['StrategyProduct']['id'], array('controller' => 'strategy_products', 'action' => 'view', $project['StrategyProduct']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Strategy Activity'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['StrategyActivity']['id'], array('controller' => 'strategy_activities', 'action' => 'view', $project['StrategyActivity']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Str Plan'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['StrPlan']['id'], array('controller' => 'str_plans', 'action' => 'view', $project['StrPlan']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Year'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['BudgetYear']['id'], array('controller' => 'budget_years', 'action' => 'view', $project['BudgetYear']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Year'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Begin Year Plan'); ?></dt>
		<dd>
			<?php echo h($project['Project']['begin_year_plan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Year Plan'); ?></dt>
		<dd>
			<?php echo h($project['Project']['end_year_plan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Req1'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_req1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Req2'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_req2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Req Total'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_req_total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Appr1'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_appr1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Appr2'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_appr2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Appr Total'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_appr_total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paid'); ?></dt>
		<dd>
			<?php echo h($project['Project']['paid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Percen Paid'); ?></dt>
		<dd>
			<?php echo h($project['Project']['percen_paid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dept Id0 Head'); ?></dt>
		<dd>
			<?php echo h($project['Project']['dept_id0_head']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dept Id1 Main'); ?></dt>
		<dd>
			<?php echo h($project['Project']['dept_id1_main']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dept Id2 Sub'); ?></dt>
		<dd>
			<?php echo h($project['Project']['dept_id2_sub']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dept Id3 Work'); ?></dt>
		<dd>
			<?php echo h($project['Project']['dept_id3_work']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dept Id4 Owner'); ?></dt>
		<dd>
			<?php echo h($project['Project']['dept_id4_owner']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Appr Stauts'); ?></dt>
		<dd>
			<?php echo h($project['Project']['appr_stauts']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Appr Date'); ?></dt>
		<dd>
			<?php echo h($project['Project']['appr_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Note'); ?></dt>
		<dd>
			<?php echo h($project['Project']['project_note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['User']['id'], array('controller' => 'users', 'action' => 'view', $project['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updt'); ?></dt>
		<dd>
			<?php echo h($project['Project']['updt']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project'), array('action' => 'edit', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Strategy Govs'), array('controller' => 'strategy_govs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Strategy Gov'), array('controller' => 'strategy_govs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Strategy Plans'), array('controller' => 'strategy_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Strategy Plan'), array('controller' => 'strategy_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Strategy Products'), array('controller' => 'strategy_products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Strategy Product'), array('controller' => 'strategy_products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Strategy Activities'), array('controller' => 'strategy_activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Strategy Activity'), array('controller' => 'strategy_activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Str Plans'), array('controller' => 'str_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Str Plan'), array('controller' => 'str_plans', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Budget Years'), array('controller' => 'budget_years', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Budget Year'), array('controller' => 'budget_years', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sent Logs'), array('controller' => 'sent_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sent Log'), array('controller' => 'sent_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sent Logs'); ?></h3>
	<?php if (!empty($project['SentLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sent Type Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Project Plan And Pay Id'); ?></th>
		<th><?php echo __('Pay Appr Id'); ?></th>
		<th><?php echo __('Dept Id Sent'); ?></th>
		<th><?php echo __('Dept Id Receive'); ?></th>
		<th><?php echo __('Dept Id Owner'); ?></th>
		<th><?php echo __('Sent Date'); ?></th>
		<th><?php echo __('Appr Date'); ?></th>
		<th><?php echo __('Appr Status'); ?></th>
		<th><?php echo __('Read Status'); ?></th>
		<th><?php echo __('Read Date'); ?></th>
		<th><?php echo __('Now Status'); ?></th>
		<th><?php echo __('Process No'); ?></th>
		<th><?php echo __('Up Down'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Updt'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($project['SentLog'] as $sentLog): ?>
		<tr>
			<td><?php echo $sentLog['id']; ?></td>
			<td><?php echo $sentLog['sent_type_id']; ?></td>
			<td><?php echo $sentLog['project_id']; ?></td>
			<td><?php echo $sentLog['project_plan_and_pay_id']; ?></td>
			<td><?php echo $sentLog['pay_appr_id']; ?></td>
			<td><?php echo $sentLog['dept_id_sent']; ?></td>
			<td><?php echo $sentLog['dept_id_receive']; ?></td>
			<td><?php echo $sentLog['dept_id_owner']; ?></td>
			<td><?php echo $sentLog['sent_date']; ?></td>
			<td><?php echo $sentLog['appr_date']; ?></td>
			<td><?php echo $sentLog['appr_status']; ?></td>
			<td><?php echo $sentLog['read_status']; ?></td>
			<td><?php echo $sentLog['read_date']; ?></td>
			<td><?php echo $sentLog['now_status']; ?></td>
			<td><?php echo $sentLog['process_no']; ?></td>
			<td><?php echo $sentLog['up_down']; ?></td>
			<td><?php echo $sentLog['user_id']; ?></td>
			<td><?php echo $sentLog['updt']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sent_logs', 'action' => 'view', $sentLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sent_logs', 'action' => 'edit', $sentLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sent_logs', 'action' => 'delete', $sentLog['id']), array('confirm' => __('Are you sure you want to delete # %s?', $sentLog['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sent Log'), array('controller' => 'sent_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
