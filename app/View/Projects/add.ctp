<?php echo $this->Form->create('Project', array('role' => 'form','class'=>'form-horizontal')); ?>
<div class="box projects form">
	<div class="box-header with-border">
	    <h3 class="box-title"><?php echo __('เลือกโครงสร้าง'); ?></h3>
	    <div class="box-tools pull-right"></div>
	</div>
	<div class="box-body">
		<?php
			echo $this->Form->input('budget_year_id', array('class'=>'required', 'label'=>array('text'=>__('ปีงบประมาณ')), 'empty'=>$empty ));
			echo $this->Form->input('strategy_gov_id', array('class'=>'required', 'label'=>array('text'=>__('ยุทธศาสตร์')), 'empty'=>$empty ));
			echo $this->Form->input('strategy_plan_id', array('class'=>'required', 'label'=>array('text'=>__('แผนงาน')), 'empty'=>$empty ));
			echo $this->Form->input('strategy_product_id', array('class'=>'required', 'label'=>array('text'=>__('ผลผลิต')), 'empty'=>$empty ));
			echo $this->Form->input('strategy_activity_id', array('class'=>'required', 'label'=>array('text'=>__('กิจกรรม')), 'empty'=>$empty ));
			echo $this->Form->input('group_plan_id', array('class'=>'required', 'label'=>array('text'=>__('กลุ่มงบงาน')), 'empty'=>$empty ));
			echo $this->Form->input('str_plan_id', array('class'=>'required', 'label'=>array('text'=>__('งบงาน')), 'empty'=>$empty ));

		?>
	</div>
</div>

<div class="box projects form">
	<div class="box-header with-border">
	    <h3 class="box-title"><?php echo __('สร้างการ (Project)'); ?></h3>
	    <div class="box-tools pull-right"></div>
	</div>
	<div class="box-body">
		<?php
		echo $this->Form->input('project_name', array('class'=>'required', 'label'=>array('text'=>__('ชื่อ การ')), 'placeholder'=>__('ชื่อ การ')));
		echo $this->Form->input('project_code', array('class'=>'required', 'label'=>array('text'=>__('รหัส')), 'placeholder'=>__('รหัส')));
		?>
	</div>
</div>

<div class="box projects form">
	<div class="box-header with-border">
	    <h3 class="box-title"><?php echo __('สร้างรายการ'); ?></h3>
	    <div class="box-tools pull-right"></div>
	</div>
	<div class="box-body">
		<span id="plus_0" class="btn btn-sm btn-flat btn-success" onclick="plusPL(0);"><i class="fa fa-plus"></i><?php echo __('เพิ่ม');?></span>
		<?php echo $this->Form->input('task_name', array('type'=>'text', 'name'=>'ProjectTask[0][task_name]', 'class'=>'required', 'label'=>array('text'=>__('รายการ')), 'placeholder'=>__('รายการ'))); ?>
		<?php echo $this->Form->input('task_code', array('type'=>'text', 'name'=>'ProjectTask[0][task_code]', 'class'=>'required', 'label'=>array('text'=>__('รหัส')), 'placeholder'=>__('รหัส'))); ?>
		<?php echo $this->Form->input('budget_req1', array('type'=>'text', 'name'=>'ProjectTask[0][budget_req1]', 'label'=>array('text'=>__('ส่วนที่ 1')), 'placeholder'=>__('ส่วนที่ 1'), 'class'=>'currency required', 'style'=>'width:200px;')); ?>
		<?php echo $this->Form->input('budget_req2', array('type'=>'text', 'name'=>'ProjectTask[0][budget_req2]', 'label'=>array('text'=>__('ส่วนที่ 2')), 'placeholder'=>__('ส่วนที่ 2'), 'class'=>'currency', 'style'=>'width:200px;')); ?>
	</div>
	<div id="showPlusPL"></div>

	<div class="box-footer">
		<div class="pull-right">
			<?php echo $this->Form->submit(__('บันทึก'),array('class'=>'btn btn-warning btn-flat')); ?>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>


<script type="text/javascript">
// ============ Dropdown List script =============
$('#ProjectBudgetYearId').change(function () {
    var year =  $('#ProjectBudgetYearId option:selected').val();
		var strategy_id =  '';
		var plan_id = '';
		var product_id = '';
		// StrategyGov
    $.post("/Utils/findFilter/",{budget_year: year, boxes: "StrategyGov" } ,function (data) {
          $('#ProjectStrategyGovId').empty().html(data);
          updateChosen();
    });

		// StrategyPlan
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id , boxes: "StrategyPlan" } ,function (data) {
					$('#ProjectStrategyPlanId').empty().html(data);
					updateChosen();
		});

		// StrategyProduct
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id , boxes: "StrategyProduct" } ,function (data) {
          $('#ProjectStrategyProductId').empty().html(data);
          updateChosen();
    });

		// StrategyActivity
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id, product:product_id , boxes: "StrategyActivity" } ,function (data) {
          $('#ProjectStrategyActivityId').empty().html(data);
          updateChosen();
    });


});

$('#ProjectStrategyGovId').change(function () {
    var year =  $('#ProjectBudgetYearId option:selected').val();
		var strategy_id =  $('#ProjectStrategyGovId option:selected').val();
		var plan_id = '';
		var product_id = '';
    $.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id , boxes: "StrategyPlan" } ,function (data) {
          $('#ProjectStrategyPlanId').empty().html(data);
          updateChosen();
    });

		// StrategyProduct
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id , boxes: "StrategyProduct" } ,function (data) {
          $('#ProjectStrategyProductId').empty().html(data);
          updateChosen();
    });

		// StrategyActivity
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id, product:product_id , boxes: "StrategyActivity" } ,function (data) {
          $('#ProjectStrategyActivityId').empty().html(data);
          updateChosen();
    });


});


$('#ProjectStrategyPlanId').change(function () {
    var year =  $('#ProjectBudgetYearId option:selected').val();
		var strategy_id =  $('#ProjectStrategyGovId option:selected').val();
		var plan_id = $('#ProjectStrategyPlanId option:selected').val();
		var product_id = '';
    $.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id , boxes: "StrategyProduct" } ,function (data) {
          $('#ProjectStrategyProductId').empty().html(data);
          updateChosen();
    });

		// StrategyActivity
		$.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id, product:product_id , boxes: "StrategyActivity" } ,function (data) {
					$('#ProjectStrategyActivityId').empty().html(data);
					updateChosen();
		});
});

$('#ProjectStrategyProductId').change(function () {
    var year =  $('#ProjectBudgetYearId option:selected').val();
		var strategy_id =  $('#ProjectStrategyGovId option:selected').val();
		var plan_id = $('#ProjectStrategyPlanId option:selected').val();
		var product_id = $('#ProjectStrategyProductId option:selected').val();

    $.post("/Utils/findFilter/",{budget_year: year, strategy:strategy_id, plan: plan_id, product:product_id , boxes: "StrategyActivity" } ,function (data) {
          $('#ProjectStrategyActivityId').empty().html(data);
          updateChosen();
    });
});

$('#ProjectGroupPlanId').change(function () {
    var year =  $('#ProjectBudgetYearId option:selected').val();
		var group_plan_id =  $('#ProjectGroupPlanId option:selected').val();

    $.post("/Utils/findFilter/",{budget_year: year, groupPlan:group_plan_id , boxes: "StrPlan" } ,function (data) {
          $('#ProjectStrPlanId').empty().html(data);
          updateChosen();
    });
});
// ============ [END] Dropdown List script =============

// ============ Plus project list script =============
function plusPL(no) {

	$('#plus_'+no).hide();
	var i = parseInt(no)+1;

	var html = '';
	html += '<div class="box-body" id="showPlusPL_'+i+'">' +
				'<hr style="border-top:1px solid #ff0000;" />' +
				'<span id="plus_'+i+'" class="btn btn-sm btn-flat btn-success" onclick="plusPL('+i+');"><i class="fa fa-plus"></i><?php echo __('เพิ่ม');?></span>&nbsp;' +
				'<span id="plus_'+i+'" class="btn btn-sm btn-flat btn-danger" onclick="removePL('+i+');"><i class="fa fa-trash"></i><?php echo __('ลบ');?></span>' +
				'<div class="form-group">' +
					'<label class="col-sm-2 control-label"><?php echo __('รายการ');?></label>' +
					'<div class="col-sm-10"><input name="ProjectTask['+i+'][task_name]" class="form-control" placeholder="<?php echo __('รายการ');?>" type="text"></div>' +
				'</div>' +
				'<div class="form-group">' +
					'<label class="col-sm-2 control-label"><?php echo __('รหัส');?></label>' +
					'<div class="col-sm-10"><input name="ProjectTask['+i+'][task_code]" class="form-control" style="width:200px;" placeholder="<?php echo __('รหัส');?>" type="text"></div>' +
				'</div>' +
				'<div class="form-group">' +
					'<label class="col-sm-2 control-label"><?php echo __('ส่วนที่ 1');?></label>' +
					'<div class="col-sm-10"><input name="ProjectTask['+i+'][budget_req1]" class="form-control currency" style="width:200px;" maxlength="500" placeholder="<?php echo __('ส่วนที่ 1');?>" type="text"></div>' +
				'</div>' +
				'<div class="form-group">' +
					'<label class="col-sm-2 control-label"><?php echo __('ส่วนที่ 2');?></label>' +
					'<div class="col-sm-10"><input name="ProjectTask['+i+'][budget_req2]" class="form-control currency" style="width:200px;" maxlength="500" placeholder="<?php echo __('ส่วนที่ 2');?>" type="text"></div>' +
				'</div>' +
			'</div>';
	$('#showPlusPL').append(html);
	$("input[type='text'].currency,input[type='hidden'].currency").autoNumeric('init');
}
function removePL(no) {
	$('#showPlusPL_'+no).remove();
	var i = parseInt(no)-1;
	$('#plus_'+i).show();
}
// ============ [END] Plus project list script =============

</script>
