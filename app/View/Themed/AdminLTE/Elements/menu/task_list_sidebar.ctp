<?php //$approvedMenuListHtml = $this->Project->getApproveDynamicAclMenu(); ?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?php echo $this->Zicure->leftAccountInfo(); ?>
        <ul class="sidebar-menu">
            <li class="header"><?php echo __('MAIN NAVIGATION'); ?></li>
                <?php echo @$menuListHtml; ?>
            <li>
                <a href="/Home/index">
                    <i class="fa fa-reply"></i> <span><?php echo __('Back'); ?></span>
                </a>
            </li>
                <?php echo @$approvedMenuListHtml; ?>
        </ul>
    </section>
</aside>