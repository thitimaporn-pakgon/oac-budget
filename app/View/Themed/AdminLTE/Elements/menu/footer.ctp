<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b><?php echo __('Version');?></b> <?php echo Configure::read('OAC.PROJECT.VERSION');?>
    </div>
    <strong><?php echo __('Copyright &copy; 2014-2015');?> <a href="#"><?php echo __('Office of the Army Comptroller');?></a>.</strong> <?php echo "All rights reserved.";?>
</footer><!-- FOOTER -->