<header class="main-header">
    <a href="/" class="logo">
        <span class="logo-mini"><?php echo isset($miniLogo) ? __($miniLogo) : null; ?></span>
        <span class="logo-lg">
            <div class="logo-box">
                <img src="/img/oac-logo.png">
            </div>
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="sidebar-toggle">
            <span class="sr-only"><?php echo __('Toggle navigation'); ?></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <!-- top sign-in username short info --> 
                    <?php echo $this->Zicure->loginFullname(); ?>
					<ul class="dropdown-menu">
                        <!-- Display user profile pan -->
                        <?php //echo $this->Zicure->loginAccountInfo(); ?>
                        <li class="user-body">


                            <div class="row">
                                <div class="col-xs-3">
                                    <a href="#"><?php //echo $this->Zicure->bootstrapLabel(__('Position'), 'warning'); ?></a>
                                </div>
                                <div class="col-xs-9">
                                    <a href="#"><?php //echo $this->Zicure->getSessionPosition(); ?></a>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-3">
                                    <a href="#"><?php //echo $this->Zicure->bootstrapLabel(__('Department '), 'info'); ?></a>
                                </div>
                                <div class="col-xs-9">
                                    <a href="#"><?php //echo $this->Zicure->getCurrentSessionDepartmentName(); ?></a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3">
                                    <a href="#"><?php //echo $this->Zicure->bootstrapLabel(__('Department Levels'), 'primary'); ?></a>
                                </div>
                                <div class="col-xs-9">
                                    <a href="#"><?php //echo $this->Zicure->getDepartmentLevelById(CakeSession::read('Auth.User.Department.department_level_id')); ?></a>
                                </div>
                            </div>

                            <!--                            <div class="row">
                                                            <div class="col-xs-3">
                                                                <a href="#"><?php //echo $this->Zicure->bootstrapLabel('Email');    ?></a>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <a href="#"><?php //echo $this->Zicure->getSessionEmail();    ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <a href="#"><?php //echo $this->Zicure->bootstrapLabel('Telephone','success');    ?></a>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <a href="#"><?php //echo $this->Zicure->bootstrapLabel($this->Zicure->getSessionPhoneNo(),'primary');    ?></a>
                                                            </div>
                                                        </div>-->
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php /*
								<a href="/Users/view/<?php //echo $this->Zicure->getCurrentSessionUserId(); ?>" class="btn btn-default btn-flat separate-button"><i class="fa fa-user"></i><?php echo __('Profile'); ?></a>
								*/ ?>
                            </div>

                            <div class="pull-left">
								<?php /*
                                <?php //$outLang = ($this->Zicure->getCurrentLanguage() == 'tha') ? 'eng' : 'tha'; ?>
								<?php $outLang = 'tha'; ?>
                                <a href="/SetLanguages/index/<?php echo $outLang; ?>" class="btn bg-orange btn-flat separate-button"><i class="fa fa-refresh"></i><?php echo __('Change Language'); ?></a>
								*/ ?>
                            </div>
                            <div class="pull-right">
                                <a href="/Users/logout" class="btn bg-red btn-flat"><i class="fa fa-power-off"></i><?php echo __('Sign out'); ?></a>
                            </div>
                        </li>

                    </ul>
                </li>

                <!-- Control Sidebar Toggle Button -->
                <!--                <li>
                                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                </li>-->

            </ul>
        </div>
    </nav>
</header>