<?php
$currentController = $this->params['controller'];
$currentPath = '';
$role_id = $this->Session->read('Auth.User.role_id');
/*
17	"หน่วยรับผิดชอบงบประมาณรอง"
18	"หน่วยปฏิบัติ"
6	"หน่วยงบประมาณกองทัพบก"
4	"หน่วยเจ้าของ"
5	"หน่วยรับผิดชอบงบประมาณหลัก"
 */
?>
<!-- Left side column. contains the logo and sidebar -->
s
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php echo $this->Zicure->leftAccountInfo(); ?>
        <!-- sidebar menu: : style can be found in sidebar.less by Pinn 2017-04-04 -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo __('MAIN NAVIGATION'); ?></li>
            <?php if ($role_id == 6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยงบประมาณกองทัพบก'); ?></span>

                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/ArmyAllocateds/inbox/"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/ArmyAllocateds/pending/"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/ArmyAllocateds/outbox/"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>                 
                    </ul>
                </li>
			<?php } ?>

			<?php if( $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-code-fork"></i>
                        <span><?php echo __('หน่วยรับผิดชอบหลัก'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/MainUnitAllocateds/inbox/"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/MainUnitAllocateds/pending/"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/MainUnitAllocateds/outbox/"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>                     
                    </ul>
                </li>
			<?php } ?>

			<?php if($role_id==17 || $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo __('หน่วยรับผิดชอบรอง'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/SecondaryAllocateds/inbox/"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/SecondaryAllocateds/pending/"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/SecondaryAllocateds/outbox/"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>                      
                    </ul>
                </li>
			<?php } ?>

			<?php if($role_id==18 || $role_id==17 || $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span><?php echo __('หน่วยปฏิบัติ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/WorkerAllocateds/inbox/"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/WorkerAllocateds/pending/"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/WorkerAllocateds/outbox/"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>                      
                    </ul>
                </li>
            <?php } ?>

            <?php if ($role_id == 4) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span><?php echo __('หน่วยเจ้าของ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/OwnerUnitAllocateds/inbox/"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/OwnerUnitAllocateds/pending/"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/OwnerUnitAllocateds/outbox/"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>                      
                    
                    </ul>
                </li>
            <?php } ?>

			<li class="treeview active">
                <a href="/Home/index">
                    <i class="fa fa-reply"></i> <span><?php echo __('Back'); ?></span>
                </a>
            </li>
        </ul>
    </section><!-- /.sidebar -->
</aside>
