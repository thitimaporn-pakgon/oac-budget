<?php
$currentController = $this->params['controller'];
$currentPath = '';
$role_id = $this->Session->read('Auth.User.role_id');
/*
17	"หน่วยรับผิดชอบงบประมาณรอง"
18	"หน่วยปฏิบัติ"
6	"หน่วยงบประมาณกองทัพบก"
4	"หน่วยเจ้าของ"
5	"หน่วยรับผิดชอบงบประมาณหลัก"
 */
?>
<!-- Left side column. contains the logo and sidebar -->

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php echo $this->Zicure->leftAccountInfo(); ?>
        <!-- sidebar menu: : style can be found in sidebar.less by Pinn 2017-04-04 -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo __('MAIN NAVIGATION'); ?></li>
            <?php if ($role_id == 6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยงบประมาณ ทบ.'); ?></span>

                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<li><a href="/<?php echo $currentController; ?>/transfering/31"><i class="fa fa-circle-o"></i><?php echo __('โอนงบประมาณ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/inbox/31"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/31"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/31"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
			<?php } ?>
			
			<?php if( $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยรับผิดชอบหลัก'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php /*
						<li><a href="/<?php echo $currentController; ?>/inbox/30"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/30"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        */ ?>
						<li><a href="/<?php echo $currentController; ?>/outbox/30"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
			<?php } ?>
				
			<?php if($role_id==17 || $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยรับผิดชอบรอง'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php /*
						<li><a href="/<?php echo $currentController; ?>/inbox/29"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/29"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        */ ?>
						<li><a href="/<?php echo $currentController; ?>/outbox/29"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
			<?php } ?>
				
			<?php if($role_id==18 || $role_id==17 || $role_id==5 || $role_id==6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยปฏิบัติ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<?php /*
						<li><a href="/<?php echo $currentController; ?>/inbox/28"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/28"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        */ ?>
						<li><a href="/<?php echo $currentController; ?>/outbox/28"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>
			
			<?php /* if ($role_id == 6) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยถือ'); ?></span>

                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<li><a href="/<?php echo $currentController; ?>/transfering/271"><i class="fa fa-circle-o"></i><?php echo __('โอนงบประมาณ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/inbox/271"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/271"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/271"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
			<?php } */ ?>

            <?php if ($role_id == 4) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('หน่วยเจ้าของ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/transfering/27"><i class="fa fa-circle-o"></i><?php echo __('โอนงบประมาณ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/27"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/27"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            
			<li class="treeview active">
                <a href="/Home/index">
                    <i class="fa fa-reply"></i> <span><?php echo __('Back'); ?></span>
                </a>
            </li>
        </ul>
    </section><!-- /.sidebar -->
</aside>
