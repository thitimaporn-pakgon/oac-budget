<?php
$currentController = $this->params['controller'];
$currentPath = '';
$dept_lid = $this->Session->read('Auth.User.Department.department_level_id')
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php echo $this->Zicure->leftAccountInfo(); ?>
        <!-- sidebar menu: : style can be found in sidebar.less by Pinn 2017-04-04 -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo __('MAIN NAVIGATION'); ?></li>
            <?php if ($dept_lid != 3) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('สปช ทบ.'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/13"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/13"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/13"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-code-fork"></i>
                        <span><?php echo __('หน่วยรับผิดชอบหลัก'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/12"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/12"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/12"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo __('หน่วยรับผิดชอบรอง'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/11"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/11"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/11"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span><?php echo __('หน่วยปฏิบัติ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/10"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/10"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/10"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if ($dept_lid == 3) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span><?php echo __('หน่วยเจ้าของ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/9"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/pending/9"><i class="fa fa-circle-o"></i><?php echo __('งานรออนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/9"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>
            <li class="treeview active">
                <a href="/Home/index">
                    <i class="fa fa-reply"></i> <span><?php echo __('Back'); ?></span>
                </a>
            </li>
        </ul>
    </section><!-- /.sidebar -->
</aside>