<?php
$currentController = $this->params['controller'];
$currentPath = '';
$dept_lid = $this->Session->read('Auth.User.Department.department_level_id')
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php echo $this->Zicure->leftAccountInfo(); ?>
        <!-- sidebar menu: : style can be found in sidebar.less by Pinn 2017-04-04 -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo __('MAIN NAVIGATION'); ?></li>
            <li>
                <a href="/Home/index">
                    <i class="fa fa-reply"></i> <span><?php echo __('Back'); ?></span>
                </a>
            </li>
            <?php if ($dept_lid != 2) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                        <span><?php echo __('สปช ทบ.'); ?></span>

                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/26"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/wait/26"><i class="fa fa-circle-o"></i><?php echo __('รอการอนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/26"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-code-fork"></i>
                        <span><?php echo __('หน่วยรับผิดชอบหลัก'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/25"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/wait/25"><i class="fa fa-circle-o"></i><?php echo __('รอการอนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/25"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo __('หน่วยรับผิดชอบรอง'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/24"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/wait/24"><i class="fa fa-circle-o"></i><?php echo __('รอการอนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/24"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-wrench"></i>
                        <span><?php echo __('หน่วยปฏิบัติ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/23"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/wait/23"><i class="fa fa-circle-o"></i><?php echo __('รอการอนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/23"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if ($dept_lid == 2) { ?>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo __('หน่วยเจ้าของงบประมาณ'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/<?php echo $currentController; ?>/inbox/22"><i class="fa fa-circle-o"></i><?php echo __('งานเข้า'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/wait/22"><i class="fa fa-circle-o"></i><?php echo __('รอการอนุมัติ'); ?></a></li>
                        <li><a href="/<?php echo $currentController; ?>/outbox/22"><i class="fa fa-circle-o"></i><?php echo __('อนุมัติ'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

        </ul>
    </section><!-- /.sidebar -->
</aside>
