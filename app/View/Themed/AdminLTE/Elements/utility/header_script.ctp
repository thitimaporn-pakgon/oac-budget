<?php
echo $this->Html->script('jquery.min.js');
//echo $this->Html->script('/libraries/jQueryUI/jquery-ui.min.js');
echo $this->Html->script('bootstrap/bootstrap.min.js');
//    echo $this->Html->script('/libraries/morris/raphael-min.js');
//    echo $this->Html->script('/libraries/morris/morris.min.js');
//    echo $this->Html->script('/libraries/sparkline/jquery.sparkline.min.js');//Sparkline
//    echo $this->Html->script('/libraries/jvectormap/jquery-jvectormap-1.2.2.min.js');//jvectormap
//    echo $this->Html->script('/libraries/jvectormap/jquery-jvectormap-world-mill-en.js');//jvectormap
//    echo $this->Html->script('/libraries/knob/jquery.knob.js');//jQuery Knob Chart
//    echo $this->Html->script('/libraries/moment/moment.min.js');
//    echo $this->Html->script('/libraries/daterangepicker/daterangepicker.js');
//    echo $this->Html->script('/libraries/datepicker/bootstrap-datepicker.js');
//    echo $this->Html->script('/libraries/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');//Bootstrap WYSIHTML5
    echo $this->Html->script('/libraries/slimScroll/jquery.slimscroll.min.js');//Slimscroll
    echo $this->Html->script('/libraries/fastclick/fastclick.min.js');//FastClick
echo $this->Html->script('AdminLTE/app.min.js'); //AdminLTE App
//echo $this->Html->script('AdminLTE/pages/dashboard.js');//AdminLTE dashboard AdminLTE dashboard demo (This is only for demo purposes)
echo $this->Html->script('AdminLTE/demo.js'); //AdminLTE for demo purposes
//
//    //Addon by sarawutt.b on 2016-03-24 07:10:21
echo $this->Html->script('/libraries/boot-jui/jquery-ui-1.9.2.custom.min.js');

echo $this->Html->script('/libraries/datepicker/bootstrap-datepicker.js');
echo $this->Html->script('/libraries/bootstrap-datetimepicker/moment.min.js');
echo $this->Html->script('/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
//
echo $this->Html->script('/libraries/fullcalendar/fullcalendar.min.js');
//    //echo $this->Html->script('/libraries/datepicker/locales/bootstrap-datepicker.th.js');
echo $this->Html->script('/libraries/fullcalendar/lang/th.js');
echo $this->Html->script('jquery.start.min.js');
echo $this->Html->script('JQuery.validate.min.js');
echo $this->Html->script('jQuery.validate.addValidate.js');


//Chosen version 1.5.1 make select list is beauty
//Adding by sarawutt.b
echo $this->Html->script('/libraries/chosen_v151/chosen.jquery.min.js');

//iCheck box version 1.1 make checkbok is beautyful
//Adding by sarawutt.b on 20160504
echo $this->Html->script('/libraries/iCheck/icheck.min.js');


/**
 * 
 * Bootstrap treview added by sarawutt.b for make tree list in the project
 */
echo $this->Html->script('/libraries/bootstrap-treeview/dist/bootstrap-treeview.min.js');

//Table DND
echo $this->Html->script('/libraries/tableDnd/jquery.tablednd.js');

//Input mask
//echo $this->Html->script('/libraries/input-mask/jquery.inputmask.min.js');
//echo $this->Html->script('/libraries/input-mask/jquery.inputmask.date.extensions.min.js');
//echo $this->Html->script('/libraries/input-mask/jquery.inputmask.extensions.min.js');
//autoNumber add currency input number
echo $this->Html->script('/libraries/autoNumber/autoNumeric-min.js');
echo $this->Html->script('/libraries/animated-bootstrap-alerts-notify/notify.js');//Jquery notification

echo $this->Html->script('/libraries/adamwdraper-Numeral/min/numeral.min.js');//Jquery notification

/**
 * 
 * Pace progress bar function
 * Added by sarawutt.b
 */
//echo $this->Html->script('/libraries/pace/pace.min.js');
echo $this->Html->script('/libraries/nprogress/nprogress.min.js');
echo $this->fetch('script');
?>

<script type="text/javascript">
//    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    //$.widget.bridge('uibutton', $.ui.button);
</script>
