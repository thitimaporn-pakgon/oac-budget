<?php
//$cakeDescription = __d('cake_dev', 'ZiCURE-APP');
$cakeDescription = __d('cake_dev', 'OACRMD');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription; ?>:
            <?php echo __($title_for_layout); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'));
        
        //Disable google robot index all page
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        echo $this->Html->meta(array('name' => 'googlebot', 'content' => 'noindex, nofollow'));
        echo $this->fetch('meta');

        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('bootstrap-theme.min.css');

        echo $this->Html->css('font-awesome.min.css');
        echo $this->Html->css('ionicons.min.css');

        echo $this->Html->css('AdminLTE/AdminLTE.min.css');
        echo $this->Html->css('pakgon.mainstyle.css');
        echo $this->Html->css('boot-jui/jquery-ui-1.10.0.custom.css');

        //ADDON ADVANCE FEATURE by sarawutt.b on 20160314
        echo $this->fetch('css');


        //javascript herder script
        echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('bootstrap/bootstrap.min.js');
        echo $this->Html->script('AdminLTE/app.js');
        echo $this->Html->script('/libraries/boot-jui/jquery-ui-1.9.2.custom.min.js');
        echo $this->fetch('script');
        ?>
    </head>
    <body>       
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <section class="xcontent"> 
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
            </section>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(function () {
        $("#flashMessage").dialog({
            title: '<?php echo __('Process confirmation') ?>',
            modal: true,
            width: '70%',
            height: 'auto',
            autoOpen: true,
            dialogClass: 'alert-dialog-responsive',
            closeOnEscape: true,
            buttons: {
                '<?php echo __('OK'); ?>': function () {
                    $(this).dialog("close");
                }
            }
        });
    });
</script>