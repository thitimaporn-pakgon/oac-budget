<?php
$cakeDescription = __d('cake_dev', 'OACRMD');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
    <head>
        <?php echo $this->Html->charset(); ?>

        <title>
            <?php echo __($cakeDescription); ?>:
            <?php echo __($title_for_layout); ?>
        </title>

        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'));

        //Disable google robot index all page
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        echo $this->Html->meta(array('name' => 'googlebot', 'content' => 'noindex, nofollow'));
        echo $this->fetch('meta');

        echo $this->Html->css('bootstrap.min.css');
        //echo $this->Html->css('bootstrap-responsive.min');
        //echo $this->Html->css('bootstrap-theme.min.css');
        echo $this->Html->css('font-awesome.min.css');
        echo $this->Html->css('ionicons.min.css');
        //echo $this->Html->css('AdminLTE/AdminLTE.min.css');
        echo $this->Html->css('AdminLTE/AdminLTE.css');
        echo $this->Html->css('AdminLTE/skins/skin-rta.css');
        echo $this->Html->css('/libraries/iCheck/flat/blue.css');
        echo $this->Html->css('/libraries/morris/morris.css');
        echo $this->Html->css('/libraries/jvectormap/jquery-jvectormap-1.2.2.css');
        echo $this->Html->css('/libraries/datepicker/datepicker3.css');
        echo $this->Html->css('/libraries/daterangepicker/daterangepicker-bs3.css');
        echo $this->Html->css('/libraries/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');

        //Addon by sarawutt.b on 2016-03-24 07:10:21
        echo $this->Html->css('boot-jui/jquery-ui-1.10.0.custom.css');
        //echo $this->Html->css('pakgon.min.css');//For Production or and UAT server
        echo $this->Html->css('pakgon.mainstyle.css'); //For Develop environment
        echo $this->Html->css('bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
        echo $this->Html->css('fullcalendar/fullcalendar.min.css');

        //Chosen version 1.5.1 make select list is beauty
        //Adding by sarawutt.b
        //echo $this->Html->css('/Libraries/chosen_v151/chosen.min.css');
        echo $this->Html->css('/libraries/chosen_v151/bootstrap-chosen/bootstrap-chosen.css');
        //iCheck box version 1.1 make checkbok is beautyful
        //Adding by sarawutt.b on 20160504
        echo $this->Html->css('/libraries/iCheck/minimal/minimal.css');
        
        
        /**
         * 
         * Bootstrap treview added by sarawutt.b for make tree list in the project
         */
        echo $this->Html->css('/libraries/bootstrap-treeview/dist/bootstrap-treeview.min.css');
	echo $this->Html->css("zicure.css");
        echo $this->fetch('css');
        echo $this->element('utility/header_script');
        ?>
    </head>
    <?php //echo $this->element('menu/top_menu_rta'); ?>
    <!--    <body class="hold-transition skin-blue sidebar-mini">-->
    <body class="fixed sidebar-mini skin-blue">
        <div class="wrapper">
            <?php //echo $this->element('menu/top_menu_rta'); ?>
            <?php echo $this->element('menu/top_menu'); ?>
            <?php echo $this->element('menu/budget_allocated_sidebar'); ?>
            <!--<div class="wrapper row-offcanvas row-offcanvas-left">-->
            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $this->element('breadcrumb'); ?>
                </section> 
                <section class="content"> 
                    <?php /* Render for Flash message one time message used */ //echo $this->Flash->render();?>
                    <?php /* Render for Session Flash message one time message used */ echo $this->Session->flash(); ?>
                    <div class="col-md-12" id="confirmDialog"></div>
                    <div class="col-md-12" id="appMessage"></div>
                    <?php echo $this->fetch('content'); ?>
                    <?php //echo $this->element('sqldump',array('btitle'=>'SQL Dump Monitoring','bclose'=>true,'bcollap'=>true,'bnoti'=>'SQL DUMP V 0.1.1'));?>
                </section>
            </div><!-- ./wrapper -->
            <?php echo $this->element('menu/footer');?>
            <?php //echo $this->element('menu/sidebar_control'); ?>
            <?php echo $this->element('modal/modal'); ?>
            <?php //echo $this->element('utility/utility_script.min');//For production environment ?>
            <?php echo $this->element('utility/utility_script'); //For Develop environment ?>
            <?php //echo $this->element('utility/utility_script_min');//Production ?>
        </div><!-- ./wrapper -->
    </body>
</html>