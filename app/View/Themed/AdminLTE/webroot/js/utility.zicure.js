/**
 * 
 * Zicure Utility for the javascript function
 * @author Sarawutt.b
 * @license Zicure
 * @since   2016.08.30
 */

/**
 * 
 * Format for amount fo currency number example 1002550 => 1,002,550.00
 * @param   string amount format inum
 * @returns string currcency format
 */
function formatMoney(inum){
    var s_inum=new String(inum);  
    var num2=s_inum.split(".",s_inum);  
    var l_inum=num2[0].length;  
    var n_inum="";  
    for(i=0;i<l_inum;i++){  
        if(parseInt(l_inum-i)%3==0){  
            if(i==0){  
                n_inum+=s_inum.charAt(i);         
            }else{  
                n_inum+=","+s_inum.charAt(i);         
            }     
        }else{  
            n_inum+=s_inum.charAt(i);  
        }  
    }  
    if(num2[1]!=undefined){  
        n_inum+="."+num2[1];  
    }  
    return n_inum;  
}

/**
 * 
 * Convert currency or amount number to thai currency word
 */
function MoneyToWord() {}
MoneyToWord.execute = function(money) {
    var result = '';
    var minus = '';

    if (money < 0) {
        minus = 'ติดลบ';
        money = money * -1;
    }

    money = parseFloat(Math.round(money * 100) / 100).toFixed(2);

    if (money == '0.00') {
        result = 'ศูนย์บาทถ้วน';
        return result;
    }

    var numbers = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'];
    var positions = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน'];

    var digit = money.length;
    var inputs = [];

    if (digit <= 15) {
        if (digit > 9) {
            inputs[0] = money.substr(0, digit - 9);
            inputs[1] = money.substr(inputs[0].length, 6);
        } else {
            inputs[0] = '00';
            inputs[1] = money.substr(0, money.length - 3);
        }
        inputs[2] = money.substr(money.indexOf('.') + 1, 2);
    } else {
        result = 'Error: ไม่สามารถรองรับจำนวนเงินที่เกินหลักแสนล้าน';
        return result;
    }

    for (i = 0; i < 3; i ++) {
        var input = inputs[i];

        if (input != '0' && input != '00') {
            var digit = input.length;

            for (j = 0; j < digit; j ++) {
                var s = input.substr(j, 1);
                var number = numbers[s];
                var position = '';

                if (number != '') {
                    position = positions[digit - (j + 1)];
                }

                if ((digit - j) == 2) {
                    if (s == '1') {
                        number = '';
                    } else if (s == '2') {
                        number = 'ยี่';
                    }
                } else if ((digit - j) == 1 && (digit != 1)) {
                    var pre_s = '0';
                    if (j > 0) {
                        pre_s = input.substr(j - 1, 1);
                    }

                    if (i == 0) {
                        if (pre_s != '0') {
                            if (s == '1') {
                                number = 'เอ็ด';
                            }
                        }
                    } else {
                        if (s == '1') {
                            number = 'เอ็ด';
                        }
                    }
                }

                result = result + number + position;
            }
        }

        if (i == 0) {
            if (input != '00') {
                result = result + 'ล้าน';
            }
        } else if (i == 1) {
            if (input != '0' && input != '00') {
                result = result + 'บาท';
                if (inputs[2] == '00') {
                    result = result + 'ถ้วน';
                }
            }
        } else {
            if (input != '00') {
                result = result + 'สตางค์';
            }
        }
    }
    return minus + result;
}

/**
 * @name BAHTTEXT.js
 * @version 1.0.2
 * @author Earthchie http://www.earthchie.com/
 * @license WTFPL v.2 - http://www.wtfpl.net/
 *
 * Copyright (c) 2014, Earthchie (earthchie@gmail.com)
 */
function BAHTTEXT(num, suffix) {
	num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces
	if (isNaN(num) || parseFloat(num) == 0) {
		return 'ศูนย์บาทถ้วน';
	} else {
		var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'];
		var n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'];

		suffix = suffix ? suffix : 'บาทถ้วน';
		if (num.indexOf('.') > -1) { // have decimal

			var parts = num.toString().split('.');

			// precision-hack; more accurate than parseFloat the whole number
			parts[1] = parseFloat('0.' + parts[1]).toFixed(2).toString().split('.')[1];

			var text = parseInt(parts[0]) ? BAHTTEXT(parts[0]) : '';

			if (parseInt(parts[1]) > 0) {
				text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
			}

			return text;

		} else {
			if (num.length > 7) { // more than (or equal to) 10 millions

				var overflow = num.substring(0, num.length - 6);
				var remains = num.slice(-6);
				return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

			} else {

				var text = "";

				for (var i = 0; i < num.length; i++) {
					if (parseInt(num.charAt(i)) > 0) {
						if (num.length > 2 && i == num.length - 1 && num.charAt(i) == 1 && suffix != 'สตางค์') {
							text += 'เอ็ด' + t[num.length - 1 - i];
						} else {
							text += n[num.charAt(i)] + t[num.length - 1 - i];
						}
					}
				}

				// grammar correction
				text = text.replace('หนึ่งสิบ', 'สิบ');
				text = text.replace('สองสิบ', 'ยี่สิบ');
				text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

				return text + suffix;
			}
		}
	}
}