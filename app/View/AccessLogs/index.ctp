<div class="accessLogs index">
    <?php echo $this->element('searchLog'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'AccessLog')); ?>
        <div class="box-body">
            <table cellpadding="0" cellspacing="0" class="table-short-information">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort(__('menu_id')); ?></th>
                        <th><?php echo $this->Paginator->sort('actions'); ?></th>
                        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('department_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($accessLogs)): ?>
                        <?php foreach ($accessLogs as $k=>$accessLog): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                                <?php
                                if ($accessLog['AccessLog']['menu_id'] == '-1') {
                                    ?>
                                    <td><?php echo __('Undefined Menus'); ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td><?php echo @$this->Html->link($accessLog['Menu']['name'], array('controller' => 'menus', 'action' => 'view', $accessLog['Menu']['id'])); ?>
                                    </td>
                                    <?php
                                }
                                ?>
                                <td><?php echo __($accessLog['AccessLog']['actions']); ?></td>
                                <td><?php
                                    $Fullname = @$this->Zicure->findUserByUid($accessLog['User']['id']);

                                    echo $this->Html->link($Fullname, array('controller' => 'Users', 'action' => 'view', @$accessLog['User']['id']));
                                    ?>
                                </td>
                                <td><?php
                                    $departmentname = @$this->Zicure->getDepartmentById($accessLog['User']['department_id']);
                                    echo @$this->Html->link($departmentname, array('controller' => 'Departments', 'action' => 'view', $accessLog['User']['department_id']));
                                    ?>
                                </td>
                                <td><?php echo $this->Zicure->dateTimeISO($accessLog['AccessLog']['created']); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($accessLog['AccessLog']['id'], 'accessLogs', true, false, false); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo '<tr class="text-center notfound"><td colspan="6">' . __('Not found data') . '</td></tr>'; ?>
                    <?php endif; ?>
                </tbody>
            </table>
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div> <!-- ./box-body -->
    </div><!-- ./box -->
</div><!-- end containing of content -->
