<div class="accessLogs view">
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'View accessLog')); ?>
        <div class="box-body">		
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <tbody>
                    <tr>
                        <th><?php echo __('Id'); ?></th>
                        <td>
                            <?php echo h($accessLog['AccessLog']['id']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Menu'); ?></th>
                        <?php
                        if ($accessLog['AccessLog']['menu_id'] == '-1') {
                            ?>
                            <td><?php echo __('Undefined Menus'); ?></td>
                            <?php
                        } else {
                            ?>
                            <td>
                                <?php echo $this->Html->link($accessLog['Menu']['name'], array('controller' => 'menus', 'action' => 'view', $accessLog['Menu']['id'])); ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <th><?php echo __('User'); ?></th>
                        <td>
                            <?php echo $this->Html->link($this->Zicure->findUserByUid($accessLog['User']['id']), array('controller' => 'users', 'action' => 'view', $accessLog['User']['id'])); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Access Url'); ?></th>
                        <td>
                            <?php echo h($accessLog['AccessLog']['access_url']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Created'); ?></th>
                        <td>
                            <?php echo h($accessLog['AccessLog']['created']); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div><!-- end col md 9 -->
    </div>
</div>