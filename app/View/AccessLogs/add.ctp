<div class="accessLogs form">
    <div class="row">
        <div class="col-md-12">
            			<?php echo $this->Form->create('AccessLog', array('role' => 'form')); ?>

            				<div class="form-group">
					<?php echo $this->Form->input('menu_id', array('class' => 'form-control', 'placeholder' => __('Menu Id')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'placeholder' => __('User Id')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('access_url', array('class' => 'form-control', 'placeholder' => __('Access Url')));?>
				</div>
            				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
				</div>

			<?php echo $this->Form->end() ?>

        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>
