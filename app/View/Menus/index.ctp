<div class="menus index simple">
    <?php echo $this->element('searchMenus'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Menu (Result)')); ?>
        <div class="box-body">
            <table class="table-short-information table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('MenuName'); ?></th>                        
                        <th><?php echo $this->Paginator->sort('url'); ?></th>
                        <th><?php echo $this->Paginator->sort('icon'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($menus)): ?>
                        <?php $i = $this->Paginator->counter("{:start}") - 1; ?>
                        <?php foreach ($menus as $k => $menu): ?>
                    <tr>
                        <td class="nindex"><?php echo ++$i; ?></td>
                        <td><?php echo h($menu['MenuList']['menu_name']); ?></td>
                        <td><?php echo h($menu['MenuList']['menu_path']); ?></td>
                        <td><?php echo h($menu['MenuList']['icon_m']); ?></td>
                        <td class="actions">
                                    <?php echo $this->Permission->getActions($menu['MenuList']['id'], 'menus', true, true, true); ?>
                        </td>
                    </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo '<tr class="text-center notfound"><td colspan="8">' . __('Not found data') . '</td></tr>'; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div> <!-- ./box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div><!-- end row -->
</div>