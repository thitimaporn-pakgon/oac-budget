<div class="menus form">
    <div class="box box-success">
        <?php echo $this->element('boxOptionHeader', array('btitle'=>'Add Menu', 'bcollap'=>true, 'bclose'=>true)); ?>
        <div class="box-body">          
            <?php echo $this->Form->create('MenuList', array('role' => 'form', 'class' => 'form-horizontal')); ?>
            <?php echo $this->Form->input('menu_name', array('label' => array('text' => __('Menu name')))); ?>
            <?php echo $this->Form->input('icon_m', array('value' => 'fa fa-circle-o','label' => array('text' => __('icon')))); ?>
            <?php echo $this->Form->input('menu_path', array('class' => 'required','label' => array('text' => __('url')))); ?>
            <?php echo $this->Form->input('system_name_id', array('options' => $this->MenuList->findListSystemName())); ?>    
            <?php echo $this->Form->input('menu_group_name_id', array('options' => $this->MenuList->findListMenuGroupName())); ?>   
            <?php echo $this->Form->submit(__('Submit'), array('before' => '<div class="col-sm-2"></div>')); ?>
            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //getSystemActionListBySystemControllerId();
        $("#sysControllerId").change(function () {
            getSystemActionListBySystemControllerId();
            $("#MenuUrl").val("");
        });

        $("#SysActionId").change(function () {
            var tmp = '/' + $("#sysControllerId option:selected").text() + '/' + $("#SysActionId option:selected").text();
            $("#MenuUrl").val(tmp);
        });
    });
</script>