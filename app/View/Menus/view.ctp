<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'View Menu', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">		
        <table cellpadding="0" cellspacing="0" class="table table-striped table-form-view">
            <tbody>
                <tr>
                    <th><?php echo __('Menu name'); ?></th>
                    <td><?php echo h($menu['MenuList']['menu_name']); ?></td>
                </tr>
               
              
                <tr>
                    <th><?php echo __('URL'); ?></th>
                    <td><?php echo h($menu['MenuList']['menu_path']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Glyphicon'); ?></th>
                    <td><?php echo "<i class=\"{$menu['MenuList']['icon_m']}\"></i>"; ?></td>
                </tr>
             
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>