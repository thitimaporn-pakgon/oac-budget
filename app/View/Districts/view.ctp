<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'District Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('District Name'); ?></th>
                    <td>
                        <?php echo h($district['District']['name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td>
                        <?php echo $this->Zicure->mainStatus($district['District']['status']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($district['District']['create_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($district['District']['update_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($district['District']['created']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($district['District']['modified']); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

