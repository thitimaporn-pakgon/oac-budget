<div class="districts index">
    <?php echo $this->element('district_search', array()); ?>
</div>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Districts (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('id', __('Province Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('District Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($districts)): ?>
                    <?php foreach ($districts as $k => $district): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                            <td><?php echo @$this->Zicure->getProvinceById(substr($district['District']['id'], 0, 3)); ?></td>
                            <td><?php echo h($district['District']['name']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($district['District']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($district['District']['id'], 'Districts'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="5">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div>