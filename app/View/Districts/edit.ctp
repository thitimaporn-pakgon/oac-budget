<div class="districts form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit District')); ?>
        <?php echo $this->Form->create('District', array('role' => 'form')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('province_id', array('class' => 'form-control required','type' => 'select','default' => substr($this->data['District']['id'], 0, 3),'options' => $this->Zicure->ListProvince(),'label' => __('Province Name')));?>
            <?php echo $this->Form->input('id', array('class' => 'form-control required','type' => 'text','readonly' => true,'label' => __('District ID')));?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required', 'label' => __('District Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('class' => 'form-control', 'label' => __('District Name Eng'))); ?>
            <?php echo $this->Form->input('status', array('class' => 'form-control required', 'type' => 'select', 'options' => $this->Zicure->mainStatus(), 'placeholder' => __('Status'))); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script>
    $(function () {
        $('#btnSubmit').click(function (e) {
            e.preventDefault();
            var form = $('FORM#DistrictEditForm');
            var isValid = form.valid();

            if (isValid == true) {

                $.post('/districts/check_already_district/' + $("#DistrictName").val() + '/' + $("#DistrictId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The district_name is already exist on the system. Please, try again.') ?>');
                        $("#DistrictName").focus();
                    }
                });

            } else {
                return false;
            }

        });

        $("#DistrictProvinceId").change(function () {
            province_Changed();
        });

    });

    function province_Changed() {
        $.post('/Districts/load_Province/' + $("#DistrictProvinceId").val(), function (data) {
            console.log(data);
            $("#DistrictId").val(data);
        });
    }

</script>
