<div class="districts form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add District')); ?>
        <?php echo $this->Form->create('District', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('province_id', array('class' => 'form-control required', 'type' => 'select', 'options' => $this->Zicure->ListProvince(), 'label' => __('Province Name'), 'placeholder' => __('ID'))); ?>
            <?php echo $this->Form->input('id', array('class' => 'form-control required', 'type' => 'text', 'readonly' => true, 'label' => __('District ID'))); ?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required', 'maxlength' => 256, 'label' => __('District Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('class' => 'form-control', 'maxlength' => 256, 'label' => __('District Name Eng'))); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<script type="text/javascript">
    $(function () {

        $("#DistrictProvinceId").change(function () {
            province_Changed();
        });

        $("#btnSubmit").click(function (e) {
            var form = $("form#DistrictAddForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/Districts/check_already_district/' + $("#DistrictName").val() + '/' + $("#DistrictId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The district_name is already exist on the system. Please, try again.') ?>');
                        $("#DistrictName").focus();
                    }
                });
            }
            return false;
        });
    });

    function province_Changed() {
        $.post('/Districts/load_Province/' + $("#DistrictProvinceId").val(), function (data) {
            console.log(data);
            $("#DistrictId").val(data);
        });
    }

</script>
