<?php
    //ini_set('max_execution_time', 100); //300 seconds = 5 minutes
    Configure::write('debug',0);
    $this->response->type('pdf');
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Content-type: application/pdf");
    //header("Content-disposition: inline; filename=file.pdf");

    echo $this->fetch('content');
?> 