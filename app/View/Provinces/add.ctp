<div class="provinces form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Province')); ?>
        <?php echo $this->Form->create('Province', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('id', array('class' => 'form-control required', 'label' => __('Province ID'), 'type' => 'text', 'maxlength' => 3)); ?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required', 'maxlength' => 50, 'label' => __('Province Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('class' => 'form-control', 'maxlength' => 50, 'label' => __('Province Name Eng'))); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var form = $("form#ProvinceAddForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/Provinces/check_already_province/' + $("#ProvinceId").val() + '/' + $("#ProvinceName").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The province_id or province_name is already exist on the system. Please, try again.') ?>');
                        $("#ProvinceId").focus();
                    }
                });
            }
            return false;
        });
    });
</script>




