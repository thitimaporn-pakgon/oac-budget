<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Province Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table-striped table-form-view">
            <tbody>
                <tr>
                    <th><?php echo __('Province Name'); ?></th>
                    <td><?php echo h($province['Province']['name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td><?php echo $this->Zicure->mainStatus($province['Province']['status']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($province['Province']['create_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($province['Province']['update_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($province['Province']['created']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($province['Province']['modified']); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

