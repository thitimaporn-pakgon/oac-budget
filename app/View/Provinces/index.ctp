<?php echo $this->element('province_search'); ?>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Provinces (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('Province Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($provinces)) : ?>
                    <?php foreach ($provinces as $k => $province): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                            <td><?php echo h($province['Province']['name']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($province['Province']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($province['Province']['id'], 'Provinces'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="4">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div><!-- end containing of content -->
</div>