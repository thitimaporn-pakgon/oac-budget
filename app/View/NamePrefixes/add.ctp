<div class="budgetYears form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add NamePrefixe')); ?>
        <?php echo $this->Form->create('NamePrefix', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('name', array('class' => ' required', 'label' => false)); ?>
            <?php //echo $this->Form->input('name_eng'); ?>
            <?php echo $this->Form->input('long_name', array('class' => 'required')); ?>
            <?php echo $this->Form->input('order_no', array('type' => 'text', 'class' => 'required digits')); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();// คำสั่งไม่ให้ระบบทำงานออโต้ เราจะทำเอง
            var form = $("form#NamePrefixAddForm");

            var isValid = form.valid();// เชคดูในช่องกรอกที่มี class required

            if (isValid == true) {
                $.post('/NamePrefixes/check_already_name/' + $("#NamePrefixName").val(), function (data) {
                    console.log(data);
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The name_prefix is already exist on the system. Please, try again.') ?>');
                        $("#NamePrefixName").focus();
                    }
                });
            } else {
                return false;
            }
        });
    });
</script>
