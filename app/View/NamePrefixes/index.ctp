<?php echo $this->element('name_prefixes'); ?>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'NamePrefixes (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('Name Prefix')); ?></th>
                    <th><?php echo $this->Paginator->sort('long_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('order_no'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($namePrefixes)): ?>
                    <?php foreach ($namePrefixes as $k => $namePrefix): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                            <td><?php echo h($namePrefix['NamePrefix']['name']); ?></td>
                            <td><?php echo h($namePrefix['NamePrefix']['long_name']); ?></td>
                            <td><?php echo h($namePrefix['NamePrefix']['order_no']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($namePrefix['NamePrefix']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($namePrefix['NamePrefix']['id'], 'NamePrefixes'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="6">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div>