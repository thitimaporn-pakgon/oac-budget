<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'NamePrefix Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Name Prefix'); ?></th>
                    <td>
                        <?php echo h($namePrefix['NamePrefix']['name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Long Name'); ?></th>
                    <td>
                        <?php echo h($namePrefix['NamePrefix']['long_name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Order No'); ?></th>
                    <td><?php echo h($namePrefix['NamePrefix']['order_no']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td><?php echo $this->Zicure->mainStatus($namePrefix['NamePrefix']['status'], true); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($namePrefix['NamePrefix']['create_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($namePrefix['NamePrefix']['update_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($namePrefix['NamePrefix']['created']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($namePrefix['NamePrefix']['modified']); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

