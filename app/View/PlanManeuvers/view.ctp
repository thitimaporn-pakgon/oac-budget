<?php
/**
*
* View page for planManeuversController it show for PlanManeuver infomation.
* @author sarawutt.b 
* @since 2017-04-18 17:58:21
* @license Zicure Corp. 
*/
?>
<div class="planManeuvers view div-view-information box box-warning">
    	<?php echo $this->element('boxOptionHeader', array('btitle'=>'Plan Maneuver Information', 'bcollap'=>true, 'bclose'=>true));?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                		<tr>
			<td class="table-view-label"><?php echo __('Plan Maneuver Id'); ?></td>
			<td class="table-view-detail"><?php echo h($planManeuver['PlanManeuver']['id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Strategic Maneuver'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($planManeuver['StrategicManeuver']['name'],'info'), "/strategic_maneuvers/view/{$planManeuver['StrategicManeuver']['id']}" ,array('title'=>__('View') . ' ' . __('Strategic Maneuver'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Code'); ?></td>
			<td class="table-view-detail"><?php echo h($planManeuver['PlanManeuver']['code']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Plan Maneuver Name'); ?></td>
			<td class="table-view-detail"><?php echo h($planManeuver['PlanManeuver']['name']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Budget Year'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($planManeuver['BudgetYear']['name'],'info'), "/budget_years/view/{$planManeuver['BudgetYear']['id']}" ,array('title'=>__('View') . ' ' . __('Budget Year'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('From Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($planManeuver['PlanManeuver']['from_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('To Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($planManeuver['PlanManeuver']['to_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('System Has Process'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($planManeuver['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$planManeuver['SystemHasProcess']['id']}" ,array('title'=>__('View') . ' ' . __('System Has Process'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Create Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($planManeuver['PlanManeuver']['create_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Update Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($planManeuver['PlanManeuver']['update_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Created'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($planManeuver['PlanManeuver']['created']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Modified'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($planManeuver['PlanManeuver']['modified']); ?></td>
		</tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    	<?php echo $this->element('boxOptionFooter');?>
</div><!-- ./div.box box-warning -->


    <div class="box box-info box-related">
        	<?php echo $this->element('boxOptionHeader',array('btitle'=>'Result Maneuver Relate List', 'bcollap'=>true, 'bclose'=>true, 'btnnew'=>'ResultManeuvers'));?>
        <div class="box-body">
            <table class="table-view-related-information table table-bodered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#');?></th>
                        		<th><?php echo __('ResultManeuver Id'); ?></th>
		<th><?php echo __('Plan Maneuver Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('ResultManeuver Name'); ?></th>
		<th><?php echo __('Budget Year Id'); ?></th>
		<th><?php echo __('From Department Id'); ?></th>
		<th><?php echo __('To Department Id'); ?></th>
		<th><?php echo __('System Has Process Id'); ?></th>
                        		<th class="actions">&nbsp;</th>
                    </tr>
                </thead>

                <?php if(!empty($planManeuver['ResultManeuver'])):?>
	<?php foreach ($planManeuver['ResultManeuver'] as $k => $resultManeuver): ?>
		<tr>
			<td class="nindex-black"><?php echo ++$k;?></td>
			<td><?php echo $resultManeuver['id']; ?></td>
			<td><?php echo $resultManeuver['plan_maneuver_id']; ?></td>
			<td><?php echo $resultManeuver['code']; ?></td>
			<td><?php echo $resultManeuver['name']; ?></td>
			<td><?php echo $resultManeuver['budget_year_id']; ?></td>
			<td><?php echo $resultManeuver['from_department_id']; ?></td>
			<td><?php echo $resultManeuver['to_department_id']; ?></td>
			<td><?php echo $resultManeuver['system_has_process_id']; ?></td>
			<td class="actions"><?php echo $this->Permission->getActions($resultManeuver['id'], 'ResultManeuvers'); ?></td>
		</tr>
	<?php endforeach; ?>
<?php else:?>
		<tr class="row-notfound">
			<td colspan="10"><?php echo __('Information Not Found');?></td>
		</tr>
<?php endif;?>
            </table>
        </div><!-- ./div.box-body -->
        	<?php echo $this->element('boxOptionFooter');?>
    </div><!-- /div.box box-info -->
    