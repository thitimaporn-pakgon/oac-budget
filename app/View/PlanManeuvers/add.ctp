<?php
/**
 *
 * Add page for planManeuversController it Add of PlanManeuver.
 * @author sarawutt.b 
 * @since 2017-04-18 17:58:21
 * @license Zicure Corp. 
 */
?>
<div class="planManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Plan Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('StrategyPlan', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'value' => $year, 'disabled' => true)); ?>
        <?php echo $this->Form->input('strategy_gov_name', array('readonly' => 'readonly', 'label' => __('Strategic Name'), 'value' => $data['StrategyGov']['strategy_gov_name'])); ?>
        <?php echo $this->Form->input('strategy_plan_name', array('class' => 'required', 'label' => __('Work Plan Name'))); ?>
        <?php echo $this->Form->input('code', array('class' => 'required')); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/PlanManeuvers/add'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
