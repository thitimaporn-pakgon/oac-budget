
<div class="planManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Plan Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('StrategyPlan', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('id'); ?>
        <?php echo $this->Form->hidden('strategy_gov_id'); ?>
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'value' => $year, 'disabled' => true)); ?>
        <?php echo $this->Form->input('strategy_gov_name', array('readonly' => 'readonly', 'label' => __('Strategic Name'), 'value' => $dataall_StrategyGov['StrategyGov']['strategy_gov_name'])); ?>
        <?php echo $this->Form->input('strategy_plan_name', array('class' => 'required', 'label' => __('Work Plan Name'))); ?>
        <?php echo $this->Form->input('code', array('class' => 'required')); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/PlanManeuvers/edit'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
