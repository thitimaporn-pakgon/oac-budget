<div class="departments form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Department')); ?>
        <?php echo $this->Form->create('Department', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('id', array('class' => 'required')); ?>
            <?php echo $this->Form->input('dept_no', array('class' => 'required')); ?>
            <?php echo $this->Form->input('short_name', array('class' => 'required')); ?>
            <?php echo $this->Form->input('full_name', array('class' => 'required')); ?>
            <?php echo $this->Form->input('order_display', array('class' => 'required digits', 'type' => 'text','wrap'=>'col-sm-3')); ?>
            <?php echo $this->Form->input('department_level_id', array('class' => 'required', 'options' => $this->Zicure->listDepartmentLevel(), 'label' => __('DepartmentLevel Name'))); ?>
            <?php echo $this->Form->input('status', array('class' => 'required', 'type' => 'select', 'options' => $this->Zicure->mainStatus())); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();// คำสั่งไม่ให้ระบบทำงานออโต้ เราจะทำเอง
            var form = $("form#DepartmentEditForm");
            var isValid = form.valid();// เชคดูในช่องกรอกที่มี class required
            if (isValid == true) {
                $.post('/Departments/check_already_department/' + $("#DepartmentDeptNo").val() + '/' + $("#DepartmentShortName").val() + '/' + $("#DepartmentFullName").val() + '/' + $('#DepartmentDepartmentLevelId').val() + '/' + $("#DepartmentId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The department_id department_shortname or department_fullname  is already exist on the system. Please, try again.') ?>');
                        $("#DepartmentLevelName").focus();
                    }
                });
            }
            return false;
        });
    });
</script>