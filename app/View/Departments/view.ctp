<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Department Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Dept No'); ?></th>
                    <td><?php echo h($department['Department']['dept_no']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Short Name'); ?></th>
                    <td><?php echo h($department['Department']['short_name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Full Name'); ?></th>
                    <td><?php echo h($department['Department']['full_name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('DepartmentLevel Name'); ?></th>
                    <td><?php echo $this->Html->link($department['DepartmentLevel']['name'], array('controller' => 'department_levels', 'action' => 'view', $department['DepartmentLevel']['id'])); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Order Display'); ?></th>
                    <td><?php echo $department['Department']['order_display']; ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td><?php echo $this->Zicure->mainStatus($department['Department']['status']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($department['Department']['create_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($department['Department']['update_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($department['Department']['created']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($department['Department']['modified']); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

