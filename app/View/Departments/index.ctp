<?php echo $this->element('department_search'); ?>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Departments (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th  class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('dept_no'); ?></th>
                    <th><?php echo $this->Paginator->sort('short_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('full_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('department_level_id', __('DepartmentLevel Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($departments)): ?>
                    <?php foreach ($departments as $k => $department): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                            <td><?php echo h($department['Department']['dept_no']); ?></td>
                            <td><?php echo h($department['Department']['short_name']); ?></td>
                            <td><?php echo h($department['Department']['full_name']); ?></td>
                            <td><?php echo $this->Html->link($department['DepartmentLevel']['name'], array('controller' => 'department_levels', 'action' => 'view', $department['DepartmentLevel']['id'])); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($department['Department']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($department['Department']['id'], 'Departments'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="7">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div><!-- /.box box-active-->