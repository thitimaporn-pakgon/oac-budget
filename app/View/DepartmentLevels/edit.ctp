<div class="departmentLevels form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Departmentlevel')); ?>
        <?php echo $this->Form->create('DepartmentLevel', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('id', array('class' => 'required')); ?>
            <?php echo $this->Form->input('name', array('class' => 'required', 'label' => __('DepartmentLevel Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('label' => __('DepartmentLevel Name Eng'))); ?>
            <?php echo $this->Form->input('description'); ?>
            <?php echo $this->Form->input('order_display', array('class' => 'required digits', 'type' => 'text','wrap'=>'col-sm-3')); ?>
            <?php echo $this->Form->input('status', array('class' => 'required', 'options' => $this->Zicure->mainStatus())); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();// คำสั่งไม่ให้ระบบทำงานออโต้ เราจะทำเอง
            var form = $("form#DepartmentLevelEditForm");
            var isValid = form.valid();// เชคดูในช่องกรอกที่มี class required
            if (isValid == true) {
                $.post('/DepartmentLevels/check_already_departmentLevel/' + $("#DepartmentLevelName").val() + '/' + $("#DepartmentLevelId").val(), function (data) {

                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The department_level is already exist on the system. Please, try again.') ?>');
                        $("#DepartmentLevelName").focus();
                    }
                });
            }
            return false;
        });
    });
</script>