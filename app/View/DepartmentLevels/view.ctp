<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'DepartmentLevel Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('DepartmentLevel Name'); ?></th>
                    <td>
                        <?php echo h($departmentLevel['DepartmentLevel']['name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td>
                        <?php echo h($departmentLevel['DepartmentLevel']['description']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Order Display'); ?></th>
                    <td><?php echo $departmentLevel['DepartmentLevel']['order_display']; ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td>
                        <?php echo $this->Zicure->mainStatus($departmentLevel['DepartmentLevel']['status']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($departmentLevel['DepartmentLevel']['create_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($departmentLevel['DepartmentLevel']['update_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($departmentLevel['DepartmentLevel']['created']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($departmentLevel['DepartmentLevel']['modified']); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>


<?php if (!empty($departmentLevel['Department'])): ?>
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Related Department', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <table cellpadding = "0" cellspacing = "0" class="table-view-related table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#'); ?></th>
                        <th><?php echo __('Dept No'); ?></th>
                        <th><?php echo __('Short Name'); ?></th>
                        <th><?php echo __('Full Name'); ?></th>
                        <th><?php echo __('DepartmentLevel Name'); ?></th>
                        <th class="actions"></th>
                    </tr>
                <thead>
                <tbody>
                    <?php foreach ($departmentLevel['Department'] as $k => $department): ?>
                        <tr>
                            <td class="nindex-black"><?php echo ++$k; ?></td>
                            <td><?php echo $department['dept_no']; ?></td>
                            <td><?php echo $department['short_name']; ?></td>
                            <td><?php echo $department['full_name']; ?></td>
                            <td><?php echo $this->Zicure->getDepartmentLevelById($department['department_level_id']); ?></td>
                            <td class="actions">
                                <?php //echo $this->Html->link(__('<span class="glyphicon glyphicon-search"></span>'), array('controller' => 'departments', 'action' => 'view', $department['id']), array('escape' => false)); ?>
                                <?php echo $this->Permission->getActions($department['id'], 'Departments', true, false, false); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div><!-- end col md 12 -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div>
