<div class="departmentLevels form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Departmentlevel')); ?>
        <?php echo $this->Form->create('DepartmentLevel', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('name', array('class' => array('required'), 'label' => __('DepartmentLevel Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('label' => __('DepartmentLevel Name Eng'))); ?>
            <?php echo $this->Form->input('description'); ?>
            <?php echo $this->Form->input('order_display', array('class' => 'required digits', 'type' => 'text','wrap'=>'col-sm-3')); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var form = $("form#DepartmentLevelAddForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/DepartmentLevels/check_already_departmentLevel/' + $("#DepartmentLevelName").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The department_level is already exist on the system. Please, try again.') ?>');
                        $("#DepartmentLevelName").focus();
                    }
                });
            }
            return false;
        });
    });
</script>


