<?php echo $this->element('departmentLevel_search'); ?>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'DepartmentLevels (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('DepartmentLevel Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('description'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($departmentLevels)): ?>
                    <?php foreach ($departmentLevels as $k => $departmentLevel): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                            <td><?php echo h($departmentLevel['DepartmentLevel']['name']); ?></td>
                            <td><?php echo h($departmentLevel['DepartmentLevel']['description']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($departmentLevel['DepartmentLevel']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($departmentLevel['DepartmentLevel']['id'], 'DepartmentLevels'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="5">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div>