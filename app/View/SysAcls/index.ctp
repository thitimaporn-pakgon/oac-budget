<div class="index SysAcl">
    <?php echo $this->element('searchAcl'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Access Control List (Result)', 'bcollap' => false, 'bclose' => false)); ?>
        <div class="box-body">
            <table cellpadding="0" cellspacing="0" class="table-short-information">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('sys_controller_id', __('Controller')); ?></th>
                        <th><?php echo $this->Paginator->sort('sys_action_id', __('Action')); ?></th>
                        <th><?php echo $this->Paginator->sort('role_id', __('Role')); ?></th>
                        <th><?php echo $this->Paginator->sort('user_id', __('User')); ?></th>
                        <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
                        <th><?php echo $this->Paginator->sort('created', __('Created')); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($sysAcls)): ?>
                        <?php foreach ($sysAcls as $k => $sysAcl): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                                <td><?php echo $this->Html->link($sysAcl['SysController']['name'], array('controller' => 'SysControllers', 'action' => 'view', $sysAcl['SysController']['id'])); ?></td>
                                <td><?php echo h($sysAcl['SysAction']['name']); ?></td>
                                <td class="center"><?php echo h($sysAcl['Role']['name']); ?></td>
                                <td class="center"><?php echo @$this->Html->link($sysAcl['User']['id'], array('controller' => 'users', 'action' => 'view', $sysAcl['User']['id'])); ?></td>
                                <td class="center"><?php echo $this->Zicure->mainStatus($sysAcl['SysAcl']['status']); ?></td>
                                <td class="center"><?php echo $this->Zicure->dateISO($sysAcl['SysAcl']['created']); ?></td>
                                <td class="actions"><?php echo $this->Permission->getActions($sysAcl['SysAcl']['id'], 'SysAcls', true, true, true); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="8" class="text-center notfound"><?php echo __('Not found data'); ?></td></tr>
                        <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./ box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div>
</div> 
<script type="text/javascript">
    $(function () {
        $("#SearchSysControllerId").change(function () {
            changeDropDown("/Utils/findActionsByControllers/" + $(this).val(), "sysActions");
        });
        $("#SearchStakeholderId").change(function () {
            changeDropDown("/Utils/findUsersByStakeholders/" + $(this).val(), "sysUsers");
        });
    });
</script>
