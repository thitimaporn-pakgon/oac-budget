<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'View Access Control List', 'bcollap' => false, 'bclose' => false)); ?>
    <?php echo $this->Form->create('ProjectPlan', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view">
            <tr>
                <th><?php echo __('Name Controller'); ?></th>
                <td><?php echo $this->Html->link($sysAcl['SysController']['name'], array('controller' => 'sys_controllers', 'action' => 'view', $sysAcl['SysController']['id'])); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Name Action'); ?></th>
                <td><?php echo $this->Html->link($sysAcl['SysAction']['name'], array('controller' => 'sys_actions', 'action' => 'view', $sysAcl['SysAction']['id'])); ?></td>
            </tr>
            <tr>
                <th><?php echo __('User'); ?></th>
                <td><?php echo @$this->Html->link($sysAcl['User']['id'], array('controller' => 'users', 'action' => 'view', @$sysAcl['User']['id'])); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Role Name'); ?></th>
                <td><?php echo $this->Html->link($sysAcl['Role']['name'], array('controller' => 'Roles', 'action' => 'view', $sysAcl['Role']['id'])); ?></td>
            </tr>
            <tr>
            <tr>
                <th><?php echo __('Status'); ?></th>
                <td><?php echo $this->Zicure->mainStatus($sysAcl['SysAcl']['status']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Create uid'); ?></th>
                <td><?php echo $this->Zicure->findUserByUid($sysAcl['SysAcl']['create_uid']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Update uid'); ?></th>
                <td><?php echo $this->Zicure->findUserByUid($sysAcl['SysAcl']['update_uid']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Created'); ?></th>
                <td><?php echo $this->Zicure->dateTimeISO($sysAcl['SysAcl']['created']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Modified'); ?></th>
                <td><?php echo $this->Zicure->dateTimeISO($sysAcl['SysAcl']['modified']); ?></td>
            </tr>
        </table>

    </div>

    <div class="box-footer">
        <?php echo $this->Permission->button(__('Back'), '/SysAcls/index/', array('class' => 'btn btn-active pull-right')); ?>
    </div><!-- /.box-footer -->
</div><!-- /.box -->
