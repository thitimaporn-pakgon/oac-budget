<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?php echo __("System ACLs Group") ?></h3>
        <button data-widget="remove" class="btn btn-box-tool pull-right"><i class="fa fa-remove"></i></button>
        <button data-widget="collapse" class="btn btn-box-tool pull-right"><i class="fa fa-minus"></i></button>
        <?php echo $this->Permission->link_button(__('Back'), '/SysAcls/index', NULL, array('class' => 'btn-default pull-right separate-button')); ?>
        <?php echo $this->Permission->button_option('<i class="fa fa-plus"></i>' . __("Add Roles"), "/Roles/add/", NULL, array('class' => 'bg-purple btn-flat pull-right')); ?>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped dataTable table-add-details" id="example2">
                        <thead>
                            <tr>
                                <th class="nindex-black"><?php echo __('#'); ?></th>
                                <th><?php echo __('Role Name'); ?></th>
                                <th><?php echo __('Active'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($roleInfos as $k => $v) : ?>
                                <tr>
                                    <td class="nindex-black"><?php echo ++$k; ?></td>
                                    <td><?php echo $this->DisplayFormat->check_empty($v['Role']['name']); ?></td>
                                    <td class="bool" style="width:80px;"><?php echo $this->Zicure->mainStatus($v['Role']['status'], true); ?></td>   
                                    <td class="actions">
                                        <?php echo $this->Permission->link_permissions("/SysAcls/add_permission/" . $v['Role']['id']); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div>
</div>




<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo __("System ACLs User") ?></h3>
        <button data-widget="remove" class="btn btn-box-tool pull-right"><i class="fa fa-remove"></i></button>
        <button data-widget="collapse" class="btn btn-box-tool pull-right"><i class="fa fa-minus"></i></button>
        <?php echo $this->Permission->link_button(__('Back'), '/SysAcls/index', NULL, array('class' => 'btn-default pull-right separate-button')); ?>
        <?php echo $this->Permission->button_option('<i class="fa fa-plus"></i>' . __("Add Users"), "/Users/add/", NULL, array('class' => 'bg-warning btn-warning pull-right')); ?>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped dataTable table-add-details">
                        <thead>
                            <tr>
                                <th class="nindex-black"><?php echo __('#'); ?></th>
                                <th><?php echo __('User Name'); ?></th>
                                <th><?php echo __('Active'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $k => $v) : ?>
                                <tr>
                                    <td class="nindex-black"><?php echo ++$k; ?></td>
                                    <td><?php echo $this->DisplayFormat->check_empty($v['User']['username']); ?></td>
                                    <td class="bool" style="width:80px;"><?php echo $this->Zicure->mainStatus($v['User']['status'], TRUE); ?></td>   
                                    <td class="actions">
                                        <?php echo $this->Permission->link_permissions("/SysAcls/add_permissionByuser/" . $v['User']['role_id'] . '/' . $v['User']['id']); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div>
</div>