<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Access Control List', 'bcollap' => false, 'bclose' => false)); ?>
    <?php echo $this->Form->create('ProjectPlan', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->create('SysAcl', array('class' => 'form-horizontal')); ?>
        <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
        <?php echo $this->Form->input('sys_controller_id', array('class' => 'required', 'options' => $this->Zicure->findListSysController(), 'id' => 'sysControllerId')); ?>
        <?php echo $this->Form->input('sys_action_id', array('class' => 'required', 'options' => $this->Zicure->findListSysAction($this->data['SysAcl']['sys_action_id']), 'id' => 'SysActionId')); ?>
        <?php echo $this->Form->input('role_id', array('class' => 'required', 'options' => $this->Zicure->findListRole())); ?> 
        <?php echo $this->Form->input('status', array('class' => 'required', 'options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit(__('Edit'), '/SysAcl/edit/', array('id' => 'btnAclEdit', 'name' => 'btnAclEdit', 'div' => false, 'label' => false, 'type' => 'submit')); ?>
        <?php echo $this->Permission->button(__('Back'), '/SysAcls/index/', array('class' => 'btn btn-active pull-right')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
<script type="text/javascript">
    $(function () {
        $("#sysControllerId").change(function () {
            getSystemActionListBySystemControllerId();
        });
    });
</script>