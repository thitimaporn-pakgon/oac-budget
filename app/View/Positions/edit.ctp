<div class="positions form">
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Position')); ?>
        <div class="box-body">  
            <?php echo $this->Form->create('Position', array('role' => 'form')); ?>
            <?php echo $this->Form->input('id'); ?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required')); ?>
            <?php echo $this->Form->input('name_eng'); ?>
            <?php echo $this->Form->input('description'); ?>
            <?php echo $this->Form->input('status', array('type' => 'select', 'options' => $this->Zicure->MainStatus())); ?>
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primmary')); ?>
            <?php echo $this->Form->end() ?>
        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>


