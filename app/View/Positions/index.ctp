<div class="positions index">
    <?php echo $this->element('searchPosition'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Position (Result)')); ?>
        <div class="box-body">	
            <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('name_eng'); ?></th>
                        <th><?php echo $this->Paginator->sort('description'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($positions)): ?>
                        <?php $i = $this->Paginator->counter("{:start}") - 1; ?>
                        <?php foreach ($positions as $position): ?>
                            <tr>
                                <td class="nindex"><?php echo ++$i; ?></td>
                                <td><?php echo h($position['Position']['name']); ?></td>
                                <td><?php echo h($position['Position']['name_eng']); ?></td>
                                <td><?php echo h($position['Position']['description']); ?></td>
                                <td><?php echo h($this->Zicure->mainStatus($position['Position']['status'])); ?></td>
                                <td><?php echo $this->Zicure->dateTimeISO($position['Position']['modified']); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($position['Position']['id'], 'positions', true, true, true); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo '<tr class="text-center notfound"><td colspan="7">' . __('Not found data') . '</td></tr>'; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div> <!-- ./box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div>
</div>