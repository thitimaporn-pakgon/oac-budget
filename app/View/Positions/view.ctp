<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'View Position', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">			
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td><?php echo h($position['Position']['name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Name Eng'); ?></th>
                    <td><?php echo h($position['Position']['name_eng']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td><?php echo h($position['Position']['description']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td><?php echo h($this->Zicure->mainStatus($position['Position']['status'])); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create By'); ?></th>
                    <td><?php echo h($this->Zicure->getUserById($position['Position']['create_uid']));?></td>
                </tr>
                <tr>
                    <th><?php echo __('Update By'); ?></th>
                    <td><?php echo h($this->Zicure->getUserById($position['Position']['update_uid']));?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td><?php echo h($position['Position']['created']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td><?php echo h($position['Position']['modified']); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./box -->


<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Related Users', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <?php if (!empty($position['User'])): ?>
            <table cellpadding = "0" cellspacing = "0" class="table-short-information table table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#'); ?></th>
                        <th><?php echo __('Username'); ?></th>
                        <th><?php echo __('First Name'); ?></th>
                        <th><?php echo __('Last Name'); ?></th>
                        <th><?php echo __('Phone No'); ?></th>
                        <th><?php echo __('Email'); ?></th>
                        <th><?php echo __('Status'); ?></th>
                        <th class="actions"></th>
                    </tr>
                <thead>
                <tbody>
                    <?php foreach ($position['User'] as $k=>$user): ?>
                        <tr>
                            <td class="nindex-black"><?php echo ++$k; ?></td>
                            <td><?php echo $user['username']; ?></td>
                            <td><?php echo $user['first_name']; ?></td>
                            <td><?php echo $user['last_name']; ?></td>
                            <td><?php echo $user['phone_no']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><?php echo $this->Zicure->mainStatus($user['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($user['id'], 'Users'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./box box-active -->