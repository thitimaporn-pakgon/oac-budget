<?php
/**
 *
 * View page for budgetTypesController it show for BudgetType infomation.
 * @author sarawutt.b 
 * @since 2017-06-06 17:41:19
 * @license Zicure Corp. 
 */
?>
<div class="budgetTypes view div-view-information box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __('Budget Type Information'), 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                <tr>
                    <td class="table-view-label"><?php echo __('Budget Type Id'); ?></td>
                    <td class="table-view-detail"><?php echo h($budgetType['BudgetType']['id']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Budget Type Name'); ?></td>
                    <td class="table-view-detail"><?php echo h($budgetType['BudgetType']['name']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Description'); ?></td>
                    <td class="table-view-detail"><?php echo h($budgetType['BudgetType']['description']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getUserById($budgetType['BudgetType']['create_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getUserById($budgetType['BudgetType']['update_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Created'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($budgetType['BudgetType']['created']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Modified'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($budgetType['BudgetType']['modified']); ?></td>
                </tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./div.box box-warning -->


<div class="box box-info box-related">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __('Expense Header Manage Relate List'), 'bcollap' => true, 'bclose' => true, 'btnnew' => 'ExpenseHeaderManages')); ?>
    <div class="box-body">
        <table class="table-view-related-information table table-bodered table-striped">
            <thead>
                <tr>
                    <th class="nindex-black"><?php echo __('#'); ?></th>
                    <th><?php echo __('ExpenseHeaderManage Id'); ?></th>
                    <th><?php echo __('Expense Header Allocated Id'); ?></th>
                    <th><?php echo __('Strategic Manage Id'); ?></th>
                    <th><?php echo __('Plan Manage Id'); ?></th>
                    <th><?php echo __('Result Manage Id'); ?></th>
                    <th><?php echo __('Event Manage Id'); ?></th>
                    <th><?php echo __('Work Group Manage Id'); ?></th>
                    <th><?php echo __('Work Manage Id'); ?></th>
                    <th><?php echo __('Project Manage Id'); ?></th>
                    <th><?php echo __('Project List Manage Id'); ?></th>
                    <th><?php echo __('To Department Id'); ?></th>
                    <th><?php echo __('System Has Process Id'); ?></th>
                    <th><?php echo __('Budget Scope'); ?></th>
                    <th><?php echo __('Budget Transfered'); ?></th>
                    <th><?php echo __('Budget Total'); ?></th>
                    <th><?php echo __('Withdraw Approved'); ?></th>
                    <th><?php echo __('Withdraw Pending'); ?></th>
                    <th><?php echo __('Transfer Approved'); ?></th>
                    <th><?php echo __('Transfer Pending'); ?></th>
                    <th><?php echo __('Hoard Approved'); ?></th>
                    <th><?php echo __('Hoard Pending'); ?></th>
                    <th><?php echo __('Return Approved'); ?></th>
                    <th><?php echo __('Return Pending'); ?></th>
                    <th><?php echo __('Budget Balance'); ?></th>
                    <th><?php echo __('Objective'); ?></th>
                    <th><?php echo __('Kpi1'); ?></th>
                    <th><?php echo __('Kpi1 Result'); ?></th>
                    <th><?php echo __('Kpi2'); ?></th>
                    <th><?php echo __('Kpi2 Result'); ?></th>
                    <th><?php echo __('Kpi3'); ?></th>
                    <th><?php echo __('Kpi3 Result'); ?></th>
                    <th><?php echo __('Kpi4'); ?></th>
                    <th><?php echo __('Kpi4 Result'); ?></th>
                    <th><?php echo __('Digit Budget Year'); ?></th>
                    <th><?php echo __('Digit Strategic'); ?></th>
                    <th><?php echo __('Digit Plan'); ?></th>
                    <th><?php echo __('Digit Result'); ?></th>
                    <th><?php echo __('Digit Event'); ?></th>
                    <th><?php echo __('Digit Work Group'); ?></th>
                    <th><?php echo __('Digit Work'); ?></th>
                    <th><?php echo __('Digit Project'); ?></th>
                    <th><?php echo __('Digit Project List'); ?></th>
                    <th><?php echo __('Digit Owner Dept'); ?></th>
                    <th><?php echo __('Budget Type Id'); ?></th>
                    <th class="actions">&nbsp;</th>
                </tr>
            </thead>

            <?php if (!empty($budgetType['ExpenseHeaderManage'])): ?>
                <?php foreach ($budgetType['ExpenseHeaderManage'] as $k => $expenseHeaderManage): ?>
                    <tr>
                        <td class="nindex-black"><?php echo ++$k; ?></td>
                        <td><?php echo $expenseHeaderManage['id']; ?></td>
                        <td><?php echo $expenseHeaderManage['expense_header_allocated_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['strategic_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['plan_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['result_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['event_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['work_group_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['work_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['project_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['project_list_manage_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['to_department_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['system_has_process_id']; ?></td>
                        <td><?php echo $expenseHeaderManage['budget_scope']; ?></td>
                        <td><?php echo $expenseHeaderManage['budget_transfered']; ?></td>
                        <td><?php echo $expenseHeaderManage['budget_total']; ?></td>
                        <td><?php echo $expenseHeaderManage['withdraw_approved']; ?></td>
                        <td><?php echo $expenseHeaderManage['withdraw_pending']; ?></td>
                        <td><?php echo $expenseHeaderManage['transfer_approved']; ?></td>
                        <td><?php echo $expenseHeaderManage['transfer_pending']; ?></td>
                        <td><?php echo $expenseHeaderManage['hoard_approved']; ?></td>
                        <td><?php echo $expenseHeaderManage['hoard_pending']; ?></td>
                        <td><?php echo $expenseHeaderManage['return_approved']; ?></td>
                        <td><?php echo $expenseHeaderManage['return_pending']; ?></td>
                        <td><?php echo $expenseHeaderManage['budget_balance']; ?></td>
                        <td><?php echo $expenseHeaderManage['objective']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi1']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi1_result']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi2']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi2_result']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi3']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi3_result']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi4']; ?></td>
                        <td><?php echo $expenseHeaderManage['kpi4_result']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_budget_year']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_strategic']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_plan']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_result']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_event']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_work_group']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_work']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_project']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_project_list']; ?></td>
                        <td><?php echo $expenseHeaderManage['digit_owner_dept']; ?></td>
                        <td><?php echo $expenseHeaderManage['budget_type_id']; ?></td>
                        <td class="actions"><?php echo $this->Permission->getActions($expenseHeaderManage['id'], 'ExpenseHeaderManages'); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr class="row-notfound">
                    <td colspan="46"><?php echo __('Information Not Found'); ?></td>
                </tr>
            <?php endif; ?>
        </table>
    </div><!-- ./div.box-body -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- /div.box box-info -->
