<?php
/**
 *
 * Add page for budgetTypesController it Add of BudgetType.
 * @author sarawutt.b 
 * @since 2017-06-06 17:41:19
 * @license Zicure Corp. 
 */
?>
<div class="budgetTypes form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __('Add Budget Type'), 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('BudgetType', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('name'); ?>
        <?php echo $this->Form->input('description'); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', __('Save')), '/BudgetTypes/add'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
