<?php
/**
 *
 * Index page for budgetTypesController it show data list for BudgetType infomation.
 * @author sarawutt.b 
 * @since 2017-06-06 17:41:19
 * @license Zicure Corp. 
 */
?>
<div class="budgetTypes index zicure-index">
    <!-- zicure-box-find search input params section -->
    <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle' => __('Budget Type Management System'), 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
            <?php echo $this->Form->input('name', array('label' => array('text' => __('Budget Type Name')))); ?>
            <?php echo $this->Form->input('status', array('type' => 'select', 'class' => 'select2 chosen', 'options' => $this->Zicure->mainStatus())); ?>
            <?php //echo $this->Form->input('dateFrom', array('type' => 'text', 'class' => 'datepicker datepicker-start')); ?>
            <?php //echo $this->Form->input('dateTo', array('type' => 'text', 'class' => 'datepicker datepicker-end')); ?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', __('Search')), '/BudgetTypes/index', array('name' => 'btnSubmitSearch')); ?>
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', __('Add Budget Type')), '/BudgetTypes/add', array('name' => 'btnAddnew', 'class' => 'btn bg-olive')); ?>
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box-footer -->
    </div><!-- /div.zicure-box-find box box-warning -->

    <!-- zicure-box-result search result section -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle' => __('Budget Type (Result)'), 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Budget Type name')); ?></th>
                        <th><?php echo $this->Paginator->sort('description'); ?></th>
                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="actions">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($budgetTypes)): ?>
                        <?php foreach ($budgetTypes as $k => $budgetType): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                                <td><?php echo h($budgetType['BudgetType']['name']); ?></td>
                                <td><?php echo h($budgetType['BudgetType']['description']); ?></td>
                                <td><?php echo $this->Zicure->dateISO($budgetType['BudgetType']['created']); ?></td>
                                <td><?php echo $this->Zicure->dateISO($budgetType['BudgetType']['modified']); ?></td>
                                <td class="actions"><?php echo $this->Permission->getActions($budgetType['BudgetType']['id'], __('BudgetTypes')); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr class="row-notfound">
                            <td colspan="6"><?php echo __('Information Not Found'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->