<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'SubDistrict Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Subdistrict Name'); ?></th>
                    <td>
                        <?php echo h($subDistrict['SubDistrict']['name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Zipcode'); ?></th>
                    <td>
                        <?php echo h($subDistrict['SubDistrict']['zipcode']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td>
                        <?php echo $this->Zicure->mainStatus($subDistrict['SubDistrict']['status']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($subDistrict['SubDistrict']['create_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td>
                        <?php echo $this->Zicure->getUserById($subDistrict['SubDistrict']['update_uid']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($subDistrict['SubDistrict']['created']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo $this->Zicure->dateISO($subDistrict['SubDistrict']['modified']); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

