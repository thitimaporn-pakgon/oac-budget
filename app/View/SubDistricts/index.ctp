<?php echo $this->element('sub_district_search'); ?>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'SubDistricts (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('id', __('Province Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('id', __('District Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('Subdistrict Name')); ?></th>
                    <th><?php echo $this->Paginator->sort('zipcode'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($subDistricts)): ?>
                    <?php foreach ($subDistricts as $k => $subDistrict): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                            <td><?php echo @$this->Zicure->getProvinceById(substr($subDistrict['SubDistrict']['id'], 0, 3)); ?></td>
                            <td><?php echo @$this->Zicure->getDistrictById(substr($subDistrict['SubDistrict']['id'], 0, 5)); ?></td>
                            <td><?php echo h($subDistrict['SubDistrict']['name']); ?></td>
                            <td><?php echo h($subDistrict['SubDistrict']['zipcode']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($subDistrict['SubDistrict']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($subDistrict['SubDistrict']['id'], 'SubDistricts'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="7">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div>