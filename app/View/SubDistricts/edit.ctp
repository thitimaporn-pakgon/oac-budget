<?php
$pvID = substr($id, 0, 3);
$dtID = substr($id, 0, 5);
?>
<div class="subDistricts form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Subdistrict')); ?>
        <?php echo $this->Form->create('SubDistrict', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('province_id', array('class' => 'required', 'id' => 'ProvinceId', 'default' => $pvID, 'options' => $this->Zicure->ListProvince(), 'label' => __('Province Name'))); ?>
            <?php echo $this->Form->input('district_id', array('class' => 'required', 'id' => 'DistrictId', 'default' => $dtID, 'options' => $this->Zicure->ListDistrict($pvID), 'label' => __('District Name'))); ?>
            <?php echo $this->Form->input('id', array('class' => 'form-control required', 'type' => 'text', 'readonly' => true, 'label' => __('SubDistrict ID'))); ?>
            <?php echo $this->Form->input('name', array('class' => 'required', 'label' => __('Subdistrict Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('label' => __('Subdistrict Name Eng'))); ?>
            <?php echo $this->Form->input('zipcode', array('class' => 'form-control required digits', 'maxlenght' => 5, 'placeholder' => __('Zipcode'))); ?>
            <?php echo $this->Form->input('status', array('class' => 'form-control required', 'options' => $this->Zicure->mainStatus())); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
    </div>
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->

<script>
    $(function () {
        $("#ProvinceId").change(function () {
            getDistrict();
        });
        $('#btnSubmit').click(function (e) {
            e.preventDefault();
            var form = $("form#SubDistrictEditForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/sub_districts/check_already_subDistrict/' + $("#SubDistrictName").val() + '/' + $("#SubDistrictId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The sub_district_name is already exist on the system. Please, try again.') ?>');
                        $("#SubDistrictName").focus();
                    }
                });
            }
        });

        $("#DistrictId").change(function () {
            district_Changed();
        });
    });

    function district_Changed() {
        $.post('/Sub_Districts/load_District/' + $("#DistrictId").val(), function (data) {
            console.log(data);
            $("#SubDistrictId").val(data);
        });
    }

</script>
