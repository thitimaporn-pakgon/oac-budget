<div class="subDistricts form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Subdistrict')); ?>
        <?php echo $this->Form->create('SubDistrict', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('province_id', array('class' => 'required','id' => 'ProvinceId','options' => $this->Zicure->ListProvince(),'label' => __('Province Name')));?>
            <?php echo $this->Form->input('district_id', array('class' => 'required','id' => 'DistrictId','options' => $this->Zicure->ListDistrict(),'label' => __('District Name')));?>
            <?php echo $this->Form->input('id', array('class' => 'required','type' => 'text','readonly' => true,'label' => __('Subdistrict ID')));?>
            <?php echo $this->Form->input('name', array('class' => 'required', 'label' => __('Subdistrict Name'))); ?>
            <?php //echo $this->Form->input('name_eng', array('placeholder' => __('Subdistrict Name Eng'))); ?>
            <?php echo $this->Form->input('zipcode', array('class' => 'required digits', 'maxlenght' => 5)); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>

<script type="text/javascript">
    $(function () {
        $("#DistrictId").prop('disabled', true);
        $("#ProvinceId").change(function () {
            var pvID = $("#ProvinceId").val();
            if (pvID == '') {
                $("#DistrictId").prop('disabled', true);
            } else {
                $("#DistrictId").prop('disabled', false);
            }
            getDistrict();
        });

        $("#DistrictId").change(function () {
            district_Changed();
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var form = $("form#SubDistrictAddForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/sub_districts/check_already_subDistrict/' + $("#SubDistrictName").val() + '/' + $("#SubDistrictId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The sub_district_name is already exist on the system. Please, try again.') ?>');
                        $("#SubDistrictName").focus();
                    }
                });
            }
        });
    });

    function district_Changed() {
        $.post('/Sub_Districts/load_District/' + $("#DistrictId").val(), function (data) {
            console.log(data);
            $("#SubDistrictId").val(data);
        });
    }

</script>
