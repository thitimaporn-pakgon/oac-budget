<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'View Role', 'bclose' => true, 'bcollap' => true)); ?>
    <div class="box-body">			
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($role['Role']['name']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name Eng'); ?></th>
                    <td>
                        <?php echo h($role['Role']['name_eng']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td>
                        <?php echo h($role['Role']['description']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td>
                        <?php echo h($this->Zicure->mainStatus($role['Role']['status'])); ?>

                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Create By'); ?></th>
                    <td>
                        <?php
                        echo h($this->Zicure->getUserById($role['Role']['create_uid']));
                        ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Update By'); ?></th>
                    <td>
                        <?php
                        echo h($this->Zicure->getUserById($role['Role']['update_uid']));
                        ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($role['Role']['created']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($role['Role']['modified']); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>


<div class="box box-success">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Related Users', 'bclose' => true, 'bcollap' => true)); ?>
    <div class="box-body">
        <?php if (!empty($role['User'])): ?>
            <table cellpadding = "0" cellspacing = "0" class="table-view-related table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('Id'); ?></th>
                        <th><?php echo __('Username'); ?></th>
                        <th><?php echo __('First Name'); ?></th>
                        <th><?php echo __('Last Name'); ?></th>
                        <th><?php echo __('Phone No'); ?></th>
                        <th><?php echo __('Email'); ?></th>
                        <th><?php echo __('Status'); ?></th>
                        <th class="actions"></th>
                    </tr>
                <thead>
                <tbody>
                    <?php foreach ($role['User'] as $k => $user): ?>
                        <tr>
                            <td class="nindex-black"><?php echo ++$k; ?></td>
                            <td><?php echo $user['username']; ?></td>
                            <td><?php echo $user['first_name']; ?></td>
                            <td><?php echo $user['last_name']; ?></td>
                            <td><?php echo $user['phone_no']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><?php echo $this->Zicure->mainStatus($user['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($user['id'], 'Users'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div><!-- end col md 12 -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div>
