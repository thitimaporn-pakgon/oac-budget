<div class="roles index">
    <?php echo $this->element('searchRole'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Role (Result)')); ?>
        <div class="box-body">	
            <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('name_eng'); ?></th>
                        <th><?php echo $this->Paginator->sort('description'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($roles)): ?>
                        <?php $i = $this->Paginator->counter("{:start}") - 1; ?>
                        <?php foreach ($roles as $role): ?>
                            <tr>
                                <td class="nindex"><?php echo ++$i; ?></td>
                                <td><?php echo h($role['Role']['name']); ?></td>
                                <td><?php echo h($role['Role']['name_eng']); ?></td>
                                <td><?php echo h($role['Role']['description']); ?></td>
                                <td><?php echo h($this->Zicure->mainStatus($role['Role']['status'])); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($role['Role']['id'], 'roles', true, true, true); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo '<tr class="text-center notfound"><td colspan="6">' . __('Not found data') . '</td></tr>'; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div> <!-- ./box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div><!-- end row -->
</div><!-- end containing of content -->