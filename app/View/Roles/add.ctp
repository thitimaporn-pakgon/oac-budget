<div class="roles form">
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Role')); ?>
        <div class="box-body">  
            <?php echo $this->Form->create('Role', array('role' => 'form')); ?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required')); ?>
            <?php echo $this->Form->input('name_eng'); ?>
            <?php echo $this->Form->input('description'); ?>
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit', 'name' => 'btnSubmit')); ?>
            <?php echo $this->Form->end() ?>
        </div>
    </div><!-- end row -->
</div>
<script type="text/javascript">
    $(function() {
        $("#btnSubmit").click(function(e) {
            e.preventDefault();
            var form = $("form#RoleAddForm");
            var isValid = form.valid();
            if (isValid == true) {
                $.post('/Roles/check_already_roles/' + $("#RoleName").val() + '/' + $("#RoleNameEng").val(), function(data) {
                    console.log(data);
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The role name already exist could not be saved. Please, try again.') ?>');
                        $("#RoleName").focus();
                    }
                });
            }
            return false;
        });
    });
</script>