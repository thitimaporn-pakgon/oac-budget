<div class="documentAttachments form">
    <div class="row">
        <div class="col-md-12">
            			<?php echo $this->Form->create('DocumentAttachment', array('role' => 'form')); ?>

            				<div class="form-group">
					<?php echo $this->Form->input('document_type', array('class' => 'form-control', 'placeholder' => __('Document Type')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('budget_type', array('class' => 'form-control', 'placeholder' => __('Budget Type')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('attachment_name', array('class' => 'form-control', 'placeholder' => __('Attachment Name')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('attachment_name_origin', array('class' => 'form-control', 'placeholder' => __('Attachment Name Origin')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('display_name', array('class' => 'form-control', 'placeholder' => __('Display Name')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('attachment_extension', array('class' => 'form-control', 'placeholder' => __('Attachment Extension')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('attachment_path', array('class' => 'form-control', 'placeholder' => __('Attachment Path')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('project_id', array('class' => 'form-control', 'placeholder' => __('Project Id')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('project_plan_id', array('class' => 'form-control', 'placeholder' => __('Project Plan Id')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ref_model', array('class' => 'form-control', 'placeholder' => __('Ref Model')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ref_id', array('class' => 'form-control', 'placeholder' => __('Ref Id')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder' => __('Status')));?>
				</div>
            				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
				</div>

			<?php echo $this->Form->end() ?>

        </div><!-- end col md 12 -->
    </div><!-- end row -->
</div>
