<div class="documentAttachments view">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Document Attachment'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive">			
            <table cellpadding="0" cellspacing="0" class="table-form-view">
                <tbody>
                    <tr>
                        <th><?php echo __('Id'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['id']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Document Type'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['document_type']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Budget Type'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['budget_type']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Attachment Name'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['attachment_name']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Attachment Name Origin'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['attachment_name_origin']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Display Name'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['display_name']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Attachment Extension'); ?></th>
                        <td>
                            <?php echo __($documentAttachment['DocumentAttachment']['attachment_extension']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Attachment Path'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['attachment_path']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Project'); ?></th>
                        <td>
                            <?php echo $this->Html->link($documentAttachment['Project']['id'], array('controller' => 'projects', 'action' => 'view', $documentAttachment['Project']['id'])); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Project Plan'); ?></th>
                        <td>
                            <?php echo $this->Html->link($documentAttachment['ProjectPlan']['id'], array('controller' => 'project_plans', 'action' => 'view', $documentAttachment['ProjectPlan']['id'])); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Ref Model'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['ref_model']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Ref Id'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['ref_id']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Status'); ?></th>
                        <td>
                            <?php echo $this->Zicure->mainStatus($documentAttachment['DocumentAttachment']['status']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Create Uid'); ?></th>
                        <td>
                            <?php echo$this->Zicure->findUserByUid($documentAttachment['DocumentAttachment']['create_uid']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Update Uid'); ?></th>
                        <td>
                            <?php echo $this->Zicure->findUserByUid($documentAttachment['DocumentAttachment']['update_uid']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Created'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['created']); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Modified'); ?></th>
                        <td>
                            <?php echo h($documentAttachment['DocumentAttachment']['modified']); ?>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div><!-- end col md 9 -->
    </div>
</div>

