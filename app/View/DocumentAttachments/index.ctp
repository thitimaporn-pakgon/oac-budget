<div class="documentAttachments index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Document Attachments'); ?></h1>
            </div>
        </div><!-- end col md 12 -->
    </div><!-- end row -->
    
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
                <thead>
                    <tr>
                                                    <th nowrap><?php echo $this->Paginator->sort('id'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('document_type'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('budget_type'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('attachment_name'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('attachment_name_origin'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('display_name'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('attachment_extension'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('attachment_path'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('project_id'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('project_plan_id'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('ref_model'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('ref_id'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('status'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('create_uid'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('update_uid'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('created'); ?></th>
                                                    <th nowrap><?php echo $this->Paginator->sort('modified'); ?></th>
                                                <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    	<?php if(!empty($documentAttachments)):?>
		<?php foreach ($documentAttachments as $documentAttachment): ?>
						<tr>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['id']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['document_type']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['budget_type']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['attachment_name']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['attachment_name_origin']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['display_name']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['attachment_extension']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['attachment_path']); ?></td>
							<td>								<?php echo $this->Html->link($documentAttachment['Project']['id'], array('controller' => 'projects', 'action' => 'view', $documentAttachment['Project']['id'])); ?>
		</td>
							<td>								<?php echo $this->Html->link($documentAttachment['ProjectPlan']['id'], array('controller' => 'project_plans', 'action' => 'view', $documentAttachment['ProjectPlan']['id'])); ?>
		</td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['ref_model']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['ref_id']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['status']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['create_uid']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['update_uid']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['created']); ?></td>
							<td nowrap><?php echo h($documentAttachment['DocumentAttachment']['modified']); ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $documentAttachment['DocumentAttachment']['id']), array('escape' => false)); ?>
								<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $documentAttachment['DocumentAttachment']['id']), array('escape' => false)); ?>
								<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $documentAttachment['DocumentAttachment']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $documentAttachment['DocumentAttachment']['id'])); ?>
							</td>
						</tr>
		<?php endforeach; ?>
	<?php else: ?>
						<?php echo '<tr class="text-center notfound"><td colspan="17">' . __('Not found data') . '</td></tr>'; ?>
	<?php endif; ?>
                </tbody>
            </table>
            						<?php echo $this->element('paginate_pages',array('options'=>true));?>
        </div> <!-- end col md 12 -->
    </div><!-- end row -->
</div><!-- end containing of content -->