<div class="sysActions index">
	<h2><?php echo __('Sys Actions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sys_controller_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('sys_status_id'); ?></th>
			<th><?php echo $this->Paginator->sort('ref1'); ?></th>
			<th><?php echo $this->Paginator->sort('ref2'); ?></th>
			<th><?php echo $this->Paginator->sort('ref3'); ?></th>
			<th><?php echo $this->Paginator->sort('create_uid'); ?></th>
			<th><?php echo $this->Paginator->sort('update_uid'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($sysActions as $sysAction): ?>
	<tr>
		<td><?php echo h($sysAction['SysAction']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($sysAction['SysController']['name'], array('controller' => 'sys_controllers', 'action' => 'view', $sysAction['SysController']['id'])); ?>
		</td>
		<td><?php echo h($sysAction['SysAction']['name']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['description']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($sysAction['SysStatus']['name'], array('controller' => 'sys_statuses', 'action' => 'view', $sysAction['SysStatus']['id'])); ?>
		</td>
		<td><?php echo h($sysAction['SysAction']['ref1']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['ref2']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['ref3']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['create_uid']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['update_uid']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['created']); ?>&nbsp;</td>
		<td><?php echo h($sysAction['SysAction']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sysAction['SysAction']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sysAction['SysAction']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sysAction['SysAction']['id']), null, __('Are you sure you want to delete # %s?', $sysAction['SysAction']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
