<div class="ui-pakgon-form">
<h1><?php  echo __('จัดการ Actions > รายละเอียด'); ?></h1>
	<table cellpadding="0" cellspacing="0" class="table-form-edit">
          <tr>
            <td><?php echo __('ชื่อ Controller'); ?></td>
            <td><?php echo $this->Html->link($sysAction['SysController']['name'], array('controller' => 'sys_controllers', 'action' => 'view', $sysAction['SysController']['id'])); ?></td>
          </tr>
          <tr>
            <td><?php echo __('ชื่อ Function'); ?></td>
            <td><?php echo h($sysAction['SysAction']['name']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('รายละเอียด'); ?></td>
            <td><?php echo h($sysAction['SysAction']['description']); ?></td>
          </tr>
          
          <tr>
            <td><?php echo __('สถานะ'); ?></td>
            <td><?php echo $this->DisplayFormat->sys_status($sysAction['SysAction']['status']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('รายละเอียด'); ?></td>
            <td><?php echo h($sysAction['SysAction']['description']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('ผู้สร้าง'); ?></td>
            <td><?php echo h($sysAction['SysAction']['create_uid']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('ผู้แก้ไข'); ?></td>
            <td><?php echo h($sysAction['SysAction']['update_uid']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('วันที่สร้าง'); ?></td>
            <td><?php echo $this->DisplayFormat->datetime_iso($sysAction['SysAction']['created']); ?></td>
          </tr>
          <tr>
            <td><?php echo __('วันที่แก้ไข'); ?></td>
            <td><?php echo $this->DisplayFormat->datetime_iso($sysAction['SysAction']['modified']); ?></td>
          </tr>
      </table>
      <br/>
      <?php echo $this->Permission->link_button("กลับหน้าหลัก" , "/SysAcls/index/"); ?>
</div>