<div class="sysActions form">
<?php echo $this->Form->create('SysAction'); ?>
	<fieldset>
		<legend><?php echo __('Add Sys Action'); ?></legend>
	<?php
		echo $this->Form->input('sys_controller_id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('sys_status_id');
		echo $this->Form->input('ref1');
		echo $this->Form->input('ref2');
		echo $this->Form->input('ref3');
		echo $this->Form->input('create_uid');
		echo $this->Form->input('update_uid');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sys Actions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sys Controllers'), array('controller' => 'sys_controllers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sys Controller'), array('controller' => 'sys_controllers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sys Statuses'), array('controller' => 'sys_statuses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sys Status'), array('controller' => 'sys_statuses', 'action' => 'add')); ?> </li>
	</ul>
</div>
