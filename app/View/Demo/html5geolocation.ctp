<input type="button" name="btnGetLocation" id="btnGetLocation" value="Get Location" class="btn btn-primary btn-flat">
<br/> <br />
<div id="location"></div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnGetLocation").click(function () {
            getLocation();
        });

    });

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $('#location').html("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        var location = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;
        $('#location').html(location);
    }
</script>