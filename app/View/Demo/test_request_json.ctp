<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Request JSON with parameters', 'bclose' => true, 'bcollap' => true)); ?>
    <div class="box-body">
        <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
        <?php echo $this->Form->input('params'); ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>