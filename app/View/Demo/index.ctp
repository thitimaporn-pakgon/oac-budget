<?php echo $this->Html->image('bg.jpg', array('class' => 'img-responsive')); ?>

<div class="col-md-12">
    <form action="/users/remember_password" id="UserRememberPasswordForm" method="post" accept-charset="utf-8">
        <div style="display:none;"><input name="_method" value="POST" type="hidden"></div>
        <h2 class="text-primary"> <i class="fa fa-lock"></i> Forgot your password?</h2>
        <hr class="col-xs-5">
        <div class="clearfix"></div>
        <div class="col-xs-5">
            <div class="form-group"><p>Please enter your email address</p>
                <div class="input email required">
                    <input name="data[User][email]" placeholder="Email" class="form-control email-field" maxlength="255" id="UserEmail" required="required" type="email">
                </div>
            </div>
        </div>
        <div class="col-xs-12"> 
            <button type="submit" class="btn btn-info"> <i class="fa fa-share"></i> Next step </button>
        </div>
    </form>
</div>
