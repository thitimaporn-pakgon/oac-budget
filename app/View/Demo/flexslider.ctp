<?php // echo $this->Html->css('/Libraries/carousel/FlexSlider/flexslider.css');       ?>
<?php echo $this->Html->css('/Libraries/carousel/FlexSlider/pgwslideshow.css'); ?>
<?php //echo $this->Html->css('/Libraries/carousel/FlexSlider/pgwslideshow_light.min.css');       ?>




<div id="container" class="cf">
    <div id="main" role="main">
        <section class="slider">
            <div id="slider" class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                </ul>
            </div>
            <div id="carousel" class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_lemon.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_donut.jpg" />
                    </li>
                    <li>
                        <img src="/Libraries/carousel/FlexSlider/demo/images/kitchen_adventurer_caramel.jpg" />
                    </li>
                </ul>
            </div>
        </section>
        
    </div>

</div>



<?php echo $this->Html->script('/Libraries/carousel/FlexSlider/jquery.flexslider-min.js'); ?>
<?php echo $this->Html->script('/Libraries/carousel/FlexSlider/demo/js/jquery.easing.js'); ?>
<?php echo $this->Html->script('/Libraries/carousel/FlexSlider/demo/js/jquery.mousewheel.js'); ?>
<?php echo $this->Html->script('/Libraries/carousel/FlexSlider/demo/js/demo.js'); ?>
<script type="text/javascript">
    $(function () {
        //SyntaxHighlighter.all();
    });
    $(window).load(function () {
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 210,
            itemMargin: 5,
            asNavFor: '#slider'
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>