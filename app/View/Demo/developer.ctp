<!-- Button confirmation  -->
<?php echo $this->Html->css("/Libraries/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"); ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-info">
            <?php echo $this->element('boxOptionHeader', array('btitle' => 'Button modal dialog', 'bcollap' => true, 'bclose' => true)); ?>
            <?php echo $this->Form->create('button', array('role' => 'form', 'type' => 'file')); ?>
            <div class="box-body">
                <?php echo $this->Form->input('datepicker', array('class' => 'datepicker')); ?>
                <?php echo $this->Form->input('example_taginput', array('class' => 'inputtag', 'data-role' => 'tagsinput', 'value' => 'Amsterdam,Washington,Sydney,Beijing,Cairo')); ?>
                <?php //echo $this->Form->input('example_taginput', array('class' => 'inputtag')); ?>
                <h3 class="tag"> Zicure Corp</h3>
                <?php echo $this->Form->input('to', array('label' => __('Send To'), 'class' => 'required email')); ?>
                <?php echo $this->Form->input('subject', array('label' => __('Subject'), 'class' => 'required')); ?>
                <?php echo $this->Form->input('number', array('class' => 'number')); ?>
                <?php echo $this->Form->input('digits', array('class' => 'digits')); ?>
                <?php echo $this->Form->input('personal_card_no', array('class' => 'citizen_no')); ?>
                <?php echo $this->Form->input('min', array('class' => 'required', 'min' => 0)); ?>
                <?php echo $this->Form->input('max', array('class' => 'required', 'max' => 100)); ?>
                <?php echo $this->Form->input('range', array('class' => 'required', 'range' => "0,100")); ?>
                <?php echo $this->Form->input('min_length', array('class' => 'required', 'minlength' => 3)); ?>
                <?php echo $this->Form->input('max_length', array('class' => 'required', 'maxlength' => 10)); ?>
                <h4 class="tag"> Zicure Corp</h4>
                <?php echo $this->Form->input('letters', array('class' => 'letters')); ?>
                <?php echo $this->Form->input('capital_letters', array('class' => 'capital_letters')); ?>
                <?php echo $this->Form->input('small_letters', array('class' => 'small_letters')); ?>
                <?php echo $this->Form->input('file_extension', array('type' => 'file', 'accept' => 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,image/gif,image/jpeg,image/png')); ?>
            </div>
            <div class="box-footer">
                <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Submit Confirm'), "/Demo/index", array('type' => 'submit', 'class' => 'btn btn-primary confirmModal', 'data-confirm-title' => 'Are you sure for process the action', 'data-confirm-message' => 'กรุณายืนยันการทำงาน คุณแน่ใจหรือไม่')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-save', 'Button Submit Confirm'), null, array('action' => '/Home/index', 'type' => 'button', 'class' => 'btn btn-info confirmModal', 'data-confirm-title' => 'Are you sure for process the action', 'data-confirm-message' => 'กรุณายืนยันการทำงาน คุณแน่ใจหรือไม่')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-save', 'Button Confirm'), null, array('type' => 'submit', 'class' => 'btn btn-info confirmModal', 'data-confirm-title' => 'Are you sure for process the action', 'data-confirm-message' => 'กรุณายืนยันการทำงาน คุณแน่ใจหรือไม่')); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-save', 'Link Confirm'), "/Demo/index", array('class' => 'btn btn-success confirmModal', 'data-confirm-title' => 'Are you sure for process the action', 'data-confirm-message' => 'กรุณายืนยันการทำงาน คุณแน่ใจหรือไม่')); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./div.box-footer -->
        </div><!-- /.box -->
    </div><!-- ./col-md-6 -->

    <div class="col-md-6">
        <div class="box box-warning">
            <?php echo $this->element('boxOptionHeader', array('btitle' => 'Modal Helper', 'bclose' => true, 'bcollape' => true)); ?>
            <div class="box-body">
                <?php echo $this->Form->input('fname'); ?>
                <?php echo $this->Bootstrap->callout(); ?>
                <?php echo $this->Bootstrap->callout('Zicure Developer Documentation', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'warning'); ?>
                <?php echo $this->Bootstrap->callout('Zicure Developer Documentation', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'danger'); ?>
                
                <!-- alert imeaiatly -->
                <?php echo $this->Bootstrap->alert('Bootstrap alert danger (default)!', 'Zicure Zicure Bootstrap Helper.'); ?>
                <?php echo $this->Bootstrap->alert('Bootstrap alert warning', 'Zicure Zicure Bootstrap Helper.', 'warning'); ?>
                <?php echo $this->Bootstrap->alert('Bootstrap alert success', 'Zicure Zicure Bootstrap Helper.', 'success'); ?>
                <?php //echo $this->Bootstrap->modal('Zicure Developer Documentation', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'); ?>

                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-map-marke', 'modalMessage'), null, array('id' => 'btnModalConfirm', 'class' => 'btn btn-danger')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-navicon', 'modalMessage'), null, array('id' => 'btnModalConfirm2', 'class' => 'btn btn-success')); ?>

                <?php echo $this->Bootstrap->br(2); ?> 
                <?php echo $this->Bootstrap->badge('Demo how to notification', 'warning'); ?>
                <?php echo $this->Bootstrap->br(2); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-default'), null, array('id' => 'btnNotification', 'class' => 'btn btn-default noti-default', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-success'), null, array('class' => 'btn btn-success noti-success', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-danger'), null, array('class' => 'btn btn-danger noti-danger', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                <?php echo $this->Bootstrap->br(2); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-warning'), null, array('class' => 'btn btn-warning noti-warning', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-info'), null, array('class' => 'btn btn-info noti-info', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                <?php //echo $this->Zicure->calDifferenceTwoDate('2016-03-01', '2016-06-01'); ?>
            </div>
        </div>
    </div>
</div><!--<./row>-->





<div class="row">
    <div class="col-md-6">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class="active"><a href="#tabA" data-toggle="tab">Zicure</a></li>
                <li><a href="#tabB" data-toggle="tab">Unicorn</a></li>
                <li class="pull-left header"><?php echo $this->Bootstrap->icon('fa-inbox', 'Pakgon'); ?></li>
            </ul><!-- ./tab-menu -->


            <div class="tab-content">
                <div class="chart tab-pane active" id="tabA" style="position: relative; height: 300px;">
                    <?php echo $this->Bootstrap->badge('Demo how to notification', 'warning'); ?>
                    <?php echo $this->Bootstrap->br(2); ?>
                    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-default'), null, array('id' => 'btnNotification', 'class' => 'btn btn-default noti-default', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-success'), null, array('class' => 'btn btn-success noti-success', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-danger'), null, array('class' => 'btn btn-danger noti-danger', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                    <?php echo $this->Bootstrap->br(2); ?>
                    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-warning'), null, array('class' => 'btn btn-warning noti-warning', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-bell-o', '.noti-info'), null, array('class' => 'btn btn-info noti-info', 'data-noti-message' => 'Zicure demo default notification.')); ?>
                </div><!-- #/tabA -->
                <div class="chart tab-pane" id="tabB">
                    <?php echo $this->Bootstrap->callout(); ?>
                    <?php echo $this->Bootstrap->callout('Zicure Developer Documentation', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'warning'); ?>
                    <?php echo $this->Bootstrap->callout('Zicure Developer Documentation', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'danger'); ?>
                    <?php echo $this->Bootstrap->alert('Bootstrap alert danger (default)!', 'Zicure Zicure Bootstrap Helper.'); ?>
                    <?php echo $this->Bootstrap->alert('Bootstrap alert warning', 'Zicure Zicure Bootstrap Helper.', 'warning'); ?>
                    <?php echo $this->Bootstrap->alert('Bootstrap alert success', 'Zicure Zicure Bootstrap Helper.', 'success'); ?>
                </div><!-- #/tabB -->
            </div><!-- ./tab-content -->

        </div><!-- /.nav-tabs-custom -->
    </div>



    <div class="col-md-6">
        <div class="box box-warning">
            <?php echo $this->element('boxOptionHeader', array('btitle' => 'Box Option Modal', 'bclose' => true, 'bcollapse' => true)); ?>
            <div class="box-body">
                <table class="makup-modal table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>simple</th>
                            <th>modal</th>
                            <th>simple</th>
                            <th>modal</th>
                            <th>simple</th>
                            <th>modal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>xxx</td>
                            <td>yyy</td>
                            <td>xxx</td>
                            <td>yyy</td>
                            <td>xxx</td>
                            <td>yyy</td>
                        </tr>
                    </tbody>
                </table>

                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script("/Libraries/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"); ?>
<script type="text/javascript">
    $(function () {


        //        $(".form-group").each(function(){
        //            var $that = $(this);
        //            $that.find("input[type='text']").has
        //        });

        //            $("input[type='text'],textarea,select").each(function(){
        //                var $that = $(this);
        //                if($that.hasClass('required')){
        //                    $that.css('border-color','blue');
        //                    $that.parents('.form-group').find('label').html($that.parents('.form-group').find('label').text() + '<span class="star-red">*</span>');
        //                }
        //            });
        $("#btnModalConfirm").click(function () {
            modalMessage('Wellcome to Modal message dialog.');
        });

        $("#btnModalConfirm2").click(function () {
            modalMessage('Wellcome to Modal message dialog.', '<?php echo 'make new modal message title' ?>');
        });

        //        $('.noti-default').on('click', function (e) {
        //            //e.preventDefault();
        //            $.notify('I am a default info box.');
        //        });
        //        
        //        $('.noti-topcenter').on('click', function (e) {
        //            e.preventDefault();
        //            $.notify('I am a default info box.', {pos: 'top-center', status: 'danger'});
        //        });

        //Application notification style
//        AppDefault('Example for notification.');
//        AppInfo('Application Information.');
//        AppSuccess('Application Successfully.');
        AppWarning('Application Warning.');
//        AppDanger('Application Danger.');
    });

</script>