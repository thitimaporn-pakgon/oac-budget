<!-- Bootstrap Fileinput -->
<!-- USED UNDER LINK -->
<!--<link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="../themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>


<script src="../js/plugins/sortable.js" type="text/javascript"></script>
<script src="../js/fileinput.js" type="text/javascript"></script>
<script src="../js/fileinput_locale_fr.js" type="text/javascript"></script>
<script src="../js/fileinput_locale_es.js" type="text/javascript"></script>
<script src="../themes/explorer/theme.js" type="text/javascript"></script>-->

<?php echo $this->Html->css('/Libraries/bootstrap-fileinput/css/fileinput.min.css'); ?>
<?php echo $this->Html->css('/Libraries/bootstrap-fileinput/themes/explorer/theme.css'); ?>


<?php echo $this->Html->script('/Libraries/bootstrap-fileinput/js/plugins/sortable.min.js'); ?>
<?php echo $this->Html->script('/Libraries/bootstrap-fileinput/js/fileinput.min.js'); ?>
<?php echo $this->Html->script('/Libraries/bootstrap-fileinput/js/locales/th.js'); ?>
<?php echo $this->Html->script('/Libraries/bootstrap-fileinput/js/locales/es.js'); ?>
<?php echo $this->Html->script('/Libraries/bootstrap-fileinput/themes/explorer/theme.js'); ?>

<div class="row">
    <div class="col-sm-6 col-md-6">
        <div class="box box-solid box-primary">
            <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'Bootstrap Fileinput', 'bcollap' => true, 'bclose' => true)); ?>
            <?php echo $this->Form->create('AFile', array('role' => 'form', 'type' => 'file')); ?>
            <div class="box-body">
                <?php echo $this->Form->input('kv-explorer', array('type' => 'file', 'label' => array('text' => __('multiple = multiple')), 'id' => 'kv-explorer', 'multiple' => 'multiple')); ?>
                <?php echo $this->Form->input('file-0a', array('type' => 'file', 'label' => array('text' => __('Exalple')), 'class' => 'file')); ?>
            </div>
            <div class="box-footer">
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-upload', 'Upload'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-refresh', 'Upload'), null, array('type' => 'reset', 'class' => 'btn btn-default')); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./div.box-footer -->
        </div><!-- /.box -->

        <div class="box box-solid">
            <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'Bootstrap Fileinput', 'bcollap' => true, 'bclose' => true)); ?>
            <?php echo $this->Form->create('CFile', array('role' => 'form', 'type' => 'file')); ?>
            <div class="box-body">
                <div class="clearfix"></div>
                <div class="row">
                    <?php echo $this->Bootstrap->label('Multi Language Inputs', 'danger', 'h4'); ?>
                    <?php echo $this->Bootstrap->br(1); ?>
                </div>
                <?php echo $this->Form->input('file-fr', array('type' => 'file', 'label' => array('text' => __('French Input')), 'id' => 'file-fr', 'multiple' => 'multiple', 'name' => 'file-fr[]')); ?>
                <hr style="border: 2px dotted">
                <?php echo $this->Form->input('file-es', array('type' => 'file', 'label' => array('text' => __('Spanish Input')), 'id' => 'file-es', 'multiple' => 'multiple', 'name' => 'file-es[]')); ?>
            </div>
            <div class="box-footer">
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-upload', 'Upload'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-refresh', 'Upload'), null, array('type' => 'reset', 'class' => 'btn btn-default')); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./div.box-footer -->
        </div><!-- /.box -->
    </div>

    <div class="col-sm-6 col-md-6">
        <div class="box box-warning">
            <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'Bootstrap Fileinput', 'bcollap' => true, 'bclose' => true)); ?>
            <?php echo $this->Form->create('BFile', array('role' => 'form', 'type' => 'file')); ?>
            <div class="box-body">
                <div class="clearfix"></div>
                <div class="row">
                    <?php echo $this->Bootstrap->label('File Upload Example', 'warning', 'h3'); ?>
                    <?php echo $this->Bootstrap->br(1); ?>
                </div>
                <?php echo $this->Form->input('file-0c', array('type' => 'file', 'label' => array('text' => __('multiple = multiple, data-min-file-count = 3')), 'class' => 'file', 'id' => 'file-0c', 'multiple' => 'multiple', 'data-min-file-count' => 3)); ?>
                <?php echo $this->Form->input('file-0d', array('type' => 'file', 'label' => array('text' => __('Exalple')), 'class' => 'file', 'id' => 'file-0d')); ?>
                <?php echo $this->Form->input('file-1', array('type' => 'file', 'label' => array('text' => __('data-overwrite-initial = false, data-min-file-count = 2')), 'class' => 'file', 'id' => 'file-1', 'multiple' => 'multiple', 'data-overwrite-initial' => 'false', 'data-min-file-count' => 2)); ?>
                <?php echo $this->Form->input('file-2', array('type' => 'file', 'label' => array('text' => __('readonly = readonly')), 'class' => 'file', 'id' => 'file-2', 'readonly' => 'readonly', 'data-show-upload' => 'false')); ?>
                <?php echo $this->Form->input('file-3', array('type' => 'file', 'label' => array('text' => __('Preview File Icon')), 'id' => 'file-3')); ?>
                <?php echo $this->Form->input('file-4', array('type' => 'file', 'label' => array('text' => __('data-upload-url="#"')), 'class' => 'file', 'id' => 'file-4')); ?>

                <div class="clearfix"></div>
                <div class="row">
                    <?php echo $this->Bootstrap->label('File Upload Example', 'success', 'h4'); ?>
                    <?php echo $this->Bootstrap->br(1); ?>
                </div>
                <?php echo $this->Permission->button(__('Disable Test'), null, array('type' => 'button', 'class' => 'btn btn-warning')); ?>
                <?php echo $this->Permission->button(__('Refresh Test'), null, array('type' => 'reset', 'class' => 'btn btn-info')); ?>
                <?php echo $this->Permission->button(__('Submit'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
                <?php echo $this->Permission->button(__('Reset'), null, array('type' => 'reset', 'class' => 'btn btn-default')); ?>

                <?php echo $this->Form->input('test-upload', array('type' => 'file', 'label' => array('text' => __('Test Upload')), 'id' => 'test-upload', 'multiple' => 'multiple', 'after' => '<div id="errorBlock" class="help-block"></div>')); ?>
                <?php echo $this->Form->input('file-5', array('type' => 'file', 'label' => array('text' => __('data-preview-file-type = any, data-upload-url = #, multiple = multiple')), 'class' => 'file', 'id' => 'file-5', 'data-preview-file-type' => 'any', 'data-upload-url' => '#', 'multiple' => 'multiple')); ?>
            </div>
            <div class="box-footer">
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-upload', 'Upload'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
                <?php echo $this->Permission->button($this->Bootstrap->icon('fa-refresh', 'Upload'), null, array('type' => 'reset', 'class' => 'btn btn-default')); ?>
                <?php echo $this->Form->end(); ?>
            </div><!-- ./div.box-footer -->
        </div><!-- /.box -->
    </div>
</div>
<div class="box-footer">
    <?php echo $this->Permission->link($this->Bootstrap->icon('fa-github', 'Tutorial'), "https://github.com/kartik-v/bootstrap-fileinput", array('class' => 'btn btn-info','target'=>'_blank')); ?>
</div><!-- ./div.box-footer -->


<script type="text/javascript">
    $('#file-fr').fileinput({
        language: 'th',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $('#file-es').fileinput({
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $("#file-0").fileinput({
        'allowedFileExtensions': ['jpg', 'png', 'gif']
    });
    $("#file-1").fileinput({
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    /*
     $(".file").on('fileselect', function(event, n, l) {
     alert('File Selected. Name: ' + l + ', Num: ' + n);
     });
     */
    $("#file-3").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-lg",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        overwriteInitial: false,
        initialPreviewAsData: true,
        initialPreview: [
            "http://lorempixel.com/1920/1080/transport/1",
            "http://lorempixel.com/1920/1080/transport/2",
            "http://lorempixel.com/1920/1080/transport/3",
        ],
        initialPreviewConfig: [
            {caption: "transport-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1},
            {caption: "transport-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2},
            {caption: "transport-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3},
        ],
    });
    $("#file-4").fileinput({
        uploadExtraData: {kvId: '10'}
    });
    $(".btn-warning").on('click', function () {
        var $el = $("#file-4");
        if ($el.attr('disabled')) {
            $el.fileinput('enable');
        } else {
            $el.fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $("#file-4").fileinput('refresh', {previewClass: 'bg-info'});
    });
    /*
     $('#file-4').on('fileselectnone', function() {
     alert('Huh! You selected no files.');
     });
     $('#file-4').on('filebrowse', function() {
     alert('File browse clicked for #file-4');
     });
     */
    $(document).ready(function () {
        $("#test-upload").fileinput({
            'showPreview': false,
            'allowedFileExtensions': ['jpg', 'png', 'gif'],
            'elErrorContainer': '#errorBlock'
        });
        $("#kv-explorer").fileinput({
            'theme': 'explorer',
            'uploadUrl': '#',
            overwriteInitial: false,
            initialPreviewAsData: true,
            initialPreview: [
                "http://lorempixel.com/1920/1080/nature/1",
                "http://lorempixel.com/1920/1080/nature/2",
                "http://lorempixel.com/1920/1080/nature/3",
            ],
            initialPreviewConfig: [
                {caption: "nature-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1},
                {caption: "nature-2.jpg", size: 872378, width: "120px", url: "{$url}", key: 2},
                {caption: "nature-3.jpg", size: 632762, width: "120px", url: "{$url}", key: 3},
            ]
        });
        /*
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });
         */
//        $.fn.fileinput.defaults = {
//    language: 'en',
//    showCaption: true,
//    showPreview: true,
//    showRemove: true,
//    showUpload: false, // <------ just set this from true to false
//    showCancel: true,
//    showUploadedThumbs: true,
//    // many more below
//};
    });
</script>