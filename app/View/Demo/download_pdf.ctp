<?php

//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//ob_start();
App::import('Vendor', 'tcpdf/xtcpdf');
$pdf = new TCPDF();
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sarawutt.b');
$pdf->SetTitle('CakePHP TCPDF');
$pdf->SetSubject('CakePHP TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, CakePHP, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('thsarabun', 'UBI', 20);

// add a page
$pdf->AddPage();

// set some text to print
$txt = <<<EOD
TCPDF Example modified by Sarawutt.b
งานหยาบเลยกรู สุนทรภู่
Default page header and footer are disabled using setPrintHeader() and setPrintFooter() methods.
EOD;

// print a block of text using Write()
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
//$pdf->Output('example_002.pdf','D');
//header("Content-type: application/pdf");
//
// header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//    header("Cache-Control: no-store, no-cache, must-revalidate");
//    header("Cache-Control: post-check=0, pre-check=0", false);
//    header("Pragma: no-cache");  
//    header("Content-type: application/pdf");
//    header("Content-disposition: inline; filename=file.pdf");
    //$size = ob_get_length();
    //header("Content-Length: $size");

//    ob_end_flush();

    //header("Connection: close");
$pdf->Output('example_002.pdf');

//var_dump($this->Pdf);exit;
?>