<!-- CK Editor -->
<div class="box box-warning">
    <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'CK Editor Advanced and full of features', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('Example', array('role' => 'form')); ?>
    <div class="box-body pad">
        <?php echo $this->Form->input('to', array('label' => __('Send To'), 'class' => 'required email')); ?>
        <?php echo $this->Form->input('subject', array('label' => __('Subject'), 'class' => 'required')); ?>
        <?php echo $this->Form->input('message', array('type' => 'textarea', 'id' => 'ckeditor-basic', 'class' => 'ckeditor-basic', 'div' => false, 'label' => false, 'value' => 'This is my textarea to be replaced with CKEditor.')); ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->button($this->Bootstrap->icon('fa-arrow-circle-right', 'Send'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- ./div.box-footer -->
</div><!-- /.box -->

<!-- TinyMCE Editor -->
<div class="box box-info">
    <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'TinyMCE Editor Advanced and full of features', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('MailExample', array('role' => 'form')); ?>
    <div class="box-body pad">
        <?php echo $this->Form->input('to', array('label' => __('Send To'), 'class' => 'required email')); ?>
        <?php echo $this->Form->input('subject', array('label' => __('Subject'), 'class' => 'required')); ?>
        <?php echo $this->Form->input('message', array('type' => 'textarea', 'id' => 'tinymceeditor', 'class' => 'tinyeditor-basic', 'div' => false, 'label' => false, 'value' => 'This is my textarea to be replaced with CKEditor.')); ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->button($this->Bootstrap->icon('fa-arrow-circle-right', 'Send'), null, array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- ./div.box-footer -->
</div><!-- /.box -->

<!-- TinyMCE Editor -->
<div class="box box-info">
    <?php echo $this->element('utilities/boxOptionHeader', array('btitle' => 'Button modal dialog', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('button', array('role' => 'form')); ?>
    <div class="box-body pad">
        <?php echo $this->Form->input('to', array('label' => __('Send To'), 'class' => 'required email')); ?>
        <?php echo $this->Form->input('subject', array('label' => __('Subject'), 'class' => 'required')); ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->button($this->Bootstrap->icon('fa-arrow-circle-right', 'Send'), null, array('type' => 'submit', 'class' => 'btn btn-warning ConfirmModal','data-confirm-title'=>'Are you sure for process the action','data-confirm-message'=>'กรุณายืนยันการทำงาน คุณแน่ใจหรือไม่')); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- ./div.box-footer -->
</div><!-- /.box -->


<?php echo $this->Html->script('/Libraries/editor/ckeditor/ckeditor.js'); ?>
<?php echo $this->Html->script('/Libraries/editor/tinymce/js/tinymce/tinymce.min.js'); ?>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('ckeditor-basic', {
            toolbar: 'Standard'
        });
    });

    tinymce.init({
        selector: '#tinymceeditor',
        theme: 'modern',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
        ],
        content_css: 'css/content.css',
        statusbar: false,
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
    });

//    tinymce.init({
//    selector: '#tinymceeditor',
//    theme: 'modern',
//    width: 600,
//    height: 300,
//    plugins: [
//      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
//      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
//      'save table contextmenu directionality emoticons template paste textcolor'
//    ],
//    content_css: 'css/content.css',
//    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
//  });
//    $(document).ready(function () {
//        CKEDITOR.replaceClass = 'ckeditor';
//    });

</script>