<?php
/**
 *
 * View page for strategicManeuversController it show for StrategicManeuver infomation.
 * @author sarawutt.b 
 * @since 2017-04-18 17:58:42
 * @license Zicure Corp. 
 */
?>
<div class="strategicManeuvers div-view-information box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Strategic Maneuver Information', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <?php // debug($data);exit; ?>
        <ul class="level-0" >
            <i class="glyphicon glyphicon-play">
                <?php echo $data['StrategicManeuver']['name']; ?>
            </i>
            <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-trash', null, 'glyphicon', 'span'), "/StrategicManeuvers/delete/{$data['StrategicManeuver']['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
            <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-pencil', null, 'glyphicon', 'span'), "/StrategicManeuvers/edit/{$data['StrategicManeuver']['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
            <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-plus', null, 'glyphicon', 'span'), "/PlanManeuvers/add/{$data['StrategicManeuver']['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
            <?php
            $plan = $this->Ebudget->getDataTree('PlanManeuver', $data['StrategicManeuver']['id']);
//            debug($plan);exit;
            ?>
            <?php if (!empty($plan)): ?>

                <?php foreach ($plan AS $plan): ?>
                    <ul class="level-1">
                        <i class="glyphicon glyphicon-play"><?php echo $plan[0]['name']; ?></i>
                        <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-trash', null, 'glyphicon', 'span'), "/PlanManeuvers/delete/{$plan[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                        <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-pencil', null, 'glyphicon', 'span'), "/PlanManeuvers/edit/{$plan[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                        <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-plus', null, 'glyphicon', 'span'), "/ResultManeuvers/add/{$plan[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                        <?php
                        $result = $this->Ebudget->getDataTree('ResultManeuver', $plan[0]['id']);
                        if (!empty($result)):
                            ?>
                            <?php foreach ($result AS $result): ?>
                                <ul class="level-2">
                                    <i class="glyphicon glyphicon-play"><?php echo $result[0]['name']; ?></i>
                                    <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-trash', null, 'glyphicon', 'span'), "/ResultManeuvers/delete/{$result[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                    <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-pencil', null, 'glyphicon', 'span'), "/ResultManeuvers/edit/{$result[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                    <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-plus', null, 'glyphicon', 'span'), "/EventManeuvers/add/{$result[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                    <?php
                                    $event = $this->Ebudget->getDataTree('EventManeuver', $result[0]['id']);
                                    if (!empty($event)):
                                        ?>
                                        <?php foreach ($event AS $event): ?>
                                            <ul class="level-3">
                                                <i class="glyphicon glyphicon-play"><?php echo $event[0]['name']; ?></i>
                                                <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-trash', null, 'glyphicon', 'span'), "/EventManeuvers/delete/{$event[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                                <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-pencil', null, 'glyphicon', 'span'), "/EventManeuvers/edit/{$event[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                                <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-plus', null, 'glyphicon', 'span'), "/WorkGroupManeuvers/add/{$event[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                                <?php
                                                $workGroup = $this->Ebudget->getDataTree('WorkGroupManeuver', $event[0]['id']);
                                                if (!empty($workGroup)):
                                                    ?>
                                                    <?php foreach ($workGroup AS $workGroup): ?>
                                                        <ul class="level-4">
                                                            <i class="glyphicon glyphicon-play"><?php echo $workGroup[0]['name']; ?></i>
                                                            <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-trash', null, 'glyphicon', 'span'), "/WorkGroupManeuvers/delete/{$workGroup[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                                            <i class="pull-right"><?php echo $this->Permission->link($this->Bootstrap->icon('glyphicon-pencil', null, 'glyphicon', 'span'), "/WorkGroupManeuvers/edit/{$workGroup[0]['id']}", array('class' => 'btn btn-primary btn-xs separate-button')); ?></i>
                                                        </ul>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div><!-- ./div.box-body -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./div.box box-warning -->
<style>
    .level-0 {
        width: 100%;
        /*padding-left: 50px;*/
        /*background-color: #8effc3;*/
        padding-bottom: 5px;
        padding-top: 5px;
    }
    .level-1 {
        width: 100%;
        /*background-color: #8effc3;*/
        /*padding-left: 70px;*/
        padding-bottom: 5px;
        padding-top: 5px;
    }
    .level-2 {
        width: 100%;
        /*background-color: #8effc3;*/
        /*padding-left: 90px;*/
        padding-bottom: 5px;
        padding-top: 5px;
    }
    .level-3 {
        width: 100%;
        /*background-color: #8effc3;*/
        /*padding-left: 110px;*/
        padding-bottom: 5px;
        padding-top: 5px;
    }  
    .level-4 {
        width: 100%;
        /*background-color: #8effc3;*/
        /*padding-left: 130px;*/
        padding-bottom: 5px;
        padding-top: 5px;
    } 

</style>