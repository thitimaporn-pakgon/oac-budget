<?php
/**
 *
 * View page for strategicManeuversController it show for StrategicManeuver infomation.
 * @author sarawutt.b 
 * @since 2017-04-18 17:58:42
 * @license Zicure Corp. 
 */
?>
<div class="strategicManeuvers div-view-information box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Strategic Maneuver Information', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->element('/structures/structure_flow',array('data'=>$data2)); ?>
    <div class="box-footer">
        <?php echo $this->Permission->linkBack('/StrategicManeuvers/index'); ?>
    </div>
</div><!-- ./div.box box-warning -->
