

<div class="strategicManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Strategic Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('StrategyGov', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'),'disabled'=>true)); ?>
        <?php echo $this->Form->input('strategy_gov_name', array('class' => 'required', 'label' => __('Strategic Name'))); ?>
        <?php echo $this->Form->input('code', array('class' => 'required digits')); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/StrategicManeuvers/edit'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
