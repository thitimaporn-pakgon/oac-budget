
<div class="strategicManeuvers index zicure-index">
    <!-- zicure-box-find search input params section -->
    <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Strategic Maneuver Management System', 'bcollap' => true, 'bclose' => true)); ?>
        <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
        <div class="box-body"> 
            <?php echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'id' => 'ddlCloneToTargetBudgetYear', 'default' => $this->Project->getCurrenBudgetYearTH())); ?>
            <?php // echo $this->Form->input('department_id', array('options' => $this->Zicure->listDepartment())); ?>
            <?php //echo $this->Form->input('dateFrom', array('type' => 'text', 'class' => 'datepicker datepicker-start')); ?>
            <?php //echo $this->Form->input('dateTo', array('type' => 'text', 'class' => 'datepicker datepicker-end')); ?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-search', __('Search')), null, array('name' => 'btnSubmitSearch', 'id' => 'btnSearch', 'class' => 'btn btn-primary')); ?>
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', __('Add Strategic Maneuver')), '/StrategicManeuvers/add', array('name' => 'btnAddnew', 'class' => 'btn bg-olive')); ?>
            <?php echo $this->element('structures/cloneBudgetStructure'); ?>
        </div><!-- /div.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.zicure-box-find box box-warning -->



    <!-- zicure-box-result search result section -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Structures (Result)', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?>
                        </th>
                        <th><?php echo $this->Paginator->sort('budget_year_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Strategic')); ?></th>
                        <!--<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($strategicManeuvers)): ?>
                        <?php foreach ($strategicManeuvers as $k => $strategicManeuver): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                        <td><?php echo h($strategicManeuver['StrategyGov']['budget_year_id']); ?></td>
                        <td><?php echo h($strategicManeuver['StrategyGov']['strategy_gov_name']); ?></td>
                        <!--<td><?php // echo $this->Zicure->dateISO($strategicManeuver['StrategyGov']['modified']); ?></td>-->
                        <td class="actions"><?php echo $this->Permission->getActions($strategicManeuver['StrategyGov']['id'], 'StrategicManeuvers', true, false, false); ?></td>
                    </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <tr class="text-center notfound">
                        <td colspan="8"><?php echo __('Information Not Found'); ?></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->