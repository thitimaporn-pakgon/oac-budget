<?php
/**
 *
 * Index page for configsController it show data list for Config infomation.
 * @author sarawutt.b 
 * @since 2017-03-07 14:53:54
 * @license Zicure Corp. 
 */
?>
<div class="configs index zicure-index">
    <!-- zicure-box-find search input params section -->
    <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Config Management System', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
            <?php echo $this->Form->input('name', array('label' => array('text' => __('Config Name')))); ?>
            <?php echo $this->Form->input('status', array('type' => 'select', 'class' => 'select2 chosen', 'options' => $this->Zicure->mainStatus())); ?>
            <?php //echo $this->Form->input('dateFrom', array('type' => 'text', 'class' => 'datepicker datepicker-start')); ?>
            <?php //echo $this->Form->input('dateTo', array('type' => 'text', 'class' => 'datepicker datepicker-end')); ?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '/configs/index', array('name' => 'btnSubmitSearch')); ?>
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', 'Add Config'), '/configs/add', array('name' => 'btnAddnew', 'class' => 'btn bg-olive')); ?>
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box-footer -->
    </div><!-- /div.zicure-box-find box box-warning -->

    <!-- zicure-box-result search result section -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Config (Result)', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('id', __('Configure ID')); ?></th>
                        <th><?php echo $this->Paginator->sort('type', __('Configure Type')); ?></th>
                        <th><?php echo $this->Paginator->sort('value', __('Configure Value')); ?></th>
                        <th><?php echo $this->Paginator->sort('display'); ?></th>
                        <th><?php echo $this->Paginator->sort('remark'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="actions">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($configs)): ?>
                        <?php foreach ($configs as $k => $config): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                                <td><?php echo h($config['Config']['id']); ?></td>
                                <td><?php echo h($config['Config']['type']); ?></td>
                                <td><?php echo h($config['Config']['value']); ?></td>
                                <td><?php echo h($config['Config']['display']); ?></td>
                                <td><?php echo h($config['Config']['remark']); ?></td>
                                <td><?php echo $this->Zicure->mainStatus($config['Config']['status'], true); ?></td>
                                <td><?php echo $this->Zicure->dateISO($config['Config']['modified']); ?></td>
                                <td class="actions"><?php echo $this->Permission->getActions($config['Config']['id'], 'Configs'); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr class="row-notfound">
                            <td colspan="9"><?php echo __('Information Not Found'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->