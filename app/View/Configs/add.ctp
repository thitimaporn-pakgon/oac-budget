<?php
/**
 *
 * Add page for configsController it Add of Config.
 * @author sarawutt.b 
 * @since 2017-03-07 14:53:56
 * @license Zicure Corp. 
 */
?>
<div class="configs form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Config', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('Config', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('id', array('type' => 'text')); ?>
        <?php echo $this->Form->input('type', array('class' => 'required')); ?>
        <?php echo $this->Form->input('value', array('class' => 'required')); ?>
        <?php echo $this->Form->input('display'); ?>
        <?php echo $this->Form->input('remark'); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/Configs/add'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
