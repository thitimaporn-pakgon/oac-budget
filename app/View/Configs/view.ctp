<?php
/**
 *
 * View page for configsController it show for Config infomation.
 * @author sarawutt.b 
 * @since 2017-03-07 14:53:55
 * @license Zicure Corp. 
 */
?>
<div class="configs view div-view-information box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Config Information', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                <tr>
                    <td class="table-view-label"><?php echo __('Config Id'); ?></td>
                    <td class="table-view-detail"><?php echo h($config['Config']['id']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Type'); ?></td>
                    <td class="table-view-detail"><?php echo h($config['Config']['type']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Value'); ?></td>
                    <td class="table-view-detail"><?php echo h($config['Config']['value']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Display'); ?></td>
                    <td class="table-view-detail"><?php echo h($config['Config']['display']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Remark'); ?></td>
                    <td class="table-view-detail"><?php echo h($config['Config']['remark']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Status'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->mainStatus($config['Config']['status'], true); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getUserById($config['Config']['create_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getUserById($config['Config']['update_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Created'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($config['Config']['created']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Modified'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($config['Config']['modified']); ?></td>
                </tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./div.box box-warning -->


