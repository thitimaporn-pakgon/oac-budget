<div class="budgetYears index">
    <?php echo $this->element('budgetYear_search'); ?>
</div>
<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'BudgetYear (Result)')); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-short-information table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nindex"><?php echo __('#'); ?></th>
                    <th><?php echo $this->Paginator->sort('name', __('Budget Year')); ?></th>
                    <th><?php echo $this->Paginator->sort('description'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($budgetYears)): ?>
                    <?php foreach ($budgetYears as $k => $budgetYear): ?>
                        <tr>
                            <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                            <td><?php echo h($budgetYear['BudgetYear']['name'], ('')); ?></td>
                            <td><?php echo h($budgetYear['BudgetYear']['description']); ?></td>
                            <td><?php echo $this->Zicure->mainStatus($budgetYear['BudgetYear']['status'], true); ?></td>
                            <td class="actions">
                                <?php echo $this->Permission->getActions($budgetYear['BudgetYear']['id'], 'BudgetYears'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo '<tr class="text-center notfound"><td colspan="5">' . __('Not found data') . '</td></tr>'; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div><!-- end containing of content -->
<!--    <p>
            <?php //echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
        </p>-->
    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
</div>