<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'BudgetYear Detail', 'bcollap' => true, 'bclose' => true)); ?>
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" class="table-form-view table table-bordered table-striped">
            <tbody>
                <tr>
                    <th><?php echo __('Budget Year'); ?></th>
                    <td><?php echo h($budgetYear['BudgetYear']['name']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td><?php echo h($budgetYear['BudgetYear']['description']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Status'); ?></th>
                    <td><?php echo $this->Zicure->mainStatus($budgetYear['BudgetYear']['status']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Create Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($budgetYear['BudgetYear']['create_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Update Uid'); ?></th>
                    <td><?php echo $this->Zicure->getUserById($budgetYear['BudgetYear']['update_uid']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($budgetYear['BudgetYear']['created']); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td><?php echo $this->Zicure->dateISO($budgetYear['BudgetYear']['modified']); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div>

