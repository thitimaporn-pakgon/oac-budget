<div class="budgetYears form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit BudgetYear')); ?>
        <?php echo $this->Form->create('BudgetYear', array('role' => 'form')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => __('Id'))); ?>
            <?php echo $this->Form->input('name', array('class' => 'form-control required digits', 'label' => __('Budget Year'))); ?>
            <?php echo $this->Form->input('description', array('class' => 'form-control required', 'placeholder' => __('Description'))); ?>
            <?php echo $this->Form->input('status', array('class' => 'form-control required', 'type' => 'select', 'options' => $this->Zicure->mainStatus(), 'placeholder' => __('Status'))); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();// คำสั่งไม่ให้ระบบทำงานออโต้ เราจะทำเอง
            var form = $("form#BudgetYearEditForm");
            var isValid = form.valid();// เชคดูในช่องกรอกที่มี class required
            if (isValid == true) {
                $.post('/BudgetYears/check_already_budget/' + $("#BudgetYearName").val() + '/' + $("#BudgetYearId").val(), function (data) {
                    if (data == 'N') {
                        form.submit();
                        return true;
                    } else {
                        AppMessage('<?php echo __('The budget_year is already exist on the system. Please, try again.') ?>');
                        $("#BudgetYearName").focus();
                    }
                });
            }
            return false;
        });
    });

</script>

