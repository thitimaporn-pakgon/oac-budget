<div class="budgetYears form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add BudgetYear')); ?>
        <?php echo $this->Form->create('BudgetYear', array('role' => 'form')); ?>
        <div class="box-body">
            <?php echo $this->Form->input('name', array('class' => 'required digits', 'label' => __('Budget Year'))); ?>
            <?php // echo $this->Form->input('description', array('class' => 'required')); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary', 'id' => 'btnSubmit')); ?>
        </div><!-- /.box-footer -->
        <?php echo $this->Form->end(); ?>
    </div><!-- /.box -->
</div>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();// คำสั่งไม่ให้ระบบทำงานออโต้ เราจะทำเอง
            var form = $("form#BudgetYearAddForm");

            var isValid = form.valid();// เชคดูในช่องกรอกที่มี class required
            if (isValid == false) {
                return false;
            }

            $.post('/BudgetYears/check_already_budget/' + $("#BudgetYearName").val(), function (data) {
                console.log(data);
                if (data == 'N') {
                    form.submit();
                    return true;
                } else {
                    AppMessage('<?php echo __('The budget_year is already exist on the system. Please, try again.') ?>');
                    $("#BudgetYearName").focus();
                }
            });
            return false;
        });
    });

</script>
