<div class="sysControllers form">
    <!-- Sys Controller add --> 
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Edit Controller'); ?></h3>
        </div>
        <div class="box-body">  
            <?php echo $this->Form->create('SysController', array('role' => 'form', 'url' => "/SysControllers/edit/{$id}")); ?>
            <?php echo $this->Form->hidden('id'); ?>
            <div class="form-group">
                <?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => __('Name'))); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('description', array('class' => 'form-control', 'placeholder' => __('Description'))); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder' => __('Status'))); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Permission->link_submit(__('Submit'), "/SysControllers/add/{$id}", array('id' => 'btnControllerAdd', 'name' => 'btnControllerAdd', 'div' => false, 'label' => false, 'type' => 'submit')); ?> 
                <?php echo $this->Permission->button_back(); ?> 
                <?php echo $this->Form->end(); ?>
            </div>
        </div><!-- end col md 12 -->
    </div><!-- end row -->

    <!-- Sys Action add for details -->
    <div class="box box-active">
        <div class="box-header">
            <h3 class="box-title"><?php echo __('Controller Actions Details'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php echo $this->Form->create('SysAction', array('role' => 'form', 'url' => "/SysControllers/addActions/{$id}")); ?>
            <table class="table-add-details table table-bordered table-striped" id="SysControllersTable">
                <tbody>
                    <tr>
                        <th class="nindex-black"><?php echo __('#'); ?></th>
                        <th><?php echo __('Function Name'); ?></th>
                        <th><?php echo __('Description'); ?></th>
                        <th><?php echo __('Status'); ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php foreach ($sysActionInfos as $k => $v): ?>
                        <tr>
                            <td class="nindex-black"><?php echo ++$k; ?></td>
                            <td class="textdetail"><?php echo $v['SysAction']['name']; ?></td>
                            <td class="textdetail"><?php echo $v['SysAction']['description']; ?></td>
                            <td class="textdetail"><?php echo $this->Zicure->mainStatus($v['SysAction']['status']); ?></td>
                            <td class="textdetail"><?php echo $this->Permission->confirmButton('<i class="fa fa-minus"></i>' . __('Delete'), "/SysControllers/deleteAction/{$id}/{$v['SysAction']['id']}", array('name' => 'btnActionsDelete', 'id' => "btnActionsDelete{$k}", 'class' => 'btn btn-danger confirmModal'), 'Do you want confirm delete the action ?'); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="nindex"><?php echo h(count($sysActionInfos) + 1); ?></td>
                        <td class="textdetail"><?php echo $this->Form->input('name', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Action name'))); ?></td>
                        <td class="textdetail"><?php echo $this->Form->input('description', array('div' => false, 'label' => false, 'placeholder' => __('Description'))); ?></td>
                        <td class="textdetail"><?php echo $this->Form->input('statuses', array('div' => false, 'label' => false, 'default' => 'A', 'disabled' => true)); ?></td>
                        <td class="textdetail"><?php echo $this->Permission->link_submit('<i class="fa fa-plus"></i>' . __('Add'), "/SysControllers/addActions/{$id}", array('id' => 'btnActionAdd', 'name' => 'btnActionAdd', 'div' => false, 'label' => false, 'type' => 'submit', 'class' => 'btn btn-success', 'disabled' => true)); ?> </td>
                    </tr>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<script type="text/javascript">
    $(function () {
        //        var controllerIndex = 0;
        //        //var promotion_step = 0;
        //        var totalIndex = 0;
        //
        //
        //        $("#btnSysControllerAdd").click(function (event) {
        //            var actionDescript = $("#SysControllerDecription").val();
        //            var actionStatus = $("#SysControllerStatuses option:selected").text();
        //            var actionName = $("#SysControllerActionName").val();
        //
        //            //$("#btnSysControllerAdd").attr("disabled",true);
        //
        //            $("#SysControllersTable tr:last").prev().before(//prev ถัดไป before วางข้อมูลก่อน
        //                    '<tr>' +
        //                    '<td class="textdetail">' + actionName + '</td>' +
        //                    '<td class="textdetail">' + actionDescript + '</td>' +
        //                    '<td class="textdetail">' + actionStatus + '</td>' +
        //                    '<td class="textdetail" align="center">' +
        //                    '<input type="hidden" name="data[actions][list][' + controllerIndex + '][name]" value="' + actionName + '">' +
        //                    '<input type="hidden" name="data[actions][list][' + controllerIndex + '][description]" value="' + actionDescript + '">' +
        //                    '<input type="hidden" name="data[actions][list][' + controllerIndex + '][status]" value="' + $("#SysControllerStatuses option:selected").val() + '">' +
        //                    '<button type="button" name="btnActionsDelete' + controllerIndex + '" class="btn btn-danger"> <i class="fa fa-minus"></i> <?php echo __('Delete'); ?> </button>' +
        //                    '</td>' +
        //                    '</tr>'
        //                    );
        //
        //            $("button[name='btnActionsDelete" + controllerIndex + "']").click(function (event) {
        //                totalIndex--;
        //                $(event.target).parent().parent().remove();
        //            });
        //
        //            ++controllerIndex;
        //            totalIndex++;
        //            //promotion_step++;
        //            $('#SysControllerActionName').val(""); //null
        //            $('#SysControllerDecription').val("");
        //            $("#btnSysControllerAdd").attr("disabled", true);
        //        });
        $("#SysActionName").keyup(function () {
            if (($(this).val() == "")) {
                $("#btnActionAdd").attr("disabled", true);
                $("#SysActionName").focus();
                return false;
            } else {
                $("#btnActionAdd").attr("disabled", false);
            }
        });

        //        $("#btnControllerAdd").click(function () {
        //            //onsole.log(totalIndex);
        //            if (totalIndex <= 0) { //เช็ค total 
        //                AppMessage("กรุณาระบบุ Actions ");
        //                //console.log("กรุณาระบบุ Actions ");
        //                return false;
        //            } else if ($("form").valid()) { //valid เช็ค error ตัวแดง
        //                $("#SysControllersTable tr:last").remove();
        //                $("#SysControllerAddForm").submit();
        //            }
        //
        //        });
    });
</script> 
