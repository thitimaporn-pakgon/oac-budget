<div class="sysController index">
    <?php echo $this->element('searchsyscon'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Controller (Result)')); ?>
        <div class="box-body">  
            <table class="table-short-information table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('id', __('Controller')); ?></th>
                        <th><?php echo $this->Paginator->sort('description', __('Description')); ?></th>
                        <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($sysControllers)): ?>
                        <?php foreach ($sysControllers as $k => $sysController): ?>
                            <tr>
                                <td class="nindex" ><?php echo @$this->Paginator->counter("{:start}") + $k; ?></td>
                                <td><?php echo h($sysController['SysController']['name']); ?></td>
                                <td><?php echo h($sysController['SysController']['description']); ?></td>
                                <td class="bool"><?php echo $this->Zicure->mainStatus($sysController['SysController']['status']); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($sysController['SysController']['id'], 'sysControllers', true, true, true); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5" class="text-center notfound"><?php echo __('No Data.'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#SearchControllerId").change(function () {
            changeDropDown("/Utils/findActionsByControllers/" + $(this).val(), "sysActions");
        });
    });
</script>
