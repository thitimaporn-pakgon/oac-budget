<div class="sysControllers form">
    <!-- Sys Controller add -->
    <div class="row">
        <div class="col-md-12">
         <div class="row">
        <div class="col-md-12 table-responsive">    
     <div class="box box-active">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo __('Add Controller');?></h3>
    </div>
    <div class="box-body">  
            <?php echo $this->Form->create('SysController', array('role' => 'form', 'url' => '/SysControllers/add')); ?>
            <div class="form-group">
                <?php echo $this->Form->input('name', array('class' => 'form-control required', 'placeholder' => __('Name'))); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('description', array('class' => 'form-control', 'placeholder' => __('Description'))); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('status', array('class' => 'form-control required', 'placeholder' => __('Status'), 'default' => 'A')); ?>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox" name="data[SysController][crude]" value="Y"> <label class="control-label" style="font-weight: bold;"><?php echo __('CRUDE'); ?></label>
                </div>
            </div>
            <div class="form-group">
                <?php echo $this->Permission->link_submit(__('Submit'), '/SysControllers/add', array('id' => 'btnControllerAdd', 'name' => 'btnControllerAdd', 'div' => false, 'label' => false, 'type' => 'submit')); ?> 
                <?php echo $this->Permission->button_back(); ?> 
                <?php echo $this->Form->end(); ?>
            </div>
        </div><!-- end col md 12 -->
    </div><!-- end row -->
       </div><!-- end col md 12 -->
    </div><!-- end row -->