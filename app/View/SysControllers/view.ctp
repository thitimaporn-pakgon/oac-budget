<div class="box box-active">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'View Controller')); ?>
    <div class="box-body">  
        <table cellpadding="0" cellspacing="0" class="table-form-view">
            <tr>
                <td><?php echo __('Name'); ?></td>
                <td><?php echo h($sysControllers['SysController']['name']); ?></td>
            </tr>
            <tr>
                <td><?php echo __('Description'); ?></td>
                <td><?php echo h($sysControllers['SysController']['description']); ?></td>
            </tr>
            <tr>
                <td><?php echo __('Status'); ?></td>
                <td><?php echo $this->Zicure->mainStatus($sysControllers['SysController']['status'], true); ?></td>
            </tr>
            <tr>
                <td><?php echo __('Created'); ?></td>
                <td><?php echo $this->Zicure->dateTimeISO($sysControllers['SysController']['created']); ?></td>
            </tr>
            <tr>
                <td><?php echo __('Modified'); ?></td>
                <td><?php echo $this->Zicure->dateTimeISO($sysControllers['SysController']['modified']); ?></td>
            </tr>
        </table>

        <br/>
        <div class="well well-success">
            <?php echo $this->Zicure->bootstrapLabel('Actions Details', 'warning'); ?>
        </div>

        <table class="table table-bordered table-striped table-add-details">
            <tr>
                <?php $index = 1; ?>
                <th class="nindex-black"><?php echo __('#'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Description'); ?></th>
                <th><?php echo __('Status'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
            </tr>
            <?php foreach ($sysActions as $k => $v): ?>
                <tr>
                    <td class="nindex-black"><?php echo $index; ?></td>
                    <td><?php echo h($v['SysAction']['name']); ?></td>
                    <td><?php echo h($v['SysAction']['description']); ?></td>
                    <td class="bool"><?php echo $this->Zicure->mainStatus($v['SysAction']['status'], true); ?></td>
                    <td><?php echo $this->Zicure->dateTimeISO($v['SysAction']['created']); ?></td>
                    <td><?php echo $this->Zicure->dateTimeISO($v['SysAction']['modified']); ?></td>
                </tr>
                <?php $index++; ?>
            <?php endforeach; ?>
        </table>
        <br/>
    </div>
    <?php echo $this->element('boxOptionFooter'); ?>
</div><!-- ./box.class.active -->
