
<div class="resultManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit Result Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('StrategyProduct', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'value' => $year, 'disabled' => true)); ?>
        <?php echo $this->Form->input('strategic', array('readonly' => 'readonly', 'label' => __('Strategic Name'), 'value' => $data_StrategyGov['StrategyGov']['strategy_gov_name'])); ?>
        <?php echo $this->Form->input('plan', array('readonly' => 'readonly', 'label' => __('Work Plan Name'), 'value' => $data_plan['StrategyPlan']['strategy_plan_name'])); ?>
        <?php echo $this->Form->input('strategy_product_name', array('class' => 'required', 'label' => __('Work Product Name'),'value'=>$data['StrategyProduct']['strategy_product_name'])); ?>
        <?php echo $this->Form->input('code', array('class' => 'required','value'=>$data['StrategyProduct']['code'])); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/ResultManeuvers/edit'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->