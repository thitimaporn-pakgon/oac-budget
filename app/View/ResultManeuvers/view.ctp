<?php
/**
*
* View page for resultManeuversController it show for ResultManeuver infomation.
* @author sarawutt.b 
* @since 2017-04-18 17:58:37
* @license Zicure Corp. 
*/
?>
<div class="resultManeuvers view div-view-information box box-warning">
    	<?php echo $this->element('boxOptionHeader', array('btitle'=>'Result Maneuver Information', 'bcollap'=>true, 'bclose'=>true));?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                		<tr>
			<td class="table-view-label"><?php echo __('Result Maneuver Id'); ?></td>
			<td class="table-view-detail"><?php echo h($resultManeuver['ResultManeuver']['id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Plan Maneuver'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($resultManeuver['PlanManeuver']['name'],'info'), "/plan_maneuvers/view/{$resultManeuver['PlanManeuver']['id']}" ,array('title'=>__('View') . ' ' . __('Plan Maneuver'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Code'); ?></td>
			<td class="table-view-detail"><?php echo h($resultManeuver['ResultManeuver']['code']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Result Maneuver Name'); ?></td>
			<td class="table-view-detail"><?php echo h($resultManeuver['ResultManeuver']['name']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Budget Year'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($resultManeuver['BudgetYear']['name'],'info'), "/budget_years/view/{$resultManeuver['BudgetYear']['id']}" ,array('title'=>__('View') . ' ' . __('Budget Year'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('From Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($resultManeuver['ResultManeuver']['from_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('To Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($resultManeuver['ResultManeuver']['to_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('System Has Process'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($resultManeuver['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$resultManeuver['SystemHasProcess']['id']}" ,array('title'=>__('View') . ' ' . __('System Has Process'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Create Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($resultManeuver['ResultManeuver']['create_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Update Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($resultManeuver['ResultManeuver']['update_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Created'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($resultManeuver['ResultManeuver']['created']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Modified'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($resultManeuver['ResultManeuver']['modified']); ?></td>
		</tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    	<?php echo $this->element('boxOptionFooter');?>
</div><!-- ./div.box box-warning -->


    <div class="box box-info box-related">
        	<?php echo $this->element('boxOptionHeader',array('btitle'=>'Event Maneuver Relate List', 'bcollap'=>true, 'bclose'=>true, 'btnnew'=>'EventManeuvers'));?>
        <div class="box-body">
            <table class="table-view-related-information table table-bodered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#');?></th>
                        		<th><?php echo __('EventManeuver Id'); ?></th>
		<th><?php echo __('Result Maneuver Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('EventManeuver Name'); ?></th>
		<th><?php echo __('Budget Year Id'); ?></th>
		<th><?php echo __('From Department Id'); ?></th>
		<th><?php echo __('To Department Id'); ?></th>
		<th><?php echo __('System Has Process Id'); ?></th>
                        		<th class="actions">&nbsp;</th>
                    </tr>
                </thead>

                <?php if(!empty($resultManeuver['EventManeuver'])):?>
	<?php foreach ($resultManeuver['EventManeuver'] as $k => $eventManeuver): ?>
		<tr>
			<td class="nindex-black"><?php echo ++$k;?></td>
			<td><?php echo $eventManeuver['id']; ?></td>
			<td><?php echo $eventManeuver['result_maneuver_id']; ?></td>
			<td><?php echo $eventManeuver['code']; ?></td>
			<td><?php echo $eventManeuver['name']; ?></td>
			<td><?php echo $eventManeuver['budget_year_id']; ?></td>
			<td><?php echo $eventManeuver['from_department_id']; ?></td>
			<td><?php echo $eventManeuver['to_department_id']; ?></td>
			<td><?php echo $eventManeuver['system_has_process_id']; ?></td>
			<td class="actions"><?php echo $this->Permission->getActions($eventManeuver['id'], 'EventManeuvers'); ?></td>
		</tr>
	<?php endforeach; ?>
<?php else:?>
		<tr class="row-notfound">
			<td colspan="10"><?php echo __('Information Not Found');?></td>
		</tr>
<?php endif;?>
            </table>
        </div><!-- ./div.box-body -->
        	<?php echo $this->element('boxOptionFooter');?>
    </div><!-- /div.box box-info -->
    