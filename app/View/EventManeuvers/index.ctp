<?php
/**
*
* Index page for eventManeuversController it show data list for EventManeuver infomation.
* @author sarawutt.b 
* @since 2017-04-18 17:49:39
* @license Zicure Corp. 
*/
?>
<div class="eventManeuvers index zicure-index">
    <!-- zicure-box-find search input params section -->
        <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle'=>'Event Maneuver Management System', 'bcollap'=>true, 'bclose'=>true));?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal'));?>
            <?php echo $this->Form->input('name', array('label'=>array('text'=>__('Event Maneuver Name'))));?>
            <?php echo $this->Form->input('status', array('type'=>'select', 'class'=>'select2 chosen', 'options'=>$this->Zicure->mainStatus()));?>
            <?php //echo $this->Form->input('dateFrom', array('type' => 'text', 'class' => 'datepicker datepicker-start')); ?>
            <?php //echo $this->Form->input('dateTo', array('type' => 'text', 'class' => 'datepicker datepicker-end')); ?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '/EventManeuvers/index', array('name'=>'btnSubmitSearch'));?>
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', 'Add Event Maneuver'), '/EventManeuvers/add', array('name'=>'btnAddnew','class'=>'btn bg-olive'));?>
            <?php echo $this->Permission->buttonBack();?>
            <?php echo $this->Form->end();?>
        </div><!-- /div.box-footer -->
    </div><!-- /div.zicure-box-find box box-warning -->

    <!-- zicure-box-result search result section -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle'=>'Event Maneuver (Result)', 'bcollap'=>true, 'bclose'=>true));?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#');?></th>
                                                                                <th><?php echo $this->Paginator->sort('result_maneuver_id');?></th>
                                                    <th><?php echo $this->Paginator->sort('code');?></th>
                                                    <th><?php echo $this->Paginator->sort('name', __('Event Maneuver name'));?></th>
                                                    <th><?php echo $this->Paginator->sort('budget_year_id');?></th>
                                                    <th><?php echo $this->Paginator->sort('from_department_id');?></th>
                                                    <th><?php echo $this->Paginator->sort('to_department_id');?></th>
                                                    <th><?php echo $this->Paginator->sort('system_has_process_id');?></th>
                                                                                                            <th><?php echo $this->Paginator->sort('created');?></th>
                                                    <th><?php echo $this->Paginator->sort('modified');?></th>
                                                <th class="actions">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($eventManeuvers)):?>
	<?php foreach ($eventManeuvers as $k => $eventManeuver): ?>
	<tr>
		<td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
		<td><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['ResultManeuver']['name'],'info'), "/result_maneuvers/view/{$eventManeuver['ResultManeuver']['id']}"); ?></td>
		<td><?php echo h($eventManeuver['EventManeuver']['code']); ?></td>
		<td><?php echo h($eventManeuver['EventManeuver']['name']); ?></td>
		<td><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['BudgetYear']['name'],'info'), "/budget_years/view/{$eventManeuver['BudgetYear']['id']}"); ?></td>
		<td><?php echo h($eventManeuver['EventManeuver']['from_department_id']); ?></td>
		<td><?php echo h($eventManeuver['EventManeuver']['to_department_id']); ?></td>
		<td><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$eventManeuver['SystemHasProcess']['id']}"); ?></td>
		<td><?php echo $this->Zicure->dateISO($eventManeuver['EventManeuver']['created']); ?></td>
		<td><?php echo $this->Zicure->dateISO($eventManeuver['EventManeuver']['modified']); ?></td>
		<td class="actions"><?php echo $this->Permission->getActions($eventManeuver['EventManeuver']['id'],'EventManeuvers'); ?></td>
	</tr>
	<?php endforeach; ?>
<?php else:?>
	<tr class="row-notfound">
			<td colspan="11"><?php echo __('Information Not Found');?></td>
		</tr>
<?php endif;?>
                </tbody>
            </table>
            <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options'=> true));?>
        </div>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->