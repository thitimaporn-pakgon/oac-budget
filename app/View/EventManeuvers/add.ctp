<?php
/**
 *
 * Add page for eventManeuversController it Add of EventManeuver.
 * @author sarawutt.b 
 * @since 2017-04-18 17:49:39
 * @license Zicure Corp. 
 */
?>
<div class="eventManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add Event Maneuver', 'bcollap' => true, 'bclose' => true)); ?>
    <?php echo $this->Form->create('StrategyActivity', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('budget_year_id', array('class' => 'required', 'options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'value' => $year, 'disabled' => true)); ?>
        <?php echo $this->Form->input('strategic', array('readonly' => 'readonly', 'label' => __('Strategic Name'), 'value' => $data_strategy_gov_name)); ?>
        <?php echo $this->Form->input('plan', array('readonly' => 'readonly', 'label' => __('Work Plan Name'), 'value' => $data_strategy_plan_name)); ?>
        <?php echo $this->Form->input('result', array('readonly' => 'readonly', 'label' => __('Work Product Name'), 'value' => $data_strategy_product_name)); ?>
        <?php echo $this->Form->input('strategy_act_name', array('class' => 'required', 'label' => __('Event Name'))); ?>
        <?php echo $this->Form->input('code', array('class' => 'required')); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/EventManeuvers/add'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
