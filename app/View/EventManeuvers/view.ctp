<?php
/**
*
* View page for eventManeuversController it show for EventManeuver infomation.
* @author sarawutt.b 
* @since 2017-04-18 17:49:39
* @license Zicure Corp. 
*/
?>
<div class="eventManeuvers view div-view-information box box-warning">
    	<?php echo $this->element('boxOptionHeader', array('btitle'=>'Event Maneuver Information', 'bcollap'=>true, 'bclose'=>true));?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                		<tr>
			<td class="table-view-label"><?php echo __('Event Maneuver Id'); ?></td>
			<td class="table-view-detail"><?php echo h($eventManeuver['EventManeuver']['id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Result Maneuver'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['ResultManeuver']['name'],'info'), "/result_maneuvers/view/{$eventManeuver['ResultManeuver']['id']}" ,array('title'=>__('View') . ' ' . __('Result Maneuver'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Code'); ?></td>
			<td class="table-view-detail"><?php echo h($eventManeuver['EventManeuver']['code']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Event Maneuver Name'); ?></td>
			<td class="table-view-detail"><?php echo h($eventManeuver['EventManeuver']['name']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Budget Year'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['BudgetYear']['name'],'info'), "/budget_years/view/{$eventManeuver['BudgetYear']['id']}" ,array('title'=>__('View') . ' ' . __('Budget Year'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('From Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($eventManeuver['EventManeuver']['from_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('To Department Id'); ?></td>
			<td class="table-view-detail"><?php echo h($eventManeuver['EventManeuver']['to_department_id']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('System Has Process'); ?></td>
			<td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($eventManeuver['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$eventManeuver['SystemHasProcess']['id']}" ,array('title'=>__('View') . ' ' . __('System Has Process'))); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Create Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($eventManeuver['EventManeuver']['create_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Update Uid'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->getUserById($eventManeuver['EventManeuver']['update_uid']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Created'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($eventManeuver['EventManeuver']['created']); ?></td>
		</tr>
		<tr>
			<td class="table-view-label"><?php echo __('Modified'); ?></td>
			<td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($eventManeuver['EventManeuver']['modified']); ?></td>
		</tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    	<?php echo $this->element('boxOptionFooter');?>
</div><!-- ./div.box box-warning -->


    <div class="box box-info box-related">
        	<?php echo $this->element('boxOptionHeader',array('btitle'=>'Work Group Maneuver Relate List', 'bcollap'=>true, 'bclose'=>true, 'btnnew'=>'WorkGroupManeuvers'));?>
        <div class="box-body">
            <table class="table-view-related-information table table-bodered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black"><?php echo __('#');?></th>
                        		<th><?php echo __('WorkGroupManeuver Id'); ?></th>
		<th><?php echo __('Event Maneuver Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('WorkGroupManeuver Name'); ?></th>
		<th><?php echo __('Budget Year Id'); ?></th>
		<th><?php echo __('From Department Id'); ?></th>
		<th><?php echo __('To Department Id'); ?></th>
		<th><?php echo __('System Has Process Id'); ?></th>
		<th><?php echo __('Budget Scope'); ?></th>
                        		<th class="actions">&nbsp;</th>
                    </tr>
                </thead>

                <?php if(!empty($eventManeuver['WorkGroupManeuver'])):?>
	<?php foreach ($eventManeuver['WorkGroupManeuver'] as $k => $workGroupManeuver): ?>
		<tr>
			<td class="nindex-black"><?php echo ++$k;?></td>
			<td><?php echo $workGroupManeuver['id']; ?></td>
			<td><?php echo $workGroupManeuver['event_maneuver_id']; ?></td>
			<td><?php echo $workGroupManeuver['code']; ?></td>
			<td><?php echo $workGroupManeuver['name']; ?></td>
			<td><?php echo $workGroupManeuver['budget_year_id']; ?></td>
			<td><?php echo $workGroupManeuver['from_department_id']; ?></td>
			<td><?php echo $workGroupManeuver['to_department_id']; ?></td>
			<td><?php echo $workGroupManeuver['system_has_process_id']; ?></td>
			<td><?php echo $workGroupManeuver['budget_scope']; ?></td>
			<td class="actions"><?php echo $this->Permission->getActions($workGroupManeuver['id'], 'WorkGroupManeuvers'); ?></td>
		</tr>
	<?php endforeach; ?>
<?php else:?>
		<tr class="row-notfound">
			<td colspan="11"><?php echo __('Information Not Found');?></td>
		</tr>
<?php endif;?>
            </table>
        </div><!-- ./div.box-body -->
        	<?php echo $this->element('boxOptionFooter');?>
    </div><!-- /div.box box-info -->
    