<?php

App::uses('Helper', 'View');

class ProjectHelper extends AppHelper {

    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap', 'Permission', 'Zicure');
    protected $_badge_style = array('inbox' => 'blue', 'pending' => 'yellow', 'wait' => 'yello', 'outbox' => 'aqua', 'aqua' => 'aqua', 'blue' => 'blue', 'info' => 'info', 'red' => 'red');

    /**
     * 
     * Function list withdrawal budget department level
     * @author  sarawutt.b
     * @param   type $systemHasProcessId if not exists return option list
     * @return  mix string name if matched parame otherwit return array options
     */
    public function displayBudgetWithdrawalDeparmentLevel($systemHasProcessId = null) {
        return ClassRegistry::init('ExpenseHeaderManage')->displayBudgetWithdrawalDeparmentLevel($systemHasProcessId);
    }

    /**
     * 
     * Function convert for the budget code LONG | SHORT
     * @author  sarawutt.b
     * @param   type $params as a array data in the database row
     * @return  string budget code
     */
    public function getProjectListName($projectlist_id) {
        return ClassRegistry::init('ExpenseHeaderAllocated')->getProjectListName($projectlist_id);
    }

    /**
     * 
     * Function convert for the budget code LONG | SHORT
     * @author  sarawutt.b
     * @param   type $params as a array data in the database row
     * @return  string budget code
     */
    public function convertBudgetCode($params = array()) {
        return ClassRegistry::init('ExpenseHeaderManeuver')->convertBudgetCode($params);
    }

    /**
     * 
     * Function git budget prefix code for the budget code LONG | SHORT
     * @author  sarawutt.b
     * @param   type $params as a array data in the database row
     * @return  string prefix budget code
     */
    public function budgetPrefixCode($params = array()) {
        return ClassRegistry::init('ExpenseHeaderManeuver')->budgetPrefixCode($params);
    }

    /**
     * 
     * Function print for payment each input
     * @author  sarawutt.b
     * @param   type $val as a numeric budget value
     * @return  string html tag
     * Deprecate
     */
    public function cellInputPaymentDetail($val = null) {
        return "<input class='payment-items form-control currenc' type='text' value='{$val}'>";
    }

    /**
     * 
     * Function print for payment each input
     * @author  sarawutt.b
     * @param   type $val as a numeric budget value
     * @return  string html tag
     * Deprecate
     */
    public function cellInputPaymentSummation($val = null) {
        return "<td><input class='payment-items form-control currenc' type='text' value='{$val}'></td>";
    }

    /**
     *
     * Get dinamemic parent menu or top of the menu
     * @author sarawutt.b
     * @param type $domain as string domain name or IP address
     * @param type $port as integer of server port number
     * @param type $role_id as integer of the role ID
     * @param type $user_id as integer of the user ID
     * @return array()
     */
    public function getApproveDynamicAclMenu($domain = null, $port = '80', $role_id = 0, $user_id = 0) {
        $SERVER_NAME = $_SERVER['SERVER_NAME'];
        $SERVER_PORT = $_SERVER['SERVER_PORT'];
        $ROLE_ID = $this->getCurrenSessionRoleId();
        $USER_ID = $this->getCurrenSessionUserId();
        $_FULL_URL = 'http://' . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];
        $menuList = ClassRegistry::init('Menu')->getDynamicAclMenu($SERVER_NAME, $SERVER_PORT, $ROLE_ID, $USER_ID, 2);
        $name_field = 'name';
        $menuListHtml = "";
        foreach ($menuList as $k => $v) {
            if (strpos($v[0]['url'], '#') !== false) {
                //if multiple level menu must be checked
                $menuListHtml .= "<li class='treeview'>
                                            <a href='#'>
                                                <i class='{$v[0]['glyphicon']}'></i>
                                                <span>{$v[0][$name_field]}</span>
                                                <i class='fa fa-angle-left pull-right'></i>
                                            </a><ul class='treeview-menu'>";
                $childMenuLists = $this->getDynamicChildMenu($SERVER_NAME, $SERVER_PORT, $ROLE_ID, $USER_ID, $v[0]['id']);
                foreach ($childMenuLists as $kk => $vv) {
                    $badge = null;
                    if (!empty($vv[0]['badge'])) {
                        $badge_count = $this->badgeFinder($vv[0]['badge']);
                        $badgeAction = null;
                        if (strpos($vv[0]['url'], 'inbox') !== false) {
                            $badgeAction = 'inbox';
                        } elseif (strpos($vv[0]['url'], 'outbox') !== false) {
                            $badgeAction = 'outbox';
                        } elseif ((strpos($vv[0]['url'], 'pending') !== false) || (strpos($vv[0]['url'], 'wait') !== false)) {
                            $badgeAction = 'pending';
                        }
                        $badgeClass = array_key_exists($badgeAction, $this->_badge_style) ? $this->_badge_style[$badgeAction] : 'info';
                        $badge = "<span class='pull-right badge bg-{$badgeClass}'>{$badge_count}</span>";
                    }
                    $menuListHtml .= "<li><a href='{$vv[0]['url']}'><i class='{$vv[0]['glyphicon']}'></i> {$vv[0][$name_field]}{$badge}</a></li>";
                }
                $menuListHtml .= "</ul></li>";
            } elseif ((strpos($v[0]['url'], 'http://') !== false) || (strpos($v[0]['url'], 'https://') !== false)) {
                $menuListHtml .= "<li><a href='{$v[0]['url']}' target='_blank'><i class='{$v[0]['glyphicon']}'></i> <span>{$v[0][$name_field]}</span></a></li>";
            } else {
                $menuListHtml .= "<li><a href='{$_FULL_URL}{$v[0]['url']}'><i class='{$v[0]['glyphicon']}'></i> <span>{$v[0][$name_field]}</span></a></li>";
            }
        }

        return $menuListHtml;
    }

    /**
     *
     * Find for dinamic choldrent manu
     * @author sarawutt.b
     * @param type $domain as string domain name or IP address
     * @param type $port as integer of server port number
     * @param type $role_id as integer of the role ID
     * @param type $user_id as integer of the user ID
     * @param type $menu_parent_id as integer of menu parent ID
     * @return array()
     */
    public function getDynamicChildMenu($domain = null, $port = '80', $role_id = 0, $user_id = 0, $menu_parent_id = -1) {
        return ClassRegistry::init('Menu')->getDynamicChildMenu($domain, $port, $role_id, $user_id, $menu_parent_id);
    }

    /**
     *
     * Finf dinamic count for badge on the menu left side
     * @author  sarawutt.b
     * @param   string $sql of query excutetion
     * @param   string $statusList of project and plan status list
     * @param   character $badgeType of badge type T = Project and Plan
     * @return  integer of badge count
     */
    public function badgeFinder($sql = null, $statusList = null) {
        return ClassRegistry::init('Menu')->badgeFinder($sql, $statusList);
    }

    /**
     *
     * List WorkGroupAllocated
     * @author Thawatchai.T
     * @since 2017-04-20
     * @param id as a integer of WorkGroupAllocated Id
     * @return array list of WorkGroupAllocated
     */
    public function findListWorkGroupAllocated($id = null) {
        return ClassRegistry::init('WorkGroupAllocated')->findListWorkGroupAllocated($id);
    }

    /**
     *
     * List WorkAllocated
     * @author Thawatchai.T
     * @since 2017-04-20
     * @param id as a integer of WorkAllocated Id
     * @return array list of WorkAllocated
     */
    public function findListWorkAllocated($id = null) {
        return ClassRegistry::init('WorkAllocated')->findListWorkAllocated($id);
    }

    /**
     *
     * List ProjectAllocated
     * @author Thawatchai.T
     * @since 2017-04-20
     * @param id as a integer of ProjectAllocated Id
     * @return array list of ProjectAllocated
     */
    public function findListProjectAllocated($id = null) {
        return ClassRegistry::init('ProjectAllocated')->findListProjectAllocated($id);
    }

    /**
     *
     * List ProjectListAllocated
     * @author Thawatchai.T
     * @since 2017-04-20
     * @param id as a integer of ProjectListAllocated Id
     * @return array list of ProjectListAllocated
     */
    public function findListProjectListAllocated($id = null) {
        return ClassRegistry::init('ProjectListAllocated')->findListProjectListAllocated($id);
    }

    /**
     *
     * List WorkGroupManage
     * @author Thawatchai.T
     * @since 2017-06-02
     * @param id as a integer of WorkGroupManage Id
     * @return array list of WorkGroupManage
     */
    public function findListWorkGroupManage($id = null) {
        return ClassRegistry::init('WorkGroupManage')->findListWorkGroupManage($id);
    }

    /**
     *
     * List WorkManage
     * @author Thawatchai.T
     * @since 2017-06-02
     * @param id as a integer of WorkManage Id
     * @return array list of WorkManage
     */
    public function findListWorkManage($id = null) {
        return ClassRegistry::init('WorkManage')->findListWorkManage($id);
    }

    /**
     *
     * List ProjectManage
     * @author Thawatchai.T
     * @since 2017-06-02
     * @param id as a integer of ProjectManage Id
     * @return array list of ProjectManage
     */
    public function findListProjectManage($id = null) {
        return ClassRegistry::init('ProjectManage')->findListProjectManage($id);
    }

    /**
     *
     * List ProjectListManage
     * @author Thawatchai.T
     * @since 2017-06-02
     * @param id as a integer of ProjectListManage Id
     * @return array list of ProjectListManage
     */
    public function findListProjectListManage($id = null) {
        return ClassRegistry::init('ProjectListManage')->findListProjectListManage($id);
    }

    /**
     *
     * Function generate budget code use for generate budget code.
     * @author  thawatchai.t
     * @param $expenseHeaderId, $part
     * @return array code
     */
    public function generateBudgetCode($expenseHeaderId = null, $part = 'A') {
        return ClassRegistry::init('ExpenseHeaderManeuver')->generateBudgetCode($expenseHeaderId, $part);
    }

    public function generateBudgetCodeAllocated($expenseHeaderId = null, $part = 'A') {
        return ClassRegistry::init('ExpenseHeaderAllocated')->generateBudgetCode($expenseHeaderId, $part);
    }

    /**
     *
     * insertExpenseHeader.
     * @author  thawatchai.t
     * @param $expenseHeaderId, $part
     * @return array code
     *
     */
    public function insertExpenseHeader($expenseHeaderId = null, $part = 'A') {
        return ClassRegistry::init('ExpenseHeaderManeuver')->insertExpenseHeader($expenseHeaderId, $part);
    }

    public function link($title = null, $url, $options = array()) {
        return $this->Permission->link($title, $url, $options);
    }

    /**
     *
     * Function getAction make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $controller as string of link destination controller
     * @param   type $linkView as boolean of is show view link
     * @param   type $linkEdit as boolean of is show edit link
     * @param   type $linkDelete as boolean of is show delete link
     * @param   type $linkUnlock as boolean of is show unblock link
     * @return  string
     */
    public function getProjectActions($id, $controller, $linkView = true, $linkEdit = true, $linkDelete = true, $linkUnlock = false) {
        $list = "";
        $patern = '<li>%s</li>';
        $id = $this->Zicure->secureEncodeParam($id);
        if ($linkView === true) {
            $list .= sprintf($patern, $this->Permission->link('View', "/" . $controller . "/view/{$id}"));
        }
        if ($linkEdit === true) {
            $list .= sprintf($patern, $this->Permission->link('Edit', "/" . $controller . "/edit/{$id}"));
        }
        if ($linkDelete === true) {
            $list .= sprintf($patern, $this->Permission->postLink('Delete', "/" . $controller . "/delete/{$id}", array('class' => 'action-delete btnGroupMenu'), __('Are you sure you want to delete # %s?', $id)));
        }
        if ($linkUnlock === true) {
            $list .= sprintf($patern, $this->Permission->link('Unlock', "/" . $controller . "/unlock/{$id}"));
        }
        return $this->Permission->__getActionMenu($list);
    }

    /**
     *
     * Function  Get Actions Menu Inbox
     * @author Numpol.J
     * @param   type $id as a integer of inbox ID[PK] of the inbox id
     * @param   type $controller of workmaneuvers
     * @param   type $linkView
     * @param   type $linkEdit
     * @param   type $linkDelete
     * @param   type $linkUnlock
     * @return type
     */
    public function getActionsMenuInbox($id, $controller, $linkView = false, $linkEdit = false, $linkDelete = false, $linkUnlock = false, $linkAdd = true) {
        $list = "";
        $patern = '<li>%s</li>';
        $id = $this->Zicure->secureEncodeParam($id);

        if ($linkView === true) {
            $list .= sprintf($patern, $this->Permission->link('View', "/" . $controller . "/viewInbox/{$id}"));
        }
        if ($linkEdit === true) {
            $list .= sprintf($patern, $this->Permission->link('Edit', "/" . $controller . "/editInbox/{$id}"));
        }
        if ($linkDelete === true) {
            $list .= sprintf($patern, $this->Permission->postLink('Delete', "/" . $controller . "/deleteInbox/{$id}", array('class' => 'action-delete btnGroupMenu'), __('Are you sure you want to delete # %s?', $id)));
        }
        if ($linkUnlock === true) {
            $list .= sprintf($patern, $this->Permission->link('Unlock', "/" . $controller . "/unlock/{$id}"));
        }
        if ($linkAdd === true) {
            $list .= sprintf($patern, $this->Permission->link('Add Work Maneuver', "/" . $controller . "/workManeuverList/{$id}"));
        }
        return $this->Permission->__getActionMenu($list);
    }

    /**
     *
     * Function getting for inbox status
     * @author  Numpol.J
     * @param   type $key as string capital for inbox status
     * @param   type return inbox status
     * @return  string
     */
    public function getInboxStatus($key = 'xxx') {
        return ClassRegistry::init('Inbox')->getInboxStatus($key);
    }

    /**
     *
     * Function getting for inbox status
     * @author  Numpol.J
     * @param   type $key as string capital for inbox status
     * @param   type return inbox status
     * @return  string
     */
    public function getInboxStatusUp($key = 'xxx') {
        return ClassRegistry::init('Inbox')->getInboxStatusUp($key);
    }

    /**
     *
     * Function getting for inbox process
     * @author  Numpol.J
     * @param   type $key as string capital for inbox status
     * @param   type return inbox process
     * @return  string
     */
    public function getInboxProcess($key = 'xxx') {
        return ClassRegistry::init('Inbox')->getInboxProcess($key);
    }

    /**
     *
     * Function getting for inbox task flag
     * @author  Numpol.J
     * @param   type $key as string capital for inbox task flag
     * @param   type return inbox task flag
     * @return  string
     */
    public function getInboxTaskFlag($key = 'xxx') {
        return ClassRegistry::init('Inbox')->getInboxTaskFlag($key);
    }

    public function findListStrategicManeuver($id = null) {
        return ClassRegistry::init('StrategicManeuver')->findListStrategicManeuver($id);
    }

    public function findListPlanManeuver($id = null) {
        return ClassRegistry::init('PlanManeuver')->findListPlanManeuver($id);
    }

    public function findListResultManeuver($id = null) {
        return ClassRegistry::init('ResultManeuver')->findListResultManeuver($id);
    }

    public function findListEventManeuver($id = null) {
        return ClassRegistry::init('EventManeuver')->findListEventManeuver($id);
    }

    public function findListWorkGroupManeuver($id = null) {
        return ClassRegistry::init('WorkGroupManeuver')->findListWorkGroupManeuver($id);
    }

    public function findListWorkManeuver($id = null) {
        return ClassRegistry::init('WorkManeuver')->findListWorkManeuver($id);
    }

    public function findListProjectManeuver($id = null) {
        return ClassRegistry::init('ProjectManeuver')->findListProjectManeuver($id);
    }

    public function findChildExpenseList($parentId = null) {
        return ClassRegistry::init('ExpenseList')->findChildExpenseList($parentId);
    }

    public function findChild3ExpenseList($parentId = null) {
        return ClassRegistry::init('ExpenseList')->findChild3ExpenseList($parentId);
    }

    /**
     *
     * Function make budget list parent html5 option daat params
     * @author sarawutt.b
     * @param type $section
     * @param type $dataId
     * @return string
     */
    public function optionBudgetParent($section = '', $dataId = -1) {
        return sprintf('data-is-parent="true" data-id="%s" data-section="%s"', $dataId, $section);
    }

    /**
     *
     * Function make budget list child html5 option data params
     * @author sarawutt.b
     * @param type $section
     * @param type $dataId
     * @param type $dataParentId
     * @return string
     */
    public function optionBudgetChild($section = '', $dataId = -1, $dataParentId = -1) {
        return sprintf('data-is-child="true" data-id="%s" data-parent-id="%s" data-section="%s"', $dataId, $dataParentId, $section);
    }

    /**
     *
     * Function make budget list child html5 option data params
     * @author sarawutt.b
     * @param type $section
     * @param type $dataId
     * @param type $dataParentId
     * @return string
     */
    public function optionBudgetChildL3($section = '', $dataId = -1, $dataParentId = -1, $dataGranParentId = -1) {
        return sprintf('data-is-child="true" data-id="%s" data-parent-id="%s" data-grand-parent-id="%s" data-level="3" data-section="%s"', $dataId, $dataParentId, $dataGranParentId, $section);
    }

    public function getBudgetCode($code = null) {
        return ClassRegistry::init('Common')->getBudgetCode($code);
    }

    /**
     *
     * @author  suphakid.s
     * @param type $budgetCode as code digit not budgetyear no
     * @param type $budgetYear as budgetyear code in 2 digit
     * @param type $process as work process ex: U = update , N = new
     * @return type array data
     */
    public function getDataExpenseBudget($budgetCode = '', $budgetYear = '', $process = 'U') {
        return ClassRegistry::init('ExpenseHeaderManeuver')->getDataExpenseBudget($budgetCode, $budgetYear, $process);
    }

    public function getDataExpenseBudgetAllocated($exp_head_id = '', $budgetYear = '', $process = 'U') {
        return ClassRegistry::init('ExpenseHeaderAllocated')->getDataExpenseBudget($exp_head_id, $budgetYear, $process);
    }

    /**
     *
     * Function get list all expend list
     * @author  Numpol.s
     * @return  array() list of expend list
     */
    public function findExpenseList() {
        return ClassRegistry::init('ExpenseList')->findExpenseList();
    }

    public function getSumBudgetIndex($budgetCode = '', $budgerYear = '') {
        return ClassRegistry::init('ExpenseHeaderManeuver')->getSumBudgetIndex($budgetCode, $budgerYear);
    }

    public function getCurrenBudgetYearTH() {
        return ClassRegistry::init('Utility')->getCurrenBudgetYearTH();
    }

    public function countCheckProcess($id = null, $model = '', $afterHasProcess = null) {
        return ClassRegistry::init('Common')->countCheckProcess($id, $model, $afterHasProcess);
    }

    /**
     * 
     * Function find list of dynamic of work group by budget year or departmentId
     * @author  Numpol.s
     * @param   type $budgetYear as integer of budget year id [PK] default current budget year id
     * @param   type $departmentId as integer of department id default get current session department id
     * @return  array List Of workGroup
     */
    public function getWorkGroupManeuver($budgetYear = null, $departmentId = null, $model = null, $fromTable = null, $joinTable = null) {
        return ClassRegistry::init('WorkGroupManeuver')->getWorkGroupManeuver($budgetYear, $departmentId, $model, $fromTable, $joinTable);
    }

    /**
     *
     * Function change array to string send to iReport 
     * @author Dosz
     * @param type $array
     * @return string
     */
}
