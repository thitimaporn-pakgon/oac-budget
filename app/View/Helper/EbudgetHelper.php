<?php

App::uses('Helper', 'View');

class EbudgetHelper extends AppHelper {
    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap');
    
    public function getDataTree($model = 'PlanManeuver',$id = null){
        return ClassRegistry::init($model)->getDataTree($id);
    }
    
}