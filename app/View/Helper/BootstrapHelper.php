<?php

/**
  | ------------------------------------------------------------------------------------------------------------------
  | Helper Permission Helper
  | @author  sarawutt.b
  | @since   2016/10/06 11:08:26
  | @license Zicure Corp
  |	------------------------------------------------------------------------------------------------------------------
  |
  |------------------------------------------------------------------------------------------------------------------------
 */
App::uses('AppHelper', 'View/Helper');

class BootstrapHelper extends AppHelper {

    public $helpers = array('Html', 'Form');

    /**
     * 
     * Function make bootstrap icon support Font Awesome and Gryph Icon
     * @author  sarawutt.b
     * @param   type $icon as string icon name class
     * @param   type $vendor as string of vendor name posible value fa|glyphicon
     * @param   type $wrapElement as string HTNL wrapping element
     * @param   type $message as a string message after the icon
     * @return  string of HTML wrapped icon
     */
    public function icon($icon = null, $message = null, $vendor = 'fa', $wrapElement = 'i') {
        return !is_null($icon) ? "<{$wrapElement} class=\"{$vendor} {$icon}\"></{$wrapElement}>" . __($message) : null;
    }

    /**
     * 
     * Function make for bootstrap label or badge
     * @author  sarawutt.b
     * @param   type $msg as a string of wrapped message
     * @param   type $class as a string bootstrap color class
     * @param   type $wrapElement as string HTNL wrapping element
     * @return  string of HTML wrapped label
     */
    public function badge($msg = null, $class = 'warning', $wrapElement = 'span') {
        return !is_null($msg) ? "<{$wrapElement} class=\"label label-{$class}\">" . __($msg) . "</{$wrapElement}>" : null;
    }

    /**
     * 
     * Function make for bootstrap label or badge
     * @author  sarawutt.b
     * @param   type $msg as a string of wrapped message
     * @param   type $class as a string bootstrap color class
     * @param   type $wrapElement as string HTNL wrapping element
     * @return  string of HTML wrapped label
     */
    public function label($msg = null, $class = 'warning', $wrapElement = 'div') {
        return $this->badge($msg, $class, $wrapElement);
    }

    public function callout($title = 'Bootstrap Collout Helper', $msg = 'Bootstrap Collout Helper', $class = 'info') {
        $petern = sprintf("<div class=\"bs-callout bs-callout-{$class}\"> 
                    <h4>%s</h4> 
                    <p>%s</p> 
                </div>", __($title), __($msg));
        return $petern;
    }

    public function modal($title = 'Bootstrap Modal Helper', $msg = 'Bootstrap Modal Helper', $class = 'lg', $options = array()) {
        $petern = sprintf("<div class=\"modal fade modal-immediately\" aria-hidden=\"false\" tabindex=\"-1\" role=\"dialog\">
                            <div class=\"modal-dialog modal-{$class}\" role=\"document\">
                              <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"%s\"><span aria-hidden=\"true\">&times;</span></button>
                                  <h4 class=\"modal-title\">%s</h4>
                                </div>
                                <div class=\"modal-body\">
                                  <p>%s</p>
                                </div>
                                <div class=\"modal-footer\">
                                  <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">%s</button>
                                  <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
                                </div>
                              </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          </div><!-- /.modal -->", __('Close'), __($title), __($msg), __('Close'));
        return $petern;
    }

    public function alert($title = 'Bootstrap Collout Helper', $msg = 'Bootstrap Collout Helper', $class = 'danger') {
        $petern = sprintf("<div class=\"alert alert-{$class} alert-dismissible fade in\" role=\"alert\"> 
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"%s\">
                                        <span aria-hidden=\"true\">×</span>
                                </button> 
                                <strong>%s</strong> %s 
                        </div>", __('Close'), __($title), __($msg));
        return $petern;
    }

    /**
     * 
     * Function Bootstrap generate main color class
     * @author  sarawutt.b
     * @param   type $index as integer of number class index
     * @return  string name color class
     */
    public function bootstrapBoxClass($index = null) {
        $bclass = array('warning', 'success', 'info', 'default', 'primary', 'danger');
        return array_key_exists($index, $bclass) ? $bclass[$index] : $bclass[$index % count($bclass)];
    }
    
    /**
     * 
     * Function css bootstrap clearfix
     * @author sarawutt.b
     * @return string
     */
    public function creafix() {
        return '<div class="clearfix"></div>';
    }

}

?>
