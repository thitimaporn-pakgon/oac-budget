<?php

/**
  | ------------------------------------------------------------------------------------------------------------------
  | Helper Display Helper
  | @author	 BU Retails
  | @since   2013/11/06 14:37:26
  | @license Pakgon Ltd ,Company
  |	------------------------------------------------------------------------------------------------------------------
  | 1.
  |------------------------------------------------------------------------------------------------------------------------
 */
App::uses('AppHelper', 'View/Helper');

class DisplayFormatHelper extends AppHelper {

    public $helpers = array('Html', 'Form', 'DataList');

    public function active_status($k = null) {
        $options = array('N' => __('Non'), 'I' => __('Inactive'), 'A' => __('Active'));
        $return = '';
        if (!is_null($k)) {
            if (array_key_exists($k, $options)) {
                $return = $options[$k];
            } elseif (empty($k)) {
                $return = __('Unknow');
            }
        } else {
            $return = $options;
        }
        return $return;
    }

    /**
     * 
     * List appoint time code  or display multi language
     * @param type $key as key of you want display
     * @author Sarawutt.b
     * @since  2014-03-01
     * @return array list of appoint time code option if you call without on $key or string with you send already params code
     */
    public function appoint_time_code($key = '') {
        $appoint_time_code = array('M' => __('Morning 8:00 AM'), 'A' => __('Afternoon 12:00 PM'), 'E' => __('Evening 16:00 PM'));
        //return empty($key) ? $appoint_time_code : $appoint_time_code[$key];
        return $appoint_time_code;
    }

    /**
     * 
     * List customer manual suspension from excel file status  or display multi language
     * @param   string $key as key of you want display
     * @author  Sarawutt.b
     * @since   2014.12.23
     * @return  array list of customer manual suspension statuse option if you call without on $key or string with you send already params code
     */
    public function customer_manual_suspension_status($key = '') {
        $result_status = array();
        $result_status = array('N' => __('New'), 'S' => __('Suspensioned'), 'R' => __('Reject Request'), 'P' => __('Re-activation'), 'E' => __('Error'), 'V' => __('Verify data'),);
        return array_key_exists($key, $result_status) ? $result_status[$key] : $result_status;
    }

    /**
     * 
     * List installation queue status  or display multi language
     * @param   string $key as key of you want display
     * @param   string $agent if not empty load list for agent
     * @author  Sarawutt.b
     * @since   2014-03-01
     * @return  array list of installation queue statuse option if you call without on $key or string with you send already params code
     */
    public function installation_queue_status($key = '', $agent = '') {
        $result_status = array();
        if (empty($agent)) {
            $result_status = array(
                'N' => __('New'),
                'A' => __('Appointed'),
                'K' => __('Acknowledge'),
                'T' => __('Activate'),
                'R' => __('AgentReject'),
                'C' => __('Installation Cancel'),
                'D' => __('Done'),
                'U' => __('Sevice Cancel'),
                'W' => __('Wait New Provision'),
                'P' => __('Cannot Close(Problem)'),
                'I' => __('Sevice Cancel'),
                'L' => __('Add/Remove Point'),
                'S' => __('Service Suspend'));
        } else {
            $result_status = array(
                'N' => __('New'),
                'A' => __('Appointed'),
                'K' => __('Acknowledge'),
                'T' => __('Activate'),
                'D' => __('Done'),
                'U' => __('Sevice Cancel'),
                'I' => __('Sevice Cancel'),
                'L' => __('Add/Remove Point'),
                'S' => __('Service Suspend'));
        }
        return empty($key) ? $result_status : @$result_status[$key];
    }

    public function installationQueueStatus($key = '0') {
        return ClassRegistry::init('InstallationQueue')->statusList($key);
    }

    public function systemList($key = 0) {
        return ClassRegistry::init('SystemWorkLog')->systemList($key);
    }

    public function statusList($key = 0) {
        return ClassRegistry::init('SystemWorkLog')->statusList($key);
    }

    public function stbStatus($code) {
        $code = strtoupper($code);
        $display = "";
        switch ($code) {
            case "A": $display = __('Stb Active');
                break;
            case "I": $display = __('Stb Inactive');
                break;
            case "N": $display = __('Stb Active');
                break;
            default : $display = __('Unknow');
        }
        return $display;
    }

    public function installationBuildingType($building_type) {
        $building_type = strtoupper($building_type);
        $sResult = "";
        switch ($building_type) {
            case 'HOME' : $sResult = __('HOME');
                break;
            case 'CONDO' : $sResult = __('CONDO');
                break;
            case 'HOTEL' : $sResult = __('HOTEL');
                break;
            case 'PUBBAR' : $sResult = __('PUBBAR');
                break;
            default : $sResult = __('Unknow');
                break;
        }
        return $sResult;
    }

    public function displayMdGllLevel($level) {
        $level = strtoupper($level);
        $sResult = "";
        switch ($level) {
            case 'M' : $sResult = __('Master Dealer');
                break;
            case 'G' : $sResult = __('GLL');
                break;
            default : $sResult = __('Unknow');
                break;
        }
        return $sResult;
    }

    public function paginate_counter() {
//      return array(
//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	);   
        return array(
            'format' => __('Total {:count} records')
        );
    }

    public function paginate_counter_adjust($i) {
//      return array(
//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//	);   
        return array(
            'format' => __('Total {:$i} records')
        );
    }

    public function subAgentContact($data) {
        $info = "";
        $info .= empty($data['contact_no1']) ? '' : $data['contact_no1'];
        $info .= empty($data['contact_no2']) ? '' : ' ,' . $data['contact_no2'];
        $info .= empty($data['contact_no3']) ? '' : ' ,' . $data['contact_no3'];
        return $this->check_empty($info);
    }

    public function working_time_selected($is_selected) {
        if ($is_selected) {
            return "Y";
        } else {
            return 'N';
        }
    }

    public function display_data_list($list_name, $key) {
        $data = $this->DataList->getList($list_name);
//        pr($data);
        if (isset($data[$key])) {
            return $data[$key];
        } else {
            return $this->unknown_key($key);
        }
    }

    public function unknown_key($key) {
        return __("Unknow") . "[$key]";
    }

    public function is_active($is_active) {
        return $this->display_data_list('status', $is_active);
    }

    public function sys_status($statas) {
        return $this->display_data_list('sys_status', $statas);
    }

    public function bool($bool) {
        if ($bool === TRUE || strtolower($bool) == 'y' || strtolower($bool) == 'yes') {
            $bool = __("Yes");
        } else {
            $bool = __("No");
        }
        return $bool;
    }

    public function reason_types($reason_types) {
        $type_list = $this->DataList->getList('reason_types');
        return @$type_list[$reason_types];
    }

    public function user_types($reason_types) {
        $type_list = $this->DataList->getList('user_types');
        return @$type_list[$reason_types];
    }

    public function name_thai($name = FALSE) {
        if (!empty($name['name_prefix'])) {
            $name_prefix = $name['name_prefix'] . " ";
        }
        if (!empty($name['first_name'])) {
            $first_name = $name['first_name'] . " ";
        }
        if (!empty($name['last_name'])) {
            $last_name = $name['last_name'];
        }
        if (empty($name_prefix) AND empty($first_name) AND empty($last_name)) {
            return "-";
        }
        return @$name_prefix . @$first_name . @$last_name;
    }

    public function timestamp_to_idate($timestamp = FALSE) {
        $timestamp = (($timestamp === FALSE) ? date('Y-m-d') : $timestamp );

        $date = substr($timestamp, 0, 10);
        @list($Y, $m, $d) = explode('-', $date, 3);
        $idate = (int) substr($Y, -2) . $m . $d;
        return $idate;
    }

    function moneyFormat($number, $thousands_sep = TRUE) {
        if (empty($number)) {
            $number = 0;
        }
        $number = round($number, 2);
        $number = (($thousands_sep) ? number_format($number, 2, ".", ",") : number_format($number, 2, ".", "") );
        return $number;
    }

    function qtyFormat($qty) {
        if (empty($qty)) {
            $qty = 0;
        }
        if ($qty - (int) $qty != 0) {
            $qty = round($qty, 3);
        } else {
            $qty = (int) $qty;
        }
        return $qty;
    }

    function percentFormat($number) {
        $number = round($number, 2);
        if ($number - (int) $number == 0) {
            $number = (int) $number;
        }
        return $number . "%";
    }

    function convertIdateToDate($idate) {
        if (!empty($idate)) {
            $idate = sprintf('%06d', $idate);

            $year = substr($idate, 0, 2);
            $month = substr($idate, 2, 2);
            $day = substr($idate, 4, 2);

            $date_format = "20$year-$month-$day";

            return $date_format;
        }
    }

    public function check_empty($data) {
        $trim_data = trim($data);
        return (empty($trim_data)) ? "-" : $data;
    }

    public function customer_status($status) {
        switch ($status) {
            case 'N':
                $status_name = __('Not verify');
                break;
            case 'V':
                $status_name = __('Verified');
                break;
            default:
                $status_name = __('Unknown');
                break;
        }
        return $status_name;
    }

//	public function sys_status($status){
//        switch ($status) {
//            case 'A':
//                $status_name = __('ใช้งาน'); break;
//            case 'N':
//                $status_name = __('ไม่ใช้งาน'); break;
//            case 'D':
//                $status_name = __('กำลังพัฒนาระบบ'); break;
//            default:
//                $status_name = __('ไม่ทราบสถานะ'); break;
//        }
//        return $status_name;
//    }


    public function list_provinces($region = "") {
        $options['fields'] = array('id', 'name');
        if (!empty($region)) {
            $options['conditions'] = array("id ILIKE '{$region}%'");
        }
        $province = ClassRegistry::init('Province')->find('list', $options);
        return $this->Form->input('province', array('div' => FALSE, 'label' => false, 'empty' => __('Please select province'), 'options' => $province));
    }

    public function list_amphurs($province = array()) {
        $options['fields'] = array('id', 'name');
        if (!empty($province)) {
            $options['conditions'] = array("id ILIKE '{$province}%'");
        }
        $amphur = ClassRegistry::init('Amphur')->find('list', $options);
        return $this->Form->input('amphur', array('div' => FALSE, 'label' => false, 'empty' => __('Please select amphur'), 'options' => $amphur));
    }

    public function list_tambons($amphur = array()) {
        $options['fields'] = array('id', 'name');
        if (!empty($amphur)) {
            $options['conditions'] = array("id ILIKE '{$amphur}%'");
        }
        $tambon = ClassRegistry::init('Tanbon')->find('list', $options);
        return $this->Form->input('tambon', array('div' => FALSE, 'label' => false, 'empty' => __('Please select tambon'), 'options' => $tambon));
    }

    public function addr_display($addr_no = "", $soi = "", $road = "", $moo = "", $tambon = "", $amphur = "", $province = "", $zipcode = "", $village = "", $building_name = "", $floor = "", $room = "") {
        $address = "";
        $address .= (empty($addr_no)) ? "" : "$addr_no ";
        $address .= (empty($village)) ? "" : __("Village Name") . $village . " ";
        $address .= (empty($building_name)) ? "" : __("Building Name") . $building_name . " ";
        $address .= (empty($floor)) ? "" : __("Floor") . $floor . " ";
        $address .= (empty($room)) ? "" : __("Room") . $room . " ";
        $address .= (empty($soi)) ? "" : __("Soi") . $soi . " ";
        $address .= (empty($road)) ? "" : __("Road") . $road . " ";
        $address .= (empty($moo)) ? "" : __("Moo") . $moo . " ";
        $address .= (empty($tambon)) ? "" : "$tambon ";
        $address .= (empty($amphur)) ? "" : "$amphur ";
        $address .= (empty($province)) ? "" : "$province ";
        $address .= (empty($zipcode)) ? "" : "$zipcode";
        $address = trim($address);
        return $address;
    }

    /**
     * Display format for agent adress
     * @param array of agent data keep on array()
     * @return string
     */
    public function addrDisplay($data = array()) {
        $data = (is_array($data)) ? $data : array();
        $addr_no = @$data['addr_no'];
        $soi = @$data['soi'];
        $road = @$data['road'];
        $moo = @$data['moo'];
        $tambon = @$data['tambon'];
        $amphur = @$data['amphur'];
        $province = @$data['province'];
        $zipcode = @$data['zipcode'];
        $village = array_key_exists('village', $data) ? $data['village'] : @$data['village_name'];
        $building_name = @$data['building_name'];
        $floor = @$data['floor'];
        $room = @$data['room'];
        $address = "";
        $address .= (empty($addr_no)) ? "" : "$addr_no ";
        $address .= (empty($village)) ? "" : __("Village Name") . $village . " ";
        $address .= (empty($building_name)) ? "" : __("Building Name") . $building_name . " ";
        $address .= (empty($floor)) ? "" : __("Floor") . $floor . " ";
        $address .= (empty($room)) ? "" : __("Room") . $room . " ";
        $address .= (empty($soi)) ? "" : __("Soi") . $soi . " ";
        $address .= (empty($road)) ? "" : __("Road") . $road . " ";
        $address .= (empty($moo)) ? "" : __("Moo") . $moo . " ";
        $address .= (empty($tambon)) ? "" : "$tambon ";
        $address .= (empty($amphur)) ? "" : "$amphur ";
        $address .= (empty($province)) ? "" : "$province ";
        $address .= (empty($zipcode)) ? "" : "$zipcode";
        $address = trim($address);
        return $this->check_empty($address);
    }

    /**
     * Display format for agent adress
     * @param array of agent data keep on array()
     * @return string
     */
    public function contactAgentSubDisplay($data = array()) {
        $data = (is_array($data)) ? $data : array();
        $contact_person = $data['contact_person'];
        $contact_no1 = $data['contact_no1'];
        $contact_no2 = $data['contact_no2'];
        $contact_no3 = $data['contact_no3'];
        $email = $data['email'];
        $infomation = "";
        $infomation .= empty($contact_person) ? "" : "คุณ    {$contact_person} ";
        $infomation .= empty($contact_no1) ? "" : "เบอร์ติดต่อ : {$contact_no1} ";
        $infomation .= empty($contact_no2) ? "" : ",{$contact_no2} ";
        $infomation .= empty($contact_no3) ? "" : ",{$contact_no3} ";
        $infomation = trim($infomation);
        return $this->check_empty($infomation);
    }

    public function fullNameDisplay($data = array()) {
        $name_prefix = @$data['name_prefix'];
        $first_name = @$data['first_name'];
        $last_name = @$data['last_name'];

        $full_name = "";
        $full_name .= (empty($name_prefix)) ? "" : "$name_prefix ";
        $full_name .= (empty($first_name)) ? "" : "$first_name ";
        $full_name .= (empty($last_name)) ? "" : "$last_name";
        $full_name = trim($full_name);
        return $this->check_empty($full_name);
    }

    public function fullNameEngDisplay($data = array()) {
        $full_name_eng = "";
        $first_name_eng = @$data['first_name_eng'];
        $last_name_eng = @$data['last_name_eng'];

        $full_name_eng .= (empty($first_name_eng)) ? "" : "$first_name_eng ";
        $full_name_eng .= (empty($full_name_eng)) ? "" : "$full_name_eng ";

        return $this->check_empty($full_name_eng);
    }

    public function requestFromDisplay($flag) {
        $name = "";
        switch ($flag) {
            case 'C':$name = __('Call Center');
                break;
            case 'L':$name = __('Agent');
                break;
            case 'E':$name = __('Event');
                break;
            case 'V':$name = __('VIP');
                break;
            case 'G':$name = __('GLL');
                break;
            case 'M':$name = __('MD');
                break;
            default:$name = __('Unknown');
        }
        return $name;
    }

    public function queueStatusDisplay($status) {

        // N=New , B=BuildingCase , L=LCOCase , S=Assigned , A=Accepted , 
        // Z=OneToZero , R=RejectByLSO , C=Cancel , D=Done(Closed) ,M=MD
        switch ($status) {
            case 'N': return __("ยังไม่นัดวัน");
            case 'M': return __('รอแจกงาน GLL');
            case 'B': return __("งานSale");
            case 'L': return __("งานติดตั้งเคเบิล");
            case 'S': return __("งานที่ยังไม่ตอบรับ");
            case 'A': return __("งานที่ตอบรับแล้ว");
            case 'Z': return __("หาLSOไม่ได้");
            case 'R': return __("ถูกปฏิเสธ");
            case 'C': return __("งานที่ถูกยกเลิก");
            case 'D': return __("งานที่ปิดแล้ว");
            default: return "-";
        }
    }

    public function queueAssignStatusList() {
        return array(
            'N' => __('ยังไม่นัดวัน'),
            'S' => __('งานที่ยังไม่ตอบรับ'),
            'A' => __('งานที่ตอบรับแล้ว'),
            'R' => __('ตัวแทนติดตั้งรับงานแล้ว'),
            'B' => __('งานSale'),
            'L' => __('งานติดตั้งเคเบิล'),
            'R' => __('ถูกปฏิเสธ'),
            'Z' => __('หาLSOไม่ได้'),
        );
    }

    public function installationQueueOutboundStatusList() {
        return array(
            'N' => __('ยังไม่นัดวัน'),
            'A' => __('งานที่ตอบรับแล้ว'),
            'R' => __('ถูกปฏิเสธ')
        );
    }

    public function date_iso($date) { // Y-m-d
        $date = empty($date) ? '0000-00-00 00:00:00' : $date;
        if (strlen($date) <= 6) {
            $date = sprintf("%06d", $date);
            $date_iso = "20" . substr($date, 0, 2) . "-" . substr($date, 2, 2) . "-" . substr($date, 4, 2);
        } elseif (strlen($date) == 8) {
            $date_iso = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
        } else {
            $date_iso = substr($date, 0, 10);
        }
        return $date_iso;
    }

    public function datetime_iso($datetime) { // Y-m-d H:i:s
        $datetime = empty($datetime) ? '0000-00-00 00:00:00' : $datetime;
        if (is_null($datetime) or ( empty($datetime)))
            return "";
        if (strlen($datetime) <= 6) {
            $datetime = sprintf("%06d", $datetime);
            $datetime_iso = "20" . substr($datetime, 0, 2) . "-" . substr($datetime, 2, 2) . "-" . substr($datetime, 4, 2) . " 00:00:00";
        } elseif (strlen($datetime) == 8) {
            $datetime_iso = substr($datetime, 0, 4) . "-" . substr($datetime, 4, 2) . "-" . substr($datetime, 6, 2) . " 00:00:00";
        } else {
            $datetime_iso = substr($datetime, 0, 19);
        }
        return $datetime_iso;
    }

    public function DateDiff($strDate1, $strDate2) {
        return (int) ((strtotime($strDate2) - strtotime($strDate1)) / ( 60 * 60 * 24 ));  // 1 day = 60*60*24
    }

    public function periodTime($period_code) {
        switch ($period_code) {
            case 'M':
                $status_name = __('Morning');
                break;
            case 'A':
                $status_name = __('Afternoon');
                break;
            case 'E':
                $status_name = __('Evening');
                break;
            case 'S':
                $status_name = __('Extra Time');
                break;
            case 'U':
                $status_name = __('Unspecified Time');
                break;
            default:
                $status_name = $period_code;
                break;
        }
        return $status_name;
    }

    public function periodTimeShort($period_code) {
        switch ($period_code) {
            case 'M':
                $status_name = __('Morning');
                break;
            case 'A':
                $status_name = __('Afternoon');
                break;
            case 'E':
                $status_name = __('Evening');
                break;
            case 'S':
                $status_name = __('Extra Time');
                break;
            case 'U':
                $status_name = __('Unspecified Time');
                break;
            default:
                $status_name = $period_code;
                break;
        }
        return $status_name;
    }

    public function periodTimeList() {
        return array('M' => __('Morning'), 'A' => __('Afternoon'), 'E' => __('Evening'));
    }

    function appointDateThaiDisplay($idate, $period_code = "") {
        if (empty($idate)) {
            return __("No Date");
        }

        $Y = (strlen($idate) == 5) ? substr($idate, 0, 1) : (int) substr($idate, 0, 2);
        $Y = $Y + 2000;
        $m = substr($idate, -4, 2);
        $d = substr($idate, -2);

        $full_date = date("D-Y-m-d", @mktime(0, 0, 0, $m, $d, $Y));
        list($day, $y, $m, $d) = explode("-", $full_date);
        $day = $this->thaiDay($day);
        $m = $this->thaiMonth($m);
        $d = (int) $d;
        //$y = $y + 543;
        //echo $_SESSION['SessionLanguage'];exit;
        if ($_SESSION['SessionLanguage'] == 'tha') {
            $y = $y + 543;
            if (!empty($period_code)) {
                $time = $this->periodTime($period_code);
                $result = "{$day}ที่ $d $m พศ. $y $time";
            } else {
                $result = "{$day}ที่ $d $m พศ. $y";
            }
        } else {
            if (!empty($period_code)) {
                $time = $this->periodTime($period_code);
                $result = "{$day} $d $m  $y $time";
            } else {
                $result = "{$day} $d $m  $y";
            }
        }


        return $result;
    }

    function appointDateThaiShortDisplay($idate, $period_code = "") {
        if (empty($idate)) {
            return "Unknown date format";
        }

        $Y = (strlen($idate) == 5) ? substr($idate, 0, 1) : (int) substr($idate, 0, 2);
        $Y = $Y + 2000;
        $m = substr($idate, -4, 2);
        $d = substr($idate, -2);

        $full_date = date("D-Y-m-d", @mktime(0, 0, 0, $m, $d, $Y));
        list($day, $y, $m, $d) = explode("-", $full_date);
        $day = $this->thaiDay($day);
        $m = $this->thaiMonthShort($m);
        $d = (int) $d;
        $y = $y + 543;
        $y = substr($y, -2);
        if (!empty($period_code)) {
            $time = $this->periodTimeShort($period_code);
            $result = "$d $m $y $time";
        } else {
            $result = "$d $m $y";
        }



        return $result;
    }

    function thaiDay($day) {
        switch ($day) {
            case 'Mon': return __("Monday");
            case 'Tue': return __("Tuesday");
            case 'Wed': return __("Wednesday");
            case 'Thu': return __("Thursday");
            case 'Fri': return __("Friday");
            case 'Sat': return __("Saturday");
            case 'Sun': return __("Sunday");
            default: return $day;
        }
    }

    function thaiMonth($m) {
        $m = (int) $m;
        switch ($m) {
            case '1': return __("January");
            case '2': return __("February");
            case '3': return __("March");
            case '4': return __("April");
            case '5': return __("May");
            case '6': return __("June");
            case '7': return __("July");
            case '8': return __("August");
            case '9': return __("September");
            case '10': return __("October");
            case '11': return __("November");
            case '12': return __("December");
            default: return $m;
        }
    }

    function thaiMonthShort($m) {
        $m = (int) $m;
        switch ($m) {
            case '1': return __("Jan.");
            case '2': return __("Feb.");
            case '3': return __("Mar.");
            case '4': return __("Apr.");
            case '5': return __("May.");
            case '6': return __("Jun.");
            case '7': return __("Jul.");
            case '8': return __("Aug.");
            case '9': return __("Sep.");
            case '10': return __("Oct.");
            case '11': return __("Nov.");
            case '12': return __("Dec.");
            default: return $m;
        }
    }

    public function queueStatusThaiDisplay($status) {
        //N=New , B=BuildingCase , L=LCOCase , S=Assigned , A=Accepted , Z=OneToZero , R=RejectByLSO , C=Cancel , D=Done(Closed)
        //N=New , A=Appointed , K=Acknowledge , R=AgentReject , C=Cancel , D=Done  2013-12-4
        switch ($status) {
            case 'N': return __("ยังไม่นัดวัน");
            case 'B': return __("Sale");
            case 'L': return __("LCO");
            case 'A': return __("Appointed");
            case 'K': return __("Acknowledge");
            case 'Z': return __("หาLSOไม่ได้");
            case 'R': return __("Agent Reject");
            case 'C': return __("Cancel");
            case 'D': return __("Done");
            case 'A1': return __("งานที่กำลังดำเนินการ");
            default: return "-";
        }
    }

    public function genderThaiDisplay($gender) {
        switch ($gender) {
            case 'M': return __("Male");
            case 'F': return __("Female");
            default: return "-";
        }
    }

    public function customerTypeThaiList() {
        return array('G' => __('Genaral'), 'L' => __('Company'), 'O' => __("Official"));
    }

    public function customerTypeAllThaiList() {
        return array('G' => __('Genaral'), 'L' => __('Company'), 'O' => __("Official"), 'V' => __('VIP'));
    }

    public function customerTypeThaiDisplay($type) {
        switch ($type) {
            case 'G': return __("Genaral");
            case 'L': return __("Company");
            case 'O': return __("Official");
            case 'V': return __("VIP");
            case 'P': return __("PSI");
            default: return "-";
        }
    }

    public function installTypeThaiDisplay($type) {
        switch ($type) {
            case 'S': return __("Satellite Dish");
            case 'C': return __("Cable");
            case 'I': return __("Internet - Cable");
            default: return "-";
        }
    }

    public function packageTypeFormat($type) {
        switch ($type) {
            case 'M': return __("Monthly");
            case 'F': return __("Package");
            default: return "-";
        }
    }

    public function isActiveFormat($code) {
        switch ($code) {
            case 'Y': return "เปิดใช้งาน";
            case 'N': return "ปิดใช้งาน";
            default: return "-";
        }
    }

    public function isCloseFormat($code) {
        switch ($code) {
            case 'Y': return __("Close Job");
            case 'N': return __("Pending P");
            default: return "-";
        }
    }

    public function queueLogStatusDisplay($code) {
//        Ex. CRE=Create , 
//        APP=Appoint , ACJ=AcceptJobByAgent , CLS=CloseJob, ACT=Activated , 
//        CPK=ChangePackage , CAP = ChangeAppointment , CBL=ChangeBilling , CAS=ChangeAssign(Agent & appoint) ,
//        SPB=SpecialCaseBuildingType , SPC=SpecialCaseLCO , SPZ = SpecialCaseZeroLSO , SPF=FinalRejectByLSO , 
//        REJ=RejectByAgent ,  CAL=CancelByCustomer
//----------------------------------------------------------------------------------
//New Action code and descript
//REG		Register new customer
//RIN		Request Installation 
//STO		Send to outbound
//OOQ		On outbound queue
//STP		Send to project team
//APP		Appointed
//ACK		LSO/LCO acknowledge job
//CLS		Close job
//ACT		Activated
//REJ		Reject job 
//INP		Installation problem
//CAN		Cancel job
//RCC		Request change customer information
//CCI		Changed customer information
//RCI		Rejected request change customer information
//RCB		Request change billing information
//CBI		Changed billing information
//RBI		Rejected request change billing information
//RCA		Request change appointment
//CAP		Changed appointment
//RAP		Rejected request change appointment
//RCP		Request change package
//CPK		Change package
//RPK		Rejected request change package
        $sAction = "Unknown";
        $code = strtoupper(trim($code));
        switch ($code) {
//            case 'CRE': $sAction="สร้างใหม่";break;
//            case 'ASO': $sAction="ส่งงานให้Outbound";break;
//            case 'APP': $sAction="นัดวันติดตั้ง";break;
//            case 'ACJ': $sAction="ตัวแทนติดตั้งรับงาน";break;
//            case 'CLS': $sAction="ติดตั้งเรียบร้อย";break;
//            case 'ACT': $sAction="เปิดสัญญาณเรียบร้อย";break;
//            case 'CPK': $sAction="เปลี่ยนแพคเกจ";break;
//            case 'CAP': $sAction="เปลี่ยนวันนัดติดตั้ง";break;
//            case 'CBL': $sAction="เปลี่ยนสถานที่จัดส่งเอกสาร";break;
//            case 'CAS': $sAction="เปลี่ยนตัวแทนติดตั้ง";break;
//            case 'SPB': $sAction="เริ่มต้นดำเนิน(การการติดตั้งที่ไม่ใช่บ้าน)";break;
//            case 'SPC': $sAction="เริ่มต้นดำเนิน(การการติดตั้งแบบเคเบิล)";break;
//            case 'SPZ': $sAction="เริ่มต้นดำเนิน(ไม่มีตัวแทนติดตั้งในพื้นที่)";break;
//            case 'REJ': $sAction="ถูกปฏิเสธจากตัวแทนติดตั้ง";break;
//            case 'CAL': $sAction="ยกเลิกการนัดติดตั้ง";break;
//            case 'EXP': $sAction="Exported";break;
//            case 'DOW': $sAction="Downloaded";break;
            case 'REG': $sAction = __("Register new customer");
                break;
            case 'RIN': $sAction = __("Request Installation");
                break;
            case 'STO': $sAction = __("Send to outbound");
                break;
            case 'OOQ': $sAction = __("On outbound queue");
                break;
            case 'STP': $sAction = __("Send to project team");
                break;
            case 'APP': $sAction = __("Appointed");
                break;
            case 'ACK': $sAction = __("LSO/LCO acknowledge job");
                break;
            case 'CLS': $sAction = __("Close job");
                break;
            case 'ACT': $sAction = __("Activated");
                break;
            case 'REJ': $sAction = __("Reject job");
                break;
            case 'INP': $sAction = __("Installation problem");
                break;
            case 'CAN': $sAction = __("Cancel job");
                break;
            case 'RCC': $sAction = __("Request change customer information");
                break;
            case 'CCI': $sAction = __("Changed customer information");
                break;
            case 'RCI': $sAction = __("Rejected request change customer information");
                break;
            case 'RCB': $sAction = __("Request change billing information");
                break;
            case 'CBI': $sAction = __("Rejected request change billing information");
                break;
            case 'RBI': $sAction = __("Register new customer");
                break;
            case 'RCA': $sAction = __("Request change appointment");
                break;
            case 'CAP': $sAction = __("Changed appointment");
                break;
            case 'RAP': $sAction = __("Rejected request change appointment");
                break;
            case 'RCP': $sAction = __("Request change package");
                break;
            case 'CPK': $sAction = __("Change package");
                break;
            case 'RPK': $sAction = __("Rejected request change package");
                break;
            case 'DAT': $sAction = __("Deactivate");
                break;
            case 'SUS': $sAction = __("Suspend");
                break;
            case 'REF': $sAction = __("Refresh");
                break;
            case 'CAS': $sAction = __("Cancel Service");
                break;
            case 'CHA': $sAction = __("Change agent");
                break;
            case 'CST': $sAction = __("Change Set Top Box");
                break;
            case 'SGL': $sAction = __("Sent to GLL");
                break;
            case 'IRA': $sAction = __("Installation Queue Re-activate");
                break;
            case 'IMS': $sAction = __("Installation Queue Manual suspension");
                break;
            case 'SNB': $sAction = __("Suspension Not Calculated Bill");
                break;
            case 'CSU': $sAction = __("Cancel Suspension Not Bill");
                break;
	    case 'PSR': $sAction = __("Promotion Stopwatch Package Register");
                break;
            case 'PSA': $sAction = __("Promotion Stopwatch Package Active");
                break;
            case 'PSD': $sAction = __("Promotion Stopwatch Package Deactive");
                break;
            case 'CCC': $sAction = __("เปลี่ยนหมายเลขบัตร");
                break;
            case 'ADP': $sAction = __("Top Up");
                break;
            case 'SSG': $sAction = __("ระงับสัญญาณชั่วคราว Sponsor Singha");
                break;
            case 'CSG': $sAction = __("ยกเลิกการระงับสัญญาณชั่วคราว Sponsor Singha");
                break;
            case 'CPR': $sAction = __("Change Package (Reduce)");
                break;
            case 'CPA': $sAction = __("Change Package (Increase)");
                break;
            default: $sAction = __("Unknown");
        }
        return $sAction;
    }

    public function contactNoSelectDisplay($data = array()) { // ใช้แสดงเบอร์โทร
        $home_no = @$data['home_no'];
        $mobile_no = @$data['mobile_no'];
        $contact_no = @$data['contact_no'];

        $contactNo = "";
        if (!empty($home_no)) {
            $contactNo = $home_no;
        } elseif (!empty($mobile_no)) {
            $contactNo = $mobile_no;
        } elseif (!empty($contact_no)) {
            $contactNo = $contact_no;
        }
        return $contact_no;
    }

//    public function fullNameDisplay($name_prefix = "" , $first_name = "" , $last_name = ""){
//        $result = ($name_prefix == "") ? "" : " $name_prefix";
//        $result .= ($first_name == "") ? "" : " $first_name";
//        $result .= ($last_name == "") ? "" : " $last_name";
//        return $this->check_empty(substr($result, 1));
//    }

    public function action_type($action) {
        switch ($action) {
            case 'A':
                $status_name = __('Open Job');
                break;
            case 'S':
                $status_name = __('Progress');
                break;
            case 'T':
                $status_name = __('Share Job');
                break;
            case 'C':
                $status_name = __('Close Job');
                break;
            default:
                $status_name = __('Unknown');
                break;
        }
        return $status_name;
    }

    public function job_status($status) {
        switch ($status) {
            case 'A':
                $status_name = __('Pending');
                $status_name .= $this->Html->image("red.png", array("alt" => "", 'height' => 15, 'width' => 15));
                break;
            case 'P':
                $status_name = __('Actualize');
                $status_name .= $this->Html->image("yellow1.png", array("alt" => "", 'height' => 15, 'width' => 15));
                break;
            case 'C':
                $status_name = __('Done');
                $status_name .= $this->Html->image("green.png", array("alt" => "", 'height' => 15, 'width' => 15));
                break;
            default:
                $status_name = __('Unknown');
                break;
        }
        return $status_name;
    }

//    public function fullNameDisplay($name_prefix = "" , $first_name = "" , $last_name = ""){
//        $result = ($name_prefix == "") ? "" : " $name_prefix";
//        $result .= ($first_name == "") ? "" : " $first_name";
//        $result .= ($last_name == "") ? "" : " $last_name";
//        return $this->check_empty(substr($result, 1));
//    }


    /* public function attachment($url){
      return $this->Html->link(
      $this->Html->image("clip.png", array("alt" => "แนบ" , 'height' => 24, 'width' => 30)),
      $url,
      array('class'=>'group1', 'escape' => false,'title'=>'แนบ')
      );
      } */

    public function change_type($key = null) {
        $change_type = array(
            'C' => __('Edit Customer Info'),
            'Q' => __('Edit Installation Info'),
            'B' => __('Edit Billing Info'),
            'P' => __('Packages'),
            'AD' => __('Add/Reduce Installation Point'),
            'CA' => __('Uninstall'),
            'S' => __('Manual Suspend'),
            'Z' => __('Change Package'),
            'L' => __('Service Cancel'),
            'R' => __('Change Package (Reduce)'),
            'A' => __('Change Package (Increase)')
        );
        return !is_null($key) ? array_key_exists($key, $change_type) ? $change_type[$key] : $change_type  : __('Unknow');
    }

    public function approve_status($status) {
        $status_name = '';
        switch ($status) {
            case 'N':
                $status_name = __('New');
                break;
            case 'P':
                $status_name = __('Pending');
                break;
            case 'R':
                $status_name = __('Reject');
                break;
            case 'S':$status_name = __('Wait approve for suspension');
                break;
            case 'W':$status_name = __('Wait approve for re-activationn');
                break;
            case 'D':
                $status_name = __('Approve Request');
                break;
            default:
                $status_name = __('Status Unknown');
                break;
        }
        return $status_name;
    }

    public function link_bpopup($ahref_name, $ahref_id, $url) {
        echo "<u>" . $this->Html->link(__($ahref_name), '#', array('id' => $ahref_id)) . "</u>";
        echo $this->_View->element('b_popup', array('id' => $ahref_id, 'url' => $url));
    }

    public function chkBrowser($nameBroser) {
        return preg_match("/" . $nameBroser . "/", $_SERVER['HTTP_USER_AGENT']);
    }

    public function servierType($serviceType) {
        $status_name = '';
        switch ($serviceType) {
            case 'A':
                $status_name = __('Add Point');
                break;
            case 'R':
                $status_name = __('Remove Point');
                break;
            case 'W':
                $status_name = __('Cancel Service');
                break;
            default:
                $status_name = __('New Customer');
                break;
        }
        return $status_name;
    }

    public function chkApproval($status) {
        switch ($status) {
            case 'Y':
                $img = $this->Html->image('right-check-marks.jpg', array('height' => 20, 'width' => 20));
                break;
            default:
                $img = $this->Html->image('wrong-check-marks.jpg', array('height' => 20, 'width' => 20));
                break;
        }
        return $img;
    }
    
    public function Document($document) {
        $status_name = '';
        switch ($document) {
            case 'C':
                $status_name = __('Complete Document');
                break;
            case 'I':
                $status_name = __('Incomplete Document');
                break;
            case 'N':
                $status_name = __('No Document');
                break;
            default:
                $status_name = __('-');
                break;
        }
        return $status_name;
    }
    
    public function Payment($payment) {
        $status_name = '';
        switch ($payment) {
            case 'F':
                $status_name = __('Full Payment');
                break;
            case 'P':
                $status_name = __('Partial Payment');
                break;
            case 'N':
                $status_name = __('No Payment');
                break;
            default:
                $status_name = __('-');
                break;
        }
        return $status_name;
    }
    
    public function package_type($package_id) {
        
        $package_type = ClassRegistry::init('PackageType')->findType($package_id);
        return $package_type;
    }

}

?>