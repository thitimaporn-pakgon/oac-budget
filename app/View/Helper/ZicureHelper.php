<?php

App::uses('Helper', 'View');

class ZicureHelper extends AppHelper {

    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap');
    private $_thai_month_arr = array(
        "0" => "",
        "1" => "มกราคม",
        "2" => "กุมภาพันธ์",
        "3" => "มีนาคม",
        "4" => "เมษายน",
        "5" => "พฤษภาคม",
        "6" => "มิถุนายน",
        "7" => "กรกฎาคม",
        "8" => "สิงหาคม",
        "9" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );
    private $_thai_month_arr_short = array(
        "0" => "",
        "1" => "ม.ค.",
        "2" => "ก.พ.",
        "3" => "มี.ค.",
        "4" => "เม.ย.",
        "5" => "พ.ค.",
        "6" => "มิ.ย.",
        "7" => "ก.ค.",
        "8" => "ส.ค.",
        "9" => "ก.ย.",
        "10" => "ต.ค.",
        "11" => "พ.ย.",
        "12" => "ธ.ค."
    );

    /**
     * 
     * Function read all Document attachment with custom conditions
     * @author sarawutt.b
     * @param type $refId as a integer of reference id
     * @param type $refModel as a string of reference model
     * @return array()
     */
    public function readAllDocumentAttachment($refId = null, $refModel = null) {
        return ClassRegistry::init('DocumentAttachment')->readAllDocumentAttachment($refId, $refModel);
    }

    /**
     * 
     * Geting Application Version Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getAppVersion() {
        return ClassRegistry::init('Config')->getAppVersion();
    }

    /**
     * 
     * Function list manu page level
     * @author sarawutt.b
     * @param type $key as a integer of page level of menu
     * @return mix 
     */
    public function listMenuPageLevel($key = 'xxx') {
        return ClassRegistry::init('Menu')->listMenuPageLevel($key);
    }

    public function getDepartmentTagById($departmentId = null) {
        $tmp = $this->getDepartmentInfoById($departmentId);
        return empty($tmp) ? '' : "({$tmp['Department']['short_name']}) {$tmp['Department']['full_name']}";
    }

    /**
     * 
     * Function get current session department full name
     * @author  sarawutt.b
     * @return  string
     */
    public function getCurrentSessionDepartmentName() {
        return $this->getDepartmentTagById($this->getCurrenSessionDepartmentId());
    }

    /**
     * 
     * Find for Department Infomation
     * @author sarawutt.b
     * @param type $id as integer of department id
     * @return array() department infomation
     */
    public function getDepartmentInfoById($id = null) {
        return ClassRegistry::init("Department")->getDepartmentInfoById($id);
    }

    /**
     * 
     * Function Get current loggedin user ID
     * @author sarawutt.b
     * @return integer of current logggedin user id
     */
    public function getSessionUserId() {
        return $this->getCurrenSessionUserId();
    }

    /**
     * 
     * Function get for main system status
     * @author  sarawutt.b
     * @param   type $key as character of status key
     * @param   type $makeShow as boolean if true then make style otherwise pure status content display without style
     * @return  string
     */
    public function mainStatus($key = 'xxx', $makeShow = FALSE) {
        $stype = array('N' => 'active', 'A' => 'success', 'I' => 'danger', 'D' => 'warning');
        return ($makeShow === true) ? $this->bootstrapLabel(ClassRegistry::init('Utility')->getMainStatus($key), $stype[$key]) : ClassRegistry::init('Utility')->getMainStatus($key);
    }

    /**
     * 
     * Function make for bootstrap label
     * @author  sarawutt.b
     * @param   type $message as string of the content where you want wrap to the label
     * @param   type $class as string bootstrap class
     * @return  string
     */
    public function bootstrapLabel($message = '', $class = 'default') {
        return $this->Bootstrap->badge($message, $class, 'label');
    }

    public function getUnlocked($key = 'xxx') {
        return ClassRegistry::init('Utility')->getUnlocked($key);
    }

    public function getSex($key = 'xxx') {
        return ClassRegistry::init('Utility')->getSex($key);
    }

    public function getCurrentSessionUserId() {
        return ClassRegistry::init('User')->getCurrentSessionUserId();
    }

    public function findUserByUid($id = null) {
        return ClassRegistry::init('User')->findUserByUid($id);
    }

    /**
     * 
     * Find for the User infomation by the user ID
     * @author  sarawutt.b
     * @param   type $id as integer of user id is a PK
     * @return  array() of User infomation where it found
     */
    public function findUserInfoById($id = null) {
        return ClassRegistry::init('User')->findUserInfoById($id);
    }

    public function listDepartment($id = null, $not = false) {
        return ClassRegistry::init('Department')->findListDepartment($id, $not);
    }

    public function getDepartmentById($id = null) {
        return ClassRegistry::init('Department')->getDepartmentById($id);
    }

    /**
     * 
     * รายการหน่วยรับผิดชอบโครงการหลัก
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $not boolean of is convert
     * @return  type array()
     */
    public function findListDepartmentCover($id = null, $not = false) {
        return ClassRegistry::init('Department')->findListDepartmentCover($id, $not);
    }

    /**
     * 
     * รายการหน่วยงาน สปช.
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $not boolean of is convert
     * @return  type array()
     */
    public function findListDepartmentSPC($id = null, $not = false) {
        return ClassRegistry::init('Department')->findListDepartmentSPC($id, $not);
    }

    /**
     * 
     * รายการหน่วยงาน สามารถอนุมัติโครงการทั้งหมด
     * @author  sarawutt.b
     * @param   type $id integer of department id
     * @param   type $not boolean of is convert
     * @return  type array()
     */
    public function findListControlDepartment($id = null, $not = false) {
        return ClassRegistry::init('Department')->findListControlDepartment($id, $not);
    }

    public function getDepartmentLevelById($id = null) {
        return ClassRegistry::init('DepartmentLevel')->getDepartmentLevelById($id);
    }

    public function listDepartmentLevel() {
        return ClassRegistry::init('DepartmentLevel')->findListDepartmentLevel();
    }

    /**
     * 
     * 
     * Function find list budget year id => name
     * @author sarawutt.b
     * @param type $id as optional a ID of budget year wish matched
     * @param type $xconditions optional for custom condition
     * @return array() of budget year list
     */
    public function findListBudgetYear($id = null, $xconditions = array()) {
        return ClassRegistry::init('BudgetYear')->findListBudgetYear($id, $xconditions);
    }

    /**
     * 
     * 
     * Function find list budget year name => name
     * @author sarawutt.b
     * @param type $id as optional a ID of budget year wish matched
     * @param type $xconditions optional for custom condition
     * @return array() of budget year list
     */
    public function findListBudgetYearName($id = null, $xconditions = array()) {
        return ClassRegistry::init('BudgetYear')->findListBudgetYearName($id, $xconditions);
    }

    public function getBudgetYearById($id = null) {
        return ClassRegistry::init('BudgetYear')->getBudgetYearById($id);
    }

    /**
     * Find budget Year by id
     * @author  Sarawutt.b
     * @return  array();
     */
    public function findBudgetYearById($id = null) {
        return ClassRegistry::init('BudgetYear')->findBudgetYearById($id);
    }

    /**
     * Find list of User
     * @author  Sarawutt.b
     * @params  $id as integer of user id
     * @param   $convert as boolean as conver are not contain
     * @return  array();
     */
    public function findListUser($id = null, $convert = false) {
        return ClassRegistry::init('User')->findListUser($id, $convert);
    }

    public function findListSysController($id = null) {
        return ClassRegistry::init('SysController')->findListSysController($id);
    }

    public function findListSysAction($id = null) {
        return ClassRegistry::init('SysAction')->findListSysAction($id);
    }

    public function getPriority($key = 'xxx') {
        return ClassRegistry::init('Utility')->getPriority($key);
    }

    public function getUserById($id = null) {
        return ClassRegistry::init('User')->getUserById($id);
    }

    public function findListMenus($id = null) {
        return ClassRegistry::init('Menu')->findListMenus($id);
    }

    public function getMenuType($key = null) {
        return ClassRegistry::init('Menu')->getMenuType($key);
    }

    public function countMenu() {
        return ClassRegistry::init('Menu')->countMenu();
    }

    public function countUser($id = null) {
        return ClassRegistry::init('User')->countUser($id);
    }

    public function countAdmin($id = null) {
        return ClassRegistry::init('User')->countAdmin($id);
    }

    public function countAlluser($id = null) {
        return ClassRegistry::init('User')->countAlluser($id);
    }

    public function findListParentMenu($id = null) {
        return ClassRegistry::init('Menu')->findListParentMenu($id);
    }

    /**
     * 
     * Function display for thai date time
     * @param type $param as a string of date time
     * @param type $format as a string display options posible value DT|DTS|DSN|DS|DFM
     */
    public function displayThaiDate($param, $format = 'DS') {
        $format = strtoupper(trim($format));
        switch ($format) {
            case 'DT' :
                return $this->thaiDateTime($param);
                break;
            case 'DTS' :
                return $this->thaiDateTimeShort($param);
                break;
            case 'DSN' :
                return $this->thaiDateShortDashNumber($param);
                break;
            case 'DS' :
                return $this->thaiDateShort($param);
                break;
            case 'DFM' :
                return $this->thaiDateFullMonth($param);
                break;
            default :
                return '-';
        }
    }

    /**
     * 
     * Function display thai version full date and time
     * @author sarawutt.b
     * @param type $time
     * @return string of thai full date time format
     */
    public function thaiDateTime($time) {   // 19 ธันวาคม 2556 เวลา 10:10:43
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        $thai_date_return .= " เวลา " . date("H:i:s", $time);
        return $thai_date_return;
    }

    /**
     * 
     * Function display thai version short date and time
     * @author sarawutt.b
     * @param type $time
     * @return string of thai short date time format
     */
    public function thaiDateTimeShort($time) {   // 19  ธ.ค. 2556 10:10:4
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= "&nbsp;&nbsp;" . $this->_thai_month_arr_short[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        $thai_date_return .= " " . date("H:i:s", $time);
        return $thai_date_return;
    }

    /**
     * 
     * Function display thai version number dash separate value
     * @author sarawutt.b
     * @param type $time
     * @return string of thai number dash separate value
     */
    public function thaiDateShortDashNumber($time) {   // 19-12-56
        $time = strtotime($time);
        $thai_date_return = date("d", $time);
        $thai_date_return .= "-" . date("m", $time);
        $thai_date_return .= "-" . substr((date("Y", $time) + 543), -2);
        return $thai_date_return;
    }

    /**
     * 
     * Function display thai version only thai date
     * @author sarawutt.b
     * @param type $time
     * @return string of thai only thai date
     */
    public function thaiDateShort($time) {   // 19  ธ.ค. 2556
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr_short[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        return $thai_date_return;
    }

    /**
     * 
     * Function display thai version only thai date in format full month
     * @author sarawutt.b
     * @param type $time
     * @return string of thai only thai date in format full month
     */
    public function thaiDateFullMonth($time) {   // 19 ธันวาคม 2556
        $time = strtotime($time);
        $thai_date_return = date("j", $time);
        $thai_date_return .= " " . $this->_thai_month_arr[date("n", $time)];
        $thai_date_return .= " " . (date("Y", $time) + 543);
        return $thai_date_return;
    }

    /**
     * 
     * Function display date time format
     * @author  sarawutt.b
     * @param   type $date as string date or date time
     * @return  string in date format only
     */
    public function dateISO($date = null) { // Y-m-d
        return (is_null($date)) ? '-' : $this->getDynamicYear($date) . date('-m-d', strtotime($date));
    }

    /**
     * 
     * Function display date format
     * @author  sarawutt.b
     * @param   type $datetime as string date or date time
     * @return  string in date and time format
     */
    public function dateTimeISO($datetime = null) { // Y-m-d H:i:s
        return (is_null($datetime)) ? '-' : $this->getDynamicYear($datetime) . date('-m-d H:i:s', strtotime($datetime));
    }

    /**
     * 
     * Function convert date or date and time to current locale
     * @author  sarawutt.b
     * @param   type $dateTime as string date or date time
     * @return  string in year format BU/BCC year
     */
    private function getDynamicYear($dateTime = null) {
        return ($this->getCurrentLanguage() == 'tha') ? date('Y', strtotime($dateTime)) + 543 : date('Y', strtotime($dateTime));
    }

    public function getSysControlllerNameById($id = null) {
        return ClassRegistry::init('SysController')->getSysControlllerNameById($id);
    }

    public function getSysActionNameById($id = null) {
        return ClassRegistry::init('SysAction')->getSysActionNameById($id);
    }

    public function findListRole($id = null) {
        return ClassRegistry::init('Role')->findListRole($id);
    }

    public function ListProvince() {
        return ClassRegistry::init('Province')->findListProvince();
    }

    public function searchProvince($pv_name = null) {
        return ClassRegistry::init('Province')->searchProvince($pv_name);
    }

    public function getProvinceById($id = null) {
        return ClassRegistry::init('Province')->getProvinceById($id);
    }

    public function getNamePrefixById($id = null) {
        return ClassRegistry::init('NamePrefix')->getNamePrefixById($id);
    }

    public function getDistrictById($id = null) {
        return ClassRegistry::init('District')->getDistrictById($id);
    }

    public function ListDistrict($provinceId = null) {
        return ClassRegistry::init('District')->findListDistrict($provinceId);
    }

    public function ListSubDistrict($districtId = null) {
        return ClassRegistry::init('SubDistrict')->findListSubDistrict($districtId);
    }

    /**
     * 
     * Find diferance between two date
     * @author  Sarawutt.b
     * @param   type $varStart as string of start date
     * @param   type $varEnd as string of end date
     * @since   2015-10-25 17:45:20
     * @return  integer of date difference
     */
    public function dateDiff($fdate, $tdate) {
        $tmpFdate = date('Y-m-d', strtotime($fdate));
        $tmpTdate = date('Y-m-d', strtotime($tdate));
        $dateFrom = new DateTime($tmpFdate);
        $dateTo = new DateTime($tmpTdate);
        $interval = $dateFrom->diff($dateTo)->format("%r%a");
        return $interval;
    }

    public function yearProcessTime($fromYear, $toYear) {
        $result = $toYear - $fromYear;
        return $result + 1;
    }

    /**
     * 
     * Get relate project / project plan document
     * @author  sarawutt.b
     * @param   type $ref_ducument_no
     * @return  type
     */
    public function getProjectDocumentRelate($ref_ducument_no = null) {
        return ClassRegistry::init('DocumentAttachment')->getProjectDocumentRelate($ref_ducument_no);
    }

    /**
     * 
     * Get relate project / project plan document
     * @author  sarawutt.b
     * @param   type $projectPlanId as a projectPlanId
     * @return  type
     */
    public function getDocumentAttachmentByProjectPlanId($projectPlanId = null) {
        return ClassRegistry::init('DocumentAttachment')->getDocumentAttachmentByProjectPlanId($projectPlanId);
    }

    public function ListPrefix() {
        return ClassRegistry::init('NamePrefix')->findListNamePrefix();
    }

    public function ListPosition() {
        return ClassRegistry::init('Position')->findListPosition();
    }

    public function ListRole() {
        return ClassRegistry::init('Role')->findListRole();
    }

    /**
     * 
     * Function currency format display the format of the currency
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @param   type $decimal as integer of decimal precition
     * @param   type $simbol as string of currentcy simbolic
     * @return  string number in currency format
     */
    public function currencyFormat($currentcy = null, $decimal = 2, $simbol = '') {
        return $this->_numberFormat($currentcy, $decimal, !empty($simbol) ? $simbol : Configure::read('Zicure.Currency'));
    }

    /**
     * 
     * Function percent of the currency format display the format of the currency
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @return  string number in currency format
     */
    public function percentFormat($currentcy = null) {
        return $this->_numberFormat($currentcy, 2, '%');
    }

    /**
     * 
     * Function for display number in format number
     * @param   type $number as number or integer or decimal
     * @since   2016-10-10
     * @return  string number in format thousan separated
     */
    public function numberFormat($number = null, $precition = 2) {
        return $this->_numberFormat($number, $precition);
    }

    /**
     * 
     * Function currency format display for number in format
     * @author  sarawutt.b
     * @param   type $currentcy as number or integer or decimal
     * @param   type $currencySymbol as string currency symbol
     * @param   type $precition as integer of amount precition
     * @since   2016-10-10
     * @return  string
     */
    private function _numberFormat($currentcy = null, $precition = 2, $currencySymbol = null) {
        $currentcy = (is_null($currentcy) || empty($currentcy)) ? 0 : $currentcy;
        return number_format($currentcy, $precition, '.', ',') . ' ' . $currencySymbol;
    }

    public function findBudgetProcessTime() {
        return ClassRegistry::init('BudgetYear')->findBudgetProcessTime();
    }

    /**
     * Get project or project plan Information where equals id parameters
     * @author  sarawutt.b
     * @param   integer $id as project pla id or project id
     * @return  array
     */
    public function getPlanProjectInfomation($id) {
        return ClassRegistry::init('DocumentAttachment')->getPlanProjectInfomation($id);
    }

    public function bootstrapBoxClass($index = null) {
        return ClassRegistry::init('Common')->bootstrapBoxClass($index);
    }

    /**
     * 
     * List of the month or find with the full name of the month name in key params
     * @author Sarawutt.b <sarawutt.b@pakgon.com>
     * @param   integer $key of month index posible value 1 - 12
     * @return  string
     */
    public function listMonth($key = 'xxx') {
        return ClassRegistry::init('BudgetYear')->listMonth($key);
    }

    /**
     * 
     * List of the quarter or find with the full name of the quather name in key params
     * @author Sarawutt.b
     * @param   integer $key of the quarther index posible value 1 - 4
     * @return  string
     */
    public function listQuarter($key = 'xxx') {
        return ClassRegistry::init('BudgetYear')->listQuarter($key);
    }

    public function num2wordsThai($num) {
        $num = str_replace(",", "", $num);
        $num_decimal = explode(".", $num);
        $num = $num_decimal[0];
        $returnNumWord = '';
        $lenNumber = strlen($num);
        $lenNumber2 = $lenNumber - 1;
        $kaGroup = array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
        $kaDigit = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
        $kaDigitDecimal = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
        $ii = 0;
        for ($i = $lenNumber2; $i >= 0; $i--) {
            $kaNumWord[$i] = substr($num, $ii, 1);
            $ii++;
        }
        $ii = 0;
        for ($i = $lenNumber2; $i >= 0; $i--) {
            if (($kaNumWord[$i] == 2 && $i == 1) || ($kaNumWord[$i] == 2 && $i == 7)) {
                $kaDigit[$kaNumWord[$i]] = "ยี่";
            } else {
                if ($kaNumWord[$i] == 2) {
                    $kaDigit[$kaNumWord[$i]] = "สอง";
                }
                if (($kaNumWord[$i] == 1 && $i <= 2 && $i == 0) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 6)) {
                    if ($kaNumWord[$i + 1] == 0) {
                        $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                    } else {
                        $kaDigit[$kaNumWord[$i]] = "เอ็ด";
                    }
                } elseif (($kaNumWord[$i] == 1 && $i <= 2 && $i == 1) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 7)) {
                    $kaDigit[$kaNumWord[$i]] = "";
                } else {
                    if ($kaNumWord[$i] == 1) {
                        $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                    }
                }
            }
            if ($kaNumWord[$i] == 0) {
                if ($i != 6) {
                    $kaGroup[$i] = "";
                }
            }
            $kaNumWord[$i] = substr($num, $ii, 1);
            $ii++;
            $returnNumWord .= $kaDigit[$kaNumWord[$i]] . $kaGroup[$i];
        }
        if (isset($num_decimal[1])) {
            //$returnNumWord.="จุด";
            $returnNumWord .= " ";
            for ($i = 0; $i < strlen($num_decimal[1]); $i++) {
                $returnNumWord .= $kaDigitDecimal[substr($num_decimal[1], $i, 1)];
            }
            $returnNumWord .= " สตางค์";
        }
        return $returnNumWord;
    }

    /**
     * 
     * Find Role infomation by role ID
     * @author  sarawutt.b
     * @param   type $id as integer of role id
     * @return  array()
     */
    public function getRoleNameById($id = null) {
        return ClassRegistry::init('Role')->getRoleNameById($id);
    }

    public function getUserNameById($id = null) {
        return ClassRegistry::init('User')->getUserNameById($id);
    }

    /**
     * 
     * Function find for budget year ID by budget Year name
     * @author  sarawutt.b
     * @param   type $name as string of budget year name like 2559, 2560, 2561 ...
     * @return  integer di of budget year id
     */
    public function findBudgetYearIdByName($name = null) {
        return ClassRegistry::init('BudgetYear')->findBudgetYearIdByName($name);
    }

    public function quarterMonth() {
        $list = array(
            '1' => 'ต.ค.',
            '2' => 'พ.ย.',
            '3' => 'ธ.ค.',
            '4' => 'ม.ค.',
            '5' => 'ก.พ.',
            '6' => 'มี.ค.',
            '7' => 'เม.ย.',
            '8' => 'พ.ค.',
            '9' => 'มิ.ย.',
            '10' => 'ก.ค.',
            '11' => 'ส.ค.',
            '12' => 'ก.ย.'
        );
        return $list;
    }

    public function getYearList() {
        $year = array();
        $fromYear = (2555);
        $curYear = (date('Y') + 5) + 543;
        for ($i = $fromYear; $i <= $curYear; $i++) {
            $year[$i] = $i;
        }
        return $this->getEmptySelect() + $year;
    }

    /**
     * 
     * Function find department short name by department id
     * @author  Numpol.S
     * @param   type $id as a integer of department id
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmentShortName($id = null) {
        return ClassRegistry::init('Department')->findDepartmentShortName($id);
    }

    /**
     * 
     * Function find department full name by department id
     * @author  Numpol.S
     * @param   type $id as a integer of department id
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmentName($id = null) {
        return ClassRegistry::init('Department')->findDepartmentName($id);
    }

    /**
     * 
     * Function find department each name with department level name
     * @author  sarawutt.b
     * @param   type $id as a integer of department id
     * @param   type $display_style part of name posible value a|b default a for the short name
     * @return  string department short name where match otherwise string dash
     */
    public function findDepartmentNameWithDepartment($id = null, $display_style = 'a') {
        return ClassRegistry::init('Department')->findDepartmentNameWithDepartment($id, $display_style);
    }

}
