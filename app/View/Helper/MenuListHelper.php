<?php

App::uses('Helper', 'View');

class MenuListHelper extends AppHelper {

    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap');

    public function findListSystemName($id = null) {
        return ClassRegistry::init('SystemName')->findListSystemName($id);
    }

    public function findListMenuGroupName($id = null) {
        return ClassRegistry::init('MenuGroupName')->findListMenuGroupName($id);
    }

}
