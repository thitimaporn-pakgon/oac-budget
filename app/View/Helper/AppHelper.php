<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    public $helpers = array('Html', 'Form', 'Session', 'Bootstrap');
    private $select_empty_msg = '---- please select ----';

    /**
     * 
     * Function get for empty option
     * @author sarawutt.b
     * @return array()
     */
    public function getEmptySelect() {
        return array('' => __($this->select_empty_msg));
    }

    /**
     * 
     * Function Login Account Info make get user header info style
     * @author sarawutt.b
     * @return string
     */
    public function loginAccountInfo() {
        $tmpInfo = $this->getLoginAccountInfo();
//        $emailInfo = __('Email') . ' : ' . $this->getSessionEmail();
//        $PhoneInfo = __('Phone') . ' : ' . $this->getSessionPhoneNo();
        $info = "<li class='user-header'>{$this->getLoginUserPicture()}<p>{$tmpInfo}</p></li>";
        return $info;
    }

    /**
     * 
     * Function Login username make current user login system online
     * @author sarawutt.b
     * @return string
     */
    public function loginUsername() {
        $username = $this->getLoginUsername();
        $info = "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>{$this->getLoginUserPicture()}<span class='hidden-xs'>{$username}</span></a>";
        return $info;
    }
	
	public function loginFullName() {
        $fullname = CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.TitleName.title_name').' '.CakeSession::read('Auth.User.first_name').' '.CakeSession::read('Auth.User.last_name') : __('ZiCURE Corp.');
		$info = "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>{$this->getLoginUserPicture()}<span class='hidden-xs'>{$fullname}</span></a>";
        return $info;
    }

    /**
     * 
     * Function leftAccountInfo make current user login profile pupup
     * @author sarawutt.b
     * @return string
     */
    public function leftAccountInfo() {
        $info = "<div class='user-panel'>
            <div class='pull-left image'>{$this->getLoginUserPicture('img-circle')}</div>
            <div class='pull-left info'>
                <p>{$this->getLoginUsername()}</p>
                <a href='#'><i class='fa fa-circle text-success'></i> Online</a>
            </div>
        </div>";
        return $info;
    }

    /**
     * 
     * Function get Login User Picture get current loged in profile picture
     * @author sarawutt.b
     * @return string
     */
    public function getLoginUserPicture($class = 'user-image') {
        $img = CakeSession::read('Auth.User.picture_path');
        $img = empty($img) ? 'avatar5.png' : $img;
        return $this->Html->image($img, array('class' => $class, 'alt' => __('User Image')));
    }

    /**
     * 
     * Function get Login Account Info get all full current logedin name
     * @author sarawutt.b
     * @return string
     */
    public function getLoginAccountInfo() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.NamePrefix.name') . ' ' . CakeSession::read('Auth.User.first_name') . ' ' . CakeSession::read('Auth.User.last_name') : __('ZiCURE Corp.');
    }

    /**
     * 
     * Function get current profile username
     * @author sarawutt.b
     * @return string
     */
    public function getLoginUsername() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.username') : __('ZiCURE Corp.');
    }

    /**
     * 
     * Function Get current loggedin user id
     * @author sarawutt.b
     * @return integer of current logggedin user id
     */
    public function getCurrenSessionUserId() {
        return CakeSession::read('Auth.User.id');
    }

    /**
     * 
     * Function Get current loggedin role id
     * @author sarawutt.b
     * @return integer of current logggedin role id
     */
    public function getCurrenSessionRoleId() {
        return CakeSession::read('Auth.User.role_id');
    }

    /**
     * 
     * Function check is already pass to authentication process
     * @author sarawutt.b
     * @return boolean with authentication process
     */
    public function checkAuthUsers() {
        return CakeSession::check('Auth.User');
    }

    /**
     * 
     * Function Get current uses language
     * @author sarawutt.b
     * @return string tha|eng
     */
    public function getCurrentLanguage() {
        return CakeSession::read('SessionLanguage');
    }

    /**
     * 
     * Function Get current loggedin user department ID
     * @author sarawutt.b
     * @return integer of current logggedin user department ID
     */
    public function getCurrenSessionDepartmentId() {
        return CakeSession::read('Auth.User.department_id');
    }

    /**
     * 
     * Function Get current loggedin user phone number
     * @author sarawutt.b
     * @return string of current logggedin phone number
     */
    public function getSessionPhoneNo() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.phone_no') : __('ZiCURE Corp.');
    }

    /**
     * 
     * Function Get current loggedin user email address
     * @author sarawutt.b
     * @return string of current logggedin user email address
     */
    public function getSessionEmail() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.email') : __('ZiCURE Corp.');
    }

    /**
     * 
     * Function Get current loggedin posionion
     * @author sarawutt.b
     * @return string of current logggedin position
     */
    public function getSessionPosition() {
        return CakeSession::check('Auth.User') ? CakeSession::read('Auth.User.Position.name') : __('ZiCURE Corp.');
    }

    /**
     * 
     * Function secure make for decoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureDecodeParam($param = null) {
        return ClassRegistry::init('Common')->findDepartmentShortName($param);
    }

    /**
     * 
     * Function secure make for encoding the encryp parameter
     * @author  sarawutt.b
     * @param   type $param as string of encryp parameter
     * @return  string
     */
    public function secureEncodeParam($param = null) {
        return ClassRegistry::init('Common')->secureEncodeParam($param);
    }

    /**
     * 
     * Function check is admin on the current loggedin
     * @author sarawutt.b
     * @return boolean true if admin current session is admin otherwise return false
     */
    public function isAdmin() {
        return ($this->getCurrenSessionRoleId() == $this->getAdminRoleId());
    }

    /**
     * 
     * Function check is SPC on the current loggedin
     * @author sarawutt.b
     * @return boolean true if SPC current session is admin otherwise return false
     */
    public function isSPCDep() {
        return ($this->getCurrenSessionDepartmentId() == $this->getSPCDepartmentRoleId());
    }

    /**
     * 
     * Function check is main department on the current loggedin
     * @author sarawutt.b
     * @return boolean true if main department current session is admin otherwise return false
     */
    public function isMainDep() {
        return ($this->getCurrenSessionDepartmentId() == $this->getMainDepartmentRoleId());
    }

    /**
     * 
     * Function check is sub department on the current loggedin
     * @author sarawutt.b
     * @return boolean true if sub department current session is admin otherwise return false
     */
    public function isSubDep() {
        return ($this->getCurrenSessionDepartmentId() == $this->getSubDepartmentRoleId());
    }

    /**
     * 
     * Function check is practice department on the current loggedin
     * @author sarawutt.b
     * @return boolean true if practice department current session is admin otherwise return false
     */
    public function isPracDep() {
        return ($this->getCurrenSessionDepartmentId() == $this->getPracticeDepartmentRoleId());
    }

    /**
     * 
     * Function check is owner department on the current loggedin
     * @author sarawutt.b
     * @return boolean true if owner department current session is admin otherwise return false
     */
    public function isOwnDep() {
        return ($this->getCurrenSessionDepartmentId() == $this->getOwnerDepartmentRoleId());
    }

    /**
     * 
     * Function repeat HTML space
     * @author sarawutt.b
     * @param type $repeat as integer round of repeat
     * @param type $em as string of element
     * @return string of number ofrepeat output space
     */
    public function nbsp($repeat = 1, $em = '&nbsp;') {
        return $this->repeatElement($repeat, $em);
    }

    /**
     * 
     * Function repeat HTML new line
     * @author sarawutt.b
     * @param type $repeat as integer round of repeat
     * @param type $em as string of element
     * @return string of number of repeat output new line
     */
    public function br($repeat = 1, $em = '<br/>') {
        return $this->repeatElement($repeat, $em);
    }

    /**
     * 
     * Function repeat HTML element
     * @author sarawutt.b
     * @param type $repeat as integer round of repeat
     * @param type $em as string of element
     * @return string of number of repeat output new line
     */
    private function repeatElement($repeat = 1, $em = '&nbsp;') {
        $msg = null;
        for ($i = 0; $i < $repeat; $i++) {
            $msg .= $em;
        }
        return $msg;
    }

    /**
     * 
     * Geting Admin Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getAdminRoleId() {
        return ClassRegistry::init('Config')->getAdminRoleId();
    }

    /**
     * 
     * Geting SPC Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getSPCDepartmentRoleId() {
        return ClassRegistry::init('Config')->getSPCDepartmentRoleId();
    }

    /**
     * 
     * Geting Main Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getMainDepartmentRoleId() {
        return ClassRegistry::init('Config')->getMainDepartmentRoleId();
    }

    /**
     * 
     * Geting Sub Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getSubDepartmentRoleId() {
        return ClassRegistry::init('Config')->getSubDepartmentRoleId();
    }

    /**
     * 
     * Geting Practice Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getPracticeDepartmentRoleId() {
        return ClassRegistry::init('Config')->getPracticeDepartmentRoleId();
    }

    /**
     * 
     * Geting Owner Department Role ID Configure
     * @author  sarawutt.b
     * @return  string
     */
    public function getOwnerDepartmentRoleId() {
        return ClassRegistry::init('Config')->getOwnerDepartmentRoleId();
    }

    /**
     * 
     * Geting anount message display in maquee style on main view 
     * @author  sarawutt.b
     * @return  string
     */
    public function getAnnounceMsg() {
        return ClassRegistry::init('Config')->getAnnounceMsg();
    }

    /**
     * 
     * Function get configure value display on hen
     * @author  sarawutt.b
     * @param   type $id as a string configre ID
     * @return  string configure value of matched otherwise boolean false
     */
    public function getConfigureValue($id = 'EMPTY') {
        return ClassRegistry::init('Config')->getConfigureValue($id);
    }

    /**
     * Function get Curren BudgetYear 
     * @author  suphakid.s
     * @return string
     * 
     */
    public function getCurrenBudgetYearTH() {
        return ClassRegistry::init('Utility')->getCurrenBudgetYearTH();
    }

}
