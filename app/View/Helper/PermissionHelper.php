<?php

/**
  | ------------------------------------------------------------------------------------------------------------------
  | Helper Permission Helper
  | @author  sarawutt.b
  | @since   2013/11/06 14:37:26
  | @license Zicure Corp
  |	------------------------------------------------------------------------------------------------------------------
  | 1. isPermission
  | 2. button_submit
  | 3. link_view
  | 4. link_edit
  | 5. link_del
  | 6. link_button
  | 7. link_munues
  | 8. link_permissions
  | 9. link_accept
  | 10. link_reject
  | 11. link_button_popup
  | 12. add_area
  | 13. link_inventory
  | 14. link_close_job
  | 15. link_cancel
  | 16. attachment
  | 17. link_print make print button no need to redirect only acl access check
  |------------------------------------------------------------------------------------------------------------------------
 */
App::uses('AppHelper', 'View/Helper');

class PermissionHelper extends AppHelper {

    public $helpers = array('Html', 'Form', 'Zicure');

    /**
     * This function display for javascript button when you click the button they turn back page
     * @author Sarawutt.b
     * @since 24/02/2015
     * @return void
     */
    public function button_back($align = 'right', $class = "btn btn-default ") {
        return $this->Form->button(__('Back'), array("div" => false, 'type' => 'button', 'class' => "{$class} btn-back pull-{$align}", 'onclick' => 'window.history.back();', 'name' => 'btn-back'));
    }

    /**
     * This function display for javascript button when you click the button they turn back page
     * @author Sarawutt.b
     * @since 24/02/2015
     * @return void
     */
    public function buttonBack($align = 'right', $class = "btn btn-default btn-back") {
        return $this->button_back($align, $class);
    }

    /**
     * This function display for direct back link when you click the button link they turn back page
     * @author Sarawutt.b
     * @since 26/08/2016
     * @return void
     */
    public function linkBack($link = null, $class = 'btn btn-default pull-right') {
        return $this->link(__('Back'), $link, array('class' => $class, 'name' => 'btn-back'));
    }

    /**
     * Correct link show link correct symbolic
     * @author  sarawutt.b
     * @since   05/06/2014
     * @param   string of urls in destinations
     * @return  html link if has permission null in cast otherwise
     */
    public function link_correct($url) {
        return $this->isPermission($url) ? $this->Html->link($this->Html->image("correct.png", array("alt" => "corrected", 'height' => 24, 'width' => 24)), $url, array('class' => 'viewLink', 'escape' => false, 'title' => 'corrected')) : NULL;
    }

    /**
     * Incorrect link show link Incorrect symbolic
     * @author  sarawutt.b
     * @since   05/06/2014
     * @param   string of urls in destinations
     * @return  html link if has permission null in cast otherwise
     */
    public function link_incorrect($url) {
        return $this->isPermission($url) ? $this->Html->link($this->Html->image("incorrect.png", array("alt" => "incorrected", 'height' => 24, 'width' => 24)), $url, array('class' => 'viewLink', 'escape' => false, 'title' => 'incorrected')) : NULL;
    }

    /**
     * This function for check current user where logedin can use any cnotroller/action
     * @author Sarawutt.b
     * @since 20/10/2013
     * @params Permission Mode True in has permission FALSE in not has
     * @return void
     */
    public function isPermission($url = NULL) {
        return ClassRegistry::init('Utility')->isPermission(Configure::read('Permission.ModePermission'), $url);
    }

    public function link_print($title = NULL, $url, $options = array('class' => 'button', 'target' => '_blank')) {
        return $this->isPermission($url) ? $this->Html->link(__($title), $url, $options) : NULL;
    }

    public function link_submit($title = 'Submit', $url, $options = array()) {
        $selfOption = array('type' => 'submit', 'class' => 'btn btn-primary');
        return $this->isPermission($url) ? $this->Form->button(__($title), array_merge($selfOption, $options)) : NULL;
    }

    public function submit_option($title = 'Submit', $url, $options = array()) {
        $selfOption = array('type' => 'submit');
        return $this->isPermission($url) ? $this->Form->button(__($title), array_merge($selfOption, $options)) : NULL;
    }

    public function link_view($url) {
        return $this->isPermission($url) ? $this->Html->link(
                        '<span class="glyphicon glyphicon-search"></span>', $url, array('class' => 'viewLink', 'escape' => false, 'title' => 'เรียกดู')
                ) : NULL;
    }

    public function link_edit($url) {
        return $this->isPermission($url) ? $this->Html->link(
                        '<span class="glyphicon glyphicon-edit"></span>', $url, array('class' => 'editLink', 'escape' => false, 'title' => 'แก้ไข')
                ) : NULL;
    }

    public function link_del($url, $msg = "Do you want to delete?") {
        return $this->isPermission($url) ? $this->Html->link(
                        '<span class="glyphicon glyphicon-remove"></span>', $url, array('class' => 'deleteLink confirmButton', 'escape' => false, 'title' => __('Delete'), 'rel' => $msg)
                ) : NULL;
    }

    public function button_option($name, $url, $msg = '', $options_additions = array()) {
        if ($msg == '') {
            $onclick = "window.location='{$url}'";
        } else {
            $onclick = "if(confirm('$msg')) window.location='{$url}'; else return false;";
        }

        $options = array('div' => FALSE, 'label' => false, 'type' => 'button', 'class' => 'btn', 'onclick' => $onclick);
        $options = array_merge_recursive($options, $options_additions);
        return $this->isPermission($url) ? $this->Form->button($name, $options) : NULL;
    }

    public function link_button($name, $url, $msg = "", $options_additions = array()) {
        if ($msg == "") {
            $onclick = "window.location='{$url}'";
        } else {
            $onclick = "if(confirm('$msg')) window.location='{$url}'; else return false;";
        }

        $options = array('div' => FALSE, 'label' => false, 'type' => 'button', 'onclick' => $onclick, 'class' => 'btn btn-active');
        $options = array_merge_recursive($options, $options_additions);
        return $this->isPermission($url) ? $this->Form->button($name, $options) : NULL;
    }

    public function link_munues($url) {
        return $this->isPermission($url) ? $this->Html->link(
                        $this->Html->image("task_menu.png", array("alt" => "แก้ไขเพิ่มเมนูให้กับกลุ่มผู้ใช้", 'height' => 24, 'width' => 24)), $url, array('class' => 'editLink', 'escape' => false, 'title' => 'แก้ไขเพิ่มเมนูให้กับกลุ่มผู้ใช้')
                ) : NULL;
    }

    public function link_permissions($url) {
        return $this->isPermission($url) ? $this->Html->link('', $url, array('class' => 'btn btn-warning fa fa-user-plus', 'escape' => false, 'title' => 'แก้ไขสิทธิ์ผู้เข้าใช้งานระบบ')
                ) : NULL;
    }

    public function link_accept($url, $msg = "ยืนยันการรับงาน") {
        return $this->isPermission($url) ? $this->Html->link(
                        $this->Html->image("accept_icon.png", array("alt" => "รับงาน", 'height' => 24, 'width' => 24)), $url, array('class' => 'deleteLink confirmButton', 'escape' => false, 'title' => 'รับงาน', 'rel' => $msg)
                ) : NULL;
    }

    public function link_reject($url, $msg = "ยืนยันการปฏิเสธงาน") {
        return $this->isPermission($url) ? $this->Html->image("reject_icon.png", array("alt" => "ปฎิเสธงาน", 'title' => 'ปฎิเสธงาน', 'height' => 24, 'width' => 24, "style" => "cursor:pointer;", 'onclick' => "var left = (screen.width/2)-(550/2);var top = (screen.height/2)-(100);window.open('{$url}','new1','location=0,toolbar=0,directories=0,status=0,menubar=0,left='+left+',top='+top+',scrollbars=0,resizable=0,width=550,height=100');")) : NULL;
    }

    public function link_button_popup($name, $url, $w, $h) {
        $onclick = "var left = (screen.width/2)-({$w}/2);var top = (screen.height/2)-({$h}/2);window.open('{$url}','new1','location=0,toolbar=0,directories=0,status=0,menubar=0,left='+left+',top='+top+',scrollbars=0,resizable=0,width={$w},height={$h}');";
        return $this->isPermission($url) ? $this->Form->button($name, array('div' => FALSE, 'label' => false, 'type' => 'button', 'onclick' => $onclick)) : NULL;
    }

    public function link_cancel($url, $msg = "ยืนยันการยกเลิก") {
        return $this->isPermission($url) ? $this->Html->link(
                        $this->Html->image("cancel_icon.png", array("alt" => "ยกเลิก", 'height' => 24, 'width' => 24)), $url, array('class' => 'deleteLink confirmButton', 'escape' => false, 'title' => 'ยกเลิก', 'rel' => $msg)
                ) : NULL;
    }

    public function attachment($url) {
        return $this->isPermission($url) ? $this->Html->link(
                        $this->Html->image("clip.png", array("alt" => "แนบ", 'height' => 24, 'width' => 30)), $url, array('class' => 'group1', 'escape' => false, 'title' => 'แนบ')
                ) : NULL;
    }

    /*
     * 
     * ----------------------------------------------------------------------------------------------------------------------------------------
     * Function link and button permission helper
     * ----------------------------------------------------------------------------------------------------------------------------------------
     * @author  sarawutt.b
     * @since   2016/05/18
     * ----------------------------------------------------------------------------------------------------------------------------------------
     */

    /**
     * 
     * Function make submit button for submit for the form if check pass permission
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @return  string as HTML submit tag if has permission otherwise return null
     */
    public function submit($title, $url, $options = array()) {
        $selfOption = array('type' => 'submit', 'class' => 'btn btn-primary');
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($title, $options) : null;
    }

    /**
     * 
     * Function make for the permission button if has permission this display for the button otherwise display none
     * @author  sarawutt.b
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function button($title, $url = null, $options = array()) {
        $icon = array_key_exists('icon', $options) ? $this->Bootstrap->icon($options['icon']) : null;
        $checkPermission = false;
        if (!is_null($url)) {
            if (array_key_exists('data-confirm-title', $options)) {
                $options['action'] = $url;
            } else {
                $options['onclick'] = "window.location='{$url}'";
            }
        } else {
            $checkPermission = true;
        }
        $selfOption = array('type' => 'button', 'class' => 'btn btn-active');
        $options = array_merge($selfOption, $options);
        return ($this->isPermission($url) || ($checkPermission)) ? $this->Form->button($icon . __($title), $options) : NULL;
    }

    /**
     * 
     * Function make confirm button display popup button befor redirect to the destination
     * @author  sarawutt.b
     * @param   type $title as string 
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @param   type $msg as string of confirmation message
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function confirmButton($title, $url, $options = array(), $msg = 'Do you want confirm for the action ?') {
        $selfOption = array('type' => 'button', 'class' => 'btn btn-active confirmModal', 'rel' => __($msg), 'action' => $url);
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($title, $options) : null;
    }

    /**
     * 
     * Function make confirmation submit form
     * @author  sarawutt.b
     * @param   type $title as string 
     * @param   type $title as string title display on the output button
     * @param   type $url as string path of the destination in action
     * @param   type $options as array() of html attr in cakePHP pathern style
     * @param   type $msg as string of confirmation message
     * @return  string HTML button if has permission otherwise return NULL
     */
    public function confirmSubmit($title, $url, $options = array(), $msg = 'Do you want confirm for the action ?') {
        $selfOption = array('type' => 'submit', 'class' => 'btn btn-primary confirmModal', 'rel' => __($msg));
        $options = array_merge($selfOption, $options);
        return $this->isPermission($url) ? $this->Form->button($title, $options) : null;
    }

    /**
     * 
     * Function make for display HTML link incrude with permission
     * @author  sarawutt.b
     * @param   type $title as string of title on the link
     * @param   type $url as string of the destination path
     * @param   array $options as array of HTML option
     * @return  string of the output link
     */
    public function link($title = null, $url, $options = array()) {
//        $icon = array_key_exists('icon', $options) ? "<i class='{$options['icon']}'></i>" : null;
//        $options['escape'] = false;
//        return $this->isPermission($url) ? $this->Html->link($icon . __($title), $url, $options) : null;
        $icon = array_key_exists('icon', $options) ? $this->Bootstrap->icon($options['icon']) : null;
        $options['escape'] = false;
        return ($this->isPermission($url) && !is_null($url)) ? $this->Html->link($icon . __($title), $url, $options) : $this->Html->link($icon . __($title), 'javascript:;', $options);
    }

    /**
     * 
     * Function make the POST Link benerfit for action link only allow post method
     * @param   type $title as string of title on the link
     * @param   type $url as string of the destination path
     * @param   type $options as array of HTML option
     * @param   type $confirmMessage as string if has a confirmation message
     * @return  string of the output link
     */
    public function postLink($title = null, $url, $options = array(), $confirmMessage = false) {
        $icon = array_key_exists('icon', $options) ? "<span class='{$options['icon']}'></span>" : null;
        return $this->isPermission($url) ? $this->Form->postLink($icon . __($title), $url, $options, $confirmMessage) : null;
    }

    /**
     * 
     * Function get for the project plan action make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $status as string of current project status
     * @return  string of the output link
     */
    public function getProjectPlanActions($id, $status) {
        $list = "";
        $patern = '<li>%s</li>';
        $status = strtoupper($status);
        $list .= sprintf($patern, $this->link('View', "/ProjectPlans/view/{$id}"));
        $planData = ClassRegistry::init('ProjectPlan')->getProjectPlanById($id);
        $planData = empty($planData) ? array() : $planData['ProjectPlan'];
        $currentDepartmentId = $this->Zicure->getCurrenSessionDepartmentId();
        switch ($status) {
            case 'NL':
            case 'DL':
                $list .= sprintf($patern, $this->link('Edit Project Plan', "/ProjectPlans/edit/{$id}"));
                $list .= sprintf($patern, $this->link('Delete', "/ProjectPlans/delete/{$id}", array('class' => 'btnGroupMenu confirmModal', 'rel' => __('Are you sure you want to delete Project plan # %s ?'))));
                break;
            case 'WA'://wait for approved by cover department
                $mustBespc = ($status === 'WA') ? false : true;
                $canApproved = ClassRegistry::init('ProjectPlan')->countWaitApproved($id, $mustBespc);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/ProjectPlans/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                    $list .= sprintf($patern, $this->link('Approve', "/ProjectPlans/approve_task/{$id}"));
                    $list .= sprintf($patern, $this->link('Reject', "/ProjectPlans/reject_task/{$id}"));
                }
                break;
            case 'AL0'://wait for approved by the spc
                //$functionCount = ($status === 'WA') ? 'countWaitApproved' : 'countWaitApprovedBySpc';
                $mustBespc = ($status === 'WA') ? false : true;
                $canApproved = ClassRegistry::init('ProjectPlan')->countWaitApproved($id, $mustBespc);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/ProjectPlans/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                    $list .= sprintf($patern, $this->link('Approve', "/ProjectPlans/approve_task/{$id}"));
                    $list .= sprintf($patern, $this->link('Reject', "/ProjectPlans/reject_task/{$id}"));
                }

                $list .= sprintf($patern, $this->link('View Comment', "/ProjectPlans/view_comment/{$id}"));
                break;
            case 'WE0':
                $canEdit = ClassRegistry::init('ProjectPlan')->countCanEditPlan($id);
                if ($canEdit > 0) {
                    $list .= sprintf($patern, $this->link('Edit Project Plan', "/ProjectPlans/edit/{$id}"));
                }

                $list .= sprintf($patern, $this->link('View Comment', "/ProjectPlans/view_comment/{$id}"));
                break;
            case 'WE':
                $canEdit = ClassRegistry::init('ProjectPlan')->countCanEditPlan($id);
                if ($canEdit > 0) {
                    $list .= sprintf($patern, $this->link('Edit Project Plan', "/ProjectPlans/edit/{$id}"));
                }

                $canApproved = ClassRegistry::init('ProjectPlan')->countWaitApproved($id, false);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/ProjectPlans/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                }

//                if (!empty($planData['approved_department_id']) && ($planData['cover_department_id'] == $currentDepartmentId)) {
//                    $list .= sprintf($patern, $this->link('Sendback', "/ProjectPlans/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
//                }

                $list .= sprintf($patern, $this->link('View Comment', "/ProjectPlans/view_comment/{$id}"));
                break;
            case 'RN'://Renews
            case 'AL'://Approved

                $list .= sprintf($patern, $this->link('View Comment', "/ProjectPlans/view_comment/{$id}"));
                break;


            case 'RL'://Reject
            case 'CL'://Cancel
            case 'DE'://Delete
        }

        return $this->__getActionMenu($list);
    }

    /**
     * 
     * Function get for the project action make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $status as string of current project status
     * @return  string of the output link
     */
    public function getProjectActions($id, $status) {
        $list = "";
        $patern = '<li>%s</li>';
        $status = strtoupper($status);
        $list .= sprintf($patern, $this->link('View', "/Projects/view/{$id}"));
        $planData = ClassRegistry::init('Project')->getProjectById($id);
        $planData = empty($planData) ? array() : $planData['Project'];
        $currentDepartmentId = $this->Zicure->getCurrenSessionDepartmentId();
        switch ($status) {
            case 'NL':
            case 'DL':
                $list .= sprintf($patern, $this->link('Project Edit', "/Projects/edit/{$id}"));
                $list .= sprintf($patern, $this->link('Delete', "/Projects/delete/{$id}", array('class' => 'btnGroupMenu confirmModal', 'rel' => __('Are you sure you want to delete Project # %s ?', $id))));
                break;
            case 'WA'://wait for approved by cover department
                $mustBespc = ($status === 'WA') ? false : true;
                $canApproved = ClassRegistry::init('Project')->countWaitApproved($id, $mustBespc);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/Projects/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                    $list .= sprintf($patern, $this->link('Approve', "/Projects/approve_task/{$id}"));
                    $list .= sprintf($patern, $this->link('Reject', "/Projects/reject_task/{$id}"));
                }
                break;
            case 'AL0'://wait for approved by the spc
                //$functionCount = ($status === 'WA') ? 'countWaitApproved' : 'countWaitApprovedBySpc';
                $mustBespc = ($status === 'WA') ? false : true;
                $canApproved = ClassRegistry::init('Project')->countWaitApproved($id, $mustBespc);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/Projects/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                    $list .= sprintf($patern, $this->link('Approve', "/Projects/approve_task/{$id}"));
                    $list .= sprintf($patern, $this->link('Reject', "/Projects/reject_task/{$id}"));
                }

                $list .= sprintf($patern, $this->link('View Comment', "/Projects/view_comment/{$id}"));
                break;
            case 'WE0':
                $canEdit = ClassRegistry::init('Project')->countCanEdit($id);
                if ($canEdit > 0) {
                    $list .= sprintf($patern, $this->link('Project Edit', "/Projects/edit/{$id}"));
                }

                $list .= sprintf($patern, $this->link('View Comment', "/Projects/view_comment/{$id}"));
                break;
            case 'WE':
                $canEdit = ClassRegistry::init('Project')->countCanEdit($id);
                if ($canEdit > 0) {
                    $list .= sprintf($patern, $this->link('Project Edit', "/Projects/edit/{$id}"));
                }

                $canApproved = ClassRegistry::init('Project')->countWaitApproved($id, false);
                if ($canApproved > 0) {
                    $list .= sprintf($patern, $this->link('Sendback', "/Projects/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
                }

//                if (!empty($planData['approved_department_id']) && ($planData['cover_department_id'] == $currentDepartmentId)) {
//                    $list .= sprintf($patern, $this->link('Sendback', "/Projects/sandback_task/{$id}")); //ส่งงานกลับจาก process approve เพื่อให้แก้ไขเพิ่มเติม
//                }

                $list .= sprintf($patern, $this->link('View Comment', "/Projects/view_comment/{$id}"));
                break;
            case 'RN'://Renews
            case 'AL'://Approved

                $list .= sprintf($patern, $this->link('View Comment', "/Projects/view_comment/{$id}"));
                break;


            case 'RL'://Reject
            case 'CL'://Cancel
            case 'DE'://Delete
        }
        return $this->__getActionMenu($list);
    }

    /**
     * 
     * Function getAction make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $controller as string of link destination controller
     * @param   type $linkView as boolean of is show view link
     * @param   type $linkEdit as boolean of is show edit link
     * @param   type $linkDelete as boolean of is show delete link
     * @param   type $linkUnlock as boolean of is show unblock link
     * @return  string
     */
    public function getActions($id, $controller, $linkView = true, $linkEdit = true, $linkDelete = true, $linkUnlock = false) {
        $list = "";
        $patern = '<li>%s</li>';
        //$isAdmin = $this->isAdmin();
        $isAdmin = true;
        if ($linkView === true) {
            $list .= sprintf($patern, $this->link('View', "/" . $controller . "/view/{$id}"));
        }
        if (($linkEdit === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->link('Edit', "/" . $controller . "/edit/{$id}"));
        }
        if (($linkDelete === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->postLink('Delete', "/" . $controller . "/delete/{$id}", array('class' => 'action-delete btnGroupMenu'), __('Are you sure you want to delete # %s?', $id)));
//            $list .= sprintf($patern, $this->postLink('Delete', "/" . $controller . "/delete/{$id}", array('class' => 'action-delete-submit btnGroupMenu'), __('Are you sure you want to delete # %s?', $id)));
        }
        if (($linkUnlock === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->link('Unlock', "/" . $controller . "/unlock/{$id}"));
        }
        return $this->__getActionMenu($list);
    }

    /**
     * 
     * Function getAction make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $controller as string of link destination controller
     * @param   type $linkView as boolean of is show view link
     * @param   type $linkEdit as boolean of is show edit link
     * @param   type $linkDelete as boolean of is show delete link
     * @param   type $linkUnlock as boolean of is show unblock link
     * @return  string
     */
    public function getSecureActions($id, $controller, $linkView = true, $linkEdit = true, $linkDelete = true, $linkUnlock = false) {
        return $this->getActions($this->secureEncodeParam($id), $controller, $linkView, $linkEdit, $linkDelete, $linkUnlock);
    }

    /**
     * 
     * Function generate html link
     * @author  sarawutt.b
     * @param   $list as a string of menu list
     * @return  string of a output list menu
     */
    public function __getActionMenu($list = '') {
        $menu = '<div class="btn-group">
                <button class="btn bg-purple btn-flat col-xs-9" type="button"><i class="fa fa-gears hidden-xs"> </i>' . __('Action') . '</button>
                <button data-toggle="dropdown" class="btn bg-purple btn-flat dropdown-toggle col-xs-3" type="button" aria-expanded="true">
                  <span class="caret"></span>
                  <span class="sr-only">' . __('Toggle Dropdown') . '</span>
                </button>
                <ul role="menu" class="dropdown-menu">
                  ' . $list . '
                </ul>
              </div>';
        return $menu;
    }

    /**
     * 
     * Delete link button style used this for render delete button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkDelete($url, $options = array()) {
        return $this->link('Delete', $url, array_merge(array('icon' => 'fa fa-remove', 'class' => 'btn btn-danger confirmModal', 'rel' => __('Are you sure for delete ?')), $options));
    }

    /**
     * 
     * Delete link button style used this for render print button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkPrint($url, $options = array()) {
        return $this->link('Print', $url, array_merge(array('icon' => 'fa fa-print', 'class' => 'btn btn-warning display=none;'), $options));
    }

    /**
     * 
     * Delete link button style used this for render print button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function linkAttachmentFile($url, $options = array()) {
        return $this->link('Add Another File', $url, array_merge(array('icon' => 'fa fa-file-text', 'class' => 'btn btn-primary separate-button'), $options));
    }

    /**
     * 
     * Add / Plus link button style used this for render add / plus button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $class option may make other button style
     * @return string
     */
    public function linkAdd($url, $class = 'btn btn-success') {
        return $this->link('Add', $url, array('icon' => 'fa fa-plus', 'class' => $class));
    }

    /**
     * 
     * Add / Plus button style used this for render add / plus button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $class option may make other button style
     * @return string
     */
    public function buttonAdd($url, $class = 'btn btn-success') {
        return $this->isPermission($url) ? $this->Form->button('<i class="fa fa-plus"></i>' . __('Add'), array('type' => 'button', 'class' => 'btn btn-primary')) : null;
    }

    /**
     * 
     * Add / Plus submit button style used this for render add / plus submit button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function submitAdd($url, $options = array()) {
        return $this->isPermission($url) ? $this->Form->button('<i class="fa fa-plus"></i>' . __('Add'), array_merge(array('type' => 'submit', 'class' => 'btn btn-success', 'name' => 'btnSubmitAdd', 'id' => 'btnSubmitAdd'), $options)) : null;
    }

    /**
     * 
     * submit save button style used this for render add / plus submit button
     * @author sarawutt.b
     * @param type $url as string where destination link
     * @param type $options is array() of cake options style
     * @return string
     */
    public function submitSave($url, $options = array()) {
        return $this->isPermission($url) ? $this->Form->button('<i class="fa fa-save"></i>' . __('Save'), array_merge(array('name' => 'btnsubmit', 'id' => 'btnsubmit', 'class' => 'btn btn-primary'), $options)) : null;
    }

    /**
     * 
     * Buton added for Document Attachment input
     * @author sarawutt.b
     * @param type $options is array() of cake options style
     * @return string
     */
    public function buttonAttachmentFile($options = array()) {
        return $this->Form->button('<i class="fa fa-file-text"></i>' . __('Add Another File'), array_merge(array('name' => 'btnAddAttachment', 'id' => 'btnAddAttachment', 'class' => 'btn btn-success', 'type' => 'button'), $options));
    }

    /**
     * 
     * Function getAction make and get main action in master index page (list page) action view|edit|delete
     * @author  sarawutt.b
     * @param   type $id as mix of link id with link params
     * @param   type $controller as string of link destination controller
     * @param   type $linkView as boolean of is show view link
     * @param   type $linkEdit as boolean of is show edit link
     * @param   type $linkDelete as boolean of is show delete link
     * @param   type $linkUnlock as boolean of is show unblock link
     * @return  string
     */
    public function getActionDelete($id, $controller, $model = null, $sector = 'A', $linkView = true, $linkEdit = true, $linkDelete = true, $linkUnlock = false) {
        $list = "";
        $patern = '<li>%s</li>';
        //$isAdmin = $this->isAdmin();
        $isAdmin = true;
        if ($linkView === true) {
            $list .= sprintf($patern, $this->link('View', "/" . $controller . "/view/{$id}"));
        }
        if (($linkEdit === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->link('Edit', "/" . $controller . "/edit/{$id}"));
        }
        if (($linkDelete === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->postLink('Delete', "/" . $controller . "/delete/{$id}/{$model}/{$sector}", array('class' => 'action-delete btnGroupMenu'), __('Are you sure you want to delete # %s?', $id)));
        }
        if (($linkUnlock === true) && ($isAdmin === true)) {
            $list .= sprintf($patern, $this->link('Unlock', "/" . $controller . "/unlock/{$id}"));
        }
        return $this->__getActionMenu($list);
    }

}

?>
