<div class="users index">
    <?php echo $this->element('searchOrg'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-active">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('User_in_org'); ?></h3>
                </div>
                <div class="box-body">
                    <!-- <div align="center"> -->
                    <table cellpadding="0" cellspacing="0" class="table-short-information" style="width:100%">
                        <thead>
                            <tr style="background-color: rgb(207, 207, 207);">
                                <th rowspan="2" class="nindex"><?php echo __('#'); ?></th>
                                <th rowspan="2" style="text-align: center;"><?php echo $this->Paginator->sort(__('org_name')); ?></th>
                                <th colspan="2" style="text-align: center;"><?php echo $this->Paginator->sort(__('User_in_system')); ?></th>
                                <th rowspan="2" style="text-align: center;"><?php echo $this->Paginator->sort('alluser'); ?></th>
                            </tr>
                            <tr style="background-color: rgb(207, 207, 207);">
                                <th style="text-align: center;" ><?php echo $this->Paginator->sort(__('admin')); ?></th>
                                <th style="text-align: center;"><?php echo $this->Paginator->sort('user'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($departments as $k => $department): ?> 
                                <tr>
                                    <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                                    <td><?php echo h($department['Department']['full_name']); ?></td>
                                    <td style="text-align: center;"><?php echo h($this->Zicure->countAdmin($department['Department']['id'])); ?></td>
                                    <td style="text-align: center;"><?php echo h($this->Zicure->countUser($department['Department']['id'])); ?></td>
                                    <td style="text-align: center;"><?php echo h($this->Zicure->countAlluser($department['Department']['id'])); ?></td>
                                </tr> 
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php echo $this->element('paginate_pages', array('options' => true)); ?>
                </div> <!-- end col md 9 -->
            </div><!-- end row -->
        </div><!-- end containing of content -->
    </div><!-- end row -->
</div>