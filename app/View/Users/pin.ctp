<div class="login-bg">
    <div class="login-space">&nbsp;</div>
    <div class="logo-name"><img src="/img/oac-logo.png" class="img-responsive"></div>
    <div class="container-fluid">
        <div class="login-row">
            <div class="login-box-bg">
                <form method="post" action="/Users/pin">
                    <div class="form-group">
                        <label for="passwordInput"><?php echo __('PIN'); ?>:</label>
                        <input type="password" name="data[User][password2]" class="form-control" id="passwordInput" placeholder=<?php echo __('Please input Pin'); ?>>
                    </div>
                    <button type="submit" value="Login" class="btn btn-sp"><?php echo __('Submit'); ?></button>
                </form>                
            </div>
        </div>
    </div>
</div>