<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Add User')); ?>
    <div class="box-body">
        <div class="users form">
            <?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'role' => 'form', 'type' => 'file')); ?>
            <?php echo $this->Form->input('username', array('class' => 'required', 'placeholder' => __('Username'))); ?>
            <?php echo $this->Form->input('password', array('class' => 'required ckpassword', 'placeholder' => __('Password'))); ?>
            <?php echo $this->Form->input('confirm_password', array('class' => 'required ', 'placeholder' => __('Confirm Password'), 'type' => 'password', 'equalTo' => '#UserPassword')); ?>
            <?php echo $this->Form->input('password2', array('class' => 'required digits', 'placeholder' => __('Password'), 'minLength' => 6, 'type' => 'password')); ?>
            <?php echo $this->Form->input('confirm_password2', array('class' => 'required', 'placeholder' => __('Confirm Password2'), 'type' => 'password', 'equalTo' => '#UserPassword2')); ?>
            <?php echo $this->Form->input('name_prefix_id', array('class' => 'required', 'options' => $this->Zicure->ListPrefix())); ?>
            <?php echo $this->Form->input('first_name', array('class' => 'required', 'placeholder' => __('First Name'))); ?>
            <?php echo $this->Form->input('last_name', array('class' => 'required', 'placeholder' => __('Last Name'))); ?>
            <?php echo $this->Form->input('personal_card_no', array('class' => 'required xcitizen_no', 'placeholder' => __('Personal Card No'))); ?>
            <?php echo $this->Form->input('military_no', array('class' => 'required digits', 'length' => '10')); ?>
            <?php echo $this->Form->input('sex', array('placeholder required' => __('Sex'), 'type' => 'select', 'options' => $this->Zicure->getSex())); ?>
            <?php echo $this->Form->input('birth_date', array('class' => 'required datepicker', 'type' => 'text', 'placeholder' => __('Age'))); ?>
            <?php echo $this->Form->input('phone_no', array('class' => 'required digits', 'placeholder' => __('Phone No'))); ?>
            <?php echo $this->Form->input('email', array('class' => 'email', 'placeholder' => __('Email'))); ?>
            <?php echo $this->Form->input('address', array('class' => 'required', 'placeholder' => __('Address'))); ?>
            <?php echo $this->Form->input('province_id', array('id' => 'ProvinceId', 'class' => 'required', 'options' => $this->Zicure->ListProvince())); ?>
            <?php echo $this->Form->input('district_id', array('id' => 'DistrictId', 'class' => 'required', 'placeholder' => __('District Id'))); ?>
            <?php echo $this->Form->input('sub_district_id', array('id' => 'SubDistrictId', 'class' => 'required', 'placeholder' => __('Sub District Id'))); ?>
            <?php echo $this->Form->input('zip_code', array('id' => 'zipCode', 'class' => 'required', 'placeholder' => __('Zip Code'), 'readonly' => true)); ?>
            <?php echo $this->Form->input('department_id', array('class' => 'required', 'options' => $this->Zicure->listDepartment())); ?>
            <?php echo $this->Form->input('position_id', array('class' => 'required', 'options' => $this->Zicure->ListPosition())); ?>
            <?php echo $this->Form->input('role_id', array('class' => 'required', 'options' => $this->Zicure->ListRole())); ?>
            <?php echo $this->Form->input('picture_path', array('class' => 'simple required', 'accept' => 'image/*', 'label' => __('Profile Picture'), 'type' => 'file')); ?>
            <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        getDistrict();
        $("#ProvinceId").change(function () {
            getDistrict();
        });
        $("#DistrictId").change(function () {
            getSubDistrict();
        });
        $("#SubDistrictId").change(function () {
            getZipcode();
        });
    });
</script>
