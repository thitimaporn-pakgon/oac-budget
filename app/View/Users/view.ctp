<?php
$isAdmin = $this->Zicure->isAdmin();
$isOwner = ($this->Zicure->getCurrentSessionUserId() == $this->data['User']['id']);
$disabled = (!$isAdmin && !$isOwner);
?>
<div class="row-fluid users form">
    <div class="box box-warning">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit User')); ?>
        <div class="box-body">
            <div class="col-md-6">
                <br>
                <?php echo $this->Form->create('User', array('role' => 'form', 'type' => 'file', 'url' => '/Users/changpicture/' . $id)); ?>
                <?php echo $this->Form->input('id', array('class' => 'required')); ?>
                <?php echo $this->Html->image($this->data['User']['picture_path'], array('class' => 'profile-img')); ?>
                <?php echo $this->Form->input('picture_path', array('class' => 'form-control', 'accept' => 'image/*', 'label' => __('Profile Picture'), 'type' => 'file', 'disabled' => $disabled)); ?>
                <?php echo $this->Permission->submit(__('Change Picture'), "/Users/view/{$id}", array('class' => 'simple btn btn-primary confirmModal', 'rel' => __('Are you sure for change the profile picture ?'), 'disabled' => true, 'id' => 'btnSubmitChangePicture')); ?>
                <?php echo $this->Permission->buttonBack('normal'); ?>
                <?php echo $this->Form->end(); ?>
                <br>
                <?php echo $this->Form->create('User', array('url' => '/Users/changpassword/' . $id)); ?>
                <?php echo $this->Form->input('id', array('class' => 'required')); ?>
                <?php echo $this->Form->input('username', array('class' => 'required', 'readonly' => true)); ?>
                <?php echo $this->Form->input('password', array('label' => array('text' => __('New Password')), 'class' => 'required ckpassword', 'placeholder' => __('New Password'), 'minLength' => 8, 'value' => '', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('confirm_password', array('class' => 'required', 'type' => 'password', 'equalTo' => '#UserPassword', 'placeholder' => __('Confirm Password'), 'readonly' => $disabled)); ?>                                        
                <?php echo $this->Permission->submit(__('Change Password'), '/Users/changpassword/' . $id, array('class' => 'btn btn-warning confirmModal', 'rel' => __('Are you sure for chang your password ?'), 'disabled' => true, 'id' => 'btnSubmitChangePassword')); ?>
                <?php echo $this->Permission->buttonBack('normal'); ?>
                <?php echo $this->Form->end(); ?>
                <br>
                <?php echo $this->Form->create('User', array('url' => '/Users/changpin/' . $id)); ?>
                <?php echo $this->Form->input('id', array('class' => 'required')); ?>
                <?php echo $this->Form->input('password2', array('label' => array('text' => __('New PIN')), 'class' => 'required', 'placeholder' => __('New PIN'), 'type' => 'password', 'minLength' => 6, 'value' => '', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('confirm_password2', array('class' => 'required', 'label' => array('text' => __('Confirm PIN')), 'type' => 'password', 'equalTo' => '#UserPassword2', 'placeholder' => __('Confirm PIN'), 'readonly' => $disabled)); ?>                                        
                <?php echo $this->Permission->submit(__('Change PIN'), '/Users/changpin/' . $id, array('class' => 'btn btn-success confirmModal', 'rel' => __('Are you sure for change your PIN ?'), 'disabled' => true, 'id' => 'btnSubmitChangePIN')); ?>
                <?php echo $this->Permission->buttonBack('normal'); ?>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="col-md-6">
                <br>
                <!-- INSERT YOURR CODE HEARE-->
                <?php echo $this->Form->create('User', array('url' => '/Users/changprofile/' . $id)); ?>
                <?php echo $this->Form->input('id', array('class' => 'required')); ?>
                <?php echo $this->Form->input('name_prefix_id', array('class' => 'required', 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('position_id', array('class' => 'required', 'disabled' => !$isAdmin)); ?>
                <?php echo $this->Form->input('role_id', array('class' => 'required', 'disabled' => !$isAdmin)); ?>
                <?php echo $this->Form->input('department_id', array('class' => 'required', 'disabled' => !$isAdmin)); ?>
                <?php echo $this->Form->input('first_name', array('class' => 'required', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('last_name', array('class' => 'required', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('military_no', array('class' => 'required digits', 'length' => '10')); ?>
                <?php echo $this->Form->input('personal_card_no', array('class' => 'required xcitizen_no', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('sex', array('class' => 'required', 'type' => 'select', 'options' => $this->Zicure->getSex(), 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('birth_date', array('class' => 'required datepicker', 'type' => 'text', 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('phone_no', array('class' => 'required digits', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('email', array('class' => 'email', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('address', array('class' => 'required', 'readonly' => $disabled)); ?>
                <?php echo $this->Form->input('province_id', array('id' => 'ProvinceId', 'class' => 'required', 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('district_id', array('id' => 'DistrictId', 'class' => 'required', 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('sub_district_id', array('id' => 'SubDistrictId', 'class' => 'required', 'disabled' => $disabled)); ?>
                <?php echo $this->Form->input('status', array('class' => 'form-control required', 'type' => 'select', 'options' => $this->Zicure->MainStatus(), 'disabled' => !$isAdmin)); ?>                
                <?php echo $this->Permission->submit(__('Change Profile'), '/Users/changprofile/' . $id, array('class' => 'simple btn btn-primary confirmModal', 'rel' => __('Are you sure change for your profile ?'), 'disabled' => $disabled)); ?>
                <?php echo $this->Permission->buttonBack('normal'); ?>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        ckChangeProfilePicture();
        ckChangePassword();
        ckChangePIN();
        //Check and enable for button change password
        $("#UserPassword,#UserConfirmPassword").keyup(function () {
            ckChangePassword();
        });

        //Check and enable for button change PIN
        $("#UserPassword2,#UserConfirmPassword2").keyup(function () {
            ckChangePIN();
        });

        //Check and enable button for submit change user profile picture
        $("#UserPicturePath").change(function () {
            ckChangeProfilePicture();

        });
        // getDistrict();
        $("#ProvinceId").change(function () {
            getDistrict();
        });
        $("#DistrictId").change(function () {
            getSubDistrict();
        });
        $("#SubDistrictId").change(function () {
            getZipcode();
        });
    });
    //Check and enable button for submit change user profile picture
    function ckChangeProfilePicture() {
        $('#btnSubmitChangePicture').attr('disabled', !($('#UserPicturePath').val() != ''));
    }
    //Check and enable for button change password
    function ckChangePassword() {
        $("#btnSubmitChangePassword").attr('disabled', !(($("#UserPassword").val() != '') && ($("#UserConfirmPassword").val() != '')));
    }
    //Check and enable for button change PIN
    function ckChangePIN() {
        $("#btnSubmitChangePIN").attr('disabled', !(($("#UserPassword2").val() != '') && ($("#UserConfirmPassword2").val() != '')));
    }
</script>