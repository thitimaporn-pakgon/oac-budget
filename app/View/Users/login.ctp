<?php if (Configure::read('CORE.ENABLED.AUTH.RECAPCHA') === true): ?>
    <script src="https://www.google.com/recaptcha/api.js?hl=th" async defer></script>
    <script type="text/javascript">
        var onloadCallback = function () {
            grecaptcha.render('html_element', {
                'sitekey': '<?php echo Configure::read('CORE.PROJECT.SITEKEY'); ?>'
            });
        };
    </script>
<?php endif; ?>
<div class="login-bg">
    <div class="login-space">&nbsp;</div>
    <div class="logo-name"><img src="/img/oac-logo.png" class="img-responsive"></div>
    <div class="container-fluid">
        <div class="login-row">
            <div class="login-box-bg">
                <form method="post" action="/Users/login" id="frmUserLogin">
                    <div class="form-group">
                        <label for="usernameInput"><?php echo __('Username'); ?>:</label>
                        <input type="text" name="data[User][username]" class="form-control" id="usernameInput" placeholder=<?php echo __('Username'); ?>>
                    </div>
                    <div class="form-group">
                        <label for="passwordInput"><?php echo __('Password'); ?>:</label>
                        <input type="password" name="data[User][password]" class="form-control" id="passwordInput" placeholder=<?php echo __('Password'); ?>>
                    </div>
                    <?php if (Configure::read('CORE.ENABLED.AUTH.RECAPCHA') === true): ?>
                        <div class="form-group">
                            <div align="center">
                                <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('CORE.PROJECT.SITEKEY'); ?>" data-callback="enableBtnSubmitLogin"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <button type="submit" value="Login" class="btn btn-sp" id="btnSubmitLogin" <?php echo (Configure::read('CORE.ENABLED.AUTH.RECAPCHA') === true) ? 'disabled="disabled"' : null; ?>><?php echo __('Submit'); ?></button>
                </form>				
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function enableBtnSubmitLogin() {
        document.getElementById("btnSubmitLogin").disabled = false;
        $("#frmUserLogin").submit();
        return true;
    }
</script>
