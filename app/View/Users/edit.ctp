<div class="users form">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Edit User')); ?>
    <?php echo $this->Form->create('User', array('role' => 'form', 'type' => 'file')); ?>
    <?php echo $this->Form->input('id', array('class' => 'required', 'placeholder' => 'Id')); ?>
    <?php echo $this->Form->input('username', array('class' => 'required', 'placeholder' => __('Username'), 'readonly' => true)); ?>
    <?php echo $this->Form->input('password', array('class' => 'required', 'placeholder' => __('Password'), 'minLength' => 8)); ?>
    <?php echo $this->Form->input('name_prefix_id', array('class' => 'required', 'placeholder' => __('Name Prefix Id'))); ?>
    <?php echo $this->Form->input('position_id', array('class' => 'required', 'placeholder' => __('Position Id'))); ?>
    <?php echo $this->Form->input('role_id', array('class' => 'required', 'placeholder' => __('Role Id'))); ?>
    <?php echo $this->Form->input('department_id', array('class' => 'required', 'placeholder' => 'Department Id')); ?>
    <?php echo $this->Form->input('first_name', array('class' => 'required', 'placeholder' => __('First Name'))); ?>
    <?php echo $this->Form->input('last_name', array('class' => 'required', 'placeholder' => __('Last Name'))); ?>
    <?php echo $this->Form->input('military_no', array('class' => 'required digits', 'length' => '10')); ?>
    <?php echo $this->Form->input('personal_card_no', array('class' => 'required xcitizen_no', 'placeholder' => __('Personal Card No'))); ?>
    <?php echo $this->Form->input('sex', array('class' => 'required', 'placeholder required' => __('Sex'), 'type' => 'select', 'options' => $this->Zicure->getSex())); ?>
    <?php echo $this->Form->input('age', array('class' => 'required digits', 'type' => 'text', 'placeholder' => __('Age'))); ?>
    <?php echo $this->Form->input('phone_no', array('class' => 'required digits', 'placeholder' => __('Phone No'))); ?>
    <?php echo $this->Form->input('email', array('class' => 'email', 'placeholder' => __('Email'))); ?>
    <?php echo $this->Form->input('address', array('class' => 'required', 'placeholder' => 'Address')); ?>
    <?php echo $this->Form->input('province_id', array('id' => 'ProvinceId', 'class' => 'required', 'data-placeholder' => __('Choose a Province...'))); ?>
    <?php echo $this->Form->input('district_id', array('id' => 'DistrictId', 'class' => 'required', 'placeholder' => __('District Id'))); ?>
    <?php echo $this->Form->input('sub_district_id', array('id' => 'SubDistrictId', 'class' => 'required', 'placeholder' => __('Sub District Id'))); ?>
    <?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder required' => __('Status'), 'type' => 'select', 'options' => $this->Zicure->MainStatus())); ?>                
    <?php echo $this->Html->image($this->data['User']['picture_path'], array('class' => 'profile-img')); ?>
    <?php echo $this->Form->input('picture_path', array('class' => 'form-control', 'accept' => 'image/*', 'label' => __('Picture'), 'type' => 'file')); ?>            
    <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
    <?php echo $this->Form->end() ?>
</div>

<script type="text/javascript">

    $(function () {
        // getDistrict();
        $("#ProvinceId").change(function () {
            getDistrict();
        });
        $("#DistrictId").change(function () {
            getSubDistrict();
        });
        $("#SubDistrictId").change(function () {
            getZipcode();
        });
    });
</script>