<div class="users index">
    <?php echo $this->element('searchUser'); ?>
    <div class="box box-active">
        <?php echo $this->element('boxOptionHeader', array('btitle' => __('User (Result)'))); ?>
        <div class="box-body">
            <table class="table-short-information table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('username'); ?></th>
                        <th><?php echo $this->Paginator->sort('first_name', __('User full name')); ?></th>
                        <th><?php echo $this->Paginator->sort('phone_no'); ?></th>
                        <th><?php echo $this->Paginator->sort('department_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('role_id'); ?></th>
                        <th><?php echo $this->Paginator->sort(__('loginfail')); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($users)): ?>
                        <?php foreach ($users as $k => $user): ?>
                            <tr>
                                <td class="nindex"><?php echo $this->Paginator->counter("{:start}") + $k; ?></td>
                                <td><?php echo h($user['User']['username']); ?></td>
                                <td><?php echo h($user['User']['first_name'] . ' ' . $user['User']['last_name']); ?></td>
                                <td><?php echo h($user['User']['phone_no']); ?></td>
                                <td><?php echo h(@$departments[$user['User']['department_id']]); ?></td>
                                <td><?php echo h(@$roles[$user['User']['role_id']]); ?></td>
                                <?php
                                if ($user['User']['loginfail'] < '4') {
                                    ?>
                                    <td><span class="label label-success"><?php echo __('Use') ?></span></td>
                                    <?php
                                } else if ($user['User']['loginfail'] >= '4') {
                                    ?>
                                    <td><span class="label label-danger"><?php echo __('locked') ?></span></td>
                                    <?php
                                }
                                ?>
                                
                                <td><?php echo $this->Zicure->mainStatus($user['User']['status'], true); ?></td>
                                <td class="actions">
                                    <?php echo $this->Permission->getActions($user['User']['id'], 'users', true, false, true, true); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo '<tr class="text-center notfound"><td colspan="9">' . __('Not found data') . '</td></tr>'; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- ./box-body -->
        <?php echo $this->element('paginate_pages', array('options' => true)); ?>
    </div>
</div>