<!-- Horizontal Form -->
<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Position Management System')); ?>
    <?php
    echo $this->Form->create('Search', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'col-sm-6 form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-3 control-label'),
            'between' => '<div class="col-sm-9">',
            'wrap'=>'col-sm-12',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('name', array('label' => array('text' => __('Position name')))); ?>
        <?php echo $this->Form->input('description'); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/Positions/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add Data"), "/Positions/add/"); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->