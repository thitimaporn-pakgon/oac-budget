<?php
/**
 *
 * Budget List Element used for insert/update/show information of all expend list in your params
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'ข้อมูลทั่วไป ขออนุมัติเงินงวด';
$bclass = isset($bclass) ? $bclass : 'box-info';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;
?>
<div class="box <?php echo $bclass;?>">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('ViewOnly', array('url' => '#', 'role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">

        <?php echo $this->Form->input('budget_code', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Budget Code') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Project->convertBudgetCode($this->request->data['ExpenseHeaderManage']))); ?>
        <?php echo $this->Form->input('budget_balance', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Budget Balance') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Zicure->numberFormat($this->request->data['ExpenseHeaderManage']['budget_balance']))); ?>

        <?php echo $this->Form->input('work_group_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Work Group') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->request->data['WorkGroupManage']['name'])); ?>
        <?php echo $this->Form->input('main_department_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Main Department Name') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Zicure->findDepartmentShortName($this->request->data['WorkGroupManage']['from_department_id']))); ?>

        <?php echo $this->Form->input('work_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Work') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->request->data['WorkManage']['name'])); ?>
        <?php echo $this->Form->input('sub_department_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Sub Department Name') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Zicure->findDepartmentShortName($this->request->data['WorkManage']['from_department_id']))); ?>

        <?php echo $this->Form->input('project_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Projects') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->request->data['ProjectManage']['name'])); ?>
        <?php echo $this->Form->input('practice_department_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Practice Department Name') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Zicure->findDepartmentShortName($this->request->data['ProjectManage']['from_department_id']))); ?>

        <?php echo $this->Form->input('project_list_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Project List') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->request->data['ProjectListManage']['name'])); ?>
        <?php echo $this->Form->input('owner_department_name', array('type' => 'text', 'div' => 'col-sm-6 form-group', 'label' => false, 'before' => '<div class="col-sm-4 control-label text-bold">' . __('Owner Department Name') . '</div>', 'wrap' => 'col-sm-8', 'readonly' => true, 'value' => $this->Zicure->findDepartmentShortName($this->request->data['ProjectListManage']['from_department_id']))); ?>

    </div>

    <div class="box-footer">
        <?php echo $this->Permission->buttonBack(null, "btn btn-warning btn-flat pull-right separate-button"); ?>
        <?php //echo $this->Permission->submit(__('บันทึก'), $url, array('class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('คุณต้องการยืนยันการทำงาน ?'), 'data-confirm-title' => __('ยืนยันการทำงาน'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>