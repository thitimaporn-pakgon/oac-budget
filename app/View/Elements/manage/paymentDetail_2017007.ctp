<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'การขออนุมัติครั้งนี้';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;

$budgetYear = isset($budgetYear) ? $budgetYear : $this->Project->getCurrenBudgetYearTH();
$budgetCode = isset($budgetCode) ? $budgetCode : '0000000000000';
$process = isset($process) ? strtoupper($process) : 'U';

$expenseLists = $this->Project->findExpenseList();
$ExpendDetailInfos = $this->Project->getDataExpenseBudget($budgetCode, substr($budgetYear, 2), $process);
$_totalYear2 = $_totalYear1 = $_totalPrimary = $_totalSecondary = $_totalSumbudget = 0;

$expendListKey = isset($ExpendDetailInfos[$budgetYear]) ? key($ExpendDetailInfos[$budgetYear]) : false;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;

$rowIndex = 0;
$sumBudgetList = array();

$budgetKeyLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10,m' => 0, 'budget_landbuild_more10,m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
$budgetSumLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10,m' => 0, 'budget_landbuild_more10,m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
?>
<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <div class="box-body">
        <div id="tablePaymentDetail" class="table-responsive no-padding">
            <table class="table table-hover rta-budget-detail table-short-information" id="tablePaymentDetailList">
                <thead>
                    <tr>
                        <th class="button-actions"></th>
                        <th class="text-left"><?php echo __('หน่วยเจ้าของงบประมาณ'); ?></th>
                        <th class="text-left"><?php echo __('หน่วยถืองบประมาณ'); ?></th>
                        <th class="budget-code text-left"><?php echo __('รหัสงบประมาณ ทบ.'); ?></th>


                        <th colspan="4" class="text-center"><?php echo __('งบบุคลากร'); ?></th>


                        <th colspan="2" class="text-center"><?php echo __('งบเงินอุดหนุน'); ?></th>


                        <th colspan="4" class="text-center"><?php echo __('งบลงทุน'); ?></th>

                        <th colspan="4" class="text-center"><?php echo __('งบดำเนินงาน'); ?></th>
                        <th colspan="8"></th>
                        <th class="text-center"><?php echo __('รวมทั้งสิ้น'); ?></th>
                    </tr>
                    <tr>
                        <th colspan="4"></th>


                        <th class="text-center"><?php echo __('เงินเดือน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างประจำ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างชั่วคราว'); ?></th>
                        <th class="text-center"><?php echo __('ค่าตอบแทนพนักงานราชการ'); ?></th>


                        <th class="text-center"><?php echo __('เงินอุดหนุนทั่วไป'); ?></th>
                        <th class="text-center"><?php echo __('เงินอุดหนุนเฉพาะกิจ'); ?></th>


                        <th colspan="2" class="text-center"><?php echo __('ค่าครุภัณฑ์'); ?></th>

                        <th colspan="2" class="text-center"><?php echo __('ที่ดินและสิ่งก่อสร้าง'); ?></th>

                        <th class="text-center"><?php echo __('ค่าตอบแทน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสาธารณูปโภค'); ?></th>
                        <th colspan="5" class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th colspan="3" class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th colspan="10"></th>


                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 1 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 1 ล้านขึ้นไป'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 10 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 10 ล้านขึ้นไป'); ?></th>
                        <th colspan="4"></th>
                        <th class="text-center"><?php echo __('ค่าอาหาร'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเบี้ยเลี้ยง'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเช่าที่พัก'); ?></th>
                        <th class="text-center"><?php echo __('ค่าพาหนะ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าซ่อมแซมทรัพย์สิน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.3'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.5'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุอื่น'); ?></th>
                        <th></th>
                    </tr>



                    <tr class="prev-summation">
                        <th colspan="4" class="text-right"><?php echo __('ยอดคงเหลือ'); ?></th>

                        <?php //for ($i = 0; $i < 22; $i++): ?>
<!--                            <th class="text-right"><?php //echo $this->Zicure->numberFormat(0);         ?></th>-->
                        <?php //endfor; ?>

                        <!-- List Expense Detail List -->
                        <?php
                        $newestExpenseDetailId = max(array_keys($this->request->data['ExpenseDetailManage']));
                        //debug(max(array_keys($this->request->data['ExpenseDetailManage'])));exit;
                        ?>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_salary']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_regular_wage']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_tmp_wage']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_offical_payment']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_general_grants']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_seperate_grants']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_goods_less1m']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_goods_more1m']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_landbuild_less10m']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_landbuild_more10m']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_payment']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_cost']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_material']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_utility']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_food']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_allowances']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_rent_accomondation']); ?></th>


                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_vehicle']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_repairs']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_sp3']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_sp5']); ?></th>
                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId]['budget_other_materials']); ?></th>

                        <th class="text-right"><?php echo $this->Zicure->currencyFormat($this->request->data['ExpenseHeaderManage']['budget_balance']); ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
//                        $i = 0;
//                        $budgetKeys = array_keys($this->request->data['ListDetailManage'][0]);
//                        foreach ($this->request->data['ListDetailManage'] as $k => $v):
//                            debug($budgetKeys[$i]);exit;
//                            if (strpos(array_keys($v)[$i], 'budget') !== false) {
//                                debug('xxxx');
//                                exit;
//                                $sumBudgetList[$k] += $v;
//                            }
                    ?>
                    <?php
                    //debug($this->request->data['ListDetailManage']);exit;
                    if (!empty($this->request->data['ListDetailManage'])):

//                        $budgetKeys = array_keys($this->request->data['ListDetailManage'][0]);
//                        //pr($budgetKeys);exit;
//                        $x = array();
//                        foreach ($budgetKeys as $k => $v) {
//                            if (strpos($v, 'budget') !== false) {
//                                $x[$v] = 0;
//                            }
//                        }
//                        pr($x);
//                        exit;
//                        extract($this->request->data['ListDetailManage'][0]);
//                        
//                        debug($budget_salary);exit;
                        //debug(strpos('budget_seperate_grants','budget') !== false);exit;
                        ?>
                        <?php
                        $total_budget_salary = $total_budget_regular_wage = $total_budget_tmp_wage = $total_budget_offical_payment = $total_budget_general_grants = $total_budget_seperate_grants = $total_budget_goods_less1m = $total_budget_goods_more1m = $total_budget_landbuild_less10m = $total_budget_landbuild_more10m = $total_budget_payment = $total_budget_cost = $total_budget_material = $total_budget_utility = $total_budget_food = $total_budget_allowances = $total_budget_rent_accomondation = $total_budget_vehicle = $total_budget_repairs = $total_budget_sp3 = $total_budget_sp5 = $total_budget_other_materials = $total_budget_total = 0;

                        foreach ($this->request->data['ListDetailManage'] as $k => $v):
                            $total_budget_salary += $v['budget_salary'];
                            $total_budget_regular_wage += $v['budget_regular_wage'];
                            $total_budget_tmp_wage += $v['budget_tmp_wage'];
                            $total_budget_offical_payment += $v['budget_offical_payment'];
                            $total_budget_general_grants += $v['budget_general_grants'];
                            $total_budget_seperate_grants += $v['budget_seperate_grants'];
                            $total_budget_goods_less1m += $v['budget_goods_less1m'];
                            $total_budget_goods_more1m += $v['budget_goods_more1m'];
                            $total_budget_landbuild_less10m += $v['budget_landbuild_less10m'];
                            $total_budget_landbuild_more10m += $v['budget_landbuild_more10m'];
                            $total_budget_payment += $v['budget_payment'];
                            $total_budget_cost += $v['budget_cost'];
                            $total_budget_material += $v['budget_material'];
                            $total_budget_utility += $v['budget_utility'];
                            $total_budget_food += $v['budget_food'];
                            $total_budget_allowances += $v['budget_allowances'];
                            $total_budget_rent_accomondation += $v['budget_rent_accomondation'];
                            $total_budget_vehicle += $v['budget_vehicle'];
                            $total_budget_repairs += $v['budget_repairs'];
                            $total_budget_sp3 += $v['budget_sp3'];
                            $total_budget_sp5 += $v['budget_sp5'];
                            $total_budget_other_materials += $v['budget_other_materials'];
                            $total_budget_total += $v['budget_total'];
                            ?>
                            <tr>
                                <td class="button-actions"><?php echo $this->Permission->button($this->Bootstrap->icon('fa-trash', __('Delete')), null, array('class' => 'btn btn-sm btn-flat btn-danger', 'disabled' => $disabled)); ?></td>
                                <td><?php echo $this->Form->input('from_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][from_department_id]", 'id' => false, 'div' => false, 'label' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['owner_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo $this->Form->input('to_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][to_department_id]", 'id' => false, 'div' => false, 'label' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['hold_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo "{$v['prefix_code']}{$v['suffix_code']}"; ?></td>

                                <!-- Budget Detail List Section -->
                                <td><?php echo $this->Form->input('budget_salary', array('name' => "data[ListDetailManage][{$rowIndex}][budget_salary]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_salary'])); ?></td>
                                <td><?php echo $this->Form->input('budget_regular_wage', array('name' => "data[ListDetailManage][{$rowIndex}][budget_regular_wage]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_regular_wage'])); ?></td>
                                <td><?php echo $this->Form->input('budget_tmp_wage', array('name' => "data[ListDetailManage][{$rowIndex}][budget_tmp_wage]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_tmp_wage'])); ?></td>
                                <td><?php echo $this->Form->input('budget_offical_payment', array('name' => "data[ListDetailManage][{$rowIndex}][budget_offical_payment]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_offical_payment'])); ?></td>
                                <td><?php echo $this->Form->input('budget_general_grants', array('name' => "data[ListDetailManage][{$rowIndex}][budget_general_grants]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_general_grants'])); ?></td>
                                <td><?php echo $this->Form->input('budget_seperate_grants', array('name' => "data[ListDetailManage][{$rowIndex}][budget_seperate_grants]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_seperate_grants'])); ?></td>
                                <td><?php echo $this->Form->input('budget_goods_less1m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_goods_less1m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_goods_less1m'])); ?></td>
                                <td><?php echo $this->Form->input('budget_goods_more1m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_goods_more1m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_goods_more1m'])); ?></td>
                                <td><?php echo $this->Form->input('budget_landbuild_less10m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_landbuild_less10m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_landbuild_less10m'])); ?></td>
                                <td><?php echo $this->Form->input('budget_landbuild_more10m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_landbuild_more10m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_landbuild_more10m'])); ?></td>
                                <td><?php echo $this->Form->input('budget_payment', array('name' => "data[ListDetailManage][{$rowIndex}][budget_payment]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_payment'])); ?></td>
                                <td><?php echo $this->Form->input('budget_cost', array('name' => "data[ListDetailManage][{$rowIndex}][budget_cost]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_cost'])); ?></td>
                                <td><?php echo $this->Form->input('budget_material', array('name' => "data[ListDetailManage][{$rowIndex}][budget_material]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_material'])); ?></td>
                                <td><?php echo $this->Form->input('budget_utility', array('name' => "data[ListDetailManage][{$rowIndex}][budget_utility]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_utility'])); ?></td>
                                <td><?php echo $this->Form->input('budget_food', array('name' => "data[ListDetailManage][{$rowIndex}][budget_food]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_food'])); ?></td>
                                <td><?php echo $this->Form->input('budget_allowances', array('name' => "data[ListDetailManage][{$rowIndex}][budget_allowances]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_allowances'])); ?></td>
                                <td><?php echo $this->Form->input('budget_rent_accomondation', array('name' => "data[ListDetailManage][{$rowIndex}][budget_rent_accomondation]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_rent_accomondation'])); ?></td>
                                <td><?php echo $this->Form->input('budget_vehicle', array('name' => "data[ListDetailManage][{$rowIndex}][budget_vehicle]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_vehicle'])); ?></td>
                                <td><?php echo $this->Form->input('budget_repairs', array('name' => "data[ListDetailManage][{$rowIndex}][budget_repairs]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_repairs'])); ?></td>
                                <td><?php echo $this->Form->input('budget_sp3', array('name' => "data[ListDetailManage][{$rowIndex}][budget_sp3]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_sp3'])); ?></td>
                                <td><?php echo $this->Form->input('budget_sp5', array('name' => "data[ListDetailManage][{$rowIndex}][budget_sp5]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_sp5'])); ?></td>
                                <td><?php echo $this->Form->input('budget_other_materials', array('name' => "data[ListDetailManage][{$rowIndex}][budget_other_materials]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => true, 'value' => $v['budget_other_materials'])); ?></td>
                                <td class="text-right"><?php echo $this->Zicure->currencyFormat($v['budget_total']); ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        <?php
                    endif;
                    ?>
                    <tr>
                        <td class="button-actions"><?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', __('Add')), null, array('class' => 'btn btn-sm btn-flat btn-success', 'disabled' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('from_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][from_department_id]", 'id' => false, 'div' => false, 'label' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment())); ?></td>
                        <td><?php echo $this->Form->input('to_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][to_department_id]", 'id' => false, 'div' => false, 'label' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment())); ?></td>
                        <td><?php echo __('60 11 1A 11 01 001 1000'); ?></td>

                        <!-- Budget Detail List Section -->
                        <td><?php echo $this->Form->input('budget_salary', array('name' => "data[ListDetailManage][{$rowIndex}][budget_salary]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_regular_wage', array('name' => "data[ListDetailManage][{$rowIndex}][budget_regular_wage]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_tmp_wage', array('name' => "data[ListDetailManage][{$rowIndex}][budget_tmp_wage]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_offical_payment', array('name' => "data[ListDetailManage][{$rowIndex}][budget_offical_payment]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_general_grants', array('name' => "data[ListDetailManage][{$rowIndex}][budget_general_grants]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_seperate_grants', array('name' => "data[ListDetailManage][{$rowIndex}][budget_seperate_grants]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_goods_less1m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_goods_less1m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_goods_more1m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_goods_more1m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_landbuild_less10m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_landbuild_less10m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_landbuild_more10m', array('name' => "data[ListDetailManage][{$rowIndex}][budget_landbuild_more10m]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_payment', array('name' => "data[ListDetailManage][{$rowIndex}][budget_payment]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_cost', array('name' => "data[ListDetailManage][{$rowIndex}][budget_cost]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_material', array('name' => "data[ListDetailManage][{$rowIndex}][budget_material]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_utility', array('name' => "data[ListDetailManage][{$rowIndex}][budget_utility]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_food', array('name' => "data[ListDetailManage][{$rowIndex}][budget_food]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_allowances', array('name' => "data[ListDetailManage][{$rowIndex}][budget_allowances]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_rent_accomondation', array('name' => "data[ListDetailManage][{$rowIndex}][budget_rent_accomondation]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_vehicle', array('name' => "data[ListDetailManage][{$rowIndex}][budget_vehicle]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_repairs', array('name' => "data[ListDetailManage][{$rowIndex}][budget_repairs]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_sp3', array('name' => "data[ListDetailManage][{$rowIndex}][budget_sp3]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_sp5', array('name' => "data[ListDetailManage][{$rowIndex}][budget_sp5]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('budget_other_materials', array('name' => "data[ListDetailManage][{$rowIndex}][budget_other_materials]", 'id' => false, 'div' => false, 'label' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled)); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->numberFormat(0); ?></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr class="payment-summation">
                        <td colspan="4" class="text-right text-bold"><?php echo __('รวมขอเบิกเงินงวด'); ?></td>
                        <?php //for ($i = 0; $i < 23; $i++):  ?>
                            <!--<td class="text-right"><?php //echo $this->Zicure->numberFormat(0); ?></td>-->
<?php //endfor;  ?>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_salary); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_regular_wage); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_tmp_wage); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_offical_payment); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_general_grants); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_seperate_grants); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_goods_less1m); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_goods_more1m); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_landbuild_less10m); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_landbuild_more10m); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_payment); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_cost); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_material); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_utility); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_food); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_allowances); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_rent_accomondation); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_vehicle); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_repairs); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_sp3); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_sp5); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_other_materials); ?></td>
                        <td class="text-right"><?php echo $this->Zicure->currencyFormat($total_budget_total); ?></td>
                    </tr>

                    <tr class="payment-summation">
                        <td colspan="4" class="text-right text-bold"><?php echo __('คงเหลือ'); ?></td>
                        <?php for ($i = 0; $i < 23; $i++): ?>
                            <td class="text-right"><?php echo $this->Zicure->numberFormat(0); ?></td>
<?php endfor; ?>
                    </tr>
                </tfoot>
            </table>
        </div> <!-- ./#tablePaymentDetailList -->


            <?php echo $this->Bootstrap->br(2); ?>
        <div class="row">
            <?php echo $this->Form->input('sum_current_period_budget', array('name' => 'data[ExpenseHeaderManage][sum_current_period_budget]', 'id' => false, 'class' => 'form-control currency', 'label' => false, 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('รวมยอดขอเงินงวดครั้งนี้') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true)); ?>
            <?php echo $this->Form->input('sum_owner_budget', array('name' => 'data[ExpenseHeaderManage][sum_owner_budget]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('เป็นหน่วยเจ้าของเดิม') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true)); ?>
<?php echo $this->Form->input('sum_other_budget', array('name' => 'data[ExpenseHeaderManage][sum_other_budget]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('โอนเปลี่ยนหน่วยเจ้าของงบประมาณ') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true)); ?>
        </div>


        <div class="box-footer">
            <?php echo $this->Permission->buttonBack(null, "btn btn-primary btn-flat pull-right separate-button"); ?>
<?php echo $this->Permission->button(__('บันทึก'), null, array('class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('คุณต้องการยืนยันการทำงาน ?'), 'data-confirm-title' => __('ยืนยันการทำงาน'))); ?>
        </div>
    </div>
</div>
<!--/box box-info-->

<script type="text/javascript">
    $(function () {
        $("input[type='text'].currency,input[type='hidden'].currency,input[type='number']").autoNumeric('init', {aSep: ',', aDec: '.', aSign: '฿ '});
        numeral.defaultFormat('0,0.00');

        var table = $("#tablePaymentDetailList");
        var em = $("tbody tr:first", table).clone();
    });
</script>