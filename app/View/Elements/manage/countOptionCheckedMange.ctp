<?php

$btitle = isset($btitle) ? $btitle : 'Zicure';
$disabled = isset($disabled) ? $disabled : false;
$bshow = isset($bshow) ? $bshow : false;
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
?>

<label for="count-checked">เลือก
<span id="count-checked">0</span> รายการ</label>
<?php echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('ส่งรออนุมัติ'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-success pull-right confirmModal', 'name' => 'btnManagesApprove', 'id' => 'btnManagesApprove')); ?>


<script type="text/javascript">
    $(function () {
        $("input:checkbox").on('ifChanged', function (event) {
            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            if ($(this).val() == '') {
                $("input:checkbox").iCheck(tmpCheck);
            }
            $('#btnManagesApprove').attr('disabled', !($("input:checkbox:checked").length > 0));
            $("#count-checked").text($(":checkbox:not(.check-all):checked").length);
        });
    });
</script>