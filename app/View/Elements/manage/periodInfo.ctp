<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'ส่วนบันทึกขอเบิกเงินงวด';
$bclass = isset($bclass) ? $bclass : 'box-success';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$url = isset($url) ? $url : $this->params->here;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;
$expenseHeaderId = $this->request->data['ExpenseHeaderManage']['id'];
?>
<div class="box <?php echo $bclass; ?>">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->element('boxOptionHeader', array('btitle' => __('ประวัติการขออนุมัติ'))); ?>
    <?php echo $this->Form->create('ViewOnly', array('url' => '#', 'role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><?php echo __('งวดที่'); ?></th>
                    <th><?php echo __('ครั้งที่'); ?></th>
                    <th><?php echo __('ยอดขออนุมัติ / บาท'); ?></th>
                    <th><?php echo __('สถานะปัจจุบัน'); ?></th>
                    <th><?php echo __('หน่วยเจ้าของสถานะ'); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>

            <tbody>
                <?php if (!empty($this->request->data['ApprovedHistory'])): ?>
                    <?php foreach ($this->request->data['ApprovedHistory'] as $k => $v): ?>						
						<tr>
							<td class="text-center"><?php echo $v['PeriodManage']['period']; ?></td>
							<td class="text-center"><?php echo $v['PeriodManage']['times']; ?></td>
							<td class="text-center"><?php echo $this->Zicure->numberFormat($v['PeriodManage']['budget_request']); ?></td>
							<td class="text-center"><?php echo '-'; ?></td>
							<td class="text-center"><?php echo $this->Zicure->findDepartmentNameWithDepartment($v['PeriodManage']['department_id']); ?></td>
							<td class="actions">
								<?php
								// ========= Edit by Tae =========== 
								// change /PaidApprovals/periodHistory/ to /$this->params['controller']/periodHistory/
								//echo $this->Permission->button($this->Bootstrap->icon('fa-search', __('Detail.')), '/PaidApprovals/periodHistory/' . $this->Zicure->secureEncodeParam($expenseHeaderId) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['id']) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['_version']) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['period']), array('type' => 'button', 'class' => 'btn btn-xs btn-flat bg-purple'))
								echo $this->Permission->button($this->Bootstrap->icon('fa-search', __('Detail.')), '/'.$this->params['controller'].'/periodHistory/' . $this->Zicure->secureEncodeParam($expenseHeaderId) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['id']) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['_version']) . '/' . $this->Zicure->secureEncodeParam($v['PeriodManage']['period']), array('type' => 'button', 'class' => 'btn btn-xs btn-flat bg-purple'));
								// ========= [END] Edit by Tae =========== 
								?>
							</td>
						</tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr class="row-notfound">
                        <td colspan="6"><?php echo __('Information Not Found'); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->buttonBack(null, "btn btn-warning btn-flat pull-right separate-button"); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>