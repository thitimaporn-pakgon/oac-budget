<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'การขออนุมัติครั้งนี้';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;

$budgetYear = isset($budgetYear) ? $budgetYear : $this->Project->getCurrenBudgetYearTH();

//$expendListKey = isset($ExpendDetailInfos[$budgetYear]) ? key($ExpendDetailInfos[$budgetYear]) : false;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;

$rowIndex = 0;
$sumBudgetList = array();

$ownerDepartmentId = $this->request->data['ExpenseHeaderManage']['to_department_id'];
$newestExpenseDetailId = empty($this->request->data['ExpenseDetailManage']) ? -1 : max(array_keys($this->request->data['ExpenseDetailManage']));
//$budgetExpenseDetailKeyLists = array('budget_salary' => 'budget_salary', 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10m' => 0, 'budget_landbuild_more10m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
//debug($this->request->data);exit;
$sumBudgetCurrent = $sumBudgetOwner = 0;
$budgetPrefixCode = $this->Project->budgetPrefixCode($this->request->data['ExpenseHeaderManage']);
$budgetKeyLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10m' => 0, 'budget_landbuild_more10m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
$budgetSumLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10m' => 0, 'budget_landbuild_more10m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
?>
<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('OwnerBudget', array('url' => $url, 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'frmPaymentBudget')); ?>
    <div class="box-body">
        <div class="table-responsive no-padding">
            <table class="table table-hover rta-budget-detail table-short-information" id="tablePaymentDetailList">
                <thead>
                    <tr>
                        <th class="button-actions"></th>
                        <th class="text-left"><?php echo __('หน่วยเจ้าของงบประมาณ'); ?></th>
                        <th class="text-left"><?php echo __('หน่วยถืองบประมาณ'); ?></th>
                        <th class="budget-code text-left"><?php echo __('รหัสงบประมาณ ทบ.'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบบุคลากร'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('งบเงินอุดหนุน'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบลงทุน'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบดำเนินงาน'); ?></th>
                        <th colspan="8"></th>
                        <th class="text-center"><?php echo __('รวมทั้งสิ้น'); ?></th>
                    </tr>
                    <tr>
                        <th colspan="4"></th>
                        <th class="text-center"><?php echo __('เงินเดือน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างประจำ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างชั่วคราว'); ?></th>
                        <th class="text-center"><?php echo __('ค่าตอบแทนพนักงานราชการ'); ?></th>
                        <th class="text-center"><?php echo __('เงินอุดหนุนทั่วไป'); ?></th>
                        <th class="text-center"><?php echo __('เงินอุดหนุนเฉพาะกิจ'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('ค่าครุภัณฑ์'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('ที่ดินและสิ่งก่อสร้าง'); ?></th>
                        <th class="text-center"><?php echo __('ค่าตอบแทน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสาธารณูปโภค'); ?></th>
                        <th colspan="5" class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th colspan="3" class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th colspan="10"></th>
                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 1 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 1 ล้านขึ้นไป'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 10 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 10 ล้านขึ้นไป'); ?></th>
                        <th colspan="4"></th>
                        <th class="text-center"><?php echo __('ค่าอาหาร'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเบี้ยเลี้ยง'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเช่าที่พัก'); ?></th>
                        <th class="text-center"><?php echo __('ค่าพาหนะ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าซ่อมแซมทรัพย์สิน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.3'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.5'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุอื่น'); ?></th>
                        <th></th>
                    </tr>

                    <tr class="prev-summation sum-1">
                        <th colspan="4" class="text-right"><?php echo __('ยอดคงเหลือ'); ?></th>
                        <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                            <th class="text-right <?php echo $kk; ?>">
                                <?php $value = $this->request->data['ExpenseDetailManage'][$newestExpenseDetailId][$kk]; ?>
                                <p><?php echo $this->Zicure->currencyFormat($value); ?></p>
                                <?php echo $this->Form->hidden($kk, array('name' => "data[ExpenseDetailManage][sum-1][{$kk}]", 'value' => $value, 'data-section-name' => $kk)); ?>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                </thead>

                <tbody>
                    <!-- Check and print existing budget list detail -->
                    <?php if (!empty($this->request->data['ListDetailManage'])): ?>
                        <?php foreach ($this->request->data['ListDetailManage'] as $k => $v): ?>
                            <tr class="row-payments">
                                <td class="button-actions"><?php echo $this->Permission->button($this->Bootstrap->icon('fa-trash', __('Delete')), "/ListDetailManages/delete/" . $this->Zicure->secureEncodeParam($v['id']), array('class' => 'btn btn-sm btn-flat btn-danger', 'disabled' => $disabled)); ?></td>
                                <td><?php echo $this->Form->input('owner_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][owner_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['owner_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo $this->Form->input('hold_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][hold_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['hold_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo $this->Form->input('prefix_subfix_code', array('name' => "data[ListDetailManage][{$rowIndex}][prefix_subfix_code]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'payment-items required', 'value' => "{$v['prefix_code']} {$v['suffix_code']}", 'disabled' => true)); ?></td>

                                <!-- Budget Detail List Section -->
                                <?php
                                foreach ($budgetKeyLists as $kk => $vv):
                                    //Check and comulative the budget to owner department
                                    //debug("{$ownerDepartmentId} == {$v['owner_department_id']} =>" . ($ownerDepartmentId == $v['owner_department_id']));
                                    if (($ownerDepartmentId == $v['owner_department_id']) && ($kk == 'budget_total')) {
                                        $sumBudgetOwner += $v['budget_total'];
                                    }

                                    if ($kk == 'budget_total') {
                                        $sumBudgetCurrent += $v['budget_total'];
                                    }

                                    $budgetSumLists[$kk] += $v[$kk];
                                    ?>
                                    <td><?php echo $this->Form->input($kk, array('name' => "data[ListDetailManage][{$rowIndex}][{$kk}]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'disabled' => true, 'value' => $v[$kk], 'data-section-name' => $kk)); ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <?php $rowIndex++; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <!-- Add for more input details -->       
                    <tr class="row-payments">
                        <?php echo $this->Form->hidden('expense_header_manage_id', array('name' => "data[ListDetailManage][{$rowIndex}][expense_header_manage_id]", 'value' => $this->request->data['ExpenseHeaderManage']['id'])); ?>
                        <?php echo $this->Form->hidden('budget_year_id', array('name' => "data[ListDetailManage][{$rowIndex}][budget_year_id]", 'value' => $this->request->data['StrategicManage']['budget_year_id'])); ?>
                        <?php echo $this->Form->hidden('prefix_code', array('name' => "data[ListDetailManage][{$rowIndex}][prefix_code]", 'value' => $budgetPrefixCode)); ?>
                        <?php echo $this->Form->hidden('suffix_code', array('name' => "data[ListDetailManage][{$rowIndex}][suffix_code]", 'value' => '')); ?>
                        <?php echo $this->Form->hidden('order_no', array('name' => "data[ListDetailManage][{$rowIndex}][order_no]", 'value' => $rowIndex)); ?>

                        <td class="button-actions"><?php echo $this->Permission->submit($this->Bootstrap->icon('fa-plus', __('Add')), $url, array('name' => 'btnPlus', 'value' => 'btnPlus', 'class' => 'btn btn-sm btn-flat btn-success', 'disabled' => $disabled)); ?></td>
                        <td><?php echo $this->Form->input('owner_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][owner_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple payment-items xrequired', 'options' => $this->Zicure->listDepartment())); ?></td>
                        <td><?php echo $this->Form->input('hold_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][hold_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple payment-items xrequired', 'options' => $this->Zicure->listDepartment())); ?></td>
                        <td><?php echo $this->Form->input('prefix_subfix_code', array('name' => "data[ListDetailManage][{$rowIndex}][prefix_subfix_code]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'payment-items xrequired', 'readonly' => true, 'value' => $budgetPrefixCode)); ?></td>

                        <!-- Budget Detail List Section -->
                        <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                            <td><?php echo $this->Form->input($kk, array('name' => "data[ListDetailManage][{$rowIndex}][{$kk}]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'readonly' => $disabled, 'value' => ($kk == 'budget_total') ? 0 : null, 'data-section-name' => $kk, 'data-v-max' => ($this->request->data['ExpenseDetailManage'][$newestExpenseDetailId][$kk] - $budgetSumLists[$kk]))); ?></td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>

                <tfoot>
                    <tr class="payment-summation sum-2">
                        <td colspan="4" class="text-right text-bold"><?php echo __('รวมขอเบิกเงินงวด'); ?></td>

                        <!-- Show period money withdrawal -->
                        <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                            <td class="text-right currency <?php echo $kk; ?>">
                                <?php $value = $budgetSumLists[$kk]; ?>
                                <p><?php echo $this->Zicure->currencyFormat($value); ?></p>
                                <?php echo $this->Form->hidden($kk, array('name' => "data[ExpenseDetailManage][sum-2][{$kk}]", 'value' => $value, 'data-section-name' => $kk)); ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>

                    <tr class="payment-summation sum-3">
                        <td colspan="4" class="text-right text-bold"><?php echo __('คงเหลือ'); ?></td>

                        <!-- Show period money withdrawal remain -->
                        <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                            <td class="text-right currency <?php echo $kk; ?>">
                                <?php $value = $this->request->data['ExpenseDetailManage'][$newestExpenseDetailId][$kk] - $budgetSumLists[$kk]; ?>
                                <p><?php echo $this->Zicure->currencyFormat($value); ?></p>
                                <?php echo $this->Form->hidden($kk, array('name' => "data[ExpenseDetailManage][sum-3][{$kk}]", 'value' => $value, 'data-section-name' => $kk)); ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tfoot>
            </table>
        </div> <!-- ./#tablePaymentDetailList -->


        <?php echo $this->Bootstrap->br(2); ?>
        <div class="row">
            <?php echo $this->Form->input('sum_current_period_budget', array('name' => 'data[ExpenseHeaderManage][sum_current_period_budget]', 'id' => false, 'class' => 'form-control currency', 'label' => false, 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('รวมยอดขอเงินงวดครั้งนี้') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true, 'value' => $sumBudgetCurrent)); ?>
            <?php echo $this->Form->input('sum_owner_budget', array('name' => 'data[ExpenseHeaderManage][sum_owner_budget]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('เป็นหน่วยเจ้าของเดิม') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true, 'value' => $sumBudgetOwner)); ?>
            <?php echo $this->Form->input('sum_other_budget', array('name' => 'data[ExpenseHeaderManage][sum_other_budget]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-6', 'wrap' => 'col-sm-6', 'before' => '<div class="col-sm-5 text-bold">' . __('โอนเปลี่ยนหน่วยเจ้าของงบประมาณ') . '</div>', 'after' => '<div class="col-sm-1 text-bold">' . __('Bath') . '</div>', 'readonly' => true, 'value' => ($sumBudgetCurrent - $sumBudgetOwner))); ?>
        </div>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->buttonBack(null, "btn btn-warning btn-flat pull-right separate-button"); ?>
        <?php echo $this->Permission->submit(__('บันทึก'), $url, array('class' => 'btn btn-primary btn-flat pull-right confirmModal', 'data-confirm-message' => __('คุณต้องการยืนยันการทำงาน ?'), 'data-confirm-title' => __('ยืนยันการทำงาน'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!--/box box-info-->

<script type="text/javascript">
    $(function () {
        AppWarning('<?php echo __('xxxxx'); ?>');
        $("input[type='text'].currency,input[type='hidden'].currency,input[type='number']").autoNumeric('init', {aSep: ',', aDec: '.', aSign: '฿ '});
        numeral.defaultFormat('0,0.00');
        var prefixCode = '<?php echo $budgetPrefixCode; ?>';
        $('input[name*="[budget_total]"]').prop('readonly', true);
        $('input.currency:not(:last)').keyup(function () {
            fillAutoSummation($(this));
        });

        $('select.payment-items').change(function () {
            $.post('/Utils/getDepartmentCode/' + $(this).val(), function (data) {
                $('input[name*="[prefix_subfix_code]"]', $(this).closest('tr')).val(prefixCode + ' ' + data);
            });
        });

        //Check validation
        $('button[name="btnPlus"]').click(function (e) {
            e.preventDefault();
            
//            console.log('btnPlus clicked');
//            console.log((($('select[name*="[owner_department_id]"]:last').val() == '') || ($('select[name*="[hold_department_id]"]:last').val() == '')));
//            return false;
            if (($('select[name*="[owner_department_id]"]:last').val() == '') || ($('select[name*="[hold_department_id]"]:last').val() == '')) {
                AppWarning('<?php echo __('กรุณาเลือกหน่วยเจ้าของงบประมาณ หรือ หน่วยถืองบประมาณ'); ?>');
                return false;
            } else {
                $('FORM#frmPaymentBudget').submit();
                return true;
            }
        });
    });

    /**
     * 
     * Function auto budget calculation
     * @author  sarawutt.b
     * @param   $that as a jquery object 
     * @returns void
     */
    function fillAutoSummation($that) {
        var $table = $('#tablePaymentDetailList');
        var $tr = $that.closest('tr');
        var sectionName = $that.data('section-name');

        var sectionPrevTotal = numeral($('tr.sum-1 th.' + sectionName + ' p', $table).text().replace(/[^0-9\.]+/g, '') || 0);

        //Row summation all of input.currency are on the same row
        var rowSummation = numeral(0);
        $('input.currency:not(:last)', $tr).each(function () {
            rowSummation.add($(this).autoNumeric('get') || 0);
        });
        $('input[name*="[budget_total]"]', $tr).autoNumeric('set', rowSummation.value());

        //Summation of all the same column
        var cellSummation = numeral(0);
        $('tr.row-payments input[data-section-name="' + sectionName + '"]', $table).each(function () {
            cellSummation.add($(this).autoNumeric('get') || 0);
        });
        $('tr.sum-2 td.' + sectionName + ' p', $table).text(numberToCurrency(cellSummation.value()) + ' ฿');
        $('tr.sum-2 td.budget_total p', $table).text(function () {
            var sumRow = numeral(0);
            $('tr.sum-2 td.currency:not(:last) p').each(function () {
                sumRow.add($(this).text().replace(/[^0-9\.]+/g, '') || 0);
            });
            return numberToCurrency(sumRow.value()) + ' ฿';
        });

        var cellGrantTotal = sectionPrevTotal.subtract(cellSummation.value());
        $('tr.sum-3 td.' + sectionName + ' p', $table).text(numberToCurrency(cellGrantTotal.value()) + ' ฿');
        $('tr.sum-3 td.budget_total p', $table).text(function () {
            var sumRow = numeral(0);
            $('tr.sum-3 td.currency:not(:last) p').each(function () {
                sumRow.add($(this).text().replace(/[^0-9\.]+/g, '') || 0);
            });
            return numberToCurrency(sumRow.value()) + ' ฿';
        });
    }
</script>