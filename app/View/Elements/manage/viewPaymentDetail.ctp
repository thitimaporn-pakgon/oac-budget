<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'การขออนุมัติครั้งนี้';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$url = isset($url) ? $url : $this->params->here;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;
$rowIndex = 0;
$budgetKeyLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10m' => 0, 'budget_landbuild_more10m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
$budgetSumLists = array('budget_salary' => 0, 'budget_regular_wage' => 0, 'budget_tmp_wage' => 0, 'budget_offical_payment' => 0, 'budget_general_grants' => 0, 'budget_seperate_grants' => 0, 'budget_goods_less1m' => 0, 'budget_goods_more1m' => 0, 'budget_landbuild_less10m' => 0, 'budget_landbuild_more10m' => 0, 'budget_payment' => 0, 'budget_cost' => 0, 'budget_material' => 0, 'budget_utility' => 0, 'budget_food' => 0, 'budget_allowances' => 0, 'budget_rent_accomondation' => 0, 'budget_vehicle' => 0, 'budget_repairs' => 0, 'budget_sp3' => 0, 'budget_sp5' => 0, 'budget_other_materials' => 0, 'budget_total' => 0);
?>
<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('OwnerBudget', array('url' => $url, 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'frmPaymentBudget')); ?>
    <div class="box-body">
        <div class="table-responsive no-padding">
            <table class="table table-hover rta-budget-detail table-short-information" id="tablePaymentDetailList">
                <thead>
                    <tr>
                        <th class="text-left"><?php echo __('หน่วยเจ้าของงบประมาณ'); ?></th>
                        <th class="text-left"><?php echo __('หน่วยถืองบประมาณ'); ?></th>
                        <th class="budget-code text-left"><?php echo __('รหัสงบประมาณ ทบ.'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบบุคลากร'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('งบเงินอุดหนุน'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบลงทุน'); ?></th>
                        <th colspan="4" class="text-center"><?php echo __('งบดำเนินงาน'); ?></th>
                        <th colspan="8"></th>
                        <th class="text-center"><?php echo __('รวมทั้งสิ้น'); ?></th>
                    </tr>
                    <tr>
                        <th colspan="3"></th>
                        <th class="text-center"><?php echo __('เงินเดือน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างประจำ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าจ้างชั่วคราว'); ?></th>
                        <th class="text-center"><?php echo __('ค่าตอบแทนพนักงานราชการ'); ?></th>
                        <th class="text-center"><?php echo __('เงินอุดหนุนทั่วไป'); ?></th>
                        <th class="text-center"><?php echo __('เงินอุดหนุนเฉพาะกิจ'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('ค่าครุภัณฑ์'); ?></th>
                        <th colspan="2" class="text-center"><?php echo __('ที่ดินและสิ่งก่อสร้าง'); ?></th>
                        <th class="text-center"><?php echo __('ค่าตอบแทน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสาธารณูปโภค'); ?></th>
                        <th colspan="5" class="text-center"><?php echo __('ค่าใช้สอย'); ?></th>
                        <th colspan="3" class="text-center"><?php echo __('ค่าวัสดุ'); ?></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th colspan="9"></th>
                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 1 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 1 ล้านขึ้นไป'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละต่ำกว่า 10 ล้าน'); ?></th>
                        <th class="text-center"><?php echo __('หน่วยละ 10 ล้านขึ้นไป'); ?></th>
                        <th colspan="4"></th>
                        <th class="text-center"><?php echo __('ค่าอาหาร'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเบี้ยเลี้ยง'); ?></th>
                        <th class="text-center"><?php echo __('ค่าเช่าที่พัก'); ?></th>
                        <th class="text-center"><?php echo __('ค่าพาหนะ'); ?></th>
                        <th class="text-center"><?php echo __('ค่าซ่อมแซมทรัพย์สิน'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.3'); ?></th>
                        <th class="text-center"><?php echo __('ค่าสป.5'); ?></th>
                        <th class="text-center"><?php echo __('ค่าวัสดุอื่น'); ?></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <!-- Check and print existing budget list detail -->
                    <?php if (!empty($this->request->data['ListDetailManage'])): ?>
                        <?php foreach ($this->request->data['ListDetailManage'] as $k => $v): ?>
                            <tr class="row-payments">
                                <td><?php echo $this->Form->input('owner_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][owner_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['owner_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo $this->Form->input('hold_department_id', array('name' => "data[ListDetailManage][{$rowIndex}][hold_department_id]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'simple required payment-items', 'options' => $this->Zicure->listDepartment($v['hold_department_id']), 'disabled' => true)); ?></td>
                                <td><?php echo $this->Form->input('prefix_subfix_code', array('name' => "data[ListDetailManage][{$rowIndex}][prefix_subfix_code]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'class' => 'payment-items required', 'value' => "{$v['prefix_code']} {$v['suffix_code']}", 'disabled' => true)); ?></td>
                                <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                                    <?php $budgetSumLists[$kk] += $v[$kk]; ?>
                                    <td>
                                        <?php echo $this->Form->input($kk, array('name' => "data[ListDetailManage][{$rowIndex}][{$kk}]", 'id' => false, 'div' => false, 'label' => false, 'wrap' => false, 'type' => 'text', 'class' => 'payment-items form-control currency text-right', 'disabled' => true, 'value' => empty($v[$kk]) ? 0 : $v[$kk], 'data-section-name' => $kk)); ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            <?php $rowIndex++; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>

                <tfoot>
                    <tr class="payment-summation sum-2">
                        <td colspan="3" class="text-right text-bold"><?php echo __('รวมขอเบิกเงินงวด'); ?></td>
                        <!-- Show period money withdrawal -->
                        <?php foreach ($budgetKeyLists as $kk => $vv): ?>
                            <td class="text-right currency <?php echo $kk; ?>"><?php echo $this->Zicure->numberFormat($budgetSumLists[$kk]); ?></td>
                        <?php endforeach; ?>
                    </tr>
                </tfoot>
            </table>
        </div> <!-- ./#tablePaymentDetailList -->
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->buttonBack(null, "btn btn-warning btn-flat pull-right separate-button"); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!--/box box-info-->