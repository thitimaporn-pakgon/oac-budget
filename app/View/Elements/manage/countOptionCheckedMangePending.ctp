<?php

$btitle = isset($btitle) ? $btitle : 'Zicure';
$disabled = isset($disabled) ? $disabled : false;
$bshow = isset($bshow) ? $bshow : false;
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
?>

<label for="count-checked">เลือก
<span id="count-checked">0</span> รายการ</label>
<?php
echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('อนุมัติ'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-success pull-right separate-button', 'name' => 'btnInputBookNo', 'id' => 'btnInputBookNo'));
echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('พิมพ์ ทบ.500-161'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-success pull-right', 'name' => 'btnInputPeriod', 'id' => 'btnInputPeriod'));
?>


<script type="text/javascript">
    $(function () {
        $("input:checkbox").on('ifChanged', function (event) {
            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            if ($(this).val() == '') {
                $("input:checkbox").iCheck(tmpCheck);
            }
            $('#btnInputPeriod').attr('disabled', !($("input:checkbox:checked").length > 0));
			$('#btnInputBookNo').attr('disabled', !($("input:checkbox:checked").length > 0));
            $("#count-checked").text($(":checkbox:not(.check-all):checked").length);
        });
		
		$('#btnInputPeriod').click(function(){
			$('#FormPending').attr('action', '/<?php echo $this->params['controller'];?>/inputPeriod').submit();
		});
		
		$('#btnInputBookNo').click(function(){
			$('#FormPending').attr('action', '/<?php echo $this->params['controller'];?>/inputBookNo').submit();
		});
    });
</script>