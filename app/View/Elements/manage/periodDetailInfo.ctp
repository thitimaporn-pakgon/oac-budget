<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'ประวัติการส่งขออนุมัติ';
$bclass = isset($bclass) ? $bclass : 'box-success';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;

$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;
$sumPeriodBudget = 0;
$currentStatus = '-';
$currentDepartment = '-';

$departmentLevelOptions = $this->Project->displayBudgetWithdrawalDeparmentLevel();
$depLevelIndex = 36;

//debug($departmentLevelOptions);
//pr($this->request->data['PeriodManage']);
?>
<div class="box <?php echo $bclass; ?>">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('ViewOnly', array('url' => '#', 'role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><?php echo __('หน่วยงาน'); ?></th>
                    <th class="text-center"><?php echo __('งวดที่'); ?></th>
                    <th class="text-center"><?php echo __('ครั้งที่'); ?></th>
                    <th class="text-center"><?php echo __('เลขที่หนังสือ'); ?></th>
                    <th class="text-center"><?php echo __('วันที่อนุมัติ'); ?></th>
                </tr>
            </thead>

            <tbody>
				<?php /* ===== COMMENT by Tae =====
                <?php for ($i = 36; $i >= 32; $i--): ?>
                    <?php if (array_key_exists($i, $this->request->data['PeriodManage'])): ?>
                        <tr>
                            <td><?php echo $this->Zicure->findDepartmentNameWithDepartment($this->request->data['PeriodManage'][$i]['department_id']); ?></td>
                            <td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['period']; ?></td>
                            <td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['times']; ?></td>
                            <td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['book_no']; ?></td>
                            <td class="text-center"><?php echo $this->Zicure->displayThaiDate($this->request->data['PeriodManage'][$i]['date_approved'], 'DS'); ?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td><?php echo $departmentLevelOptions[$i]; ?></td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                        </tr>
                    <?php endif; ?>
                <?php endfor; ?>
				*/ // ===== [END] COMMENT by Tae ===== ?>
				
				<?php // ===== EDIT by Tae ===== ?>
				<?php foreach($this->request->data['PeriodManage'] as $i => $v): ?>
					<tr>
						<td><?php echo $this->Zicure->findDepartmentNameWithDepartment($this->request->data['PeriodManage'][$i]['department_id']); ?></td>
						<td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['period']; ?></td>
						<td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['times']; ?></td>
						<td class="text-center"><?php echo $this->request->data['PeriodManage'][$i]['book_no']; ?></td>
						<td class="text-center"><?php echo $this->Zicure->displayThaiDate($this->request->data['PeriodManage'][$i]['date_approved'], 'DS'); ?></td>
					</tr>
				<?php endforeach; ?>
				<?php // ===== [END] EDIT by Tae ===== ?>
            </tbody>
        </table>

        <?php echo $this->Bootstrap->br(2); ?>
        <div class="row">
            <?php echo $this->Form->input('budget_scope', array('name' => 'data[ExpenseHeaderManage][budget_scope]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-12', 'wrap' => 'col-sm-4', 'before' => '<div class="col-sm-3 text-right text-bold">' . __('ยอดขออนุมัติ') . '</div>', 'after' => '<div class="col-sm-2 text-bold">' . __('Bath') . '</div>', 'readonly' => true, 'value' => $sumBudgetDetailTotal)); ?>
            <?php //echo $this->Form->input('current_status', array('name' => 'data[ExpenseHeaderManage][current_status]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-12', 'wrap' => 'col-sm-4', 'before' => '<div class="col-sm-3 text-right text-bold">' . __('สถานะปัจจุบัน') . '</div>', 'readonly' => true, 'value' => $currentStatus)); ?>
            <?php //echo $this->Form->input('current_department', array('name' => 'data[ExpenseHeaderManage][current_department]', 'id' => false, 'label' => false, 'class' => 'form-control currency', 'div' => 'form-group col-sm-12', 'wrap' => 'col-sm-4', 'before' => '<div class="col-sm-3 text-right text-bold">' . __('หน่วยงานของสถานะปัจจุบัน') . '</div>', 'readonly' => true, 'value' => $currentDepartment)); ?>
        </div>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->buttonBack(null, "btn btn-warning btn-flat pull-right separate-button"); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>