<?php

$btitle = isset($btitle) ? $btitle : 'Zicure';
$disabled = isset($disabled) ? $disabled : false;
$bshow = isset($bshow) ? $bshow : false;
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
$budgetCode = isset($budgetCode) ? $budgetCode : false;
$periodBudget = isset($periodBudget) ? $periodBudget : false;

?>
<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => $btitle)); ?>
    <?php echo $this->Form->create('Search', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
        <?php
          echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
          echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $work_grouplist,'default'=>$defaultOptions['A']));
          echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $work_list ,'default'=>$defaultOptions['B']));
          echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $project_list,'default'=>$defaultOptions['C']));
          if($budgetCode){
            echo $this->Form->input('budget_code', array('label'=>array('text'=>__('รหัสงบประมาณ ทบ.'))));
          }elseif($periodBudget){
            echo $this->Form->input('period', array('class' => 'form-control', 'label' => false, 'div' => 'form-group col-sm-5', 'wrap' => 'col-sm-3', 'before' => '<label class="col-sm-5 control-label" for="SearchPeriod">' . __('งวดที่') . '</label>'));
            echo $this->Form->input('times', array('label' => false, 'class' => 'form-control', 'div' => 'form-group col-sm-5', 'wrap' => 'col-sm-3', 'before' => '<label class="col-sm-2 control-label" for="SearchTimes">' . __('ครั้งที่') . '</label>'));
          }
        ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '/OwnerManagesBudget/withdrawBudget', array('name' => 'btnSubmitSearch')); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
