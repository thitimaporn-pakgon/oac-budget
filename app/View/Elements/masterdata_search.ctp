<?php
$search_box_header = isset($search_box_header) ? $search_box_header : 'Search';
$action = isset($action) ? $action : '#';
$placeholder = isset($placeholder) ? $placeholder : null;
?>
<div class="box box-info">
    <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="SearchMasterDataSearch"><?php echo __('Master Data'); ?></label>
            <div class="col-sm-9 input-group input-group-sm">
                <?php echo $this->Form->input('master_data_search', array('div' => false, 'label' => false, 'class' => 'simple form-control', 'placeholder' => $placeholder)); ?>
                <span class="input-group-btn"><button type="submit" class="btn btn-primary" type="submit" id="btnSearch" name="data[Search][btnSearch]" action="SEARCHMASTERDATA"><i class = "fa fa-search"></i><?php echo __('Search'); ?></button></span>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>