<?php
$btitle = isset($btitle) ? $btitle : 'Zicure';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
?>
<div class="box-header with-border">
    <h3 class="box-title"><?php echo __($btitle); ?></h3>
    <div class="box-tools pull-right">
        <?php if (!is_null($bnoti)): ?>
            <?php echo $this->element('notificationButton', array('btitle' => $bnoti)); ?>
        <?php endif; ?>
        <?php if ($bcollap): ?>
            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
        <?php endif; ?>
        <?php if ($bclose): ?>
            <button data-widget="remove" class="btn btn-box-tool"><i class="fa fa-remove"></i></button>
        <?php endif; ?> 
    </div>
</div>
