<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => $title));?>
    <?php
    echo $this->Form->create('InboxFilter', array('class' => 'form-horizontal'));
    ?>
    <div class="box-body">
        <?php
            if($title == 'Inbox'){
              echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
              echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $this->Project->findListWorkGroupManage()));
              echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $this->Project->findListWorkManage()));
              echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $this->Project->findListProjectManage()));
            }else{
              echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
              echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $this->Project->findListWorkGroupManage()));
              if($system_has_process != 26){
                echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $this->Project->findListWorkManage()));
                echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $this->Project->findListProjectManage()));
              }



            }

        ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/BudgetPlans/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
