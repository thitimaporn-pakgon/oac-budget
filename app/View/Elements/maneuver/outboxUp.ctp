<?php

$bcollap = isset($bcollap) ? $bcollap : true;
$bclose = isset($bclose) ? $bclose : true;

$datas = isset($datas) ? $datas : $datas;
$titleSreach = isset($titleSreach) ? $titleSreach : "Outbox " . Inflector::humanize(Inflector::underscore($this->params['controller'])) . " Management System";
$titleResult = isset($titleResult) ? $titleResult : "Outbox " . Inflector::humanize(Inflector::underscore($this->params['controller'])) . " (Result)";
$url = isset($url) ? $url : $this->here;
$controllerView = isset($controllerView) ? $controllerView : $this->params['controller'];
$arrays = array();
$enableReportButton = isset($enableReportButton) && $enableReportButton == true ? 'show' : 'none';
?>

<div class="projectListManeuvers search inbox">
    <div class="box box-success">
        <?php echo $this->element('boxOptionHeader', array('btitle' => $titleSreach, 'bcollap' => $bcollap, 'bclose' => $bclose)); ?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal', 'url' => $url)); ?>
            <?php echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'), 'default' => $this->Zicure->getCurrenBudgetYearTH())); ?>
            <?php echo $this->Form->input('work_group_maneuver_id', array('label' => array('text' => __('Name Work Group Maneuver')),'options' => $this->Project->getWorkGroupManeuver()));?>
            <?php echo $this->Form->input('work_maneuver_id', array('label' => array('text' => __('Work Maneuver name')),'options' => $this->Project->getWorkGroupManeuver()));?>
            <?php echo $this->Form->input('project_maneuver_id', array('label' => array('text' => __('Project Maneuver')),'options' => $this->Project->getWorkGroupManeuver()));?>
            <?php // echo $this->Form->input('work_group_maneuver_id', array('options' => $this->Project->findListWorkGroupManeuver())); ?>
            <?php // echo $this->Form->input('work_maneuver_id', array('options' => $this->Project->findListWorkManeuver())); ?>
            <?php // echo $this->Form->input('project_maneuver_id', array('options' => $this->Project->findListProjectManeuver())); ?>
        </div>
        <div class="box-footer">
            <?php echo $this->Permission->button($this->Bootstrap->icon('fa-search', 'Search'), "{$url}", array('type' => 'button', 'class' => 'btn btn-primary pull-left separate-button', 'onclick' => false, 'name' => 'btnSubmitSearch', 'id' => 'btnSubmitSearch')); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="projectListManeuvers inbox">
    <div class="box box-primary">
        <?php echo $this->element('boxOptionHeader', array('btitle' => $titleResult, 'bcollap' => $bcollap, 'bclose' => $bclose)); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex-black" rowspan="2"><?php echo __('#'); ?></th>
                        <th class="text-center" rowspan="2"><?php echo __('Project List Maneuver'); ?></a></th>
                        <th class="text-center" rowspan="2"><?php echo __('Budget Year'); ?></th>
                        <th class="text-center" colspan="2"><?php echo __('Budget Year Allowcated'); ?></th>
                        <th class="text-center" colspan="3"><?php echo __('Annual Budget'); ?></th>
                        <th class="text-center" rowspan="2"><?php echo __('modified'); ?></th>
                        <!--<th class="actions">&nbsp;</th>-->
                    </tr>
                    <tr>
                        <th><?php echo __('previous Budget Year'); ?></th>
                        <th><?php echo __('Secondary previous Budget Year'); ?></th>
                        <th><?php echo __('Primary Budget'); ?></th>
                        <th><?php echo __('Secondary Budget'); ?></th>
                        <th><?php echo __('Sum Budget'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($datas)): ?>
                    <?php $totalPrevious = $totalSecondaryPrevious = $totalPrimary = $totalSecondary = $totalSumBudget = $previousBudgetYear = $secondaryPreviousBudgetYear = $primaryBudget = $secondaryBudget = $sumAmount = 0.00;?>
                        <?php foreach ($datas as $k => $data): ?>
                    <?php 
                    if(in_array($data['WorkGroupManeuver']['id'], $arrays) !=1){
                      $arrays[] = $data['WorkGroupManeuver']['id'];
                    }
                    ?>
                            <?php $code = $this->Project->generateBudgetCode($data['ExpenseHeaderManeuver']['id'], 'A'); ?>
                            <?php $id = $this->Zicure->secureEncodeParam($data['ExpenseHeaderManeuver']['id']); ?>
                            <?php $sumBudget = $this->Project->getSumBudgetIndex($this->Project->getBudgetCode($code['digit_full_code']), $code['digit_budget_year']); ?>
                    <?php
                    $previousBudgetYear = $sumBudget[1]['sum_budget'];
                    $secondaryPreviousBudgetYear = $sumBudget[0]['sum_budget'];
                    $primaryBudget = $sumBudget[2]['primary_budget'];
                    $secondaryBudget = $sumBudget[2]['secondary_budget'];
                    $sumAmount =  $sumBudget[2]['sum_budget'];
                    $totalPrevious += $previousBudgetYear;
                    $totalSecondaryPrevious += $secondaryPreviousBudgetYear;
                    $totalPrimary += $primaryBudget;
                    $totalSecondary += $secondaryBudget;
                    $totalSumBudget += $sumAmount;
                    ?>
                    <tr>
                        <td class="nindex">
                                    <?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                        <td><?php echo h($data['ProjectListManeuver']['name']); ?><br>
                                    <?php echo $this->Permission->link($code['digit_full_code'], "/{$controllerView}/view/{$id}"); ?>
                        </td>
                        <td><?php echo $data['ProjectListManeuver']['budget_year_id']; ?></td>
                        <td><?php echo $this->Zicure->numberFormat($previousBudgetYear); ?></td>
                        <td><?php echo $this->Zicure->numberFormat($secondaryPreviousBudgetYear); ?></td>
                        <td><?php echo $this->Zicure->numberFormat($primaryBudget); ?></td>
                        <td><?php echo $this->Zicure->numberFormat($secondaryBudget); ?></td>
                        <td><?php echo $this->Zicure->numberFormat($sumAmount); ?></td>
                        <td><?php echo (empty($data['ProjectManeuver']['modified'])) ? '-' : $this->Zicure->dateISO($data['ProjectListManeuver']['modified']); ?></td>
                        <!--<td class="actions"><?php // echo $this->Permission->link('Edit',"/ProjectListManeuvers/edit/{$data['ProjectListManeuver']['id']}",array('class'=>'btn btn-warning'));                      ?></td>-->
                    </tr>
                            <?php unset($sumBudget); ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <tr class="row-notfound">
                        <td colspan="10"><?php echo __('Information Not Found'); ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td colspan=3" class="text-right text-bold"><?php echo ('ยอดรวมวงเงินทั้งสิ้น');?></td>
                        <td class="text-right text-bold"><?php echo $this->Zicure->numberFormat($totalPrevious);?></td>
                        <td class="text-right text-bold"><?php echo $this->Zicure->numberFormat($totalSecondaryPrevious);?></td>
                        <td class="text-right text-bold"><?php echo $this->Zicure->numberFormat($totalPrimary);?></td>
                        <td class="text-right text-bold"><?php echo $this->Zicure->numberFormat($totalSecondary);?></td>
                        <td class="text-right text-bold"><?php echo $this->Zicure->numberFormat($totalSumBudget);?></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div>
        <?php echo $this->Form->create('WorkBudgetManeuver', array('class' => 'form-horizontal', 'url' => '/SpcBudgetManeuvers/jasperExporExpenseDetailExcel')); ?>
        <div class="box-footer">
            <?php echo $this->Permission->button('<i class="fa fa-arrow-circle-down"></i>' . __('ExportExcel'), null, array('type' => 'submit', 'class' => 'btn bg-orange separate-button pull-right confirmModal separate-button', 'name' => 'btnExcel', 'id' => 'btnExcel', 'style' => "display: $enableReportButton")); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    var countItem = $("input[type='checkbox']:checked").length;
    $(function () {
        var urlAction = '<?php echo $url; ?>';
        $('#btnSubmitSearch').click(function () {
            $('FORM#SearchOutboxForm').attr('action', urlAction).submit();
        });

        $('#btnDelete').click(function () {
            confirmModal('<?php echo __('Please confirm for delete the ProjectListManeuvers ?'); ?>', function (result) {
                if (result == true) {
                    $('FORM#ProjectListManeuverOutboxForm').attr('action', '/ProjectListManeuvers/deleteInbox').submit();
                    return true;
                } else {
                    return false;
                }
            });
        });

        $("input[type='checkbox'],input[type='radio']").on('ifChecked', function (e) {
            var actionClass = e.target.className, inputName = e.target.name;
            countItem++;
            if (countItem > 0) {
                $('#btnApprove').attr('disabled', false);
                $('#btnDelete').attr('disabled', false);
            }
        });

        $("input[type='checkbox'],input[type='radio']").on('ifUnchecked', function (e) {
            var actionClass = e.target.className, inputName = e.target.name;
            countItem--;
            if (countItem <= 0) {
                $('#btnApprove').attr('disabled', true);
                $('#btnDelete').attr('disabled', true);
            }
        });

        // Dosz Code.===============

        var controller = '<?php echo $this->params['controller']; ?>';
        var workGroupIds = <?php echo json_encode($arrays); ?>;
        var workId = <?php echo $workId; ?>;
        var projectId = <?php echo $projectId; ?>;
        console.log(workGroupIds.toString());
        console.log(workId);
        console.log(projectId);
        $("#btnExcel").click(function () {
            var form = $(this).closest('form');
            form.attr('action', '/' + controller + '/jasperExporExpenseDetailExcel/primary_budget/' + workGroupIds.toString() + '/' + workId + '/' + projectId);
        });

        $("#SearchBudgetYearId").change(function () {
            getWorkGroup();
        });

        $("#SearchWorkGroupManeuverId").change(function () {
            getWork();
        });

        $("#SearchWorkManeuverId").change(function () {
            getProject();
        });

        function getWorkGroup() {
            $.post("/WorkGroupManeuvers/findWorkGroup/" + $("#SearchBudgetYearId").val(), function (data) {
                $("#SearchWorkGroupManeuverId").html(data);
                getWork();
            });
        }

        function getWork() {
            $.post("/WorkManeuvers/findWork/" + $("#SearchWorkGroupManeuverId").val(), function (data) {
                $("#SearchWorkManeuverId").html(data);
                getProject();
            });
        }
        function getProject() {
            $.post("/ProjectManeuvers/findProject/" + $("#SearchWorkManeuverId").val(), function (data) {
                $("#SearchProjectManeuverId").html(data);
                updateChosen();
            });
        }

        // =========================

    });

</script>
