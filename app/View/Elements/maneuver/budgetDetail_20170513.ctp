<?php
$btitle = isset($btitle) ? $btitle : 'นำเสนอคำขอ (ภาคจัดทำ)';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = isset($disabled) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;
$digitCode = isset($digitCode) ? $digitCode : '';
$budgetYear = isset($budgetYear) ? $budgetYear : $this->Project->getCurrenBudgetYearTH();
$shortBudgetYear = substr($budgetYear, 2);


?>


<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => $btitle, 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('OwnerBudget', array('url' => $url, 'role' => 'form', 'class' => 'form-horizontal', 'type' => 'file')); ?>

    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table class="zicure-table-result table table-bordered" id="BudgetDetailTable">
                    <thead>
                        <tr class="budget-head">
                            <th class="text-center" rowspan="2"><?php echo __('ประเภทรายจ่าย'); ?></th>
                            <th class="text-center" colspan="2"><?php echo __('งบประมาณที่ผ่านมา'); ?></th>
                            <th class="text-center" colspan="2"><?php echo __('งบประมาณปี %s', $budgetYear); ?></th>
                            <th class="budget-scope-summary" rowspan="2"><?php echo __('รวมเป็นเงิน'); ?></th>
                        </tr>
                        <tr class="budget-head">
                            <th class="budget-scope"><?php echo __('ปี %s', $budgetYear - 2); ?></th>
                            <th class="budget-scope"><?php echo __('ปี %s', $budgetYear - 1); ?></th>
                            <th class="budget-scope"><?php echo __('ส่วนที่ 1'); ?></th>
                            <th class="budget-scope"><?php echo __('ส่วนที่ 2'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($data)): ?>
                            <?php $index = 0; ?>
                            <!--Expense list level I-->
                            <?php foreach ($data as $k => $expenseList): ?>
                                <?php $expenseList = $expenseList[0]; ?>
                                <tr data-accordion-parent="<?php echo $expenseList['id']; ?>">
                                    <td>
                                        <span class="glyphicon glyphicon-triangle-right btn-xs" role="drill-down-icon"></span><?php echo $expenseList['name']; ?>
                                        {<?php echo $this->Project->optionBudgetParent('all', $expenseList['id']); ?>}
                                        <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'value' => $expenseList['id'])); ?>
                                    </td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('year-2', $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 2][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 2][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('year-1', $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 1][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 1][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('1', $expenseList['id']); ?>>
                                        <?php echo isset($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['primary_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['primary_budget']): $this->Zicure->currencyFormat(0); ?>
                                        <?php echo $this->Form->hidden('primary_budget', array('name' => "data[OwnerBudget][{$index}][primary_budget]", 'id' => false)); ?>
                                    </td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('2', $expenseList['id']); ?>>
                                        <?php echo isset($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['secondary_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['secondary_budget']): $this->Zicure->currencyFormat(0); ?>
                                        <?php echo $this->Form->hidden('secondary_budget', array('name' => "data[OwnerBudget][{$index}][secondary_budget]", 'id' => false)); ?>
                                    </td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('total', $expenseList['id']); ?>>
                                        <?php echo isset($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$expenseList['id']]['ExpenseDetailManeuver']['sum_budget']): $this->Zicure->currencyFormat(0); ?>
                                        <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'id' => false)); ?>
                                    </td>
                                </tr>


                                <!--Expense list level II-->
                                <?php $index++; ?>
                                <?php $childExpenseLists = $this->Project->findChildExpenseList($expenseList['id']); ?>
                                <?php foreach ($childExpenseLists as $k => $childExpenseList): ?>
                                    <?php $childExpenseList = $childExpenseList[0]; ?>
                                    <tr class="budget-list <?php echo ($childExpenseList['has_input'] == 'N') ? 'bg-success' : null; ?>" data-accordion-child-of="<?php echo $expenseList['id']; ?>" style="display: none;">
                                        <td>
                                            <?php echo ($childExpenseList['has_input'] == 'N') ? '<span class="glyphicon glyphicon-triangle-right btn-xs" role="drill-down-icon"></span>' : ''; ?>
                                            <?php echo ($childExpenseList['name']); ?>
                                            {<?php echo $this->Project->optionBudgetChild('all', $childExpenseList['id'], $expenseList['id']); ?>}
                                            <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'id' => false, 'value' => $childExpenseList['id'])); ?>
                                        </td>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('year-2', $childExpenseList['id'], $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 2][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 2][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('year-1', $childExpenseList['id'], $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 1][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 1][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>

                                        <?php if ($childExpenseList['has_input'] == 'Y'): ?>
                                            <td class="text-right"><input type="text" name="data[OwnerBudget][<?php echo $index; ?>][primary_budget]" class="form-control currency money-right" <?php echo $this->Project->optionBudgetChild('1', $childExpenseList['id'], $expenseList['id']); ?>/></td>
                                            <td class="text-right"><input type="text" name="data[OwnerBudget][<?php echo $index; ?>][secondary_budget]" class="form-control currency money-right" <?php echo $this->Project->optionBudgetChild('2', $childExpenseList['id'], $expenseList['id']); ?>/></td>
                                        <?php else: ?>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChild('1', $childExpenseList['id'], $expenseList['id']); ?>>
                                                <?php echo isset($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['primary_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['primary_budget']) : $this->Zicure->currencyFormat(0); ?>
                                                <?php echo $this->Form->hidden('primary_budget', array('name' => "data[OwnerBudget][{$index}][primary_budget]")); ?>
                                            </td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChild('2', $childExpenseList['id'], $expenseList['id']); ?>>
                                                <?php echo isset($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['secondary_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['secondary_budget']) : $this->Zicure->currencyFormat(0); ?>
                                                <?php echo $this->Form->hidden('secondary_budget', array('name' => "data[OwnerBudget][{$index}][secondary_budget]")); ?>
                                            </td>
                                        <?php endif; ?>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('total', $childExpenseList['id'], $expenseList['id']); ?>>
                                            <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'class' => 'currency money-right')); ?>
                                            <?php echo isset($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$childExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?>
                                        </td>
                                    </tr>

                                    <!--Expense list level III-->
                                    <?php $child3ExpenseLists = $this->Project->findChild3ExpenseList($childExpenseList['id']); ?>
                                    <?php foreach ($child3ExpenseLists as $k => $child3ExpenseList): ?>
                                        <?php $index++; ?>
                                        <?php $child3ExpenseList = $child3ExpenseList[0]; ?>
                                        <tr class="budget-list-level3" data-accordion-child-of="<?php echo $expenseList['id']; ?>" style="display: none;">
                                            <td>
                                                <?php echo ($child3ExpenseList['name']); ?>
                                                {<?php echo $this->Project->optionBudgetChildL3('all', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']); ?>}
                                                <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'id' => false, 'value' => $child3ExpenseList['id'])); ?>
                                            </td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('year-2', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 2][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 2][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('year-1', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']); ?>><?php echo isset($options[$budgetYear - 1][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear - 1][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?></td>
                                            <td class="text-right"><?php printf("<input type='text' name='data[OwnerBudget][{$index}][primary_budget]' class='form-control currency money-right' %s />", $this->Project->optionBudgetChildL3('1', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id'])); ?></td>
                                            <td class="text-right"><?php printf("<input type='text' name='data[OwnerBudget][{$index}][secondary_budget]' class='form-control currency money-right' %s />", $this->Project->optionBudgetChildL3('2', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id'])); ?></td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('total', $child3ExpenseList['id'], $childExpenseList['id']); ?>>
                                                <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'id' => false, 'class' => 'currency money-right')); ?> 
                                                <?php echo isset($options[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) ? $this->Zicure->currencyFormat($options[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailManeuver']['sum_budget']) : $this->Zicure->currencyFormat(0); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <!--End Expense list level III-->   

                                    <?php $index++; ?>
                                <?php endforeach; ?>
                                <!--End Expense list level II-->

                                <?php $index++; ?>
                            <?php endforeach; ?>
                            <!--End Expense list level I-->

                        <?php else: ?>
                            <?php echo '<tr class="text-center notfound"><td colspan="6">' . __('Information Not Found') . '</td></tr>'; ?>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr class="sum-budget-list text-right">
                            <td><?php echo ('รวมเป็นเงิน'); ?></td>
                            <td data-is-grand-total="true" data-section="year-2"><?php echo $this->Form->hidden('sum_budget_total', array('class' => 'currency money-right')) . $this->Zicure->currencyFormat(0); ?></td>
                            <td data-is-grand-total="true" data-section="year-1"><?php echo $this->Form->hidden('sum_budget_total', array('class' => 'currency money-right')) . $this->Zicure->currencyFormat(0); ?></td>
                            <td data-is-grand-total="true" data-section="1"><?php echo $this->Form->hidden('sum_budget_total', array('class' => 'currency money-right')) . $this->Zicure->currencyFormat(0); ?></td>
                            <td data-is-grand-total="true" data-section="2"><?php echo $this->Form->hidden('sum_budget_total', array('class' => 'currency money-right')) . $this->Zicure->currencyFormat(0); ?></td>
                            <td data-is-grand-total="true" data-section="total"><?php echo $this->Form->hidden('sum_budget_total', array('class' => 'currency money-right')) . $this->Zicure->currencyFormat(0); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <?php echo $this->Form->input('objective', array('name' => "data[ExpenseHeaderManeuver][objective]", 'type' => 'textarea')); ?>
                <div class="row form-group">
                    <?php echo $this->Form->input('kpi1', array('name' => "data[ExpenseHeaderManeuver][kpi1]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi1')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi1'))); ?>
                    <?php echo $this->Form->input('kpi1_result', array('name' => "data[ExpenseHeaderManeuver][kpi1_result]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi1 result')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi1 result'))); ?>
                </div>
                <div class="row form-group">
                    <?php echo $this->Form->input('kpi2', array('name' => "data[ExpenseHeaderManeuver][kpi2]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi2')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi2'))); ?>
                    <?php echo $this->Form->input('kpi2_result', array('name' => "data[ExpenseHeaderManeuver][kpi2_result]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi2 result')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi2 result'))); ?>
                </div>
                <div class="row form-group">
                    <?php echo $this->Form->input('kpi3', array('name' => "data[ExpenseHeaderManeuver][kpi3]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi3')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi3'))); ?>
                    <?php echo $this->Form->input('kpi3_result', array('name' => "data[ExpenseHeaderManeuver][kpi3_result]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi3 result')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi3 result'))); ?>
                </div>
                <div class="row form-group">
                    <?php echo $this->Form->input('kpi4', array('name' => "data[ExpenseHeaderManeuver][kpi4]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi4')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi4'))); ?>
                    <?php echo $this->Form->input('kpi4_result', array('name' => "data[ExpenseHeaderManeuver][kpi4_result]", 'class' => 'form-control', 'div' => false, 'label' => array('class' => 'col-sm-2', 'text' => __('kpi4 result')), 'wrap' => 'col-sm-4', 'placeholder' => __('kpi4 result'))); ?>
                </div>
                <div class="form-group divMultiplieFile">
                    <label class="col-sm-2 control-label"><?php echo __('Orther Attachment File'); ?></label>
                    <div class="col-sm-10">
                        <input type="file" name="data[ExpenseHeaderManeuver][document_attachment_id][]" class="mutiplefile" accept="<?php echo Configure::read('CORE.DOCUMENT.OFFICE_PDF'); ?>" data-msg-accept="<?php echo Configure::read('OAC.DOCUMENT.OFFICE_PDF.MASSAGE'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/OwnerBudgets/add'); ?>
        <?php echo $this->Form->button('<i class="fa fa-plus"></i>' . __('Add Another File'), array('type' => 'button', 'name' => 'btnAddMoreFile', 'id' => 'btnAddMoreFile', 'class' => 'btn btn-success')); ?>
        <?php echo $this->Permission->linkBack("/OwnerBudgets/inbox"); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $(function () {
        $("input[type='text'].currency,input[type='hidden'].currency,input[type='number']").autoNumeric('init', {aSep: ',', aDec: '.', aSign: '฿ '});

        //Toggle for table up and down
        $("[data-accordion-parent]").click(function () {
            var parent_id = $(this).data('accordion-parent');
            var $drill_down_icon = $("span[role='drill-down-icon']", $(this));
            var $child = $("tr[data-accordion-child-of='" + parent_id + "']");
            $child.slideToggle("fast", function () {
                if ($child.is(":visible")) {
                    $drill_down_icon.removeClass("glyphicon-triangle-right");
                    $drill_down_icon.addClass("glyphicon-triangle-bottom");
                } else {
                    $drill_down_icon.removeClass("glyphicon-triangle-bottom");
                    $drill_down_icon.addClass("glyphicon-triangle-right");
                }
            });
        });

        //Add more file button behavior
        var el = '<div class="form-group divMultiplieFile">' + $(".divMultiplieFile").html() + '</div>';
        $("#btnAddMoreFile").click(function () {
            $(".divMultiplieFile:last").after(el);
        });

        $('.currency').on('focusout', function () {
            autoCalculation($(this), $("#BudgetDetailTable"));
        });

        //$('.currency').trigger('focusout');
    });

    function autoCalculation(events, table) {
        var $this = events;
        var parentId = $this.data("parent-id");
        var section = $this.data("section");
        var grandParentId = $this.data('grand-parent-id') || -1;
        var $parent = $("[data-id='" + parentId + "'][data-section='" + section + "']", table);
        var $parentSectionTotal = $("[data-id='" + parentId + "'][data-section='total']", table);
        var $grandParentSectionTotal = $("[data-id='" + grandParentId + "'][data-section='total']", table);
        var parentTotal = 0;

        $("[data-parent-id='" + parentId + "'][data-section='" + section + "']", table).each(function () {
            var $child = $(this);
            var childId = $child.data('id');
            var child1Val;
            var tagName = $child.prop("tagName");
            var childTotal = 0;
            if (tagName === "INPUT") {
                child1Val = $child.autoNumeric('get') || 0;
            } else {
                child1Val = $child.text().replace(/[^0-9\.]+/g, '') || 0;
            }

            if (!child1Val) {
                child1Val = 0;
            }
            parentTotal += (childTotal = numeral(child1Val).value());
            if (section != "total" && section != "year-2" && section != "year-1") {

                var $child2;
                var child2Val;
                if (section == "1") {
                    $child2 = $("[data-id='" + childId + "'][data-section='2']", table);
                } else if (section == "2") {
                    $child2 = $("[data-id='" + childId + "'][data-section='1']", table);
                }

                tagName = $child2.prop("tagName");
                if (tagName === "INPUT") {
                    child2Val = $child2.autoNumeric('get') || 0;
                } else {
                    child2Val = $child2.text().replace(/[^0-9\.]+/g, '') || 0;
                }
                if (!child2Val) {
                    child2Val = 0;
                }
                childTotal += numeral(child2Val).value();
                $("[data-id='" + childId + "'][data-section='total']", table).text(numeral(childTotal).format('0,0.00') + ' ฿');
                //$("[data-id='" + childId + "'][data-section='total']", table).change();
            }

        });

        //Set trail total summation
        $parent.text(numeral(parentTotal).format('0,0.00') + ' ฿');

        //Level 1 parent
        var r = $('[data-id="' + parentId + '"][data-section="year-1"]', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var s = $('[data-id="' + parentId + '"][data-section="year-2"]', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var o = $('[data-id="' + parentId + '"][data-section="1"]', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var t = $('[data-id="' + parentId + '"][data-section="2"]', table).text().replace(/[^0-9\.]+/g, '') || 0;
        $parentSectionTotal.text(numeral(0).add(r).add(s).add(o).add(t).format('0,0.00') + ' ฿');
        if (grandParentId !== -1) {
            //Calculate all of parent value of level 3
            var budgets = {'year-1': 0, 'year-2': 0, '1': 0, '2': 0, 'total': 0};
            $('[data-parent-id="' + grandParentId + '"]', table).each(function () {
                var t = $(this).text().replace(/[^0-9\.]+/g, '') || 0;
                budgets[$(this).data('section')] += numeral(t).value();
            });

            $('[data-id="' + grandParentId + '"][data-section="year-2"]', table).text(numeral(budgets['year-2']).format('0,00.00') + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="year-1"]', table).text(numeral(budgets['year-1']).format('0,00.00') + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="1"]', table).text(numeral(budgets['1']).format('0,00.00') + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="2"]', table).text(numeral(budgets['2']).format('0,00.00') + ' ฿');
            $grandParentSectionTotal.text(numeral(0).add(budgets['year-1']).add(budgets['year-2']).add(budgets['1']).add(budgets['2']).format('0,0.00') + ' ฿');
        }


        //Make all grand total
        var budgets = {'year-1': 0, 'year-2': 0, '1': 0, '2': 0, 'total': 0};
        $("[data-is-parent]", table).each(function () {
            var t = $(this).text().replace(/[^0-9\.]+/g, '') || 0;
            budgets[$(this).data('section')] += numeral(t).value();
        });

        $("[data-is-grand-total][data-section='year-2']").text(numeral(budgets['year-2']).format('0,00.00') + ' ฿');
        $("[data-is-grand-total][data-section='year-1']").text(numeral(budgets['year-1']).format('0,00.00') + ' ฿');
        $("[data-is-grand-total][data-section='1']").text(numeral(budgets['1']).format('0,00.00') + ' ฿');
        $("[data-is-grand-total][data-section='2']").text(numeral(budgets['2']).format('0,00.00') + ' ฿');
        $("[data-is-grand-total][data-section='total']").text(numeral(0).add(budgets['year-1']).add(budgets['year-2']).add(budgets['1']).add(budgets['2']).format('0,0.00') + ' ฿');

        //$parent.change();
    }
</script>