<div class="box box-info">

    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Configure Management System')); ?>
    <?php
    echo $this->Form->create('Config', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'col-sm-6 form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-4 control-label'),
            'between' => '<div class="col-sm-8">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>

    <div class="box-body">
        <?php echo $this->Form->input('value', array('label' => __('Config Value'))); ?>
        <?php echo $this->Form->input('type', array('label' => __('Config Type'))); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/Configs/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add Config"), "/Configs/add"); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>