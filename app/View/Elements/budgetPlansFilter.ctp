<div class="box box-info">
    <?php //echo $this->element('boxOptionHeader', array('btitle' => 'Inbox'));
		echo $this->element('boxOptionHeader', array('btitle' => $btitle));
    ?>
    <?php
    echo $this->Form->create('InboxFilter', array('class' => 'form-horizontal'));
    ?>
    <div class="box-body">
        <?php
            if($title == 'Inbox'){
              echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => array('text'=>__('Budget Year'))));
              echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $this->Project->findListWorkGroupAllocated()));
              echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $this->Project->findListWorkAllocated()));
              echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $this->Project->findListProjectAllocated()));
            }else{
              echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => array('text'=>__('Budget Year'))));
              echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $this->Project->findListWorkGroupAllocated()));
              if($system_has_process != 27){
                echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $this->Project->findListWorkAllocated()));
                echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $this->Project->findListProjectAllocated()));
              }
            }

        ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/BudgetPlans/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
