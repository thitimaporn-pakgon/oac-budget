<?php echo $this->Permission->button('<i class="fa fa-arrow-circle-down"></i>' . __('ExportExcel'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn bg-orange separate-button pull-right confirmModal separate-button', 'name' => 'btnExcel', 'id' => 'btnExcel')); ?>
<?php echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('Reject'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-danger pull-right confirmModal separate-button', 'name' => 'btnWorkManeuverReject', 'id' => 'btnWorkManeuverReject')); ?>

<script type="text/javascript">

    $(function () {

        //Dosz Akenoppawut Code.
        var expenseIds;
        $("input:checkbox").on('ifChanged', function (event) {
            var val = [];
            var expenseId;
            $("input:checkbox:not(.check-all):checked").each(function (i) {
                val[i] = $(this).val();
                expenseId = val[i].split("|");
                val[i] = expenseId[0];
            });
            expenseIds = val.toString();

            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            if ($(this).val() == '') {
                $("input:checkbox").iCheck(tmpCheck);
            }
            $('#btnExcel').attr('disabled', !($("input:checkbox:checked").length > 0));
            $('#btnWorkManeuverReject').attr('disabled', !($("input:checkbox:checked").length > 0));

        });

        $("#btnExcel").click(function () {
            var form = $(this).closest('form');
            var controller = '<?php echo $this->params['controller']; ?>';
            form.attr('action','/' + controller + '/jasperExporRptManeuverExcel/' + expenseIds);
        });

        $("#btnWorkManeuverReject").click(function () {
            var form = $(this).closest('form');
            form.attr('action', 'rejectBudget');
        });
        //======================================

    });

</script>