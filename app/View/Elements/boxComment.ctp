<?php
$btitle = isset($btitle) ? $btitle : 'Zicure';
$disabled = isset($disabled) ? $disabled : false;
$bshow = isset($bshow) ? $bshow : false;
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
$listFunctionStatus = ($bshow === true) ? 'getListProjectPlanStatusOrigin' : 'getProjectPlanStatus';
$isCommentRequired = empty($this->data['ProjectComment']) ? 'required' : null;
$spcProcess = ((strtoupper($this->data[$model]['status']) == 'AL0') && (!empty($this->data[$model]['approved_department_id'])));
$controller = Inflector::pluralize($model);
$hasCommentConclusion = false;
$hasComment = false;
?>

<div class="box box-warning">
    <?php echo $this->element('boxOptionHeader', array('btitle' => __($btitle), 'bcollap' => true, 'bclose' => true, 'bnoti' => __($bnoti))); ?>
    <?php echo $this->Form->create('ProjectComment', array('class' => 'form-horizontal')); ?>
    <div class="box-body">
        <div class="col-md-12">
            <?php echo $this->Form->input("{$model}.id"); ?>
            <?php if ($bshow === true): ?>
                <div class="divAreaContent">
                    <?php echo $this->Form->input('comment_txt', array('name' => 'data[ProjectComment][comment_txt][]', 'class' => $isCommentRequired, 'disabled' => $disabled, 'rows' => 3)); ?>

                </div>
            <?php else: ?>
                <?php if (!empty($this->data['ProjectComment'])): ?>
                    <?php foreach ($this->data['ProjectComment'] as $k => $v): ?>
                        <?php if (!empty($v['comment_txt'])): ?>
                            <?php $hasComment = true; ?>
                            <?php echo $this->Form->input('comment_txt', array('rows' => 3, 'value' => $v['comment_txt'], 'disabled' => $disabled)); ?>
                        <?php endif; ?>
                        <?php if (!empty($v['comment_conclusion'])): ?>
                            <?php $hasCommentConclusion = true; ?>
                            <?php echo $this->Form->input('comment_conclusion', array('id' => 'ProjectCommentCommentConclusion' . $k, 'name' => 'data[ProjectComment][comment_conclusion][]', 'value' => $v['comment_conclusion'], 'disabled' => true, 'rows' => 8)); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>

            <!-- check must be approved by spc -->
            <?php if (in_array($this->data[$model]['status'], array('WE', 'WA')) && (empty($this->data[$model]['approved_department_id'])) && ($disabled === false)): ?>
                <?php echo $this->Form->input("{$model}.approved_department_id", array('options' => $this->Zicure->listDepartment($this->Zicure->getCurrenSessionDepartmentId(), true), 'disabled' => $disabled, 'class' => 'required')); ?>
            <?php endif; ?>

            <!-- Add more conclusion -->
            <?php if (($spcProcess === true) && ($disabled === false)): ?>
                <div class="divConclusion">
                    <?php echo $this->Form->input('comment_conclusion', array('class' => 'required', 'rows' => 12)); ?>
                </div> <!--./div.divConclusion -->
            <?php endif; ?>

            <?php //echo $this->Form->input('ProjectPlan.status', array('options' => $this->Zicure->$listFunctionStatus(), 'disabled' => $disabled)); ?>
        </div><!-- /.col-md-12 -->
    </div><!-- /.box-body -->
    <div class="box-footer">
        <?php if ($bshow): ?>
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Submit'), $_SERVER['REQUEST_URI'], array('name' => 'btnsubmit', 'class' => 'btn btn-primary confirmModal', 'rel' => __('Are you sure for add comment to the project plan ?'), 'btitle' => __('Confirm for add comment to the project plan'))); ?>
            <?php echo $this->Form->button($this->Bootstrap->icon('fa-plus', 'Add Another Comment'), array('type' => 'button', 'name' => 'btnAddMoreComment', 'id' => 'btnAddMoreComment', 'class' => 'btn btn-success')); ?>
            <?php //echo $this->Form->button($this->Bootstrap->icon('fa-plus', 'Add Another Conclusion'), array('type' => 'button', 'name' => 'btnAddMoreConclusion', 'id' => 'btnAddMoreConclusion', 'class' => 'btn btn-warning')); ?>
        <?php endif; ?>
        <?php if ($hasComment === true): ?>
            <?php echo $this->Permission->confirmButton($this->Bootstrap->icon('fa-print', __("Print") . ' ' . __('Comment (Owner budget)')), "/{$controller}/printCommentBudget/{$id}", array('name' => 'btnPrint', 'class' => 'btn btn-success btn-flat confirmModal', 'rel' => __('Are you sure for print comment (Owner budget) ?'), 'btitle' => __('Print comment (Owner budget)'))); ?>
            <?php echo $this->Permission->confirmButton($this->Bootstrap->icon('fa-print', __("Print") . ' ' . __('Comment (Cover Department)')), "/{$controller}/printCommentMain/{$id}", array('name' => 'btnPrint', 'class' => 'btn btn-warning btn-flat confirmModal', 'rel' => __('Are you sure for print comment (Cover Department) ?'), 'btitle' => __('Print comment (Cover Department)'))); ?>
        <?php endif; ?>
        <?php if ($hasCommentConclusion === true): ?>
            <?php echo $this->Permission->confirmButton($this->Bootstrap->icon('fa-print', __("Print") . ' ' . __('Comment (Board)')), "/{$controller}/printCommentConclusion/{$id}", array('name' => 'btnPrint', 'class' => 'btn btn-danger btn-flat confirmModal', 'rel' => __('Are you sure for print comment (Board) ?'), 'btitle' => __('Print comment (Board)'))); ?>
        <?php endif; ?>
        <?php echo $this->Permission->buttonBack(); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
    $(function () {
        var el = '<div class="divAreaContent">' + $(".divAreaContent").html() + '</div>';
        $("#btnAddMoreComment").click(function () {
            $(".divAreaContent:last").after(el);
        });

        //Add for another conclusion
//        var el = '<div class="divConclusion">' + $(".divConclusion").html() + '</div>';
//        $("#btnAddMoreConclusion").click(function () {
//            $(".divConclusion:last").after(el);
//        });
    });
</script>