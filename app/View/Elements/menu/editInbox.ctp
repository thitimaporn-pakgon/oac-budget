<?php
/**
*
* Edit page for workManeuversController it EditInbox of WorkManeuver.
* @author Numpol.J 
* @since 2017-04-27 02:45:30
* @license Zicure Corp. 
*/
?>
<div class="workManeuvers form div-form-submit box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle'=>'Edit Inbox', 'bcollap'=>true, 'bclose'=>true)); ?>
    <?php echo $this->Form->create('WorkManeuver', array('role'=>'form','class'=>'form-horizontal')); ?>
    <div class="box-body">
        <?php echo $this->Form->input('id'); ?>
	<?php echo $this->Form->input('code'); ?>
	<?php echo $this->Form->input('name'); ?>
	<?php echo $this->Form->input('to_department_id'); ?>
    </div><!-- /div.box box-body -->
    <div class="box-footer">
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/WorkManeuvers/editInbox'); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
        <?php echo $this->Form->end(); ?>
    </div><!-- /div.box box-footer -->
</div><!-- /div.box box-primary -->
