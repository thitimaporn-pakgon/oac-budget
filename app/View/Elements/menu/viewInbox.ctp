<?php

/**
*
* View page for workManeuversController it show for Inbox From WorkGroupManeuver infomation.
* @author Numpol.J 
* @since 2017-04-27 01:13:10
* @license Zicure Corp. 
*/
?>
<?php // debug($inbox); exit;?>
<div class="workManeuvers view div-view-information box box-warning">
    	<?php echo $this->element('boxOptionHeader', array('btitle'=>'InBox From WorkGroupManeuver Information', 'bcollap'=>true, 'bclose'=>true));?>
    <div class="box-body">
        <table class="table-view-information table table-bordered table-striped">
            <tbody>
                <tr>
                    <td class="table-view-label"><?php echo __('In Box Id'); ?></td>
                    <td class="table-view-detail"><?php echo h($inbox['Inbox']['id']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Sub Ject'); ?></td>
                    <td class="table-view-detail"><?php echo h($inbox['Inbox']['subject']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('From Department Id'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->findDepartmentShortName($inbox['Inbox']['from_department_id']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('To Department Id'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->findDepartmentShortName($inbox['Inbox']['to_department_id']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('From Uid'); ?></td>
                    <td class="table-view-detail"><?php echo empty($inbox['Inbox']['from_uid']) ? '-' : h($inbox['Inbox']['from_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('To Uid'); ?></td>
                    <td class="table-view-detail"><?php echo empty($inbox['Inbox']['to_uid']) ? '-' : h($inbox['Inbox']['to_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('System Has Process'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($inbox['SystemHasProcess']['name'],'info'), "/system_has_processes/view/{$inbox['SystemHasProcess']['id']}" ,array('title'=>__('View') . ' ' . __('System Has Process'))); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Ref Id'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Permission->link($this->Bootstrap->badge($inbox['WorkGroupManeuver']['name']), "/work_group_maneuvers/view/{$inbox['WorkGroupManeuver']['id']}", array('title' => _('View') . ' ' . _('Work Group Maneuver'))); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Ref Model'); ?></td>
                    <td class="table-view-detail"><?php echo h($inbox['Inbox']['ref_model']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Task Flag'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getInboxTaskFlag($inbox['Inbox']['task_flag']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Remark'); ?></td>
                    <td class="table-view-detail"><?php echo empty($inbox['Inbox']['remark']) ? '-' : h($inbox['Inbox']['remark']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Status'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getInboxStatus($inbox['Inbox']['status']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Create Uid'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->getUserById($inbox['Inbox']['create_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Update Uid'); ?></td>
                    <td class="table-view-detail"><?php echo empty($inbox['Inbox']['update_uid']) ? '-' : $this->Zicure->getUserById($inbox['Inbox']['update_uid']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Created'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($inbox['Inbox']['created']); ?></td>
                </tr>
                <tr>
                    <td class="table-view-label"><?php echo __('Modified'); ?></td>
                    <td class="table-view-detail"><?php echo $this->Zicure->dateTimeISO($inbox['Inbox']['modified']); ?></td>
                </tr>
            </tbody>
        </table><!-- ./table table-view-information -->
    </div><!-- ./div.box-body -->
    	<?php echo $this->element('boxOptionFooter');?>
</div><!-- ./div.box box-warning -->