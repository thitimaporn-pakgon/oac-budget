<div class="workManeuvers index zicure-index">
    <!-- zicure-box-find search input params section -->
    <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle'=>'In Box Search', 'bcollap'=>true, 'bclose'=>true));?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal'));?>
            <?php echo $this->Form->input('name', array('label'=>array('text'=>__('name'))));?>
            <?php echo $this->Form->input('budget_year_id', array('options' => $budgetYears));?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '/WorkManeuvers/workManeuverList', array('name'=>'btnSubmitSearch'));?>
            <?php echo $this->Permission->buttonBack();?>
            <?php echo $this->Form->end();?>
        </div><!-- /div.box-footer -->
    </div><!-- /div.zicure-box-find box box-warning -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle'=>'InBox (Result)', 'bcollap'=>true, 'bclose'=>true));?>
        <?php echo $this->Form->create('WorkManeuver', array('class' => 'form-horizontal')); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#');?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Name Work Group Maneuver'));?></th>
                        <th><?php echo $this->Paginator->sort('name', __('Name Work Maneuver'));?></th>
                        <th><?php echo $this->Paginator->sort('modified');?></th>
                    </tr>
                <td>&nbsp;<input type="checkbox" id="checkAll" name="checkAll"/></td>
                </thead> 
                <tbody>
                    <?php // debug($pendings); exit;?>
                    <?php if(!empty($pendings)): ?>
                    <?php foreach ($pendings as $k => $pending): ?>
                    <tr>
                        <td class="nindex"><input type="checkbox" name="data[WorkManeuver][][id]" value="<?php echo $pending['WorkGroupManeuver']['id'] . '|' . $pending['WorkManeuver']['id']; ?>">
                            <?php echo $this->Paginator->counter('{:start}') + $k; ?>
                        </td>
                        <td><?php echo $this->Permission->link($pending['WorkGroupManeuver']['name'], "/WorkManeuvers/workManeuverList/{$this->Zicure->secureEncodeParam($pending['WorkGroupManeuver']['id'])}"); ?></td>
                        <td><?php echo ($pending['WorkManeuver']['name']); ?></td>
                        <td><?php echo $this->Zicure->dateISO($pending['WorkManeuver']['modified']); ?></td>
                        <!--<td class="actions"><?php // echo $this->Permission->getActions($pending['Inbox']['id'],'Inboxs'); ?></td>-->
                    </tr>
                    <?php endforeach; ?>
                    <?php else:?>
                    <?php echo '<tr class="text-center notfound"><td colspan="13">' . __('Information Not Found') . '</td></tr>'; ?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
        <div class="box box-footer">
            <?php echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('Approve'),'/WorkManeuvers/approveWorkManeuver', array('disabled' => true, 'type' => 'button', 'class' => 'btn btn-success pull-right separate-button', 'onclick' => false, 'name' => 'btnWorkManeuverApprove', 'id' => 'btnWorkManeuverApprove')); ?>
        <?php echo $this->Form->end(); ?>
        </div>
        <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options'=> true));?>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->

<script type="text/javascript">
    var countItem = $("input[type='checkbox']:checked").length;
    $(function () {

        $('#btnWorkManeuverApprove').click(function () {
            confirmModal('<?php echo __('Please confirm for commit the Work Maneuver ?'); ?>', function (result) {
                if (result == true) {
                    $('FORM#WorkManeuverPendingForm').attr('action', '/WorkManeuvers/approveWorkManeuver').submit();
                    return true;
                } else {
                    return false;
                }
            });
        });

        $("input[type='checkbox']").on('ifChecked', function (e) {
            var actionClass = e.target.className, inputName = e.target.name;
            countItem++;
            if (countItem > 0) {
                $('#btnWorkManeuverApprove').attr('disabled', false);
            }
        });

        $("input[type='checkbox']").on('ifUnchecked', function (e) {
            var actionClass = e.target.className, inputName = e.target.name;
            countItem--;
            if (countItem <= 0) {
                $('#btnWorkManeuverApprove').attr('disabled', true);
            }
        });
    });

</script>