<div class="workManeuvers index zicure-index">
    <!-- zicure-box-find search input params section -->
    <div class="box box-warning zicure-box-find">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Inbox Search', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <?php echo $this->Form->create('Search', array('class' => 'form-horizontal')); ?>
            <?php echo $this->Form->input('name', array('label' => array('text' => __('name')))); ?>
            <?php echo $this->Form->input('budget_year_id', array('options' => $budgetYears));?>
        </div><!-- /div.box-body -->
        <div class="box-footer">
            <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '/WorkManeuvers/inbox', array('name' => 'btnSubmitSearch')); ?>
            <?php echo $this->Permission->buttonBack(); ?>
            <?php echo $this->Form->end(); ?>
        </div><!-- /div.box-footer -->
    </div><!-- /div.zicure-box-find box box-warning -->
    <div class="box box-info zicure-box-result">
        <?php echo $this->element('boxOptionHeader', array('btitle' => 'Inbox (Result)', 'bcollap' => true, 'bclose' => true)); ?>
        <div class="box-body">
            <table class="zicure-table-result table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="nindex"><?php echo __('#'); ?></th>
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="actions">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php // $workGroupId = $this->Zicure->secureEncodeParam($inboxs['WorkGroupManeuver']['id'])?>
                    <?php // debug($workGroupId); exit;?>
                    <?php if(!empty($inboxs)):?>
                    <?php foreach ($inboxs as $k => $inbox): ?>
                    <tr>
                        <td class="nindex"><?php echo $this->Paginator->counter('{:start}') + $k; ?></td>
                        <td><?php echo $this->Permission->link($inbox['WorkGroupManeuver']['name'], "/WorkManeuvers/workManeuverList/{$this->Zicure->secureEncodeParam($inbox['WorkGroupManeuver']['id'])}"); ?></td>
                        <td><?php echo $this->Zicure->dateISO($inbox['WorkGroupManeuver']['modified']); ?></td>
                        <td class="actions"><?php echo $this->Project->getActionsMenuInbox($inbox['WorkGroupManeuver']['id'],'WorkManeuvers'); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    <?php else:?>
                    <?php echo '<tr class="text-center notfound"><td colspan="13">' . __('Information Not Found') . '</td></tr>'; ?>
                    <?php endif;?>
                </tbody>
            </table>
            <!-- Page Pagination Section -->
            <?php echo $this->element('paginate_pages', array('options' => true)); ?>
        </div>
    </div><!-- /div.zicure-box-result box box-info -->
</div><!-- /div.index zicure-index -->