<?php
$disabled = isset($disabled) ? $disabled : true;
$data = isset($data) ? $data : false;
?>
<div class="box-body">
    <?php // debug($data);exit; ?>
    <div id="treeview">
    </div>
</div><!-- ./div.box-body -->
<script>
    $(function () {
        var data = [<?php echo $data; ?>];
        $('#treeview').treeview({
            color: "#428bca",
            expandIcon: "glyphicon glyphicon-stop",
            collapseIcon: "glyphicon glyphicon-unchecked",
            nodeIcon: "glyphicon glyphicon-circle-arrow-right",
            showTags: true,
            highlightSelected: false,
            data: data
        });
    });
</script>