<?php

$budgetYearLists = $this->Zicure->findListBudgetYear();
unset($budgetYearLists['']);
?>

<?php echo $this->Form->hidden('source_budget_year_id', array('id' => 'targetCloneFromBudgetYear')); ?>
<div class="pull-right btn-group structureAllocated">
    <button type="button" class="btn btn-info btn-flat"><?php echo __('Pull Allocated'); ?></button>
    <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only"><?php echo __('Allocated'); ?></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <?php foreach ($budgetYearLists as $k => $v): ?>
        <li><?php echo $this->Permission->link($v, "/StrategicManeuvers/getPullDataAllocate/{$this->Zicure->secureEncodeParam($v)}", array('name' => 'btnPullAllocate', 'class' => 'separate-button', 'data-source-budget-year' => $v)); ?></li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="pull-right btn-group structureManeuver">
    <button type="button" class="btn btn-warning btn-flat col-sm-10"><?php echo __('Pull Maneuver'); ?></button>
    <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only"><?php echo __('Maneuver'); ?></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <?php foreach ($budgetYearLists as $k => $v): ?>
        <li><?php echo $this->Permission->link($v, "/StrategicManeuvers/getPullDataManeuver/{$this->Zicure->secureEncodeParam($v)}", array('name' => 'btnPullManeuver', 'class' => 'separate-button', 'data-source-budget-year' => $v)); ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<script type="text/javascript">
    $(function () {

        /**
         * 
         * Function submit clone budget information
         * @author Numpol.J
         * @returns void if confirm then submit form to be start clonning
         */
        $('a[name="btnPullManeuver"],a[name="btnPullAllocate"]').click(function (e) {
            e.preventDefault();
            var inputName = $(this).attr('name');
            var title = null, action = null;
            var targetCloneFromBudgetYear = $(this).data('source-budget-year');
            var ddlCloneToTargetBudgetYear = $('#ddlCloneToTargetBudgetYear').val();

            if (inputName == 'btnPullManeuver') {
                if(ddlCloneToTargetBudgetYear <= targetCloneFromBudgetYear){
                    AppWarning('ไม่สามารถดึงโครงสร้างได้ ปีที่ต้องการดึงงบประมาณต้องน้อยกว่าปีที่ต้องการนำมาทำรายการ, กรุณาลองใหม่อีกครั้ง');
                    return false;
                }
                title = 'ภาคจัดทำ';
                action = '/StrategicManeuvers/getPullDataManeuver/';
            } else if (inputName == 'btnPullAllocate') {
                if(ddlCloneToTargetBudgetYear < targetCloneFromBudgetYear){
                    AppWarning('ไม่สามารถดึงโครงสร้างได้ ปีที่ต้องการดึงงบประมาณต้องน้อยกว่าหรือเท่าปีที่ต้องการนำมาทำรายการ, กรุณาลองใหม่อีกครั้ง');
                    return false;
                }
                title = 'ภาคจัดสรร';
                action = '/StrategicManeuvers/getPullDataAllocate';
            } else {
                AppWarning("ข้อมูลไม่สอดคล้องกรุณาระบุ ปีงบประมาณที่ถูกต้อง");
                return false;
            }

            $("#targetCloneFromBudgetYear").val(targetCloneFromBudgetYear);
            if ((ddlCloneToTargetBudgetYear == '') || (targetCloneFromBudgetYear == '')) {
                AppDanger('กรุณาเลือกปีงบประมาณปลายทาง หรือปีงบประมาณต้นทางของการดึงโครงสร้างสำหรับ ' + title + 'ด้วย!');
            } else {
                $(".modal-title").text('ยืนยันการดึงโครงสร้างงบประมาณ สำหรับ' + title);
                confirmModal('ท่านต้องการดึงโครงสร้างงบประมาณจากปีงบประมาณ ' + targetCloneFromBudgetYear + ' มาใช้งานในปีงบประมาณ ' + ddlCloneToTargetBudgetYear + ' สำหรับ ' + title + ' หรือไม่ ? (ข้อมูลเดิมในปีงบประมาณ ' + ddlCloneToTargetBudgetYear + ' จะโดนทับ)', function (result) {
                    if (result == true) {
                        $('#SearchIndexForm').attr('action', action);
                        $('#SearchIndexForm').submit();
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        });

        $('#btnSearch').click(function () {
            $('#SearchIndexForm').attr('action', '/StrategicManeuvers/index');
            $('#SearchIndexForm').submit();
        });
    });
</script>