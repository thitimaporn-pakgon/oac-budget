<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'System Access Logs Management System')); ?>
    <?php
    echo $this->Form->create('Search', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-2 control-label'),
            'between' => '<div class="col-sm-10">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('actions'); ?>
        <?php echo $this->Form->input('createFrom', array('class' => 'datepicker datepicker-start', 'type' => 'text')); ?>
        <?php echo $this->Form->input('createTo', array('class' => 'datepicker datepicker-end', 'type' => 'text')); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/AccessLogs/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>