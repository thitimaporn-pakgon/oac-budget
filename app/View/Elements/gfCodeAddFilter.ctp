<div class="box box-info">
    <?php //echo $this->element('boxOptionHeader', array('btitle' => 'Inbox'));
        echo $this->element('boxOptionHeader', array('btitle' => 'GFMISManagement'));
    ?>
    <?php
    echo $this->Form->create('GFCodesFilter', array('class' => 'form-horizontal'));
    ?>
    <div class="box-body">
    <?php
      if($mode == "add"){
        echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
        echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $work_grouplist));
        echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $work_list));
        echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $project_list));
        echo $this->Form->input('project_list', array('label'=>array('text'=>__('รายการ')),'options' => $list_projectlist));
      }else{
        echo $this->Form->input('budget_year_id', array('label' => __('Budget Year'), 'type'=>'text', 'value'=>$year, 'readonly'=>'readonly', 'placeholder'=>''));
        echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'value' => (!empty($work_group)) ? $work_group_name[$work_group] : '', 'readonly'=>'readonly', 'placeholder'=>''));
        echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')), 'value' => (!empty($work)) ? $work_name[$work] : '', 'readonly'=>'readonly', 'placeholder'=>''));
        echo $this->Form->input('project', array('label'=>array('text'=>__('การ')), 'value' =>  (!empty($project)) ? $project_name[$project] : '', 'readonly'=>'readonly', 'placeholder'=>''));
        echo $this->Form->input('project_list', array('label'=>array('text'=>__('รายการ')), 'value' => (!empty($project_list)) ? $project_list_name[$project_list] : '', 'readonly'=>'readonly', 'placeholder'=>''));
      }

    ?>
    </div>
    <?php if($mode == "add"){ ?>
    <div class="box-footer">
        <button id="btnShowlistdata" type="button" value="show_data" class="btn btn-primary"><?php echo __('Show Data'); ?></button>
        <?php //echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/GFCodes/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php } ?>
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
