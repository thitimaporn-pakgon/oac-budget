<!-- Horizontal Form -->
<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'User Management System')); ?>
    <?php
    echo $this->Form->create('Search', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'col-sm-6 form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-5 control-label'),
            'between' => '<div class="col-sm-7">',
            'wrap'=>'col-md-12',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('username'); ?>
        <?php echo $this->Form->input('name', array('label' => __('Name & Surename'), 'class' => 'simple', 'placeholder' => __('Name & Surename'))); ?>
        <?php echo $this->Form->input('military_no'); ?>
        <?php echo $this->Form->input('personal_card_no', array('class' => 'citizen_no')); ?>
        <?php echo $this->Form->input('emial', array('class' => 'email')); ?>
        <?php echo $this->Form->input('phone', array('type' => 'text')); ?>
        <?php echo $this->Form->input('role_id', array('options' => $roles)); ?>
        <?php echo $this->Form->input('department_id', array('options' => $departments)); ?>
        <?php echo $this->Form->input('loginfail', array('label' => array('text' => __('Locked')), 'options' => $this->Zicure->getUnlocked())); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/Users/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add Data"), "/Users/add/"); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->