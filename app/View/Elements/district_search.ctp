<?php
$selectedProvince = @$this->data['District']['province_id'];
?>
<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'District Management System')); ?>
    <?php
    echo $this->Form->create('District', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-2 control-label'),
            'between' => '<div class="col-sm-10">',
            'wrap' => 'col-sm-12',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('district_name', array('label' => array('text' => __('District Name')))); ?>
        <?php echo $this->Form->input('province_id', array('id' => 'ProvinceId', 'options' => $this->Zicure->ListProvince(), 'label' => array('text' => __('Province Name')))); ?>
        <?php //echo $this->Form->input('district_id', array('id' => 'DistrictId', 'options' => $this->Zicure->ListDistrict($selectedProvince), 'default' => @$this->data['District']['district_id'], 'label' => array('text' => __('District Name')))); ?>
        <?php echo $this->Form->input('status', array('type' => 'select', 'options' => $this->Zicure->mainStatus())); ?>
    </div>
    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/Districts/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add District"), "/Districts/add"); ?>
        <?php //echo $this->Permission->link_button(__('Back'), '/Districts/index/', NULL, array("div" => false, 'label' => false, 'type' => 'button', 'class' => 'pull-right', 'disabled' => true)); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->

<script type="text/javascript">
//    $(function () {
//        $("#DistrictId").prop('disabled', renderDisplay());
//        $("#ProvinceId").change(function () {
//            getDistrict();
//            $("#DistrictId").prop('disabled', renderDisplay());
//        });
//    });
//
//    function renderDisplay() {
//        return ($.trim($("#ProvinceId").val()) == '') ? true : false;
//    }
</script>