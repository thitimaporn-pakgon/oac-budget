<?php
$btitle = isset($btitle) ? $btitle : 'Zicure';
$class = isset($class) ? $class : 'btn btn-default pull-right';
$link = isset($link) ? $link : false;

?>
<div class="box-footer">
    <?php if (($link !== false)): ?>
        <?php echo $this->Permission->link(__('Back'), $link, array('class' => $class, 'name' => 'btn-back')); ?>
    <?php else: ?>
        <?php echo $this->Form->button(__('Back'), array('type' => 'button', 'class' => $class, 'onclick' => 'window.history.back();', 'name' => 'btn-back')); ?>
    <?php endif; ?>
</div>
