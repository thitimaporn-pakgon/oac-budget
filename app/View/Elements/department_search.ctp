<!-- Horizontal Form -->
<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Department Management System')); ?>
    <?php
    echo $this->Form->create('Search', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'col-sm-6 form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-4 control-label'),
            'between' => '<div class="col-sm-8">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('dept_no'); ?>
        <?php echo $this->Form->input('name', array('label' => array('text' => __('Department Name')))); ?>
        <?php echo $this->Form->input('department_level_id', array('label' => array('text' => __('DepartmentLevel Name')), 'options' => $this->Zicure->listDepartmentLevel())); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus()));?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/Departments/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add Data"), "/Departments/add"); ?>
        <?php //echo $this->Permission->link_button(__('Back'), '/departments/index/', NULL, array("div" => false, 'label' => false, 'type' => 'button', 'class' => 'pull-right', 'disabled' => true));  ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->






