<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Name prefix Management System')); ?>
    <?php
    echo $this->Form->create('NamePrefixes', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-2 control-label'),
            'between' => '<div class="col-sm-10">',
            'wrap' => 'col-sm-12',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php echo $this->Form->input('name', array('label' => array('text' => __('Name Prefix')))); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/NamePrefixes/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add NamePrefixe"), "/NamePrefixes/add"); ?>
        <?php //echo $this->Permission->link_button(__('Back'), '/NamePrefixes/index/', NULL, array("div" => false, 'label' => false, 'type' => 'button', 'class' => 'pull-right', 'disabled' => true)); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->


