<?php

$btitle = isset($btitle) ? $btitle : 'Zicure';
$disabled = isset($disabled) ? $disabled : false;
$bshow = isset($bshow) ? $bshow : false;
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$bnoti = isset($bnoti) ? $bnoti : null;
$btnApprove = isset($btnApprove) ? 'none' : 'show'; 
$btnReject = isset($btnReject) ? 'show' : 'none';
$btnExport = isset($btnExport) ? 'show' : 'none';
?>

<label for="count-checked">เลือก
    <span id="count-checked">0</span> รายการ</label>
    
    
    
<?php echo $this->Permission->button('<i class="fa fa-arrow-circle-down"></i>' . __('ExportExcel'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn bg-orange separate-button pull-right confirmModal separate-button', 'name' => 'btnExcel', 'id' => 'btnExcel', 'style' => "display: {$btnExport};")); ?>
<?php echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('Reject'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-danger pull-right confirmModal separate-button', 'name' => 'btnWorkManeuverReject', 'id' => 'btnWorkManeuverReject', 'style' => "display: {$btnReject};")); ?>
<?php echo $this->Permission->button('<i class="fa fa-file-text"></i>' . __('Approve'), null, array('disabled' => true, 'type' => 'submit', 'class' => 'btn btn-success pull-right confirmModal', 'name' => 'btnWorkManeuverApprove', 'id' => 'btnWorkManeuverApprove')); ?>


<script type="text/javascript">
    $(function () {
        // Dosz Code.===============
        var expenseIds;
        var controller = '<?php echo $this->params['controller']; ?>';
        //==========================

        $("input:checkbox").on('ifChanged', function (event) {
            var tmpCheck = ($(this).prop('checked') == true) ? 'check' : 'uncheck';
            if ($(this).val() == '') {
                $("input:checkbox").iCheck(tmpCheck);
            }
            $('#btnWorkManeuverApprove').attr('disabled', !($("input:checkbox:checked").length > 0));
            $("#count-checked").text($(":checkbox:not(.check-all):checked").length);

            // Dosz Code.==============================================
            var val = [];
            var expenseId;
            $("input:checkbox:not(.check-all):checked").each(function (i) {
                val[i] = $(this).val();
                expenseId = val[i].split("|");
                val[i] = expenseId[0];
            });
            expenseIds = val.toString();

            $('#btnExcel').attr('disabled', !($("input:checkbox:checked").length > 0));
            $('#btnWorkManeuverReject').attr('disabled', !($("input:checkbox:checked").length > 0));
            //==========================================================


        });

        // Dosz Code.====================================
        $("#btnExcel").click(function () {
            var form = $(this).closest('form');
            form.attr('action', '/' + controller + '/jasperExporRptManeuverExcel/' + expenseIds);
        });

        $("#btnWorkManeuverReject").click(function () {
            var form = $(this).closest('form');
            form.attr('action', '/' + controller + '/rejectBudget');
        });
        
        $("#btnWorkManeuverApprove").click(function () {
            var form = $(this).closest('form');
            form.attr('action',  '/' + controller + '/approve');
        });
        //================================================


    });
</script>