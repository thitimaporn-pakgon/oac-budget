<div class="box box-info">
    <?php echo $this->element('boxOptionHeader', array('btitle' => 'Sub-District Management System')); ?>
    <?php
    $selectedProvince = @$this->data['SubDistrict']['province_id'];
    //$selectedDistrict = @$this->data['SubDistrict']['district_id'];
    ?>
    <?php
    echo $this->Form->create('SubDistrict', array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'form-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'col-sm-2 control-label'),
            'between' => '<div class="col-sm-10">',
            'wrap' => 'col-sm-12',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
    )));
    ?>
    <div class="box-body">
        <?php //echo $this->Form->input('sub_district_id', array('id' => 'SubDistrictId', 'options' => $this->Zicure->ListSubDistrict($selectedDistrict), 'label' => array('text' => __('Subdistrict Name')))); ?>
        <?php echo $this->Form->input('sub_district_name', array('label' => array('text' => __('Subdistrict Name')))); ?>
        <?php echo $this->Form->input('province_id', array('id' => 'ProvinceId', 'options' => $this->Zicure->ListProvince(), 'label' => array('text' => __('Province Name')))); ?>
        <?php echo $this->Form->input('district_id', array('id' => 'DistrictId', 'default' => @$this->data['District']['district_id'], 'options' => $this->Zicure->ListDistrict($selectedProvince), 'label' => array('text' => __('District Name')))); ?>
        <?php echo $this->Form->input('status', array('options' => $this->Zicure->mainStatus())); ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/SubDistricts/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
        <?php echo $this->Permission->link_button('<i class="fa fa-plus"></i>' . __("Add Subdistrict"), "/SubDistricts/add"); ?>
        <?php //echo $this->Permission->link_button(__('Back'), '/SubDistricts/index/', NULL, array("div" => false, 'label' => false, 'type' => 'button', 'class' => 'pull-right', 'disabled' => true)); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->


<script type="text/javascript">
    $(function () {
        $("#DistrictId").prop('disabled', DistrictRenderDisplay());
        $("#SubDistrictId").prop('disabled', SubDistrictRenderDisplay());

        $("#ProvinceId").change(function () {
            getDistrict();
            $("#DistrictId").prop('disabled', DistrictRenderDisplay());
            $("#SubDistrictId").prop('disabled', SubDistrictRenderDisplay2());
        });

        $("#DistrictId").change(function () {
            getSubDistrict();
            $("#SubDistrictId").prop('disabled', SubDistrictRenderDisplay());
        });
    });

    function DistrictRenderDisplay() {
        return ($.trim($("#ProvinceId").val()) == '') ? true : false;
    }

    function SubDistrictRenderDisplay() {
        return ($.trim($("#DistrictId").val()) == '') ? true : false;
    }

    function SubDistrictRenderDisplay2() {
        return (($.trim($("#ProvinceId").val()) == '') || ($.trim($("#DistrictId").val()) == '')) ? true : false;
    }

</script>

