<div class="box box-info">
    <?php //echo $this->element('boxOptionHeader', array('btitle' => 'Inbox'));
		echo $this->element('boxOptionHeader', array('btitle' => $btitle));
    ?>
    <?php
    echo $this->Form->create('InboxFilter', array('class' => 'form-horizontal'));
    ?>
    <div class="box-body">
		<?php
		echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
		echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $this->Project->findListWorkGroupAllocated()));
		echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $this->Project->findListWorkAllocated()));
		echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $this->Project->findListProjectAllocated()));            
		?>
		
		<?php if($title != 'transfering') { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo __('งวดที่');?></label>
			<div class="col-sm-1">
				<select>
					<option value="">(<?php echo __('ระบุ');?>)</option>
					<option value="1">1</option>
					<option value="2">2</option>
				</select>
			</div>
			
			<label class="col-sm-1 control-label"><?php echo __('ครั้งที่');?></label>
			<div class="col-sm-1">
				<select>
					<option value="">(<?php echo __('ระบุ');?>)</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
			</div>
		</div>
		<?php } ?>
		
    </div>
	
    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '/BudgetPlans/index/', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
