<div class="box box-info">
    <?php //echo $this->element('boxOptionHeader', array('btitle' => 'Inbox'));
		echo $this->element('boxOptionHeader', array('btitle' => $btitle));
    ?>
    <?php
    echo $this->Form->create('InboxFilter', array('class' => 'form-horizontal'));
    ?>
    <div class="box-body">
        <?php
              echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year')));
              echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $work_grouplist));
              echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $work_list));
              echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $project_list));

              echo $this->Form->input('exhcode', array('label'=>array('text'=>__('รหัสงบประมาณทบ.'))));
        ?>
    </div>

    <div class="box-footer">
        <?php echo $this->Permission->link_submit('<i class="fa fa-search"></i>' . __('Search'), '', array('div' => false, 'label' => false, 'name' => 'btnsubmit')); ?>
    </div><!-- /.box-footer -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
