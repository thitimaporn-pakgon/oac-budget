<?php
/**
 * Element  Pagination make paginate
 * @author  sarawutt.b
 * @param   [options] as boolean make option for another pagiinate page |optional
 * @since   2016/01/21
 */
?>
<?php
$params = $this->Paginator->params();
//if ($params['pageCount'] > 1) {
?>
<div class="box-footer clearfix">
    <p class="pagination-summary">
        <?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
    </p>
    <ul class="pagination pagination-sm no-margin pull-right">
        <?php
        $this->Paginator->options(array('url' => $this->passedArgs));
        echo $this->Paginator->prev(__(Configure::read('Pagination.Prev.Text')), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('modulus' => Configure::read('Pagination.Modulus'), 'separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__(Configure::read('Pagination.Next.Text')), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>

</div>
<?php if (isset($options)): ?>
    <?php echo $this->Paginator->options(array('url' => $this->passedArgs)); ?>
<?php endif; ?>
<?php
//} ?>