<?php

  $btitle = isset($btitle) ? $btitle : 'Zicure';
  $disabled = isset($disabled) ? $disabled : false;
  $bshow = isset($bshow) ? $bshow : false;
  $bcollap = isset($bcollap) ? $bcollap : false;
  $bclose = isset($bclose) ? $bclose : false;
  $bnoti = isset($bnoti) ? $bnoti : null;
  $budgetCode = isset($budgetCode) ? $budgetCode : false;
  $showAddEditButton = isset($showAddEditButton) ? $showAddEditButton : false;
  $disabled = isset($disabled) ? $disabled : false;
  $showSearchButton = isset($showSearchButton) ? $showSearchButton : false;
?>
<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => $btitle)); ?>
    <?php echo $this->Form->create('Search', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="box-body">
      <?php
        if($this->Zicure->isSPCDep()):
          echo $this->Form->input('budget_year_id', array('options' => $this->Zicure->findListBudgetYear(), 'label' => __('Budget Year'),'default'=>$defaultOptions['Y'], 'disabled'=>$disabled));
          echo $this->Form->input('work_group', array('label'=>array('text'=>__('กลุ่มงบงาน')), 'options' => $work_grouplist,'default'=>$defaultOptions['A'], 'disabled'=>$disabled));
          echo $this->Form->input('work', array('label'=>array('text'=>__('งบงาน')),'options' => $work_list ,'default'=>$defaultOptions['B'], 'disabled'=>$disabled));
          echo $this->Form->input('project', array('label'=>array('text'=>__('การ')),'options' => $project_list,'default'=>$defaultOptions['C'], 'disabled'=>$disabled));
          echo $this->Form->input('project_list', array('label'=>array('text'=>__('รายการ')),'options' => $list_projectlist,'default'=>$defaultOptions['D'], 'disabled'=>$disabled));
        endif;

        if($budgetCode):
          echo $this->Form->input('budget_code', array('label'=>array('text'=>__('รหัสงบประมาณ ทบ.')), 'readonly'=>$disabled  ));
        endif;
      ?>
    </div>
    <div class="box-footer">
      <?php if($showSearchButton): ?>
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-search', 'Search'), '#', array('id'=>'btnSearch')); ?>
      <?php endif; ?>
      <?php if($showAddEditButton): ?>
        <?php if($this->Zicure->isSPCDep()): ?>
          <?php echo $this->Permission->button($this->Bootstrap->icon('fa-plus', 'Add GFMIS'), '/GFCodes/add', array('name'=>'btnAddnew','class'=>'btn bg-olive'));?>
          <?php echo $this->Permission->button($this->Bootstrap->icon('fa-pencil', 'Edit GFMIS'), '#', array('id'=>'btnEditnew', 'name'=>'btnEditnew','class'=>'btn bg-yellow'));?>
        <?php endif; ?>
      <?php endif; ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
