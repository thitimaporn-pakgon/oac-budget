<h1>
    <?php echo __($title_for_layout); ?>
    <small><?php echo __('Control panel'); ?></small>
</h1>
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i><?php echo __('Home'); ?></a></li>
    <li><a href="<?php echo "/{$this->params['controller']}" ?>"><?php echo __(Inflector::humanize($this->params['controller'])); ?></a></li>
    <li class="active"><?php echo __(Inflector::humanize($this->params['action'])); ?></li>
</ol>