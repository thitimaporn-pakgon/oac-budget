<?php
/**
 * 
 * Budget List Element used for insert/update/show information of all expend list in your params 
 * @author sarawutt.b
 * @var $btitle as a string of content of the box
 * @var $bcollap as a boolean making for the box can be collapsed
 * @var $bclose as a boolean making for the box can be closed
 * @var $disabled as a boolean making for input can be submit
 * @var $url as a string url pettern of submit target default is current path
 * @var $budgetYear as a string of making of the budget year
 * @var $budgetCode as a string of military budget code
 * @var $process as a character of process N = NEW mean you want to insert new budget , U = UPDATE mean you want to update it
 */
$btitle = isset($btitle) ? $btitle : 'นำเสนอคำขอ (ภาคจัดทำ)';
$class = isset($class) ? $class : 'box-default';
$bcollap = isset($bcollap) ? $bcollap : false;
$bclose = isset($bclose) ? $bclose : false;
$disabled = (isset($disabled) && is_bool($disabled)) ? $disabled : false;
$data = isset($data) ? $data : array();
$url = isset($url) ? $url : $this->params->here;

$budgetYear = isset($budgetYear) ? $budgetYear : $this->Project->getCurrenBudgetYearTH();
$budgetCode = isset($budgetCode) ? $budgetCode : '0000000000000';
$process = isset($process) ? strtoupper($process) : 'U';

$expenseLists = $this->Project->findExpenseList();

$ExpendDetailInfos = $this->Project->getDataExpenseBudgetAllocated($expenseId, substr($budgetYear, 2), $process);
//pr($ExpendDetailInfos);die();
$_totalYear2 = $_totalYear1 = $_totalPrimary = $_totalSecondary = $_totalSumbudget = 0;

$expendListKey = isset($ExpendDetailInfos[$budgetYear]) ? key($ExpendDetailInfos[$budgetYear]) : false;
$readonlyStr = ($disabled === true) ? 'disabled="disabled"' : null;
?>


<div class="box box-primary">
    <?php echo $this->element('boxOptionHeader', array('btitle' => $btitle, 'bclose' => $bclose, 'bcollap' => $bcollap)); ?>
    <?php echo $this->Form->create('OwnerBudget', array('url' => $url, 'role' => 'form', 'class' => 'form-horizontal', 'type' => 'file')); ?>

    <?php if ($expendListKey !== false): ?>
        <?php // echo $this->Form->hidden('ExpenseHeaderAllocated.expense_headder_id', array('class' => 'required', 'value' => $ExpendDetailInfos[$budgetYear][$expendListKey]['ExpenseDetailAllocated']['expense_headder_id'])); ?>
    <?php endif; ?>


    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table class="zicure-table-result table table-bordered" id="BudgetDetailTable">
                    <thead>
                        <tr class="budget-head">
                            <th class="text-center" rowspan="2"><?php echo __('ประเภทรายจ่าย'); ?></th>
                            <th class="text-center" colspan="2"><?php echo __('งบประมาณที่ผ่านมา'); ?></th>
                            <th class="text-center" colspan="1"><?php echo __('งบประมาณปี %s', $budgetYear); ?></th>
                            <th class="budget-scope-summary" rowspan="2"><?php echo __('รวมเป็นเงิน'); ?></th>
                        </tr>
                        <tr class="budget-head">
                            <th class="budget-scope"><?php echo __('ปี %s', $budgetYear - 2); ?></th>
                            <th class="budget-scope"><?php echo __('ปี %s', $budgetYear - 1); ?></th>
                            <th class="budget-scope"><?php echo __('ส่วนที่ 1'); ?></th>
                            <!--<th class="budget-scope"><?php echo __('ส่วนที่ 2'); ?></th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($expenseLists)): ?>
                            <?php $index = 0; ?>
                            <!--Expense list level I-->
                            <?php
                            foreach ($expenseLists as $k => $expenseList):
                                $expenseList = $expenseList[0];
                                $_expendYear2 = isset($ExpendDetailInfos[$budgetYear - 2][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 2][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                $_expendYear1 = isset($ExpendDetailInfos[$budgetYear - 1][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 1][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                $_expendPrimary = isset($ExpendDetailInfos[$budgetYear][$expenseList['id']]['ExpenseDetailAllocated']['primary_budget']) ? $ExpendDetailInfos[$budgetYear][$expenseList['id']]['ExpenseDetailAllocated']['primary_budget'] : 0;
                                $_expendSumBudget = isset($ExpendDetailInfos[$budgetYear][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear][$expenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;

                                //Calculation all section total
                                $_totalYear2 += $_expendYear2;
                                $_totalYear1 += $_expendYear1;
                                $_totalPrimary += $_expendPrimary;
                                $_totalSumbudget += $_expendSumBudget;
                                ?>

                                <tr data-accordion-parent="<?php echo $expenseList['id']; ?>">
                                    <td>
                                        <span class="glyphicon glyphicon-triangle-right btn-xs" role="drill-down-icon"></span><?php echo $expenseList['name']; ?>
                                        <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'value' => $expenseList['id'])); ?>
                                    </td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('year-2', $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear2); ?></td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('year-1', $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear1); ?></td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('1', $expenseList['id']); ?>>
                                        <p><?php echo $this->Zicure->currencyFormat($_expendPrimary); ?></p>
                                        <?php echo $this->Form->hidden('primary_budget', array('name' => "data[OwnerBudget][{$index}][primary_budget]", 'id' => false, 'value' => $_expendPrimary)); ?>
                                    </td>
                                    <td class="text-right" <?php echo $this->Project->optionBudgetParent('total', $expenseList['id']); ?>>
                                        <p><?php echo $this->Zicure->currencyFormat($_expendSumBudget); ?></p>
                                        <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'id' => false, 'value' => $_expendSumBudget)); ?>
                                    </td>
                                </tr>


                                <!--Expense list level II-->
                                <?php $index++; ?>
                                <?php $childExpenseLists = $this->Project->findChildExpenseList($expenseList['id']); ?>
                                <?php
                                foreach ($childExpenseLists as $k => $childExpenseList):
                                    $childExpenseList = $childExpenseList[0];
                                    $_expendYear2 = isset($ExpendDetailInfos[$budgetYear - 2][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 2][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                    $_expendYear1 = isset($ExpendDetailInfos[$budgetYear - 1][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 1][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                    $_expendPrimary = isset($ExpendDetailInfos[$budgetYear][$childExpenseList['id']]['ExpenseDetailAllocated']['primary_budget']) ? $ExpendDetailInfos[$budgetYear][$childExpenseList['id']]['ExpenseDetailAllocated']['primary_budget'] : null;
                                    $_expendSumBudget = isset($ExpendDetailInfos[$budgetYear][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear][$childExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                    ?>
                                    <tr class="budget-list <?php echo ($childExpenseList['has_input'] == 'N') ? 'bg-red' : null; ?>" data-accordion-child-of="<?php echo $expenseList['id']; ?>" style="display: none;">
                                        <td>
                                            <?php echo ($childExpenseList['has_input'] == 'N') ? '<span class="glyphicon glyphicon-triangle-right btn-xs" role="drill-down-icon"></span>' : ''; ?>
                                            <?php echo ($childExpenseList['name']); ?>
                                            <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'id' => false, 'value' => $childExpenseList['id'])); ?>
                                        </td>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('year-2', $childExpenseList['id'], $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear2); ?></td>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('year-1', $childExpenseList['id'], $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear1); ?></td>

                                        <?php if ($childExpenseList['has_input'] == 'Y'): ?>
                                            <td class="text-right"><input type="text" name="data[OwnerBudget][<?php echo $index; ?>][primary_budget]" class="form-control currency money-right" <?php echo $this->Project->optionBudgetChild('1', $childExpenseList['id'], $expenseList['id']); ?> value="<?php echo $_expendPrimary; ?>" <?php echo ($disabled === true) ? 'disabled="disabled"' : null; ?>/></td>
                                        <?php else: ?>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChild('1', $childExpenseList['id'], $expenseList['id']); ?>>
                                                <p><?php echo $this->Zicure->currencyFormat($_expendPrimary); ?></p>
                                                <?php echo $this->Form->hidden('primary_budget', array('name' => "data[OwnerBudget][{$index}][primary_budget]", 'value' => $_expendPrimary)); ?>
                                            </td>
                                        <?php endif; ?>
                                        <td class="text-right" <?php echo $this->Project->optionBudgetChild('total', $childExpenseList['id'], $expenseList['id']); ?>>
                                            <p><?php echo $this->Zicure->currencyFormat($_expendSumBudget); ?></p>
                                            <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'class' => 'currency money-right', 'value' => $_expendSumBudget)); ?>
                                        </td>
                                    </tr>

                                    <!--Expense list level III-->
                                    <?php $child3ExpenseLists = $this->Project->findChild3ExpenseList($childExpenseList['id']); ?>
                                    <?php
                                    foreach ($child3ExpenseLists as $k => $child3ExpenseList):
                                        $index++;
                                        $child3ExpenseList = $child3ExpenseList[0];
                                        $_expendYear2 = isset($ExpendDetailInfos[$budgetYear - 2][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 2][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                        $_expendYear1 = isset($ExpendDetailInfos[$budgetYear - 1][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear - 1][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                        $_expendPrimary = isset($ExpendDetailInfos[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailAllocated']['primary_budget']) ? $ExpendDetailInfos[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailAllocated']['primary_budget'] : null;
                                        $_expendSumBudget = isset($ExpendDetailInfos[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget']) ? $ExpendDetailInfos[$budgetYear][$child3ExpenseList['id']]['ExpenseDetailAllocated']['sum_budget'] : 0;
                                        ?>
                                        <tr class="budget-list-level3" data-accordion-child-of="<?php echo $expenseList['id']; ?>" style="display: none;">
                                            <td>
                                                <?php echo ($child3ExpenseList['name']); ?>
                                                <?php echo $this->Form->hidden('expense_list_id', array('name' => "data[OwnerBudget][{$index}][expense_list_id]", 'id' => false, 'value' => $child3ExpenseList['id'])); ?>
                                            </td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('year-2', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear2); ?></td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('year-1', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']); ?>><?php echo $this->Zicure->currencyFormat($_expendYear1); ?></td>
                                            <td class="text-right"><?php printf("<input type='text' name='data[OwnerBudget][{$index}][primary_budget]' class='form-control currency money-right' %s value='{$_expendPrimary}' %s />", $this->Project->optionBudgetChildL3('1', $child3ExpenseList['id'], $childExpenseList['id'], $expenseList['id']), $readonlyStr); ?></td>
                                            <td class="text-right" <?php echo $this->Project->optionBudgetChildL3('total', $child3ExpenseList['id'], $childExpenseList['id']); ?>>
                                                <p><?php echo $this->Zicure->currencyFormat($_expendSumBudget); ?></p>
                                                <?php echo $this->Form->hidden('sum_budget', array('name' => "data[OwnerBudget][{$index}][sum_budget]", 'id' => false, 'class' => 'currency money-right', 'value' => $_expendSumBudget)); ?> 
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <!--End Expense list level III-->   

                                    <?php $index++; ?>
                                <?php endforeach; ?>
                                <!--End Expense list level II-->

                                <?php $index++; ?>
                            <?php endforeach; ?>
                            <!--End Expense list level I-->

                        <?php else: ?>
                            <?php echo '<tr class="text-center notfound"><td colspan="6">' . __('Information Not Found') . '</td></tr>'; ?>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr class="sum-budget-list text-right">
                            <td><?php echo ('รวมเป็นเงิน'); ?></td>
                            <td data-is-grand-total="true" data-section="year-2"><?php echo $this->Form->hidden('total_year_2', array('name' => 'data[ExpenseHeaderAllocated][grand-total][year-2]', 'class' => 'currency money-right', 'value' => $_totalYear2)) . $this->Zicure->currencyFormat($_totalYear2); ?></td>
                            <td data-is-grand-total="true" data-section="year-1"><?php echo $this->Form->hidden('total_year_1', array('name' => 'data[ExpenseHeaderAllocated][grand-total][year-1]', 'class' => 'currency money-right', 'value' => $_totalYear1)) . $this->Zicure->currencyFormat($_totalYear1); ?></td>
                            <td data-is-grand-total="true" data-section="1"><?php echo $this->Form->hidden('total_primary', array('name' => 'data[ExpenseHeaderAllocated][grand-total][primary_budget]', 'class' => 'currency money-right', 'value' => $_totalPrimary)) . $this->Zicure->currencyFormat($_totalPrimary); ?></td>
                            <td data-is-grand-total="true" data-section="total"><?php echo $this->Form->hidden('total_sum_budget', array('name' => 'data[ExpenseHeaderAllocated][grand-total][sum_budget]', 'class' => 'currency money-right', 'value' => $_totalSumbudget)) . $this->Zicure->currencyFormat($_totalSumbudget); ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?php $this->Form->hidden('sum_val', array('name'=>'sum_val','value'=>$_totalPrimary)); ?>
        <?php echo $this->Permission->submit($this->Bootstrap->icon('fa-save', 'Save'), '/OwnerBudgetAllocateds/add', array('disabled' => $disabled)); ?>
        <?php echo $this->Permission->buttonBack('normal'); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $(function () {
        $("input[type='text'].currency,input[type='hidden'].currency,input[type='number']").autoNumeric('init', {aSep: ',', aDec: '.', aSign: '฿ '});
        numeral.defaultFormat('0,0.00');

        //Toggle for table up and down
        $("[data-accordion-parent]").click(function () {
            var parent_id = $(this).data('accordion-parent');
            var $drill_down_icon = $("span[role='drill-down-icon']", $(this));
            var $child = $("tr[data-accordion-child-of='" + parent_id + "']");
            $child.slideToggle("fast", function () {
                if ($child.is(":visible")) {
                    $drill_down_icon.removeClass("glyphicon-triangle-right");
                    $drill_down_icon.addClass("glyphicon-triangle-bottom");
                } else {
                    $drill_down_icon.removeClass("glyphicon-triangle-bottom");
                    $drill_down_icon.addClass("glyphicon-triangle-right");
                }
            });
        });

        //Add more file button behavior
        var el = '<div class="form-group divMultiplieFile">' + $(".divMultiplieFile").html() + '</div>';
        $("#btnAddMoreFile").click(function () {
            $(".divMultiplieFile:last").after(el);
        });

        $('.currency').on('focusout', function () {
            autoCalculation($(this), $("#BudgetDetailTable"));
        });

        //$('.currency').trigger('focusout');
    });

    function autoCalculation(events, table) {
        var $this = events;
        var parentId = $this.data("parent-id");
        var section = $this.data("section");
        var grandParentId = $this.data('grand-parent-id') || -1;
        var $parent = $("[data-id='" + parentId + "'][data-section='" + section + "'] > p", table);
        var $parentInput = $("[data-id='" + parentId + "'][data-section='" + section + "'] > input", table);
        var $parentSectionTotal = $("[data-id='" + parentId + "'][data-section='total'] > p", table);
        var $parentSectionTotalInput = $("[data-id='" + parentId + "'][data-section='total'] > input", table);
        var $grandParentSectionTotal = $("[data-id='" + grandParentId + "'][data-section='total'] > p", table);
        var $grandParentSectionTotalInput = $("[data-id='" + grandParentId + "'][data-section='total'] > input", table);
        var parentTotal = 0;

        $("[data-parent-id='" + parentId + "'][data-section='" + section + "']", table).each(function () {
            var $child = $(this);
            var childId = $child.data('id');
            var child1Val;
            var tagName = $child.prop("tagName");
            var childTotal = 0;
            if (tagName === "INPUT") {
                child1Val = $child.autoNumeric('get') || 0;
            } else {
                child1Val = $child.find('p').text().replace(/[^0-9\.]+/g, '') || 0;
            }

            if (!child1Val) {
                child1Val = 0;
            }
            parentTotal += (childTotal = numeral(child1Val).value());
            if (section != "total" && section != "year-2" && section != "year-1") {

                var $child2;
                var child2Val;
                if (section == "1") {
                    $child2 = $("[data-id='" + childId + "'][data-section='2']", table);
                } else if (section == "2") {
                    $child2 = $("[data-id='" + childId + "'][data-section='1']", table);
                }

                tagName = $child2.prop("tagName");
                if (tagName === "INPUT") {
                    child2Val = $child2.autoNumeric('get') || 0;
                } else {
                    child2Val = $child2.find('p').text().replace(/[^0-9\.]+/g, '') || 0;
                }
                if (!child2Val) {
                    child2Val = 0;
                }
                childTotal += numeral(child2Val).value();
                $("[data-id='" + childId + "'][data-section='total'] > p", table).text(numeral(childTotal).format() + ' ฿');
                $("[data-id='" + childId + "'][data-section='total'] > input", table).val(numeral(childTotal).format() + ' ฿');
            }

        });

        //Set trail total summation
        $parent.text(numeral(parentTotal).format('0,0.00') + ' ฿');
        $parentInput.val(numeral(parentTotal).format('0,0.00') + ' ฿');

        //Level 1 parent
        var r = $('[data-id="' + parentId + '"][data-section="year-1"] > p', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var s = $('[data-id="' + parentId + '"][data-section="year-2"] > p', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var o = $('[data-id="' + parentId + '"][data-section="1"] > p', table).text().replace(/[^0-9\.]+/g, '') || 0;
        var t = $('[data-id="' + parentId + '"][data-section="2"] > p', table).text().replace(/[^0-9\.]+/g, '') || 0;
        $parentSectionTotal.text(numeral(0).add(r).add(s).add(o).add(t).format() + ' ฿');
        $parentSectionTotalInput.val(numeral(0).add(r).add(s).add(o).add(t).format() + ' ฿');
        if (grandParentId !== -1) {
            //Calculate all of parent value of level 3
            var budgets = {'year-1': 0, 'year-2': 0, '1': 0, '2': 0, 'total': 0};
            $('[data-parent-id="' + grandParentId + '"]', table).each(function () {
                var t = $(this).find('p').text().replace(/[^0-9\.]+/g, '') || 0;
                budgets[$(this).data('section')] += numeral(t).value();
            });

            $('[data-id="' + grandParentId + '"][data-section="year-2"] > p', table).text(numeral(budgets['year-2']).format() + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="year-1"] > p', table).text(numeral(budgets['year-1']).format() + ' ฿');

            $('[data-id="' + grandParentId + '"][data-section="1"] > p', table).text(numeral(budgets['1']).format() + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="1"] > input', table).val(numeral(budgets['1']).format() + ' ฿');

            $('[data-id="' + grandParentId + '"][data-section="2"] > p', table).text(numeral(budgets['2']).format() + ' ฿');
            $('[data-id="' + grandParentId + '"][data-section="2"] > input', table).val(numeral(budgets['2']).format() + ' ฿');
            $grandParentSectionTotal.text(numeral(0).add(budgets['year-1']).add(budgets['year-2']).add(budgets['1']).add(budgets['2']).format() + ' ฿');
            $grandParentSectionTotalInput.val(numeral(0).add(budgets['year-1']).add(budgets['year-2']).add(budgets['1']).add(budgets['2']).format() + ' ฿');
        }


        //Make all grand total
        var budgets = {'year-1': 0, 'year-2': 0, '1': 0, '2': 0, 'total': 0};
        $("[data-is-parent]", table).each(function () {
            var t = $(this).find('p').text().replace(/[^0-9\.]+/g, '') || 0;
            budgets[$(this).data('section')] += numeral(t).value();
        });

        //Calculation all for grand total budget
        $("[data-is-grand-total][data-section='year-2']").text(numeral(budgets['year-2']).format() + ' ฿');
        $("[data-is-grand-total][data-section='year-1']").text(numeral(budgets['year-1']).format() + ' ฿');
        $("[data-is-grand-total][data-section='1']").text(numeral(budgets['1']).format() + ' ฿');
        $("[data-is-grand-total][data-section='2']").text(numeral(budgets['2']).format() + ' ฿');
        $("[data-is-grand-total][data-section='total']").text(numeral(0).add(budgets['year-1']).add(budgets['year-2']).add(budgets['1']).add(budgets['2']).format() + ' ฿');
    }

</script>