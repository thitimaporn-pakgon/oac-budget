<?php
/**
 * SysAction Fixture
 */
class SysActionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary'),
		'sys_controller_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 512),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'sys_actions_uq_name_key' => array('unique' => true, 'column' => array('sys_controller_id', 'name')),
			'sys_actions_controller_idx' => array('unique' => false, 'column' => array('id', 'sys_controller_id')),
			'sys_actions_name_idx' => array('unique' => false, 'column' => 'name'),
			'sys_actions_status_idx' => array('unique' => false, 'column' => 'status')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '57156c8a-a8a8-4fff-94dd-315e7d541294',
			'sys_controller_id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-19 06:23:54',
			'modified' => '2016-04-19 06:23:54'
		),
	);

}
