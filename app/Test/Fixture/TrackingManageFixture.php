<?php
/**
 * TrackingManage Fixture
 */
class TrackingManageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'period_manage_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'departmant_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'status' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'period_manage_id' => '',
			'departmant_id' => '',
			'status' => 'Lorem ipsum dolor sit ame'
		),
	);

}
