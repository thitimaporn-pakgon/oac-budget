<?php
/**
 * PeriodManage Fixture
 */
class PeriodManageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'system_has_process_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'department_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'expense_header_manage_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'period' => array('type' => 'integer', 'null' => true, 'default' => null),
		'times' => array('type' => 'integer', 'null' => true, 'default' => null),
		'budget_request' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'system_has_process_id' => 1,
			'department_id' => '',
			'expense_header_manage_id' => '',
			'period' => 1,
			'times' => 1,
			'budget_request' => '',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-06-06 13:42:29',
			'modified' => '2017-06-06 13:42:29'
		),
	);

}
