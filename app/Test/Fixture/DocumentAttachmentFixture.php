<?php
/**
 * DocumentAttachment Fixture
 */
class DocumentAttachmentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'document_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'budget_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2),
		'attachment_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'attachment_name_origin' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'display_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1024),
		'attachment_extension' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
		'attachment_path' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'project_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'project_plan_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'ref_model' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'ref_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'document_type' => 'Lorem ipsum dolor ',
			'budget_type' => '',
			'attachment_name' => 'Lorem ipsum dolor sit amet',
			'attachment_name_origin' => 'Lorem ipsum dolor sit amet',
			'display_name' => 'Lorem ipsum dolor sit amet',
			'attachment_extension' => 'Lorem ip',
			'attachment_path' => 'Lorem ipsum dolor sit amet',
			'project_id' => 1,
			'project_plan_id' => 1,
			'ref_model' => 'Lorem ipsum dolor sit amet',
			'ref_id' => 1,
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-20 03:16:07',
			'modified' => '2016-04-20 03:16:07'
		),
	);

}
