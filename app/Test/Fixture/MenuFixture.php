<?php
/**
 * Menu Fixture
 */
class MenuFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'type' => array('type' => 'integer', 'null' => true, 'default' => null),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'name_eng' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'domain' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128),
		'port' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6),
		'sys_controller_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'sys_action_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'url' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128),
		'order_no' => array('type' => 'integer', 'null' => true, 'default' => null),
		'glyphicon' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 512),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'type' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'name_eng' => 'Lorem ipsum dolor sit amet',
			'domain' => 'Lorem ipsum dolor sit amet',
			'port' => 'Lore',
			'sys_controller_id' => '',
			'sys_action_id' => '',
			'url' => 'Lorem ipsum dolor sit amet',
			'order_no' => 1,
			'glyphicon' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-28 02:19:47',
			'modified' => '2016-04-28 02:19:47'
		),
	);

}
