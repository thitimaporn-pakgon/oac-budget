<?php
/**
 * SysAcl Fixture
 */
class SysAclFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'menu_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id'),
			'sys_acls_menu_id_user_id_role_id_idx' => array('unique' => false, 'column' => array('menu_id', 'role_id', 'user_id')),
			'sys_acls_status_idx' => array('unique' => false, 'column' => 'status')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'menu_id' => 1,
			'user_id' => 1,
			'role_id' => 1,
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-27 02:31:43',
			'modified' => '2016-04-27 02:31:43'
		),
	);

}
