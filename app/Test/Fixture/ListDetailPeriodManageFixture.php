<?php
/**
 * ListDetailPeriodManage Fixture
 */
class ListDetailPeriodManageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'list_detail_manage_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'period_manage_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'expense_header_manage_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'list_detail_manage_id' => '',
			'period_manage_id' => '',
			'expense_header_manage_id' => '',
			'created' => '2017-06-14 11:48:18'
		),
	);

}
