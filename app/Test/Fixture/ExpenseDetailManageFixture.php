<?php
/**
 * ExpenseDetailManage Fixture
 */
class ExpenseDetailManageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'expense_list_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'expense_header_manage_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'budget_year_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'primary_budget' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'secondary_budget' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month1' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month2' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month3' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month4' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month5' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month6' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month7' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month8' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month9' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month10' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month11' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'month12' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'sum_month' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'expense_list_id' => 1,
			'expense_header_manage_id' => '',
			'budget_year_id' => 1,
			'primary_budget' => '',
			'secondary_budget' => '',
			'month1' => '',
			'month2' => '',
			'month3' => '',
			'month4' => '',
			'month5' => '',
			'month6' => '',
			'month7' => '',
			'month8' => '',
			'month9' => '',
			'month10' => '',
			'month11' => '',
			'month12' => '',
			'sum_month' => '',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 15:06:07',
			'modified' => '2017-04-18 15:06:07'
		),
	);

}
