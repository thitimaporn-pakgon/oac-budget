<?php
/**
 * ExpenseAllocated Fixture
 */
class ExpenseAllocatedFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'expense_list_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'work_group_allocated_id' => array('type' => 'biginteger', 'null' => true, 'default' => '0'),
		'work_allocated_id' => array('type' => 'biginteger', 'null' => true, 'default' => '0'),
		'project_allocated_id' => array('type' => 'biginteger', 'null' => true, 'default' => '0'),
		'project_list_allocated_id' => array('type' => 'biginteger', 'null' => true, 'default' => '0'),
		'budget_year_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'sum_budget' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'expense_list_id' => 1,
			'work_group_allocated_id' => '',
			'work_allocated_id' => '',
			'project_allocated_id' => '',
			'project_list_allocated_id' => '',
			'budget_year_id' => 1,
			'sum_budget' => '',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 14:07:23',
			'modified' => '2017-04-18 14:07:23'
		),
	);

}
