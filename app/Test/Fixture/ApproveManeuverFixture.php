<?php
/**
 * ApproveManeuver Fixture
 */
class ApproveManeuverFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'expense_header_maneuver_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'from_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'to_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'is_approve' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1),
		'is_return' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1),
		'is_edit' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1),
		'approve_by' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'return_by' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'edit_by' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'remark' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'expense_header_maneuver_id' => '',
			'from_department_id' => 1,
			'to_department_id' => 1,
			'is_approve' => 'Lorem ipsum dolor sit ame',
			'is_return' => 'Lorem ipsum dolor sit ame',
			'is_edit' => 'Lorem ipsum dolor sit ame',
			'approve_by' => '',
			'return_by' => '',
			'edit_by' => '',
			'remark' => 'Lorem ipsum dolor sit amet',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 14:57:59',
			'modified' => '2017-04-18 14:57:59'
		),
	);

}
