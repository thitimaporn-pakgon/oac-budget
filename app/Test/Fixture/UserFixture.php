<?php
/**
 * User Fixture
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64),
		'name_prefix_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'position_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'master_status_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'department_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'first_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256),
		'last_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256),
		'personal_card_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 13),
		'sex' => array('type' => 'string', 'null' => true, 'default' => 'M', 'length' => 1),
		'age' => array('type' => 'integer', 'null' => false, 'default' => null),
		'phone_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 128),
		'address' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1024),
		'province_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10),
		'district_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15),
		'sub_district_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'picture_path' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'last_login' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'username' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'name_prefix_id' => 1,
			'position_id' => 1,
			'role_id' => 1,
			'master_status_id' => 1,
			'department_id' => 1,
			'first_name' => 'Lorem ipsum dolor sit amet',
			'last_name' => 'Lorem ipsum dolor sit amet',
			'personal_card_no' => 'Lorem ipsum',
			'sex' => 'Lorem ipsum dolor sit ame',
			'age' => 1,
			'phone_no' => 'Lorem ipsum dolor ',
			'email' => 'Lorem ipsum dolor sit amet',
			'address' => 'Lorem ipsum dolor sit amet',
			'province_id' => 'Lorem ip',
			'district_id' => 'Lorem ipsum d',
			'sub_district_id' => 'Lorem ipsum dolor ',
			'picture_path' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit ame',
			'last_login' => '2016-04-19 06:25:39',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-19 06:25:39',
			'modified' => '2016-04-19 06:25:39'
		),
	);

}
