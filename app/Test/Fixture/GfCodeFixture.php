<?php
/**
 * GfCode Fixture
 */
class GfCodeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'work_group_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'work_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'project_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'project_list_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'expense_list_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'expense_header_allocated_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'gf_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'main_event' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'budget_account' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'work_group_allocated_id' => '',
			'work_allocated_id' => '',
			'project_allocated_id' => '',
			'project_list_allocated_id' => '',
			'expense_list_id' => 1,
			'expense_header_allocated_id' => '',
			'gf_code' => 'Lorem ipsum dolor sit amet',
			'main_event' => 'Lorem ipsum dolor sit amet',
			'budget_account' => 'Lorem ipsum dolor sit amet',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-05-11 15:39:33',
			'modified' => '2017-05-11 15:39:33'
		),
	);

}
