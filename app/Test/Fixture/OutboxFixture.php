<?php
/**
 * Outbox Fixture
 */
class OutboxFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'outboxs';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'subject' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'from_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'from_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'to_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'to_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'ref_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'ref_model' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'system_has_process_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'task_flag' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 3),
		'remark' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 512),
		'is_active' => array('type' => 'string', 'null' => false, 'default' => 'Y', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'subject' => 'Lorem ipsum dolor sit amet',
			'from_department_id' => 1,
			'from_uid' => 1,
			'to_department_id' => 1,
			'to_uid' => 1,
			'ref_id' => '',
			'ref_model' => 'Lorem ipsum dolor sit amet',
			'system_has_process_id' => 1,
			'task_flag' => 'L',
			'remark' => 'Lorem ipsum dolor sit amet',
			'is_active' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 17:03:14',
			'modified' => '2017-04-18 17:03:14'
		),
	);

}
