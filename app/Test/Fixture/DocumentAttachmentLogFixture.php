<?php
/**
 * DocumentAttachmentLog Fixture
 */
class DocumentAttachmentLogFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'document_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 512),
		'budget_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2),
		'attachment_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'attachment_name_origin' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'display_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'attachment_extension' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10),
		'attachment_path' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 512),
		'project_plan_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'project_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'ref_model' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'ref_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'_version_' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => array('id', '_version_'))
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'document_type' => 'Lorem ipsum dolor sit amet',
			'budget_type' => '',
			'attachment_name' => 'Lorem ipsum dolor sit amet',
			'attachment_name_origin' => 'Lorem ipsum dolor sit amet',
			'display_name' => 'Lorem ipsum dolor sit amet',
			'attachment_extension' => 'Lorem ip',
			'attachment_path' => 'Lorem ipsum dolor sit amet',
			'project_plan_id' => '',
			'project_id' => '',
			'ref_model' => 'Lorem ipsum dolor sit amet',
			'ref_id' => '',
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-03-15 17:14:12',
			'modified' => '2017-03-15 17:14:12',
			'_version_' => ''
		),
	);

}
