<?php
/**
 * SystemHasProcess Fixture
 */
class SystemHasProcessFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1),
		'sq_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'sq_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor ',
			'type' => 'Lorem ipsum dolor sit ame',
			'sq_id' => 1,
			'sq_name' => 'Lorem ipsum dolor ',
			'description' => 'Lorem ipsum dolor sit amet',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 17:49:07',
			'modified' => '2017-04-18 17:49:07'
		),
	);

}
