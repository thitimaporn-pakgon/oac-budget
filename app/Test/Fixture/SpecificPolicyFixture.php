<?php
/**
 * SpecificPolicy Fixture
 */
class SpecificPolicyFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'year_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 4),
		'department_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'remark' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'document_attachment_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'year_code' => 'Lo',
			'department_id' => '',
			'remark' => 'Lorem ipsum dolor sit amet',
			'document_attachment_id' => '',
			'create_uid' => '',
			'update_uid' => '',
			'created' => '2017-04-01 00:04:20',
			'modified' => '2017-04-01 00:04:20'
		),
	);

}
