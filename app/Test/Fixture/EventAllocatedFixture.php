<?php
/**
 * EventAllocated Fixture
 */
class EventAllocatedFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'result_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 3),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'budget_year_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'from_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'to_department_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'system_has_process_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'budget_scope' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'budget_balance' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'result_allocated_id' => '',
			'code' => 'L',
			'name' => 'Lorem ipsum dolor sit amet',
			'budget_year_id' => 1,
			'from_department_id' => 1,
			'to_department_id' => 1,
			'system_has_process_id' => 1,
			'budget_scope' => '',
			'budget_balance' => '',
			'create_uid' => '',
			'update_uid' => '',
			'created' => '2017-04-18 14:06:52',
			'modified' => '2017-04-18 14:06:52'
		),
	);

}
