<?php
/**
 * District Fixture
 */
class DistrictFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 5, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256),
		'name_eng' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 'Lor',
			'name' => 'Lorem ipsum dolor sit amet',
			'name_eng' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit ame',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-20 03:09:04',
			'modified' => '2016-04-20 03:09:04'
		),
	);

}
