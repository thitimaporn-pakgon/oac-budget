<?php
/**
 * ExpenseHeaderAllocated Fixture
 */
class ExpenseHeaderAllocatedFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'project_list_allocated_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'to_department_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'system_has_process_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'budget_scope' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'objective' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'kpi1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'kpi1_result' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'kpi2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'kpi2_result' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'kpi3' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'kpi3_result' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'kpi4' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200),
		'kpi4_result' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'project_list_allocated_id' => '',
			'to_department_id' => 1,
			'system_has_process_id' => 1,
			'budget_scope' => '',
			'objective' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'kpi1' => 'Lorem ipsum dolor sit amet',
			'kpi1_result' => '',
			'kpi2' => 'Lorem ipsum dolor sit amet',
			'kpi2_result' => '',
			'kpi3' => 'Lorem ipsum dolor sit amet',
			'kpi3_result' => '',
			'kpi4' => 'Lorem ipsum dolor sit amet',
			'kpi4_result' => '',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-04-18 14:08:35',
			'modified' => '2017-04-18 14:08:35'
		),
	);

}
