<?php
/**
 * AccessLog Fixture
 */
class AccessLogFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'menu_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'access_url' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 512),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'menu_id' => 1,
			'user_id' => 1,
			'access_url' => 'Lorem ipsum dolor sit amet',
			'create_uid' => 1,
			'created' => '2016-04-20 03:13:06'
		),
	);

}
