<?php
/**
 * Department Fixture
 */
class DepartmentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'dept_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 4),
		'short_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256),
		'full_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024),
		'department_level_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'department_cover_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'dept_no' => 'Lo',
			'short_name' => 'Lorem ipsum dolor sit amet',
			'full_name' => 'Lorem ipsum dolor sit amet',
			'department_level_id' => 1,
			'department_cover_id' => 1,
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2016-04-20 03:08:25',
			'modified' => '2016-04-20 03:08:25'
		),
	);

}
