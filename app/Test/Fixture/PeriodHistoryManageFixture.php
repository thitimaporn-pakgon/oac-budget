<?php
/**
 * PeriodHistoryManage Fixture
 */
class PeriodHistoryManageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'period_manage_id' => array('type' => 'biginteger', 'null' => false, 'default' => null),
		'expense_list_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'expense_header_manage_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'system_has_process_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'budget' => array('type' => 'decimal', 'null' => true, 'default' => null),
		'owner_department_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'hold_department_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'create_department_id' => array('type' => 'biginteger', 'null' => true, 'default' => null),
		'prefix_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20),
		'suffix_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 7),
		'create_uid' => array('type' => 'integer', 'null' => false, 'default' => null),
		'update_uid' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'period_manage_id' => '',
			'expense_list_id' => 1,
			'expense_header_manage_id' => '',
			'system_has_process_id' => 1,
			'budget' => '',
			'owner_department_id' => '',
			'hold_department_id' => '',
			'create_department_id' => '',
			'prefix_code' => 'Lorem ipsum dolor ',
			'suffix_code' => 'Lorem',
			'create_uid' => 1,
			'update_uid' => 1,
			'created' => '2017-06-06 13:41:54',
			'modified' => '2017-06-06 13:41:54'
		),
	);

}
