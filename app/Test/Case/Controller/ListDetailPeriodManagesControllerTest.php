<?php
App::uses('ListDetailPeriodManagesController', 'Controller');

/**
 * ListDetailPeriodManagesController Test Case
 */
class ListDetailPeriodManagesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.list_detail_period_manage',
		'app.list_detail_manage',
		'app.expense_list',
		'app.expense_header_manage',
		'app.project_list_manage',
		'app.project_manage',
		'app.work_manage',
		'app.work_group_manage',
		'app.event_manage',
		'app.result_manage',
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.inbox',
		'app.expense_header_allocated',
		'app.project_list_allocated',
		'app.project_allocated',
		'app.work_allocated',
		'app.work_group_allocated',
		'app.event_allocated',
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.expense_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated',
		'app.work_group_maneuver',
		'app.event_maneuver',
		'app.result_maneuver',
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.work_maneuver',
		'app.project_maneuver',
		'app.project_list_maneuver',
		'app.expense_header_maneuver',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.outbox',
		'app.budget_type',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.period_manage',
		'app.department',
		'app.department_level',
		'app.tracking_manage',
		'app.departmant',
		'app.menu',
		'app.sys_acl',
		'app.sys_controller',
		'app.sys_action',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.access_log',
		'app.common'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
