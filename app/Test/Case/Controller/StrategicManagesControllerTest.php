<?php
App::uses('StrategicManagesController', 'Controller');

/**
 * StrategicManagesController Test Case
 */
class StrategicManagesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.inbox',
		'app.outbox',
		'app.plan_manage',
		'app.result_manage',
		'app.event_manage',
		'app.work_group_manage',
		'app.work_manage',
		'app.project_manage',
		'app.project_list_manage',
		'app.expense_header_manage',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.expense_list',
		'app.menu',
		'app.sys_acl',
		'app.sys_controller',
		'app.sys_action',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.department',
		'app.department_level',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.access_log'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
