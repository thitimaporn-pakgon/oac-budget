<?php
App::uses('ResultAllocatedsController', 'Controller');

/**
 * ResultAllocatedsController Test Case
 */
class ResultAllocatedsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.inbox',
		'app.outbox',
		'app.event_allocated',
		'app.work_group_allocated',
		'app.expense_allocated',
		'app.expense_list',
		'app.work_allocated',
		'app.project_allocated',
		'app.project_list_allocated',
		'app.expense_header_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated',
		'app.menu',
		'app.sys_acl',
		'app.sys_controller',
		'app.sys_action',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.department',
		'app.department_level',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.access_log'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
