<?php
App::uses('DocumentAttachmentsController', 'Controller');

/**
 * DocumentAttachmentsController Test Case
 */
class DocumentAttachmentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.document_attachment',
		'app.project',
		'app.department',
		'app.department_level',
		'app.department_cover',
		'app.project_plan',
		'app.construct',
		'app.budget_year',
		'app.construct_detail',
		'app.maintenance',
		'app.maintenance_detail',
		'app.management',
		'app.management_detail',
		'app.medical',
		'app.medical_detail',
		'app.project_activity',
		'app.project_alternative',
		'app.alternative',
		'app.project_comment',
		'app.project_detail',
		'app.reasearch',
		'app.reasearch_detail',
		'app.troop',
		'app.troop_detail'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
