<?php
App::uses('WorkGroupManeuver', 'Model');

/**
 * WorkGroupManeuver Test Case
 */
class WorkGroupManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_group_maneuver',
		'app.event_maneuver',
		'app.result_maneuver',
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.work_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkGroupManeuver = ClassRegistry::init('WorkGroupManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkGroupManeuver);

		parent::tearDown();
	}

}
