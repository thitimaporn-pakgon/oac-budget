<?php
App::uses('SystemHasProcess', 'Model');

/**
 * SystemHasProcess Test Case
 */
class SystemHasProcessTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.system_has_process',
		'app.inbox',
		'app.outbox'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SystemHasProcess = ClassRegistry::init('SystemHasProcess');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SystemHasProcess);

		parent::tearDown();
	}

}
