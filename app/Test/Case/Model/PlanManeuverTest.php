<?php
App::uses('PlanManeuver', 'Model');

/**
 * PlanManeuver Test Case
 */
class PlanManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.result_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlanManeuver = ClassRegistry::init('PlanManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlanManeuver);

		parent::tearDown();
	}

}
