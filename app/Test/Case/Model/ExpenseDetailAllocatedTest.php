<?php
App::uses('ExpenseDetailAllocated', 'Model');

/**
 * ExpenseDetailAllocated Test Case
 */
class ExpenseDetailAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_detail_allocated',
		'app.expense_list',
		'app.expense_header_allocated',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseDetailAllocated = ClassRegistry::init('ExpenseDetailAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseDetailAllocated);

		parent::tearDown();
	}

}
