<?php
App::uses('StrategicManeuver', 'Model');

/**
 * StrategicManeuver Test Case
 */
class StrategicManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.strategic_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.plan_maneuver',
		'app.result_maneuver',
		'app.event_maneuver',
		'app.work_group_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StrategicManeuver = ClassRegistry::init('StrategicManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StrategicManeuver);

		parent::tearDown();
	}

}
