<?php
App::uses('ProjectListManage', 'Model');

/**
 * ProjectListManage Test Case
 */
class ProjectListManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_list_manage',
		'app.project_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_header_manage',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectListManage = ClassRegistry::init('ProjectListManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectListManage);

		parent::tearDown();
	}

}
