<?php
App::uses('PlanManage', 'Model');

/**
 * PlanManage Test Case
 */
class PlanManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.result_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlanManage = ClassRegistry::init('PlanManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlanManage);

		parent::tearDown();
	}

}
