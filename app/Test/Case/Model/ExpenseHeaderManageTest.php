<?php
App::uses('ExpenseHeaderManage', 'Model');

/**
 * ExpenseHeaderManage Test Case
 */
class ExpenseHeaderManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_header_manage',
		'app.project_list_manage',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.expense_list',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseHeaderManage = ClassRegistry::init('ExpenseHeaderManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseHeaderManage);

		parent::tearDown();
	}

}
