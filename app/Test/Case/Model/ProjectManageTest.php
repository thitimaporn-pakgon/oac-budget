<?php
App::uses('ProjectManage', 'Model');

/**
 * ProjectManage Test Case
 */
class ProjectManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_manage',
		'app.work_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.project_list_manage',
		'app.expense_header_manage',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectManage = ClassRegistry::init('ProjectManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectManage);

		parent::tearDown();
	}

}
