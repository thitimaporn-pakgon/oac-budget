<?php
App::uses('ApproveAllocated', 'Model');

/**
 * ApproveAllocated Test Case
 */
class ApproveAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.approve_allocated',
		'app.expense_header_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ApproveAllocated = ClassRegistry::init('ApproveAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ApproveAllocated);

		parent::tearDown();
	}

}
