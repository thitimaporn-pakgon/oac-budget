<?php
App::uses('DocumentAttachment', 'Model');

/**
 * DocumentAttachment Test Case
 */
class DocumentAttachmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.document_attachment',
		'app.project',
		'app.project_plan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DocumentAttachment = ClassRegistry::init('DocumentAttachment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DocumentAttachment);

		parent::tearDown();
	}

}
