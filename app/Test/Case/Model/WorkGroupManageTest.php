<?php
App::uses('WorkGroupManage', 'Model');

/**
 * WorkGroupManage Test Case
 */
class WorkGroupManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_group_manage',
		'app.event_manage',
		'app.result_manage',
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.work_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkGroupManage = ClassRegistry::init('WorkGroupManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkGroupManage);

		parent::tearDown();
	}

}
