<?php
App::uses('EventManeuver', 'Model');

/**
 * EventManeuver Test Case
 */
class EventManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.event_maneuver',
		'app.result_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.work_group_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EventManeuver = ClassRegistry::init('EventManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EventManeuver);

		parent::tearDown();
	}

}
