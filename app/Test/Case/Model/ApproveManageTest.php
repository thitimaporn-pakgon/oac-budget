<?php
App::uses('ApproveManage', 'Model');

/**
 * ApproveManage Test Case
 */
class ApproveManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.approve_manage',
		'app.expense_header_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ApproveManage = ClassRegistry::init('ApproveManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ApproveManage);

		parent::tearDown();
	}

}
