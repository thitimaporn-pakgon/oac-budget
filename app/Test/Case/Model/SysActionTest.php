<?php
App::uses('SysAction', 'Model');

/**
 * SysAction Test Case
 */
class SysActionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sys_action',
		'app.sys_controller',
		'app.sys_acl',
		'app.user',
		'app.role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SysAction = ClassRegistry::init('SysAction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SysAction);

		parent::tearDown();
	}

}
