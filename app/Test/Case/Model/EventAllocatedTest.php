<?php
App::uses('EventAllocated', 'Model');

/**
 * EventAllocated Test Case
 */
class EventAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.event_allocated',
		'app.result_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.work_group_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EventAllocated = ClassRegistry::init('EventAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EventAllocated);

		parent::tearDown();
	}

}
