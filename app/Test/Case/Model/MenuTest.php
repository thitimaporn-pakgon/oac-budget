<?php
App::uses('Menu', 'Model');

/**
 * Menu Test Case
 */
class MenuTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.menu',
		'app.sys_controller',
		'app.sys_acl',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.department',
		'app.department_level',
		'app.department_cover',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.user_role',
		'app.sys_action'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Menu = ClassRegistry::init('Menu');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Menu);

		parent::tearDown();
	}

}
