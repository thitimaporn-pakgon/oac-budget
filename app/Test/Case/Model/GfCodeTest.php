<?php
App::uses('GfCode', 'Model');

/**
 * GfCode Test Case
 */
class GfCodeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.gf_code',
		'app.work_group_allocated',
		'app.event_allocated',
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.inbox',
		'app.expense_header_allocated',
		'app.project_list_allocated',
		'app.project_allocated',
		'app.work_allocated',
		'app.expense_allocated',
		'app.expense_list',
		'app.approve_allocated',
		'app.expense_detail_allocated',
		'app.work_group_maneuver',
		'app.event_maneuver',
		'app.result_maneuver',
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.work_maneuver',
		'app.project_maneuver',
		'app.project_list_maneuver',
		'app.expense_header_maneuver',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.outbox'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GfCode = ClassRegistry::init('GfCode');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GfCode);

		parent::tearDown();
	}

}
