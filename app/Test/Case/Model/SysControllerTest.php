<?php
App::uses('SysController', 'Model');

/**
 * SysController Test Case
 */
class SysControllerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sys_controller',
		'app.sys_acl',
		'app.sys_action',
		'app.user',
		'app.role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SysController = ClassRegistry::init('SysController');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SysController);

		parent::tearDown();
	}

}
