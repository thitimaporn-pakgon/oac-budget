<?php
App::uses('SysAcl', 'Model');

/**
 * SysAcl Test Case
 */
class SysAclTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sys_acl',
		'app.sys_controller',
		'app.sys_action',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.department',
		'app.department_level',
		'app.department_cover',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.user_role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SysAcl = ClassRegistry::init('SysAcl');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SysAcl);

		parent::tearDown();
	}

}
