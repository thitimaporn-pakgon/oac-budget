<?php
App::uses('SubDistrict', 'Model');

/**
 * SubDistrict Test Case
 */
class SubDistrictTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sub_district'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SubDistrict = ClassRegistry::init('SubDistrict');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SubDistrict);

		parent::tearDown();
	}

}
