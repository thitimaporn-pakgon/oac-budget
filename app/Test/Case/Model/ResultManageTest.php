<?php
App::uses('ResultManage', 'Model');

/**
 * ResultManage Test Case
 */
class ResultManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.result_manage',
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.event_manage',
		'app.work_group_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResultManage = ClassRegistry::init('ResultManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResultManage);

		parent::tearDown();
	}

}
