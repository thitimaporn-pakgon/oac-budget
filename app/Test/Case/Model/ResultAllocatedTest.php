<?php
App::uses('ResultAllocated', 'Model');

/**
 * ResultAllocated Test Case
 */
class ResultAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.event_allocated',
		'app.work_group_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResultAllocated = ClassRegistry::init('ResultAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResultAllocated);

		parent::tearDown();
	}

}
