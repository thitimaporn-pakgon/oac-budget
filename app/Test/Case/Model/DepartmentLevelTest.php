<?php
App::uses('DepartmentLevel', 'Model');

/**
 * DepartmentLevel Test Case
 */
class DepartmentLevelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.department_level',
		'app.department'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DepartmentLevel = ClassRegistry::init('DepartmentLevel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DepartmentLevel);

		parent::tearDown();
	}

}
