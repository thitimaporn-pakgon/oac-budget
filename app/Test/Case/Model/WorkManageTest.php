<?php
App::uses('WorkManage', 'Model');

/**
 * WorkManage Test Case
 */
class WorkManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_manage',
		'app.work_group_manage',
		'app.event_manage',
		'app.result_manage',
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.project_manage',
		'app.project_list_manage',
		'app.expense_header_manage',
		'app.approve_manage',
		'app.expense_detail_manage',
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkManage = ClassRegistry::init('WorkManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkManage);

		parent::tearDown();
	}

}
