<?php
App::uses('StrategicAllocated', 'Model');

/**
 * StrategicAllocated Test Case
 */
class StrategicAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.plan_allocated',
		'app.result_allocated',
		'app.event_allocated',
		'app.work_group_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StrategicAllocated = ClassRegistry::init('StrategicAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StrategicAllocated);

		parent::tearDown();
	}

}
