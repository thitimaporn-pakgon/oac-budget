<?php
App::uses('ExpenseHeaderAllocated', 'Model');

/**
 * ExpenseHeaderAllocated Test Case
 */
class ExpenseHeaderAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_header_allocated',
		'app.project_list_allocated',
		'app.system_has_process',
		'app.approve_allocated',
		'app.expense_detail_allocated',
		'app.expense_list',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseHeaderAllocated = ClassRegistry::init('ExpenseHeaderAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseHeaderAllocated);

		parent::tearDown();
	}

}
