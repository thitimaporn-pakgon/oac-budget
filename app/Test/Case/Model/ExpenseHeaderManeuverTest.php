<?php
App::uses('ExpenseHeaderManeuver', 'Model');

/**
 * ExpenseHeaderManeuver Test Case
 */
class ExpenseHeaderManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_header_maneuver',
		'app.project_list_maneuver',
		'app.system_has_process',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.expense_list',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseHeaderManeuver = ClassRegistry::init('ExpenseHeaderManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseHeaderManeuver);

		parent::tearDown();
	}

}
