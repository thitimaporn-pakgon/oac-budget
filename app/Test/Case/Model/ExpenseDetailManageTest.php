<?php
App::uses('ExpenseDetailManage', 'Model');

/**
 * ExpenseDetailManage Test Case
 */
class ExpenseDetailManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_detail_manage',
		'app.expense_list',
		'app.expense_header_manage',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseDetailManage = ClassRegistry::init('ExpenseDetailManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseDetailManage);

		parent::tearDown();
	}

}
