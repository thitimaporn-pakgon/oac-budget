<?php
App::uses('PlanAllocated', 'Model');

/**
 * PlanAllocated Test Case
 */
class PlanAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.result_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlanAllocated = ClassRegistry::init('PlanAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlanAllocated);

		parent::tearDown();
	}

}
