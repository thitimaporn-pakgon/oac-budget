<?php
App::uses('AccessLog', 'Model');

/**
 * AccessLog Test Case
 */
class AccessLogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.access_log',
		'app.menu',
		'app.user',
		'app.name_prefix',
		'app.position',
		'app.role',
		'app.sys_acl',
		'app.sys_controller',
		'app.sys_action',
		'app.master_status',
		'app.department',
		'app.department_level',
		'app.department_cover',
		'app.province',
		'app.district',
		'app.sub_district',
		'app.user_role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AccessLog = ClassRegistry::init('AccessLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AccessLog);

		parent::tearDown();
	}

}
