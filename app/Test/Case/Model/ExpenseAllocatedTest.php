<?php
App::uses('ExpenseAllocated', 'Model');

/**
 * ExpenseAllocated Test Case
 */
class ExpenseAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_allocated',
		'app.expense_list',
		'app.work_group_allocated',
		'app.work_allocated',
		'app.project_allocated',
		'app.project_list_allocated',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseAllocated = ClassRegistry::init('ExpenseAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseAllocated);

		parent::tearDown();
	}

}
