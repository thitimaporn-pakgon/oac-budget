<?php
App::uses('Outbox', 'Model');

/**
 * Outbox Test Case
 */
class OutboxTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.outbox',
		'app.system_has_process'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Outbox = ClassRegistry::init('Outbox');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Outbox);

		parent::tearDown();
	}

}
