<?php
App::uses('ProjectAllocated', 'Model');

/**
 * ProjectAllocated Test Case
 */
class ProjectAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_allocated',
		'app.work_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_allocated',
		'app.expense_list',
		'app.work_group_allocated',
		'app.project_list_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectAllocated = ClassRegistry::init('ProjectAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectAllocated);

		parent::tearDown();
	}

}
