<?php
App::uses('EventManage', 'Model');

/**
 * EventManage Test Case
 */
class EventManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.event_manage',
		'app.result_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.work_group_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EventManage = ClassRegistry::init('EventManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EventManage);

		parent::tearDown();
	}

}
