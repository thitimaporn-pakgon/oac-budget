<?php
App::uses('Inbox', 'Model');

/**
 * Inbox Test Case
 */
class InboxTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.inbox',
		'app.system_has_process'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Inbox = ClassRegistry::init('Inbox');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Inbox);

		parent::tearDown();
	}

}
