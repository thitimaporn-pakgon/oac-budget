<?php
App::uses('SpecificPolicy', 'Model');

/**
 * SpecificPolicy Test Case
 */
class SpecificPolicyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.specific_policy',
		'app.department',
		'app.department_level'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SpecificPolicy = ClassRegistry::init('SpecificPolicy');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SpecificPolicy);

		parent::tearDown();
	}

}
