<?php
App::uses('DocumentAttachmentLog', 'Model');

/**
 * DocumentAttachmentLog Test Case
 */
class DocumentAttachmentLogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.document_attachment_log'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DocumentAttachmentLog = ClassRegistry::init('DocumentAttachmentLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DocumentAttachmentLog);

		parent::tearDown();
	}

}
