<?php
App::uses('ExpenseDetailManeuver', 'Model');

/**
 * ExpenseDetailManeuver Test Case
 */
class ExpenseDetailManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_detail_maneuver',
		'app.expense_list',
		'app.expense_header_maneuver',
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseDetailManeuver = ClassRegistry::init('ExpenseDetailManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseDetailManeuver);

		parent::tearDown();
	}

}
