<?php
App::uses('WorkAllocated', 'Model');

/**
 * WorkAllocated Test Case
 */
class WorkAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_allocated',
		'app.work_group_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_allocated',
		'app.expense_list',
		'app.project_allocated',
		'app.project_list_allocated',
		'app.expense_header_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkAllocated = ClassRegistry::init('WorkAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkAllocated);

		parent::tearDown();
	}

}
