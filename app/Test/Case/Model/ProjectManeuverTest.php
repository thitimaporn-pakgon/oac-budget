<?php
App::uses('ProjectManeuver', 'Model');

/**
 * ProjectManeuver Test Case
 */
class ProjectManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_maneuver',
		'app.work_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.project_list_maneuver',
		'app.expense_header_maneuver',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectManeuver = ClassRegistry::init('ProjectManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectManeuver);

		parent::tearDown();
	}

}
