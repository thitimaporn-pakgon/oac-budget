<?php
App::uses('ProjectListAllocated', 'Model');

/**
 * ProjectListAllocated Test Case
 */
class ProjectListAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_list_allocated',
		'app.project_allocated',
		'app.work_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_allocated',
		'app.expense_list',
		'app.work_group_allocated',
		'app.expense_header_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectListAllocated = ClassRegistry::init('ProjectListAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectListAllocated);

		parent::tearDown();
	}

}
