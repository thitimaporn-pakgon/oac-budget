<?php
App::uses('WorkGroupAllocated', 'Model');

/**
 * WorkGroupAllocated Test Case
 */
class WorkGroupAllocatedTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_group_allocated',
		'app.event_allocated',
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_allocated',
		'app.expense_list',
		'app.work_allocated',
		'app.project_allocated',
		'app.project_list_allocated',
		'app.expense_header_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkGroupAllocated = ClassRegistry::init('WorkGroupAllocated');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkGroupAllocated);

		parent::tearDown();
	}

}
