<?php
App::uses('ExpenseList', 'Model');

/**
 * ExpenseList Test Case
 */
class ExpenseListTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExpenseList = ClassRegistry::init('ExpenseList');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExpenseList);

		parent::tearDown();
	}

}
