<?php
App::uses('ProjectListManeuver', 'Model');

/**
 * ProjectListManeuver Test Case
 */
class ProjectListManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_list_maneuver',
		'app.project_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.expense_header_maneuver',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.expense_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectListManeuver = ClassRegistry::init('ProjectListManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectListManeuver);

		parent::tearDown();
	}

}
