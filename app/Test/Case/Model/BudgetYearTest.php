<?php
App::uses('BudgetYear', 'Model');

/**
 * BudgetYear Test Case
 */
class BudgetYearTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.budget_year'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BudgetYear = ClassRegistry::init('BudgetYear');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BudgetYear);

		parent::tearDown();
	}

}
