<?php
App::uses('ApproveManeuver', 'Model');

/**
 * ApproveManeuver Test Case
 */
class ApproveManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.approve_maneuver',
		'app.expense_header_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ApproveManeuver = ClassRegistry::init('ApproveManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ApproveManeuver);

		parent::tearDown();
	}

}
