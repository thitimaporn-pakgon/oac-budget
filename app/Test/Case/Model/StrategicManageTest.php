<?php
App::uses('StrategicManage', 'Model');

/**
 * StrategicManage Test Case
 */
class StrategicManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.plan_manage',
		'app.result_manage',
		'app.event_manage',
		'app.work_group_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StrategicManage = ClassRegistry::init('StrategicManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StrategicManage);

		parent::tearDown();
	}

}
