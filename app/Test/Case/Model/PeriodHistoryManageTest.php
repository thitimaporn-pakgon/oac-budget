<?php
App::uses('PeriodHistoryManage', 'Model');

/**
 * PeriodHistoryManage Test Case
 */
class PeriodHistoryManageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.period_history_manage',
		'app.period_manage',
		'app.expense_list',
		'app.expense_header_manage',
		'app.project_list_manage',
		'app.project_manage',
		'app.work_manage',
		'app.work_group_manage',
		'app.event_manage',
		'app.result_manage',
		'app.plan_manage',
		'app.strategic_manage',
		'app.budget_year',
		'app.system_has_process',
		'app.inbox',
		'app.expense_header_allocated',
		'app.project_list_allocated',
		'app.project_allocated',
		'app.work_allocated',
		'app.work_group_allocated',
		'app.event_allocated',
		'app.result_allocated',
		'app.plan_allocated',
		'app.strategic_allocated',
		'app.expense_allocated',
		'app.approve_allocated',
		'app.expense_detail_allocated',
		'app.work_group_maneuver',
		'app.event_maneuver',
		'app.result_maneuver',
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.work_maneuver',
		'app.project_maneuver',
		'app.project_list_maneuver',
		'app.expense_header_maneuver',
		'app.approve_maneuver',
		'app.expense_detail_maneuver',
		'app.outbox',
		'app.approve_manage',
		'app.expense_detail_manage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PeriodHistoryManage = ClassRegistry::init('PeriodHistoryManage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PeriodHistoryManage);

		parent::tearDown();
	}

}
