<?php
App::uses('ResultManeuver', 'Model');

/**
 * ResultManeuver Test Case
 */
class ResultManeuverTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.result_maneuver',
		'app.plan_maneuver',
		'app.strategic_maneuver',
		'app.budget_year',
		'app.system_has_process',
		'app.event_maneuver',
		'app.work_group_maneuver'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResultManeuver = ClassRegistry::init('ResultManeuver');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResultManeuver);

		parent::tearDown();
	}

}
